class Carton
  require 'guided_randomness'
  require 'digest'

  attr_reader :carton
  
  # el numero de serie dentro del cual se encuentra el carton
  attr_accessor :serie
  
  
  FILAS = 3
  COLUMNAS = 9
  DEBUG = false
  
  def initialize()
    # iunternamente llama a reset_carton .. ais que no lo llamamos
    #self.set_positions()
    # populamos el carton con los numeros.
    self._reset_carton()
    @sorted_array = []
    #self.populate()
  end  
  
  def _reset_carton
    @carton = Array.new(FILAS) { Array.new(COLUMNAS,0) }
  end
  
  def sorted_array
    # Para devolver una copia del array, no la referencia
    return @sorted_array.dup
  end
  
  def fila(number)
    @carton[number -1]
  end

  def columna(number)
    col = []
    @carton.each do |f|
      col.push(f[number -1])
    end
    return col
  end
  
  def get(columna, fila)
    #return @carton[fila -1][columna -1]
    return @carton[fila][columna]
  end
  
  def set(columna,fila,valor)
#     @carton[fila -1][columna -1] = valor
    @carton[fila][columna] = valor
  end
  
  def to_s
    return "\n" + @carton[0].to_s + "\n" + @carton[1].to_s + "\n" + @carton[2].to_s + "\n"
  end
  
  def to_sql(serie,carton_number)
    
    first_line = @carton[0].map{|x| (x == 0)? -1 : x }.join(",")
    second_line = @carton[1].map{|x| (x == 0)? -1 : x }.join(",")
    third_line = @carton[2].map{|x| (x == 0)? -1 : x }.join(",")

    sql = "insert into card set series = " + serie + ", number = "+ carton_number + ", firstline = '" + first_line + "', secondline = '" + second_line + "', thirdline = '" + third_line + "', created = NOW();"
    
    return sql
  end
  
  def filas_checksum
    filas_encrypted = []
    FILAS.times.each do |f|
      filas_encrypted.push(Digest.hexencode(self.fila(f).to_s))
    end
    return filas_encrypted
  end
  
  def set_values(column,val)
    val_sorted = val.sort
    FILAS.times.each do |f|
      if (self.get(column,f) != 0)
        # si no quedaorn valores por asignar, chillamos
        raise "not enought values" if (val_sorted.length == 0)
        self.set(column,f,val_sorted.shift)
      end
    end
    # si todavia qeudaorn valores por asignar, chillamos
    raise "Not enought space for those values" if (val_sorted.length != 0)
  end

  
  private
  
  # Restriccion de 3 seguidos ...
  # Rearma la distribucion de probabilidades 
  # suponemos que se pasa el vector prob acorde a lo que se quiere hacer.
  def _chequeo_restriccion_3(fila, r, prob)
    top_limit =  (prob.length) - 1 
    if (r > 0 and r < top_limit)
      # estamos en el medio
      if (@carton[fila][r-1]==1)
        # el anterior fue selecionado
        print "el anterior fue selecionado." if DEBUG
        prob[r-2] = 0 if (r > 1)
        prob[r+1] = 0
      elsif (@carton[fila][r+1]==1)
        # el siguiente fue selecionado
        print "el siguiente fue selecionado." if DEBUG
        prob[r-1] = 0
        prob[r+2] = 0 if (r+1 < top_limit)
      elsif (r>1 and @carton[fila][r-2]==1)
        # el 2 anterior fue selecionado
        print "el 2 anterior fue selecionado." if DEBUG
        prob[r-1] = 0
      elsif (r+1 < top_limit and @carton[fila][r+2]==1)
        # el 2 siguiente fue selecionado
        print "el 2 siguiente fue selecionado." if DEBUG
        prob[r+1] = 0
      end
    elsif (r == 0)
      #estamos en el borde menor y el siguiente ya fue seleccionado
      prob[r+2] = 0 if (@carton[fila][r+1]==1) 
      #estamos en el borde menor y el 2 siguiente ya fue seleccionado
      prob[r+1] = 0 if (@carton[fila][r+2]==1)     
      
    elsif (r = top_limit)
      #estamos en el borde mayor, y el anterior ya fue seleccionado.
      prob[r-2] = 0 if (@carton[fila][r-1]==1)
      #estamos en el borde mayor, y el 2 anterior ya fue seleccionado.
      prob[r-1] = 0 if (@carton[fila][r-2]==1)
    end
    # retornamos el nuevo vector de probabilidades
    return prob
  end
  
  def _re_chequeo_restriccion_3(fila)
    count = 0
    @carton[fila].each do |c|
      count = (c != 0) ? count + 1 : 0
      raise "Todo mal en algun lado" if (count == 3)
    end
    return true
  end
  
end
