class Serie
  require './carton.rb'
  
  # load "./serie.rb";  s = Serie.new(); s.set_positions();s
  
  CARTONES = 6
  SERIE_RETRIES = 100
  CARTONES_RETRIES = 10
  
  DEBUG = false
  WARN = false
  INFO = false
  
  COLUMNS = 9
  COLUMNS_DISTRIBUTION = [9.00,10.00,10.00,10.00,10.00,10.00,10.00,10.00,11.00]
  
  def initialize()
    # 9 columnas, y tiene que haber 10 valores dentro de la serie
    _reset()
  end

  def _reset()
    @cartones = {}
    @cartones_index = []
    @position_serie_controller = COLUMNS_DISTRIBUTION.clone
    
    @own_position_controller = []
    CARTONES.times do |t|
      @own_position_controller[t] = [0,0,0,0,0,0,0,0,0]
    end
    
    @numbers = [ (1 .. 9).to_a.shuffle,
                 (10 .. 19).to_a.shuffle,
                 (20 .. 29).to_a.shuffle,
                 (30 .. 39).to_a.shuffle,
                 (40 .. 49).to_a.shuffle,
                 (50 .. 59).to_a.shuffle,
                 (60 .. 69).to_a.shuffle,
                 (70 .. 79).to_a.shuffle,
                 (80 .. 90).to_a.shuffle
               ]
    
  end
  
  def cartones
    return @cartones.values
  end  
  
  def _set_value(carton_number,row)
    @own_position_controller[carton_number][row] += 1
    @position_serie_controller[row] -= 1
  end
  
  def _remove_random_carton_from_serie
    carton_to_remove = @cartones_index.shuffle.shift
    self._remove_carton_from_serie(carton_to_remove)
    return carton_to_remove
  end
  
  def _remove_carton_from_serie(carton_number)
    
    print "Sacando el carton " + carton_number.to_s + " de la serie." if WARN
    
    print "\t own_positions: " + @own_position_controller[carton_number].to_s + "\n" if DEBUG
    print "\t controller: " + @position_serie_controller.to_s + "\n" if DEBUG
    
    # hacemos revert de las posiciones
    COLUMNS.times do |row|
      if (@own_position_controller[carton_number][row] > 0)
        @position_serie_controller[row] += @own_position_controller[carton_number][row]
        @own_position_controller[carton_number][row] = 0
      end
    end
    
    # eliminamos el carton
    @cartones.delete(carton_number.to_s)
    print "\t\t Despues .. :\n" if DEBUG
    print "\t\t own_positions: " + @own_position_controller[carton_number].to_s + "\n" if DEBUG
    print "\t\t controller: " + @position_serie_controller.to_s + "\n" if DEBUG
  end
  
  def to_s
    print "cantidad de cartones en la serie: " + @cartones.length.to_s + "\n"
    print "Controlador de la serie: " + @position_serie_controller.to_s + "\n"
    print "Control individual: " + @own_position_controller.to_s + "\n"
    @cartones.each_value do |c|
      c.to_s
    end
  end
  
  def to_sql(serie_number)
    sql = ""
    n = 1
    @cartones.values.each do |carton|
      carton_number = n + (6 * (serie_number-1))
      sql += carton.to_sql(serie_number.to_s,carton_number.to_s)
      n += 1
    end
    
    return sql
  end
  
  def set_positions(tries = 0)
    begin
      CARTONES.times do |c|
          print "Carton " + c.to_s + "\n" if INFO
          self._set_positions(c)
      end
      
      self._serie_check()
      
      # llenamos con numeros
      
      CARTONES.times do |c|
          print "Llenando carton " + c.to_s + "\n" if INFO
          self._set_values(c)
      end
      
      
    rescue Exception => e
      if (tries < SERIE_RETRIES)
        print "Fallo, Muchas veces. Volvemos a generar la serie \n" if WARN
        self._reset()
        self.set_positions(tries + 1 )
      else
        print "Se zarpo en fallas la serie, cortamos aca\n"
        raise e
      end
    end
  end
  
  def get_filas_checksum
    fs = []
    @cartones.values.each do |carton|
      fs.concat(carton.filas_checksum())
    end
    
    return fs
  end
  
  def _set_values(carton_number)
    COLUMNS.times.each do |column|
      arr_values = []
      @own_position_controller[carton_number][column].times do
        arr_values.push(@numbers[column].shift)
      end
      @cartones[carton_number.to_s].set_values(column,arr_values)
    end
  end

  
  
  def _serie_check()
    raise "Insuficientes cartones" if (@cartones.length != 6)
    @position_serie_controller.each do |p|
      raise "Something wrong happened" if (p != 0)
    end
  end
  
  def _set_positions(carton_number,times = 0)
    begin
      
      new_carton = Carton.new()
      inner_set_positions(new_carton,carton_number)
      
      # TODO: poner numeros.
      @cartones[carton_number.to_s] = new_carton
      @cartones_index.push(carton_number)
    rescue Exception => e
      print "\tFallo, volvemos a intentar con el carton. intentos actuales:" + times.to_s + " \n" if WARN
      # Primero volvemos atras el carton qeu quisimos meter.
      self._remove_carton_from_serie(carton_number)

      if (times < CARTONES_RETRIES)        
        
        if (@cartones_index.length > 0)
          # tenemos mas cartones ya metidos, agarramos uno aleatoreo, lo sacmaos y volvemos a calcular

          carton_number_removed = self._remove_random_carton_from_serie()
          print "\t\t Re-intentando con el carton:" + carton_number_removed.to_s + " \n" if WARN
          self._set_positions(carton_number_removed,0)

          print "\t\t Y Termianmos de Reintentar con el carton:" + carton_number.to_s + " \n" if WARN
          #re-try ese carton una vez mas
          self._set_positions(carton_number,times + 1)
        else
          raise e
        end
      else
        print "###############3 No mas intentos\n" if WARN
        raise e
      end
    end
  end
  
  def _empty_probs
    return [1,1,1,1,1,1,1,1,1]
  end
  
  def inner_set_positions(carton, carton_number)
    all_values = COLUMNS.times.to_a #[0,1,2,3,4,5,6,7,8] 
    top_limit =  COLUMNS - 1 
    result_fila1 = []
    # agarramos fila aleatoreamente.
    filas = [0,1,2].shuffle()

    fila1 = filas.pop()
    print "@@@ 1 fila: " + fila1.to_s  + "\n" if DEBUG
    
    # cualquier valor != 0
    prob = self._empty_probs
    
    # recalculate probs.
    prob = _recalculate_prob(prob,5)
    
    5.times do |i|
      print "\t prob: " + prob.length.to_s + prob.to_s  + "\n" if DEBUG
      r = all_values.get_rand(prob)
      print "\t r: " + r.to_s + "\n" if DEBUG
      prob[r] = 0;
      self._set_value(carton_number,r)

      print "\t position_serie_controller: " + @position_serie_controller.to_s + "\n" if DEBUG
      #@carton[fila1][r]=1
      carton.set(r,fila1,1)
      
      result_fila1.push(r)
      
      # restriccion de no tres seguidos.
      prob = _chequeo_restriccion_3(carton,fila1, r, prob, (5-i))    
    end
    
    self._re_chequeo_restriccion_3(carton,fila1)
    
    ###############
    ##### Fila 2
    ####
    # tomamos otra fila
    fila2 = filas.pop()
    result_fila2 = []
    # cambiamso probs .. Equiprobables
    prob = self._empty_probs
    # recalculate probs.
    prob = _recalculate_prob(prob,5)

    print "@@@ 2 fila: " + fila2.to_s   + "\n" if DEBUG
    5.times do |i|
      print "\t prob: " + prob.length.to_s + prob.to_s  + "\n" if DEBUG
      r = all_values.get_rand(prob)
      print "\t r: " + r.to_s + "\n" if DEBUG
      prob[r] = 0
      self._set_value(carton_number,r)
      print "\t position_serie_controller: " + @position_serie_controller.to_s + "\n" if DEBUG
      carton.set(r,fila2,1)
      result_fila2.push(r)
      
      # Restriccion de 3 seguidos ...
      prob = _chequeo_restriccion_3(carton,fila2, r, prob, 5-i)
    end
    
    self._re_chequeo_restriccion_3(carton,fila2)
    ############################3
    ####
    ####  Fila 3
    
    # tomamos la ultima fila
    fila3 = filas.pop()
    print " @@@ 3 fila: " + fila3.to_s   + "\n" if DEBUG
    result_fila3 = []
    prob = self._empty_probs

    count = 5
    all_values.each do |r|
      # TODO: agregar validacion de 3
      if (carton.get(r,fila1) != 1 and carton.get(r,fila2) != 1)
        # aca si dan 3 seguidos se pudre todo ....
        if (r>1 and carton.get(r-1,fila3) == 1 and carton.get(r-2,fila3) == 1)
          raise "TODO MAL, SE DIO LA FEA!!!" 
        end
        if ( r < (top_limit-1) and carton.get(r+1,fila3) == 1 and carton.get(r+2,fila3) == 1 )
          raise "TODO MAL, SE DIO LA FEA!!!" 
        end
        if (r >0 and r < top_limit and carton.get(r+1,fila3) == 1 and carton.get(r-1,fila3) == 1)
          raise "TODO MAL, SE DIO LA FEA!!!" 
        end
        # okay .. no dio 3 seguidos ... 
        
        # chequeamos que efectivamente podamos ocupar esa casilla dad la serie.
        raise "TODO MAL, SE DIO LA FEA para la serie!!!"  if (@position_serie_controller[r] == 0)
        
        #okay ....
        print "\t r obligado: " + r.to_s + "\n" if DEBUG
        self._set_value(carton_number,r)
        print "\t position_serie_controller: " + @position_serie_controller.to_s + "\n" if DEBUG
        count -=1
        
        prob[r] = 0
        
        carton.set(r,fila3,1)

        result_fila3.push(r)
        # guardamos para modificar probabilidades que salgna los 3 seguidos
        # Restriccion de 3 seguidos ...
        
        prob = _chequeo_restriccion_3(carton,fila3, r, prob, count)
      end
      
    end
    

    faltan = count
    # Calculamos la 3 fila ...
    faltan.times do
      print "\t  prob: " + prob.length.to_s + prob.to_s + "\n" if DEBUG
      r = all_values.get_rand(prob)
      print "\t r: " + r.to_s + "\n" if DEBUG
      prob[r] = 0;
      self._set_value(carton_number,r)
      carton.set(r,fila3,1)
      print "\t position_serie_controller: " + @position_serie_controller.to_s + "\n" if DEBUG
      result_fila3.push(r)
      count -= 1 
      # Restriccion de 3 seguidos ...      
      prob = _chequeo_restriccion_3(carton,fila3, r, prob, count)
    end
    self._re_chequeo_restriccion_3(carton,fila3)
  end
  
  def _recalculate_prob(prob,cantidad_necesaria = 5)
    # si no necesitamos mas posiciones para esta fila, solo retornamos.
    return prob if (cantidad_necesaria == 0)
    
    count = 0.00
    positions = []
    #Calculamos probabilidades
    
    COLUMNS.times do |i|
      prob[i] = 0  if (@position_serie_controller[i] == 0)
        
      if (prob[i] != 0)
        count += 1
        positions.push(i)
        # prob[i] = @position_serie_controller[i]/10.00
        prob[i] = @position_serie_controller[i] / COLUMNS_DISTRIBUTION[i]
      end
        
    end
    
    raise "invalid serie. This row will have (" + count.to_s + ") less than " + cantidad_necesaria.to_s + " numbers. " + prob.to_s if (count < cantidad_necesaria)
    
    return prob
  end
  
    # Restriccion de 3 seguidos ..
  # Rearma la distribucion de probabilidades 
  # suponemos que se pasa el vector prob acorde a lo que se quiere hacer.
  def _chequeo_restriccion_3(carton,fila, r, prob, cantidad_necesaria = 5)
    
    # si no necesitamos mas posiciones para esta fila, solo retornamos.
    return prob if (cantidad_necesaria == 0)

    top_limit =  COLUMNS - 1
    if (r > 0 and r < top_limit)
      # estamos en el medio
      #if (@carton[fila][r-1]==1)
      if (carton.get(r-1,fila) == 1) 
        # el anterior fue selecionado
        print "\tel anterior fue selecionado.\n" if DEBUG
        prob[r-2] = 0 if (r > 1)
        prob[r+1] = 0
      elsif (carton.get(r+1,fila) == 1) # elsif (@carton[fila][r+1]==1)
        # el siguiente fue selecionado
        print "\tel siguiente fue selecionado.\n" if DEBUG
        prob[r-1] = 0
        prob[r+2] = 0 if (r+1 < top_limit)
      elsif (r>1 and carton.get(r-2,fila) == 1)
        # el 2 anterior fue selecionado
        print "\tel 2 anterior fue selecionado.\n" if DEBUG
        prob[r-1] = 0
      elsif (r+1 < top_limit and carton.get(r+2,fila) == 1)
        # el 2 siguiente fue selecionado
        print "\tel 2 siguiente fue selecionado.\n" if DEBUG
        prob[r+1] = 0
      end
    elsif (r == 0)
      #estamos en el borde menor y el siguiente ya fue seleccionado
      prob[r+2] = 0 if (carton.get(r+1,fila)==1) 
      #estamos en el borde menor y el 2 siguiente ya fue seleccionado
      prob[r+1] = 0 if (carton.get(r+2,fila)==1)     
    elsif (r = top_limit)
      #estamos en el borde mayor, y el anterior ya fue seleccionado.
      prob[r-2] = 0 if (carton.get(r-1,fila)==1)
      #estamos en el borde mayor, y el 2 anterior ya fue seleccionado.
      prob[r-1] = 0 if (carton.get(r-2,fila)==1)
    end
    
    # retornamos el nuevo vector de probabilidades
    return _recalculate_prob(prob, cantidad_necesaria)
    
  end
  
  def _re_chequeo_restriccion_3(carton,fila)
    top = COLUMNS-1
    
    COLUMNS.times do |i|
      if (carton.get(i,fila) != 0)
        if (i <= top-2)
          # los dos siguientes
          raise "BUUU" if (carton.get(i+1,fila) != 0 and carton.get(i+2,fila) != 0)
        end
        if (i >= 2)
          #los dos anteriores
          raise "BUUU" if (carton.get(i-1,fila) != 0 and carton.get(i-2,fila) != 0)
        end
        
        if (i <= top-1 and i >= 1)
          # el siguiente y el anterior
          raise "BUUU" if (carton.get(i+1,fila) != 0 and carton.get(i-1,fila) != 0)
        end
      end
    end
    
  end

end
