class Generador
  require './serie'

  # # Como usarlo
  #
  # load "./generador.rb";  g = Generador.new(); g.generate(3)
  #
  # g.sql
  #
  # g.dump_to_file("algun_archivo")
  #
  ######
  
  attr_reader :sql, :n

  DEBUG = false
  WARN = true
  INFO = true
  
  def initialize()
    self.reset
  end  
  
  def reset
    @sql = ""
    @n = 0
    @filas = {}
    return true
  end
  
  def generate(series)
    series.times do | s |
      print "Generando serie " + (s + 1).to_s + "\n"
      add_serie(s+1)
    end
    
  end
    
  def add_serie(serie_number)
    begin 
      s = Serie.new()
      s.set_positions()
      s_filas = s.get_filas_checksum
      self._check_filas(s_filas)
      @sql += s.to_sql(serie_number)
      @n += 1
    rescue Exception => e
      print "Algo en la serie esta repetido. Regeneramos" if WARN
      self.add_serie(serie_number)
      #raise e
    end
  end
  
  def _check_filas(filas_checksums)
    filas_checksums.each do |fc|
      raise "fial repetida" if @filas.has_key?(fc)
      @filas[fc] = 1
    end
  end
  
  def dump_to_file(filename)
    # escribimos sql
    File.open(filename + ".sql", 'w') {|f| f.write(@sql) }
  end
  
end