
function addBingoListeners(app, bingo) {
 // bingoStarted (and selling finished)
    bingo.addListener('bingoStarted', function() {
        ['front', 'back'].forEach(function(s) { 
            app.settings.socket[s].emit('bingoStarted', { 'bingo': bingo.toJSON() });
        });                          
    });
    
    // sellingStarted
    bingo.addListener('sellingStarted', function() {
        console.log("Emitting sellingStarted that finishes: " + bingo.sellingFinishesAt);
        ['front', 'back'].forEach(function(s) { 
            app.settings.socket[s].emit('sellingStarted', { bingo: bingo.toJSON() });
        });                          
    });

    // numberCalled
    bingo.addListener('numberCalled', function(number) {
        app.settings.socket.front.emit('numberCalled', { 'bingoId': bingo.id, 'calledNumbers': bingo.calledNumbers });
        app.settings.socket.back.emit('numberCalled', { 'bingoId': bingo.id, 'calledNumbers': bingo.calledNumbers, 'remainingNumbers': bingo.remainingNumbers });
    });

    // line
    bingo.addListener('line', function(clientCard, lineNumbers) {
        ['front', 'back'].forEach(function(s) { 
            app.settings.socket[s].emit('line', { 'clientCard': clientCard, 'lineNumbers': lineNumbers });
        });                          
    });

    // bingo
    bingo.addListener('bingo', function(clientCard) {
        ['front', 'back'].forEach(function(s) { 
            app.settings.socket[s].emit('bingo', { 'clientCard': clientCard });
        });                          
    });

    // cancellation
    bingo.addListener('cancel', function() {
        ['front', 'back'].forEach(function(s) { 
            app.settings.socket[s].emit('bingoCancelled', { 'bingo': bingo });
        });                          
    });

    // finish
    bingo.addListener('finish', function() {
        ['front', 'back'].forEach(function(s) { 
            app.settings.socket[s].emit('bingoFinished', { 'bingo': bingo });
        });                          
    });

    // just passed accumulated order
    bingo.addListener('justPassedAccumulatedOrder', function() {
        ['front', 'back'].forEach(function(s) { 
            app.settings.socket[s].emit('justPassedAccumulatedOrder');
        });                          
    });

    // bingoUpdated
    bingo.addListener('bingoUpdated', function() {
        ['front', 'back'].forEach(function(s) { 
            app.settings.socket[s].emit('bingoUpdated', { 'bingo': bingo });
        });                          
    });

    bingo.addListener('cardsPurchased', function() {
        //console.log("Card purchased. Emitting bingoUpdated.");
        ['front', 'back'].forEach(function(s) { 
            app.settings.socket[s].emit('bingoUpdated', { 'bingo': bingo });
        });                          
    });
}

function notifyBingoUpdateVisitorsCount(socket, bingo) {
    // Emit a notification
    socket.sockets.emit('updateVisitorsCount', { 'bingoId': bingo.id, 'visitorsCount': bingo.visitorsCount, 'shownTotalClientsCount': bingo.shownTotalClientsCount });
}

function notifyUpdateVisitorsCount(socket, data) {
    // data.shownTotalClientsCount
    // Emit a notification. As there is no bingo running visitorsCount equals totalClientsCount.
    socket.sockets.emit('updateVisitorsCount', { 'visitorsCount': data.shownTotalClientsCount, 'shownTotalClientsCount': data.shownTotalClientsCount });
}

function notifyAccumulatedPrize(socket, data) {
    var accumulatedSettings = {
        'accumulatedPrize': data.accumulatedPrize,
        'accumulatedOrder': data.accumulatedOrder
    };
    socket.sockets.emit('updateAccumulatedPrize', accumulatedSettings);
}

function virtualClientsManager(bingo, argClientsId) {
    // bingo: a Bingo instance
    // clientsId: an array containing virtual clients id

    /* A random number of clients between half clientsId length and clientsId length
     * will be used, to pretend they are real clients.
     * 
     * Every client buys two series
     */

    var patio = require('patio');
    var comb = require('comb');
    var Client = patio.getModel('client');

    // Get a random number between floor of argClientsId.length/2 and clientsId.length
    var howMany = Math.floor(Math.random() * (argClientsId.length - argClientsId.length / 2 + 1) + argClientsId.length / 2);
    // howMany = 16;
    var clientsId = argClientsId.slice(0, howMany);
    var clients = [];

    bingo.addListener('sellingStarted', function() {
        console.log("virtualClientsManager: sellingStarted received");

        if (bingo.alreadyRunVirtualClientsManager) {
            console.log("virtualClientsManager: has already been run");
            return;
        }
        
        bingo.alreadyRunVirtualClientsManager = true;

        comb.serial([
            function() {
                if (bingo.isNew) {
                    return bingo.persist();
                }
            },
            function() {
                // Retrieve client and make deposit

                var allReady = [];
                console.log("virtualClientsManager: starting purchase for " + clientsId.length + " clients");
                clientsId.forEach(function(clientId) {
                    // console.log("virtualClientsManager: fetching client #" + clientId);
                    var ret = new comb.Promise();
                    allReady.push(ret);
                    Client.filter({id: clientId}).first().then(
                        function(client) {
                            clients.push(client);
                            // Make a deposit for 12 cards
                            var amount = Math.ceil(bingo.cardPrice * 12 * 10) / 10;
                            console.log("virtualClientsManager: Making deposit for client #" + client.id + " of $" + amount);
                            client.makeDeposit(amount, 'Deposito para cliente virtual').then(
                                function() {
                                    ret.callback();
                                },
                                
                                function(error) {
                                    console.log("ERROR al hacer deposito para cliente virtual #" + clientId + ": " + error);
                                }
                            );
                        },
                        function(error) {
                            console.log("ERROR fetching client: " + error);
                        }
                    );
                });

                return comb.when(allReady);
            },
            function() {
                var purchases = [];
                
                clients.forEach(function(client) {
                    purchases.push(function() {
                        var ret = new comb.Promise();
                        // Buy two series
                        client.buyCards(bingo, 2, true).then(
                            function(clientCards) {
                                console.log("Cliente virtual #" + client.id + ": compra correcta de cartones " + clientCards.map(function(cc) { return cc.number; }));
                                ret.callback();
                                               },
                            function(error) {
                                console.log("ERROR al comprar cartones para cliente virtual #" + client.id + ": " + error);
                                ret.errback();
                            }
                        );

                        return ret;
                    });
                });

                return comb.serial(purchases);
            }
        ]);
    });

    return howMany;
};

// Global list of chat clients
var clients = [];
function handleChatOn(io) {
    var sockets = io.of('/chat-client.html');
    sockets.on('connection', function (socket) {
        /*
         Handle requests to join the chat-room
         -------------------------------------
         */
        socket.on('join', function(nick, callback) {
            // If the nickname isn't in use, join the user
            if (clients.indexOf(nick) < 0) {

                // Store the nickname, we'll use it when sending messages
                socket.nick = nick;

                // Add the nickname to the global list
                clients.push(nick);

                // Send a message to all clients that a new user has joined
                socket.broadcast.emit("user-joined", nick);

                // Callback to the user with a successful flag and the list of clients
                callback(true, clients);

                // If the nickname is already in use, reject the request to join
            } else {
                callback(false);
            }
        });

        
        /*
         Handle chat messages
         --------------------
         */
        socket.on("chat", function(message) {
            // Check that the client has already joined successfully,
            // and that the message isn't just an empty string,
            // then foward the message to all clients
            if (socket.nick && message) {
                sockets.emit("chat", {sender: socket.nick, message: message});
            }
        });


        /*
         Handle client disconnection
         ---------------------------
         */
        socket.on("disconnect", function() {
            // Check that the user has already joined successfully
            if (socket.nick) {
                // Remove the client from the global list
                clients.splice(clients.indexOf(socket.nick), 1);
                // Let all the remaining clients know of the disconnect
                sockets.emit("user-left", socket.nick);
            }
        });
    });
};


exports.addBingoListeners = addBingoListeners;
exports.notifyUpdateVisitorsCount = notifyUpdateVisitorsCount;
exports.notifyBingoUpdateVisitorsCount = notifyBingoUpdateVisitorsCount;
exports.notifyAccumulatedPrize = notifyAccumulatedPrize;
exports.virtualClientsManager = virtualClientsManager;
exports.handleChatOn = handleChatOn;
