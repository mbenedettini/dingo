var utils = require(__dirname + '/../lib/utils');

xdescribe("Database initialization", function() {
    it("Recreates schema", function(done) {
        utils.recreateSchema().then(
            function() {
                console.log('- db: schema successfully recreated');
                done();
            }
        );
    });
});
