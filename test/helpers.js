var patio = require('patio');
var comb = require('comb');
var utils = require(__dirname + '/../lib/utils');

var testHelpers = {
    cleanDb : function() {
        return utils.cleanDb();

    }
};

module.exports = testHelpers;