// TODO: must be a better way to require utils without knowing the WHOLE path
var utils   = require(__dirname + '/../../../lib/utils');
//var testHelpers = utils.testHelpers;
//var testHelpers = require(__dirname + '/../../helpers');
var patio   = require('patio');
var comb    = require('comb');
var __      = require('underscore');
var should  = require('should');

var fixtures = utils.loadFixtures();

var e = function(error) { 
    console.log("ERROR: " + error); 
    true.should.be.false;
};

describe("bingo.buyClientCards", function() {
    var Bingo;
    var Card;
    var Client;

    var sampleBingo;
    var sampleCard;
    var sampleClient;
    
    beforeEach(function(done) {
        utils.cleanDb().then(
            function() { done(); },
            function(error) {
                console.log("Error when cleaning db: "  + error);
            }
        );
    });
    beforeEach(function(done) {
        var db = utils.getDbConnection();  // force db connection
        
        // NOTE: this beforeEach (and the previous afterEach) gets executed
        // with every IT clause.
        
        utils.getSyncedModels().then(
            function() {
                comb.serial([
                    function() {
                        if (sampleBingo && sampleBingo.isRunning) {
                            return sampleBingo.pause();
                        }
                        return new comb.Promise().callback();
                    },
                    function() {
                        return utils.cleanDb();
                    },
                    function() {
                        Bingo = patio.getModel('bingo');
                        Card = patio.getModel('card');
                        Client = patio.getModel('client');
                        
                        var b = new Bingo(fixtures.bingo[0]);
                        b.currentState = 10; // SELLING
                        b.linePrizeRate = 0.05;
                        b.bingoPrizeRate = 0.25;

                        sampleCard = new Card(fixtures.card[0]);
                        sampleClient = new Client(fixtures.client[0]);
                        
                        return comb.serial([
                            comb.hitch(b, "save"),
                            comb.hitch(sampleCard, "save"),
                            comb.hitch(sampleClient, "save"),
                            function() { 
                                /* Weird things happen if we dont re-fetch the bingo again.
                                 * Specially with Bingo.lastClientCardSold.
                                 * I believe it's a patio bug.
                                 */
                                console.log("Refetching bingo");
                                return Bingo.one().then(
                                    function(bingo) {
                                        sampleBingo = bingo;
                                    }
                                );
                            }
                            
                        ]);
                    }
                ]).then(
                    function() {
                        done();
                    },
                    function(error) { e(error); }
                );

            },
            function(error) {
                console.log("ERROR when importing : " + error);
            }
        );
    });
    
    

    describe("ClientCards buying", function() {

        it("Must remember the first and last cards bought and increase prizes", function(done) {
            var Card = patio.getModel('card');
            var cards = [];
            var i;
            for (i=2; i<=6; i++) {
                var c = new Card({
                    series: 1, 
                    number: i, 
                    firstLine: sampleCard.firstLineNumbers,
                    secondLine: sampleCard.secondLineNumbers,
                    thirdLine: sampleCard.thirdLineNumbers
                });
                cards.push(c);
            }
            
            comb.serial([
                function () {
                    return Card.save(cards);
                },
                function() {
                    // Initial but important check.
                    should.not.exist(sampleBingo.lastClientCardSold);
                    should.not.exist(sampleBingo.firstSoldCardNumber);
                    should.not.exist(sampleBingo.lastSoldCardNumber);

                    sampleBingo.linePrize.should.eql(0);
                    sampleBingo.bingoPrize.should.eql(0);
                    
                    var ret = new comb.Promise();
                    sampleBingo.buyClientCards(1).then(
                        function(clientCards) {
                            clientCards.length.should.eql(1);
                            clientCards[0].card.number.should.eql(sampleCard.number);
                            clientCards[0].card.id.should.eql(sampleCard.id);
                            
                            console.log("Testing bingo association and price");
                            clientCards[0].price.should.eql(sampleBingo.cardPrice);

                            sampleBingo.firstSoldCardNumber.should.eql(sampleCard.number);
                            sampleBingo.lastSoldCardNumber.should.eql(sampleCard.number);

                            // linePrize should have increased by 1.00 * 0.05
                            sampleBingo.linePrize.should.eql(0.05);
                            // bingoPrize should have increased by 1.00 * 0.25
                            sampleBingo.bingoPrize.should.eql(0.25);
                            
                            /* TODO: ClientCard stubs should be used and therefore there would
                             * be no need for a sampleClient.
                             */
                            clientCards[0].client = sampleClient;
                            
                            clientCards[0].bingo.then(function(bingo) {
                                bingo.id.should.eql(sampleBingo.id);
                                sampleBingo.save().then(
                                    function() {
                                        // Cannot test sampleBingo.lastClientCardSold.id before because it had not been saved.
                                        sampleBingo.lastClientCardSold.id.should.eql(clientCards[0].id);

                                        ret.callback();
                                    }
                                );
                            });
                        },
                        function(error) {
                            console.log("ERROR: " + error);
                        }
                    );

                    return ret;
                },
                function() {
                    /* OK, at the moment the last bought card is sampleCard.
                     * Let's buy another one and it should be cards[0].
                     */
                    console.log("2nd purchase");

                    sampleBingo.firstSoldCardNumber.should.eql(sampleCard.number);
                    sampleBingo.lastSoldCardNumber.should.eql(sampleCard.number);

                    return sampleBingo.buyClientCards(1).then(
                            function(clientCards) {
                                clientCards.length.should.eql(1);
                                clientCards[0].card.number.should.eql(cards[0].number);
                                clientCards[0].card.id.should.eql(cards[0].id);

                                clientCards.forEach(function(c) {
                                    c.isSeries.should.be.false;
                                });

                                sampleBingo.linePrize.should.eql(0.10);
                                sampleBingo.bingoPrize.should.eql(0.50);

                                sampleBingo.firstSoldCardNumber.should.eql(sampleCard.number);
                                sampleBingo.lastSoldCardNumber.should.eql(cards[0].number);

                            },
                            function(error) {
                                console.log("ERROR: " + error);
                            }  
                        );
                },
                function() {
                    /* Buy two more. They should be cards[1] and cards[2].
                     */
                    console.log("3rd purchase");
                    return sampleBingo.buyClientCards(2).then(
                        function(clientCards) {
                            clientCards.length.should.eql(2);
                            clientCards[0].card.number.should.eql(cards[1].number);
                            clientCards[0].card.id.should.eql(cards[1].id);
                            clientCards[1].card.number.should.eql(cards[2].number);
                            clientCards[1].card.id.should.eql(cards[2].id);

                            clientCards.forEach(function(c) {
                                c.isSeries.should.be.false;
                            });

                            sampleBingo.linePrize.should.eql(0.20);
                            sampleBingo.bingoPrize.should.eql(1.00);

                            sampleBingo.firstSoldCardNumber.should.eql(sampleCard.number);
                            sampleBingo.lastSoldCardNumber.should.eql(cards[2].number);

                        },
                        function(error) {
                            console.log("ERROR: " + error);
                        }
                    );
                },
                function() {
                    /* Three more. The first two are cards[3] and cards[4]. The last one
                     * should be sampleCard as the sequence is reinitiated.
                     */
                    console.log("4th purchase");
                    return sampleBingo.buyClientCards(3).then(
                        function(clientCards) {
                            clientCards.length.should.eql(3);
                            clientCards[0].card.number.should.eql(cards[3].number);
                            clientCards[0].card.id.should.eql(cards[3].id);
                            clientCards[1].card.number.should.eql(cards[4].number);
                            clientCards[1].card.id.should.eql(cards[4].id);
                            clientCards[2].card.number.should.eql(sampleCard.number);
                            clientCards[2].card.id.should.eql(sampleCard.id);

                            clientCards.forEach(function(c) {
                                c.isSeries.should.be.false;
                            });

                            sampleBingo.linePrize.should.eql(0.35);
                            sampleBingo.bingoPrize.should.eql(1.75);

                            sampleBingo.firstSoldCardNumber.should.eql(sampleCard.number);
                            sampleBingo.lastSoldCardNumber.should.eql(sampleCard.number);
                            
                        },
                        function(error) {
                            console.log("ERROR: " + error);
                        }
                    );
                },
                function() {
                    console.log("5th purchase");
                    /* Buy 5 cards to leave sampleCard ready to be bought */
                    var ret = new comb.Promise();
                    sampleBingo.buyClientCards(5).then(
                        function() {
                            /* At this point the last card sold is the last one in the sequence.
                             * The next one to be sold must be the first one in the sequence (which
                             * is sampleCard).
                             */

                            sampleBingo.linePrize.should.eql(0.60);
                            sampleBingo.bingoPrize.should.eql(3.00);

                            sampleBingo.firstSoldCardNumber.should.eql(sampleCard.number);
                            sampleBingo.lastSoldCardNumber.should.eql(cards[4].number);

                            sampleBingo.buyClientCards(1).then(
                                function(clientCards) {
                                    clientCards.length.should.eql(1);
                                    clientCards[0].card.number.should.eql(sampleCard.number);
                                    clientCards[0].card.id.should.eql(sampleCard.id);

                                    sampleBingo.linePrize.should.eql(0.65);
                                    sampleBingo.bingoPrize.should.eql(3.25);

                                    sampleBingo.firstSoldCardNumber.should.eql(sampleCard.number);
                                    sampleBingo.lastSoldCardNumber.should.eql(sampleCard.number);

                                    ret.callback();
                                },
                                e
                            );
                        },
                        e
                    );
                    return ret;
                }
            ]).then(
                function() { done(); },
                function(error) {
                    console.log("Error: " + error);
                }
            );
            

        });

        it("Must get the last card sold from the last bingo of the same price", function(done) {
            this.timeout(3000);
            /* Bingos sell cards on a per-price basis. That is, a bingo of a certain X price sells cards
             * from, say, 22 to 45, the next bingo of the same X price will start selling the card number 46.
             * 
             * If a bingo of a different price Y sells cards between them it only looks other bingos of the same price Y.
             * 
             * */
            var Card = patio.getModel('card');
            var cards = [];
            var i;
            for (i=2; i<=6; i++) {
                var c = new Card({
                    series: 1, 
                    number: i, 
                    firstLine: sampleCard.firstLineNumbers,
                    secondLine: sampleCard.secondLineNumbers,
                    thirdLine: sampleCard.thirdLineNumbers
                });
                cards.push(c);
            }

            // make sure sampleBingo will be able to be started
            sampleBingo.currentState = 0;

            // more bingos to help test the algorithm
            var previousBingoSamePrice = new Bingo(fixtures.bingo[0]);
            previousBingoSamePrice.finished = new Date();

            var anotherPreviousBingoSamePrice = new Bingo(fixtures.bingo[0]);
            anotherPreviousBingoSamePrice.finished = new Date(previousBingoSamePrice.finished.getTime() + 1000);

            var previousBingoOtherPrice = new Bingo(fixtures.bingo[0]);
            previousBingoOtherPrice.cardPrice += 1;
            previousBingoOtherPrice.finished = new Date();

            
            comb.serial([
                comb.hitch(Card, "save", cards),
                comb.hitch(previousBingoSamePrice, "save"),
                comb.hitch(previousBingoSamePrice, "start"),
                comb.hitch(previousBingoSamePrice, "reload"), // needed for lastClientCardSold not to be a promise (patio bug).
                function() {
                    var ret = new comb.Promise();
                    // At this point the bingo previousBingoSamePrice is in selling state
                    should.not.exist(previousBingoSamePrice.lastClientCardSold);
                    previousBingoSamePrice.buyClientCards(1).then(
                        function(clientCards) {
                            var c = clientCards.shift();
                            previousBingoSamePrice.lastClientCardSold.number.should.eql(c.number);
                            c.number.should.eql(sampleCard.number);
                            ret.callback();
                        }
                    );
                    return ret;
                },
                comb.hitch(previousBingoSamePrice, "pause"),
                function () {
                    // We leave it in finished state, so that its lastClientCardSold can be taken into account by anotherPreviousBingoSamePrice
                    previousBingoSamePrice.currentState = 40; // finished
                    return previousBingoSamePrice.save();
                },

                comb.hitch(anotherPreviousBingoSamePrice, "save"),
                comb.hitch(anotherPreviousBingoSamePrice, "reload"),
                function() {                    
                    should.not.exist(anotherPreviousBingoSamePrice.lastClientCardSold);
                    return (new comb.Promise()).callback();
                },
                comb.hitch(anotherPreviousBingoSamePrice, "start"),
                comb.hitch(anotherPreviousBingoSamePrice, "reload"),
                function() {
                    var ret = new comb.Promise();
                    anotherPreviousBingoSamePrice.lastClientCardSold.number.should.eql(previousBingoSamePrice.lastClientCardSold.number);                   
                    anotherPreviousBingoSamePrice.buyClientCards(1).then(
                        function(clientCards) {
                            var c = clientCards.shift();
                            anotherPreviousBingoSamePrice.lastClientCardSold.number.should.eql(c.number);
                            c.number.should.eql(previousBingoSamePrice.lastClientCardSold.number + 1);
                            ret.callback();
                        }
                    );
                    return ret;
                },
                comb.hitch(anotherPreviousBingoSamePrice, "pause"),
                function () {
                    anotherPreviousBingoSamePrice.currentState = 40; // finished
                    return anotherPreviousBingoSamePrice.save();
                },


                comb.hitch(previousBingoOtherPrice, "save"),
                comb.hitch(previousBingoOtherPrice, "reload"),
                function() {                    
                    should.not.exist(previousBingoOtherPrice.lastClientCardSold);
                    return (new comb.Promise()).callback();
                },
                comb.hitch(previousBingoOtherPrice, "start"),
                comb.hitch(previousBingoOtherPrice, "reload"),
                function() {
                    var ret = new comb.Promise();
                    should.not.exist(previousBingoOtherPrice.lastClientCardSold);
                    previousBingoOtherPrice.buyClientCards(1).then(
                        function(clientCards) {
                            /* As this bingo has a different cardPrice, the selling starts from the beginning: sampleCard (the first card with the lowest number) */
                            var c = clientCards.shift();
                            previousBingoOtherPrice.lastClientCardSold.number.should.eql(c.number);
                            c.number.should.eql(sampleCard.number);
                            ret.callback();
                        }
                    );
                    return ret;
                },
                comb.hitch(previousBingoOtherPrice, "pause"),
                function () {
                    previousBingoOtherPrice.currentState = 40; // finished
                    return previousBingoOtherPrice.save();
                },


                // sampleBingo has the same price as the first two bingos
                comb.hitch(sampleBingo, "save"),
                comb.hitch(sampleBingo, "reload"),
                function() {                    
                    should.not.exist(sampleBingo.lastClientCardSold);
                    return (new comb.Promise()).callback();
                },
                comb.hitch(sampleBingo, "start"),
                comb.hitch(sampleBingo, "reload"),
                function() {
                     var ret = new comb.Promise();
                     sampleBingo.lastClientCardSold.number.should.eql(anotherPreviousBingoSamePrice.lastClientCardSold.number);                   
                     sampleBingo.buyClientCards(1).then(
                         function(clientCards) {
                             var c = clientCards.shift();
                             sampleBingo.lastClientCardSold.number.should.eql(c.number);
                             c.number.should.eql(anotherPreviousBingoSamePrice.lastClientCardSold.number + 1);                   
                             ret.callback();
                         }
                     );
                     return ret;
                },
                comb.hitch(sampleBingo, "pause")
               
            ]).then(
                function() { done(); },
                function(error) {
                    console.log("Error: " + error);
                }
            );
        });

        it("Must cancel itself when selling cancellation timeout arrives", function(done) {
            this.timeout(4000);

            // make sure sampleBingo will be able to be started
            sampleBingo.currentState = 0;
            sampleBingo.sellingInterval = 4;
            // these conditions will never be met
            sampleBingo.minClientsCount = 2;
            sampleBingo.minTotalClientsCount = 2;
            // Will cancel itself in two seconds from the moment it starts selling
            sampleBingo.sellingCancellationTimeout = 2;

            var listenedToCancel = false;

            sampleBingo.save().then(
                function() {
                    sampleBingo.addListener('cancel', function() {
                        console.log("Listened to cancel");
                        listenedToCancel = true;
                    });

                    var startTime = new Date();
                    sampleBingo.start();
                }
            );

            setTimeout(function() {
                console.log("Verifying!");
                listenedToCancel.should.be.true;
                sampleBingo.isCancelled.should.be.true;
                done();
            }, 3000);
        });

        it("Must not cancel itself if starting conditions have been met", function(done) {
            this.timeout(6000);

            sampleBingo.currentState = 0;
            // sell countdown is 2 seconds
            sampleBingo.sellingInterval = 2;
            // these conditions will be faked
            sampleBingo.minClientsCount = 2;
            sampleBingo.minTotalClientsCount = 2;
            // Will cancel itself in two seconds from the moment it starts selling
            sampleBingo.sellingCancellationTimeout = 2;

            var listenedToCancel = false;

            sampleBingo.save().then(
                function() {
                    sampleBingo.addListener('cancel', function() {
                        console.log("Listened to cancel");
                        listenedToCancel = true;
                    });

                    var startTime = new Date();
                    sampleBingo.start();
                    
                    setTimeout(function() {
                        // Fake conditions for the selling countdown to begin (and the bingo to start at the end of it)
                        should.exist(sampleBingo._sellingCancellationTimer);
                        should.not.exist(sampleBingo.sellingFinishesAt);
                        sampleBingo.clientsCount = 2;
                        sampleBingo.setTotalClientsCount(2);
                    }, 1000);

                    setTimeout(function() {
                        /* At this moment the bingo is selling with sellingTimeout activated */
                        console.log("Verifying!");
                        sampleBingo.isSelling.should.be.true;
                        should.not.exist(sampleBingo._sellingCancellationTimer);
                        should.exist(sampleBingo.sellingFinishesAt);
                        
                        sampleBingo.pause().then(function() {
                            listenedToCancel.should.be.false;
                            sampleBingo.isCancelled.should.be.false;
                            done();
                        });
                    }, 2000);

                }
            );

        });

        it("Must register how many cards were sold", function(done) {
            var Card = patio.getModel('card');
            var cards = [];
            var i;
            for (i=2; i<=6; i++) {
                var c = new Card({
                    series: 1, 
                    number: i, 
                    firstLine: sampleCard.firstLineNumbers,
                    secondLine: sampleCard.secondLineNumbers,
                    thirdLine: sampleCard.thirdLineNumbers
                });
                cards.push(c);
            }
            
            comb.serial([
                function () {
                    return Card.save(cards);
                },
                function() {
                    // Initial but important check.
                    should.not.exist(sampleBingo.lastClientCardSold);
                    should.not.exist(sampleBingo.firstSoldCardNumber);
                    should.not.exist(sampleBingo.lastSoldCardNumber);

                    sampleBingo.linePrize.should.eql(0);
                    sampleBingo.bingoPrize.should.eql(0);
                    
                    var ret = new comb.Promise();
                    sampleBingo.buyClientCards(1).then(
                        function(clientCards) {
                            sampleBingo.clientCardsCount.should.eql(1);

                            sampleBingo.buyClientCards(3).then(
                                function(clientCards) {
                                    sampleBingo.clientCardsCount.should.eql(4);

                                    done();
                                }
                            );
                        }
                    );
                }
            ]);

        });

        it("Must be able to sell series", function(done) {
            var Card = patio.getModel('card');
            var cards = [sampleCard];
            var i;
            for (i=2; i<=24; i++) {
                var c = new Card({
                    series: 1, 
                    number: i, 
                    firstLine: sampleCard.firstLineNumbers,
                    secondLine: sampleCard.secondLineNumbers,
                    thirdLine: sampleCard.thirdLineNumbers
                });
                cards.push(c);
            }
            
            comb.serial([
                function () {
                    return Card.save(cards);
                },
                function() {
                    should.not.exist(sampleBingo.lastClientCardSold);
                    should.not.exist(sampleBingo.firstSoldCardNumber);
                    should.not.exist(sampleBingo.lastSoldCardNumber);

                    sampleBingo.linePrize.should.eql(0);
                    sampleBingo.bingoPrize.should.eql(0);
                    
                    var ret = new comb.Promise();
                    sampleBingo.buyClientCards(1, true).then(
                        function(clientCards) {
                            clientCards.length.should.eql(6);
                            clientCards[0].card.number.should.eql(cards[0].number);
                            clientCards[0].card.id.should.eql(cards[0].id);

                            clientCards.forEach(function(c) {
                                c.isSeries.should.be.true;
                            });
                            
                            sampleBingo.firstSoldCardNumber.should.eql(cards[0].number);
                            sampleBingo.lastSoldCardNumber.should.eql(cards[5].number);

                            // linePrize should have increased by 1.00 * 6 * 0.05
                            sampleBingo.linePrize.should.eql(0.30);
                            // bingoPrize should have increased by 1.00 * 6 * 0.25
                            sampleBingo.bingoPrize.should.eql(1.50);
                            
                            clientCards[0].client = sampleClient;
                            
                            clientCards[0].bingo.then(function(bingo) {
                                sampleBingo.save().then(
                                    function() {
                                        sampleBingo.lastClientCardSold.id.should.eql(clientCards[5].id);

                                        ret.callback();
                                    }
                                );
                            });
                        },
                        function(error) {
                            console.log("ERROR: " + error);
                        }
                    );

                    return ret;
                },
                function() {
                    /* Buy a single card */

                    return sampleBingo.buyClientCards(1).then(
                            function(clientCards) {
                                clientCards.length.should.eql(1);
                                clientCards[0].card.number.should.eql(cards[6].number);

                                clientCards[0].isSeries.should.be.false;

                            },
                            function(error) {
                                console.log("ERROR: " + error);
                            }  
                        );
                },
                function() {
                    /* And two series */

                    return sampleBingo.buyClientCards(2, true).then(
                            function(clientCards) {
                                console.log("Two series!");
                                clientCards.length.should.eql(12);
                                clientCards[0].card.number.should.eql(cards[12].number);
                                clientCards[11].card.number.should.eql(cards[23].number);

                                clientCards.forEach(function(c) {
                                    c.isSeries.should.be.true;
                                });

                            },
                            function(error) {
                                console.log("ERROR: " + error);
                            }  
                        );
                },
                function() {
                    /* Another series must begin from top (the first card ) */

                    return sampleBingo.buyClientCards(1, true).then(
                            function(clientCards) {
                                console.log("Another series");
                                clientCards.length.should.eql(6);
                                clientCards[0].card.number.should.eql(cards[0].number);
                                clientCards[5].card.number.should.eql(cards[5].number);

                                clientCards.forEach(function(c) {
                                    c.isSeries.should.be.true;
                                });

                            },
                            function(error) {
                                console.log("ERROR: " + error);
                            }  
                        );
                },
            ]).then(
                function() { done(); },
                function(error) {
                    console.log("Error: " + error);
                }
            );
            

        });
    });
});
