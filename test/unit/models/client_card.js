// TODO: must be a better way to require utils without knowing the WHOLE path
var utils = require(__dirname + '/../../../lib/utils');
var patio = require('patio');
var comb = require('comb');
var should = require('should');
var fixtures = utils.loadFixtures();

describe("Simple client card operations", function() {
    var Client;
    var Bingo;
    var Card;
    var ClientCard;
    var Transaction;
    
    var sampleClient;
    var sampleBingo;
    var sampleCard;
    
    before(function(done) {
        // Cleanup tables
        utils.cleanDb().then(function() { done(); });
    });
    
    before(function(done) {
        var db = utils.getDbConnection();  // force db connection
        
        utils.getSyncedModels()
            .then(
                function() {
                    Client = patio.getModel('client');
                    Bingo = patio.getModel('bingo');
                    Card = patio.getModel('card');
                    ClientCard = patio.getModel('client_card');
                    Transaction = patio.getModel('transaction');
                    
                    patio.syncModels().then(
                        /* Create a sample client */
                        function() {
                            sampleClient = new Client(fixtures.client[0]);
                            sampleBingo = new Bingo({
                                'description':          'SampleBingo', 
                                'starts':               new Date(),
                                'cardPrice':            3.33,
                                'cardColor':            1,
                                'lapseBetweenNumbers':  5
                            });
                            sampleCard = new Card(fixtures.card[0]);

                            comb.serial([
                                comb.hitch(sampleClient, "save"),
                                comb.hitch(sampleBingo, "save"),
                                comb.hitch(sampleCard, "save"),
                                comb.hitch(sampleClient, "makeDeposit", 10.00, "Sample deposit")
                            ]).then(function() { done(); });
                        },
                        function(error) {
                            console.log("ERROR when syncing: " + error);
                        }
                    );
                },
                function(error) {
                    console.log("ERROR when importing : " + error);
                }
            );
    });
    
    describe("ClientCard", function() {
        it("Creates an instance and has some properties", function(done) {
            
            sampleBingo.should.be.ok;
            sampleClient.should.be.ok;
            sampleCard.should.be.ok;
            
            var cc = ClientCard.createPurchase(3.33, sampleClient, sampleCard, sampleBingo);

            comb.serial([
                function() {
                    return cc.save();
                },
                function() {
                    cc.client.then(function(client) {
                        client.id.should.eql(sampleClient.id);
                    });
                },
                function() {
                    // ClientCard.card is eagerly loaded so it is not a Promise but the object itself.
                    cc.card.id.should.eql(sampleCard.id);
                    
                    // Lines are borrowed from the Card
                    cc.firstLineNumbers.should.eql(sampleCard.firstLineNumbers);
                    cc.secondLineNumbers.should.eql(sampleCard.secondLineNumbers);
                    cc.thirdLineNumbers.should.eql(sampleCard.thirdLineNumbers);
                    
                    cc.firstLineNumbers.should.eql([1,2,3,4,5]);
                    cc.secondLineNumbers.should.eql([6,7,8,9,10]);
                    cc.thirdLineNumbers.should.eql([11,12,13,14,15]);
                    
                    // Initially, called numbers must be empty
                    cc.firstLineCalled.should.eql([]);
                    cc.secondLineCalled.should.eql([]);
                    cc.thirdLineCalled.should.eql([]);
                },
                function() {
                    return cc.bingo.then(function(bingo) {
                        bingo.id.should.eql(sampleBingo.id);
                    });
                },
                done
            ]);
        });

        it("Creates an instance in a different way", function(done) {
            
            sampleBingo.should.be.ok;
            sampleClient.should.be.ok;
            sampleCard.should.be.ok;
            
            var cc = ClientCard.create({ card: sampleCard });
            cc.bingo = sampleBingo;
            cc.price = sampleBingo.cardPrice;
            cc.client = sampleClient;

            comb.serial([
                function() {
                    return cc.save();
                },
                function() {
                    cc.client.then(function(client) {
                        client.id.should.eql(sampleClient.id);
                    });
                },
                function() {
                    cc.bingo.then(function(bingo) {
                        bingo.id.should.eql(sampleBingo.id);
                        cc.price.should.eql(sampleBingo.cardPrice);
                    });
                },
                function() {
                    cc.card.id.should.eql(sampleCard.id);
                    
                },
                done
            ]);
        });
        
        it("Receives numbers until a line is called out", function(done) {
            var self = this;
            // Some previous checks
            sampleBingo.should.be.ok;
            sampleClient.should.be.ok;
            sampleCard.should.be.ok;
            
            sampleCard.firstLineNumbers.should.be.ok;
            sampleCard.secondLineNumbers.should.be.ok;
            sampleCard.thirdLineNumbers.should.be.ok;
            
            var cc = ClientCard.createPurchase(3.33, sampleClient, sampleCard, sampleBingo);
            var i = 0;
            var numbers = [1,2,3,4,5];
            
            cc.addListener('line', function(clientCard, lineNumbers) {
                clearInterval(self.interval);
                cc.reload().then(function() {
                    cc.firstLineCalled.should.eql([1,2,3,4,5]);
                    lineNumbers.should.eql([1,2,3,4,5]);
                    cc.id.should.eql(clientCard.id);
                    // Make sure all numbers have been pushed
                    i.should.eql(5);
                    done();
                });
            });
            
            cc.addListener('bingo', function(ccId) {
                throw new Error('Bingo shouldnt have been called!');
            });
            
            cc.save().then(
                function() {
                    cc.firstLineNumbers.should.be.ok;
                    cc.secondLineNumbers.should.be.ok;
                    cc.thirdLineNumbers.should.be.ok;
                    
                    cc.firstLineCalled.should.eql([]);
                    cc.secondLineCalled.should.eql([]);
                    cc.thirdLineCalled.should.eql([]);
                    
                    self.interval = setInterval(function() {
                        var number = numbers[i];
                        if (number >= 0) {
                            i++; // Increment before calling the number
                            cc.pushNumber(number);
                        }
                    }, 100);
                }
            );
        });
        
        it("Receives more numbers until a line is called out", function(done) {
            var self = this;
            var cc = ClientCard.createPurchase(3.33, sampleClient, sampleCard, sampleBingo);
            var i = 0;
            var numbers = [6,99,7,33,44,8,9,22,10];
            //~ var count = 0;
            
            cc.addListener('line', function(clientCard, lineNumbers) {
                clearInterval(self.interval);
                cc.secondLineCalled.should.eql([6,7,8,9,10]);
                lineNumbers.should.eql([6,7,8,9,10]);
                cc.id.should.eql(clientCard.id);
                // Make sure all numbers have been pushed
                i.should.eql(9);
                done();
            });
            
            cc.addListener('bingo', function(ccId) {
                throw new Error('Bingo shouldnt have been called!');
            });
            
            cc.save().then(
                function() {
                    cc.firstLineNumbers.should.be.ok;
                    cc.secondLineNumbers.should.be.ok;
                    cc.thirdLineNumbers.should.be.ok;
                    
                    cc.firstLineCalled.should.eql([]);
                    cc.secondLineCalled.should.eql([]);
                    cc.thirdLineCalled.should.eql([]);
                    
                    self.interval = setInterval(function() {
                        var number = numbers[i];
                        if (number >= 0) {
                            i++;
                            cc.pushNumber(number);
                        }
                    }, 100);
                }
            );
        });
        
        it("Receives numbers until a bingo is called out", function(done) {
            this.timeout(10000);
            var self = this;
            
            var cc = ClientCard.createPurchase(3.33, sampleClient, sampleCard, sampleBingo);
            var i = 0;
            var numbers = [90,1,20,2,3,21,4,22,23,5,27,11,12,28,29,30,13,31,14,32,33,15,6,24,7,25,26,8,9,10];
            var lineCalled = false;
            
            cc.addListener('line', function(clientCard, lineNumbers) {
                cc.reload().then(function() {
                    // Check for line only once
                    if (!lineCalled) {
                        i.should.eql(10);
                        lineCalled = true;
                    }
                });
            });
            
            cc.addListener('bingo', function(ccId) {
                clearInterval(self.interval);
                // Reload the ClientCard to make sure called numbers were persisted
                cc.reload().then(function() {
                    cc.firstLineCalled.should.eql([1,2,3,4,5]);
                    cc.secondLineCalled.should.eql([6,7,8,9,10]);
                    cc.thirdLineCalled.should.eql([11,12,13,14,15]);
                    
                    lineCalled.should.be.true;
                    i.should.eql(30);
                    
                    done();
                });
            });
            //~ 
            cc.save().then(
                function() {
                    cc.firstLineNumbers.should.be.ok;
                    cc.secondLineNumbers.should.be.ok;
                    cc.thirdLineNumbers.should.be.ok;
                    
                    cc.firstLineCalled.should.eql([]);
                    cc.secondLineCalled.should.eql([]);
                    cc.thirdLineCalled.should.eql([]);
                    
                    self.interval = setInterval(function() {
                        var number = numbers[i];
                        if (number >= 0) {
                            i++;
                            cc.pushNumber(number);
                        }
                    }, 300);
                }
            );
        });

        it("Returns an array of available cards for purchasing", function(done) {
            var cards = [];
            var i;
            for (i=2; i<=6; i++) {
                var c = new Card({
                    series: 1, 
                    number: i, 
                    firstLine: sampleCard.firstLine,
                    secondLine: sampleCard.secondLine,
                    thirdLine: sampleCard.thirdLine
                });
                cards.push(c);
            }
            
            comb.serial([
                function () {
                    return Card.save(cards)
                },
                function () {
                    return ClientCard.getAvailables(null, 1).then(
                            function(clientCards) {
                                clientCards.length.should.eql(1);
                                clientCards[0].card.id.should.eql(sampleCard.id);
                            },
                            function(error) {
                                console.log("ERROR: " + error);
                            }
                    );
                },
                function() {
                    return ClientCard.getAvailables(null, 4).then(
                            function(clientCards) {
                                clientCards.length.should.eql(4);
                                clientCards[0].card.id.should.eql(sampleCard.id);
                                clientCards[1].card.id.should.eql(cards[0].id);
                                clientCards[2].card.id.should.eql(cards[1].id);
                                clientCards[3].card.id.should.eql(cards[2].id);
                            },
                            function(error) {
                                console.log("ERROR: " + error);
                            }
                    );
                },
                function() {
                    var lastClientCardSold = new ClientCard({
                        card: cards[0],
                        client: sampleClient,
                        bingo: sampleBingo,
                        price: sampleBingo.cardPrice
                    });
                    return lastClientCardSold.save().then(
                        function(cc) {
                            return ClientCard.getAvailables(cc, 2).then(
                                function(clientCards) {
                                    clientCards.length.should.eql(2);
                                    clientCards[0].card.id.should.eql(cards[1].id);
                                    clientCards[1].card.id.should.eql(cards[2].id);
                                },
                                function(error) {
                                    console.log("ERROR: " + error);
                                }
                            );
                        },
                        function(error) {
                            console.log("ERROR: " + error);
                        }
                    );
                },
                done
            ]);
        });
        
        it("Can tell if has won a line", function(done) {
            var cc = ClientCard.createPurchase(1.00, sampleClient, sampleCard, sampleBingo);
            cc.save().then(
                function() {
                    cc.isLineWinner([1,2,3,4,5]).should.eql([1,2,3,4,5]);
                    should.not.exist(cc.isLineWinner([1,2,3,4])); // returns null
                    should.not.exist(cc.isLineWinner([0,1,2,3]));
                    should.not.exist(cc.isLineWinner([]));
                    should.not.exist(cc.isLineWinner([1,2,3,4,6,7,8]));

                    cc.isLineWinner([6,7,8,9,10]).should.eql([6,7,8,9,10]);
                    should.not.exist(cc.isLineWinner([6,7,8,9]));
                    should.not.exist(cc.isLineWinner([5,6,7,8]));
                    should.not.exist(cc.isLineWinner([5,6,7,9,10]));

                    cc.isLineWinner([11,12,13,14,15]).should.eql([11,12,13,14,15]);
                    should.not.exist(cc.isLineWinner([12,13,14,15,16]));
                    should.not.exist(cc.isLineWinner([10,11,12,13]));
                    done();
                }
            );
        });

        it("Can tell if has won bingo", function(done) {
            var cc = ClientCard.createPurchase(1.00, sampleClient, sampleCard, sampleBingo);
            var onlyWinners = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];
            var winnersPlusOthers = [90,1,20,2,3,21,4,22,23,5,6,24,7,25,26,8,9,10,27,11,12,28,29,30,13,31,14,32,33,15];
            cc.save().then(
                function() {
                    // isBingoWinner() returns winner numbers
                    cc.isBingoWinner(onlyWinners).should.eql(onlyWinners);
                    cc.isBingoWinner(winnersPlusOthers).should.eql(onlyWinners);
                    should.not.exist(cc.isBingoWinner([]));
                    should.not.exist(cc.isBingoWinner(onlyWinners.slice(0,14)));
                    should.not.exist(cc.isBingoWinner(winnersPlusOthers.slice(0, winnersPlusOthers.length - 1)));
                    done();
                }
            );
        });
    });
});
