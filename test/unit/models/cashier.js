var patio = require('patio');
var comb = require('comb');
var should = require('should');
var utils = require(__dirname + '/../../../lib/utils');
var fixtures = utils.loadFixtures();

describe("Cashier creation and authentication", function() {

    before(function(done) {
        var db = utils.getDbConnection();
        return comb.serial([
            comb.hitch(utils, "cleanDb"),
            comb.hitch(utils, "getSyncedModels")
        ]).then(
            function() {
                done();
            },
            function(error) {
                console.log("Error on before(): " + error);
            }
        );;
    });

    beforeEach(function(done) {
        var Cashier = patio.getModel('cashier');

        Cashier.remove().then(function() { done(); });
    });

    it("Creates a cashier, generates a password and authenticates", function(done) {
        var Cashier = patio.getModel('cashier');

        var c = new Cashier(fixtures.cashier[0]);
        var password;

        comb.serial([
            comb.hitch(c, "save"),
            function generatePassword() {
                var ret = new comb.Promise();
                c.generatePassword().then(
                    function(generatedPwd) {
                        should.exist(generatedPwd);

                        password = generatedPwd;
                        ret.callback();
                    }
                );
                return ret;
            },
            function authenticate() {
                var ret = new comb.Promise();

                c.authenticate(password).then(
                    function(authOk) {
                        authOk.should.be.true;
                        ret.callback();
                    },
                    function(error) {
                        console.log("ERROR on authenticate: " + error);
                    }
                );

                return ret;
            }
        ]).then(
            function() {
                done();
            }
        );
    });

    it("Creates a cashier, generates a password and authenticates with a wrong password 3 times", function(done) {
        var Cashier = patio.getModel('cashier');

        var c = new Cashier(fixtures.cashier[0]);
        var password;

        comb.serial([
            comb.hitch(c, "save"),
            function generatePassword() {
                var ret = new comb.Promise();
                c.generatePassword().then(
                    function(generatedPwd) {
                        should.exist(generatedPwd);

                        password = generatedPwd;
                        ret.callback();
                    }
                );
                return ret;
            },
            function authenticate() {
                var ret = new comb.Promise();

                c.authenticate(password).then(
                    function(authOk) {
                        authOk.should.be.true;
                        ret.callback();
                    },
                    function(error) {
                        console.log("ERROR on authenticate: " + error);
                    }
                );

                return ret;
            },
            function authenticateWrong() {
                var ret = new comb.Promise();

                c.authenticate("blah").then(
                    function(authOk) {
                        authOk.should.be.false;
                        c.badLoginsCount.should.eql(1);
                        ret.callback();
                    }                
                );

                return ret;
            },
            function authenticateWrong() {
                var ret = new comb.Promise();

                c.authenticate("buh").then(
                    function(authOk) {
                        authOk.should.be.false;
                        c.badLoginsCount.should.eql(2);
                        ret.callback();
                    }
                );

                return ret;
            },
            function authenticateWrong() {
                var ret = new comb.Promise();

                c.authenticate("beh").then(
                    function(authOk) {
                        authOk.should.be.false;
                        c.badLoginsCount.should.eql(3);
                        ret.callback();
                    }
                );

                return ret;
            },
            function testForBlocked() {
                
                c.isActive.should.be.false;

            },
            function authenticate() {
                /* It won't work even with the right password as the cashier has been blocked */
                var ret = new comb.Promise();

                c.authenticate(password).then(
                    function(authOk) {
                        authOk.should.be.false;
                        c.badLoginsCount.should.eql(3);
                        ret.callback();
                    }
                );

                return ret;
            },
            function resetPassword() {
                var ret = new comb.Promise();
                
                c.generatePassword().then(
                    function(newPwd) {
                        password = newPwd;
                        c.badLoginsCount.should.eql(0);
                        
                        ret.callback();
                    }
                );

                return ret;
            },
            function authenticate() {
                /* Password has been reset. It should work */
                var ret = new comb.Promise();

                c.authenticate(password).then(
                    function(authOk) {
                        authOk.should.be.true;
                        c.badLoginsCount.should.eql(0);
                        ret.callback();
                    }                
                );

                return ret;
            }
        ]).then(
            function() {
                done();
            }
        );
    });

    it("Tests for suspend/unsuspend cashier", function(done) {
        var Cashier = patio.getModel('cashier');

        var c = new Cashier(fixtures.cashier[0]);
        var password;

        comb.serial([
            comb.hitch(c, "save"),
            function generatePassword() {
                var ret = new comb.Promise();
                c.generatePassword().then(
                    function(generatedPwd) {
                        should.exist(generatedPwd);

                        password = generatedPwd;
                        ret.callback();
                    }
                );
                return ret;
            },
            function test() {
                c.isActive.should.be.true;
                c.isBlocked.should.be.false;
                c.isSuspended.should.be.false;
            },
            function suspend() {
                return c.suspend();
            },
            function test() {
                var ret = new comb.Promise();

                c.isActive.should.be.false;
                c.isBlocked.should.be.false;
                c.isSuspended.should.be.true;

                c.authenticate(password).then(
                    function(authOk) {
                        authOk.should.be.false;

                        ret.callback();
                    }
                );

                return ret;
            },
            function unsuspend() {
                return c.unsuspend();
            },
            function test() {
                var ret = new comb.Promise();

                c.isActive.should.be.true;
                c.isBlocked.should.be.false;
                c.isSuspended.should.be.false;

                c.authenticate(password).then(
                    function(authOk) {
                        authOk.should.be.true;

                        ret.callback();
                    }
                );

                return ret;
            },
        ]).then(
            function() {
                done();
            }
        );

    });

    it("Tests for suspend/unsuspend cashier with clients associated to it", function(done) {
        this.timeout(3000);
        var Cashier = patio.getModel('cashier');
        var Client = patio.getModel('client');

        var cashier = new Cashier(fixtures.cashier[0]);
        var clients = [];

        comb.serial([
            function setup() {
                var ret = new comb.Promise();
                cashier.save().then(function() {
                    var allClientsSaved = [];
                    var i;
                    for (i=0; i<5; i++) {
                        var client = new Client(fixtures.client[0]);
                        client.username = client.username + i;
                        client.cashier = cashier;
                        clients.push(client);
                        allClientsSaved.push(client.save());
                    }
                    comb.when(allClientsSaved).then(
                        function() {
                            // one of the clients will be already suspended
                            clients[0].suspend().then(
                                comb.hitch(ret, "callback")
                            );
                        }
                    );
                });
                return ret;
            },
            function test() {
                clients[0].isSuspended.should.be.true;
                clients[1].isSuspended.should.be.false;
                clients[2].isSuspended.should.be.false;
                clients[3].isSuspended.should.be.false;
                clients[4].isSuspended.should.be.false;
            },
            function suspendCashier() {
                return comb.serial([
                    comb.hitch(cashier, "suspend"),
                    function() {
                        // reload clients to reflect changes performed by cashier.suspend()
                        return comb.when(clients.map(function(c) { return c.reload(); }));
                    }
                ]);
            },
            function test() {
                cashier.isSuspended.should.be.true;
                clients.forEach(function(c) {
                    c.isSuspended.should.be.true;
                });
            },
            function unsuspendOneOfThem() {
                return clients[1].unsuspend();
            },
            function test() {
                clients[0].isSuspended.should.be.true;
                clients[1].isSuspended.should.be.false;
                clients[2].isSuspended.should.be.true;
                clients[3].isSuspended.should.be.true;
                clients[4].isSuspended.should.be.true;
            },
            function unsuspendCashier() {
                return comb.serial([
                    comb.hitch(cashier, "unsuspend"),
                    function() {
                        return comb.when(clients.map(function(c) { return c.reload(); }));
                    }
                ]);
            },
            function test() {
                cashier.isSuspended.should.be.false;
                clients.forEach(function(c) {
                    c.isSuspended.should.be.false;
                });
            }
        ]).then(function() { done(); });
    });

    
});