var utils   = require(__dirname + '/../../../lib/utils');
var patio   = require('patio');
var comb    = require('comb');
var sinon   = require('sinon');
var fixtures = utils.loadFixtures();

var e = function(error) { 
    console.log("ERROR : " + error);
    true.should.be.false;
};

describe("Settings", function() {
    var models;
    
    
    beforeEach(function(done) {
        var db = utils.getDbConnection();  // force db connection
        
        comb.serial([
            function() {
                return utils.cleanDb();
            },
            function() {
                var ret = new comb.Promise();
                var r = utils.getSyncedModels().then(
                    function(m) {
                        models = m;
                        ret.callback();
                    },
                    function(error) {
                        e(error);
                    }
                );
                return ret;
            }
        ]).then(
            function() {
                done();
            },
            function(error) {
                e(error);
                done();
            }
        );             
    });

    describe("Settings get & set", function() {
        
        it("Has a basic behaviour", function(done) {
            var sampleKey = "myKey";
            var sampleValue = "myValue";
            var newValue = "newValue";

            comb.serial([
                function() {
                    var ret = new comb.Promise();
                    models.Setting.setValue(sampleKey, sampleValue).then(
                        function() {
                            models.Setting.db.transaction(function() {
                                var txRet = new comb.Promise();
                                models.Setting.getValue(sampleKey, true).then( // forUpdate
                                    function(value) {
                                        //console.log("Testing");
                                        value.should.eql("myValue");
                                        
                                        models.Setting.setValue(sampleKey, newValue, true).then(
                                            function() {
                                                txRet.callback();
                                            }
                                        );

                                    }
                                );
                                return txRet;
                            }).then(
                                function() {
                                    models.Setting.getValue(sampleKey).then(
                                        function(value) {
                                            //console.log("Testing an update");
                                            value.should.eql("newValue");
                                            ret.callback();
                                        }
                                    );
                                }
                            );
                        },
                        function(error) { e(error);}
                    );

                    return ret;
                },
                function() {
                    return models.Setting.getAll().then(
                        function(settings) {
                            // console.log("Testing getAll");
                            settings["myKey"].should.eql("newValue");

                            done();
                        }
                    );
                }
            ]);

        });
    });     
});
