// TODO: must be a better way to require utils without knowing the WHOLE path
var utils   = require(__dirname + '/../../../lib/utils');
var patio   = require('patio');
var comb    = require('comb');
var __      = require('underscore');
var fixtures = utils.loadFixtures();
var should  = require('should');
var sinon = require("sinon");

describe("Some bingo attributes", function() {
    var models;
    var Bingo;
    var Card;
    var sampleBingo;
    var sampleCard;
    
    beforeEach(function(done) {
        utils.cleanDb().then(function() { done(); });
    });
    
    beforeEach(function(done) {
        var db = utils.getDbConnection();  // force db connection
        
        // NOTE: this beforeEach (and the previous afterEach) gets executed
        // with every IT clause.
        
         utils.getSyncedModels().then(
             function(m) {
                 models = m;
                 Bingo = patio.getModel('bingo');
                 Card = patio.getModel('card');
                 
                 sampleBingo = new Bingo(fixtures.card[0]);
                 sampleCard = new Card(fixtures.card[0]);
                 
                 comb.serial([                     
                     comb.hitch(sampleBingo, "save"),
                     comb.hitch(sampleCard, "save")
                     
                 ]).then(
                     function() { 
                         sampleBingo.startDelay = 0;
                         done();
                     });
                 
             },
             function(error) {
                 console.log("ERROR when syncing: " + error);
             }
         );
    });
    
    afterEach(function(done) {
        comb.serial([
            function() {
                if (sampleBingo.isRunning) {
                    return sampleBingo.pause();
                }
                return new comb.Promise().callback();
            },
            function() {
                    done();
            },
            
        ]);
        //done();
    });
    
    
    describe("Some Bingo attributes", function() {
        it("Accepts remainingNumbers and calledNumbers as empty arrays when creating", function(done) {
            var bingoData = fixtures.bingo[0];
            bingoData.remainingNumbers = [];
            bingoData.calledNumbers = [];

            var bingo = new Bingo(bingoData);
            bingo.save().then(
                function(bingo) {
                    bingo.remainingNumbers.should.not.eql([]);
                    bingo.calledNumbers.should.eql([]);
                    done();
                }
            );
        });

        it("Has remainingNumbers and calledNumbers optional arguments when creating", function(done) {
            var bingoData = fixtures.bingo[0];
            delete bingoData.remainingNumbers;
            delete bingoData.calledNumbers;

            var bingo = new Bingo(bingoData);
            bingo.save().then(
                function(bingo) {
                    bingo.remainingNumbers.should.not.eql([]);
                    bingo.calledNumbers.should.eql([]);
                    done();
                }
            );
        });

        it("Accepts remainingNumbers as array with empty content when creating", function(done) {
            var bingoData = fixtures.bingo[0];
            bingoData.remainingNumbers = ["", 0];

            var bingo = new Bingo(bingoData);
            bingo.save().then(
                function(bingo) {
                    bingo.remainingNumbers.should.not.eql([]);
                    bingo.calledNumbers.should.eql([]);
                    done();
                }
            );
        });

        it("Accepts defined remainingNumbers and calledNumbers when creating", function(done) {
            var bingoData = fixtures.bingo[0];
            bingoData.remainingNumbers = [1,2,3,4,5];
            bingoData.calledNumbers = [6,7,8,9,10];

            var bingo = new Bingo(bingoData);
            bingo.save().then(
                function(bingo) {
                    bingo.remainingNumbers.should.eql([1,2,3,4,5]);
                    bingo.calledNumbers.should.eql([6,7,8,9,10]);
                    done();
                }
            );
        });

        
        it("Should know if has to be started", function(done) {
            var startsInThePast = new Date();
            startsInThePast.setMinutes((new Date()).getMinutes() - 2);
            sampleBingo.starts = startsInThePast;
            sampleBingo.update().then(
                function() {
                    sampleBingo.canStart.should.be.true;
                    
                    var startsInTheFuture = new Date();
                    startsInTheFuture.setMinutes((new Date()).getMinutes() + 2);
                    
                    sampleBingo.starts = startsInTheFuture;
                    sampleBingo.update().then(
                        function () {
                            sampleBingo.canStart.should.be.false;
                            
                            // Tell mocha we're done here.
                            done();
                        }
                    );
                },
                done // will be called back with the error as argument
            );
        });
        
    });
    
    describe("Bingo states", function() {
        it("Goes through several states", function(done) {
            this.timeout(6000);
            sampleBingo.isActive.should.be.false;
            sampleBingo.isSelling.should.be.false;
            sampleBingo.isRunning.should.be.false;
            sampleBingo.isPaused.should.be.false;
            sampleBingo.isFinished.should.be.false;
            sampleBingo.canStart.should.be.true;

            var sellingStartedEventCount = 0;
            var sellingFinishedEventCount = 0;
          
            sampleBingo.addListener('sellingStarted', function() {
                    sellingStartedEventCount++;
            });

            // This will run when selling has finished and the bingo is in running state
            sampleBingo.addListener('bingoStarted', function() {
                // Remove us from listeners queue. We only want us to be run once.
                sampleBingo.removeAllListeners('bingoStarted');

                sampleBingo.isActive.should.be.true;
                sampleBingo.isSelling.should.be.false;
                sampleBingo.isRunning.should.be.true;
                sampleBingo.isPaused.should.be.false;
                sampleBingo.isFinished.should.be.false;
                sampleBingo.canStart.should.be.false;
                sampleBingo.sellingFinishesAt.should.not.exist;

                comb.serial([
                    function() {
                        var ret = new comb.Promise();
                        // Pause it
                        sampleBingo.pause().then(function () { sampleBingo.reload().then(function () {                            
                            sampleBingo.isActive.should.be.true;
                            sampleBingo.isSelling.should.be.false;
                            sampleBingo.isRunning.should.be.false;
                            sampleBingo.isPaused.should.be.true;
                            sampleBingo.isFinished.should.be.false;
                            sampleBingo.canStart.should.be.true;

                            ret.callback();
                            });
                        });

                        return ret;
                    },
                    function() {
                        var ret = new comb.Promise();
                        // Start it again
                        sampleBingo.start().then(
                            function () { 
                                sampleBingo.reload().then(function () {
                                    sampleBingo.isActive.should.be.true;
                                    sampleBingo.isSelling.should.be.false;
                                    sampleBingo.isRunning.should.be.true;
                                    sampleBingo.isPaused.should.be.false;
                                    sampleBingo.isFinished.should.be.false;
                                    sampleBingo.canStart.should.be.false;

                                    ret.callback();
                                }); 
                            },
                            function(error) {
                                console.log("ERROR when starting: " + error);
                            }
                        );

                        return ret;

                    },
                    function() {
                        var ret = new comb.Promise();
                        console.log("Faking bingo payout");
                        var clientCard = sinon.stub(new models.ClientCard());
                        clientCard.id = 1;
                        clientCard.isBingoWinner.returns([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]);
                        clientCard.payBingo = function() {
                            return (new comb.Promise()).callback();
                        };
                        
                        sampleBingo.payBingos([clientCard]).then( // Needed cause emits bingo notification
                            function() {
                                sampleBingo._finish().then(function() {
                                    ret.callback();
                                });
                            }
                        );
                        
                        return ret;
                    }
                    
                ]).then(
                    function() {},
                    function(error) {
                        console.log("ERROR: " + error);
                        false.should.be.true;
                        done();
                    }
                );

            }); // END of bingoStarted listener

            sampleBingo.addListener('bingo', function () {
                // Give Bingo.payBingos() a few milliseconds to get its job done.
                // (among other things, it calls Bingo._finish()).
                setTimeout(function() {
                    console.log("Bingo has been called. Testing...");
                    sampleBingo.isActive.should.be.false;
                    sampleBingo.isSelling.should.be.false;
                    sampleBingo.isRunning.should.be.false;
                    sampleBingo.isPaused.should.be.false;
                    sampleBingo.isFinished.should.be.true;
                    sampleBingo.canStart.should.be.false;

                    done();

                }, 500);
            }); // END of bingo listener



            // MAIN

            comb.serial([
                function() {
                    sampleBingo.sellingInterval = 2;
                    // Require 1 of each to start bingo
                    sampleBingo.minTotalClientsCount = 1;
                    sampleBingo.minClientsCount = 1;
                    return sampleBingo.save();
                },
                function() {
                    var ret = new comb.Promise();
                    sampleBingo.start().then(
                        function() {
                            // sampleBingo is now selling cards without a timeout, yet
                            sampleBingo.isActive.should.be.true;
                            sampleBingo.isSelling.should.be.true;
                            sampleBingo.isRunning.should.be.false;
                            sampleBingo.isPaused.should.be.false;
                            sampleBingo.isFinished.should.be.false;
                            sampleBingo.canStart.should.be.false;

                            should.not.exist(sampleBingo.sellingFinishesAt);

                            ret.callback();
                        }
                    );

                    return ret;
                },
                function() {
                    var ret = new comb.Promise();
                    sellingStartedEventCount.should.eql(1); // emitted by the previous start()

                    // Fake selling criteria
                    sampleBingo.registerNewClient();
                    sampleBingo.setTotalClientsCount(1).then(function() {

                        // sampleBingo is now selling cards with selling timeout
                        sampleBingo.isActive.should.be.true;
                        sampleBingo.isSelling.should.be.true;
                        sampleBingo.isRunning.should.be.false;
                        sampleBingo.isPaused.should.be.false;
                        sampleBingo.isFinished.should.be.false;
                        sampleBingo.canStart.should.be.false;

                        
                        sampleBingo.sellingFinishesAt.should.be.within((new Date()).getTime() - 250, (new Date()).getTime() + 2250);
                        
                        ret.callback();

                    });

                    return ret;
                },
                function() {
                    sellingStartedEventCount.should.eql(2);
                    
                    var ret = new comb.Promise();
                    sampleBingo.pause().then(function() {
                        sampleBingo.reload().then(function() {
                            // pausing it should return it to the previous state
                            sampleBingo.isActive.should.be.false;
                            sampleBingo.isSelling.should.be.false;
                            sampleBingo.isRunning.should.be.false;
                            sampleBingo.isPaused.should.be.false;
                            sampleBingo.isFinished.should.be.false;
                            sampleBingo.canStart.should.be.true;

                            should.not.exist(sampleBingo.sellingFinishesAt);

                            ret.callback();
                        });
                    });
                    return ret;
                },
                function() {
                    sellingStartedEventCount.should.eql(2);

                    // starting it now should be like starting all over again
                    var ret = new comb.Promise();
                    sampleBingo.start().then(function() {
                        // sampleBingo is now selling cards
                        sampleBingo.isActive.should.be.true;
                        sampleBingo.isSelling.should.be.true;
                        sampleBingo.isRunning.should.be.false;
                        sampleBingo.isPaused.should.be.false;
                        sampleBingo.isFinished.should.be.false;
                        sampleBingo.canStart.should.be.false;

                        // NOTE: at the moment we do not expect starting conditions to be met before the bingo has s
                        // Selling criteria has already been met, so the timeout should be present
                        sampleBingo.sellingFinishesAt.should.be.within((new Date()).getTime() - 250, (new Date()).getTime() + 2250);

                        // Previous start() emits a single sellingStarted event
                        sellingStartedEventCount.should.eql(3);

                        ret.callback();
                    });
                    
                    return ret;
                }
            ]).then(
                function() {},
                function(error) {
                    console.log(error);
                    true.should.be.false;
                    done();
                }
            );
        });
        
        it.skip("Can be paused and resumed", function(done) {
            sampleBingo.start().then(
                function() {
                    sampleBingo.isRunning.should.be.true;
                    sampleBingo.isPaused.should.be.false;
                    sampleBingo.canStart.should.be.false;
                    
                    sampleBingo.pause().then(
                        function() {
                            sampleBingo.isRunning.should.be.false;
                            sampleBingo.isPaused.should.be.true;
                            sampleBingo.canStart.should.be.true;
                            

                            sampleBingo.start().then( 
                                function() { 
                                    sampleBingo.isRunning.should.be.true;
                                    sampleBingo.isPaused.should.be.false;
                                    sampleBingo.canStart.should.be.false;
                                    
                                    // Bingo shouldnt be startable again
                                    sampleBingo.start().then(
                                        function () {},
                                        function (error) {
                                            error.should.be.an.instanceof(Error);
                                            
                                            done();
                                        }
                                    );
                                }
                            );
                        }
                    );
                }
            );
        });
    });

    describe("Volatile creation", function() {
        it("Can be created without being saved. Will be saved on first purchase.", function(done) {
            var volatileBingo = new Bingo(fixtures.bingo[0]);
            volatileBingo.currentState = 0;

            volatileBingo.start().then(
                function() {
                    volatileBingo.isNew.should.be.true;
                    should.not.exist(volatileBingo.id);

                    volatileBingo.persist().then(function() {
                        volatileBingo.buyClientCards(1, false).then(
                            function(clientCards) {
                                clientCards.length.should.eql(1);
                                volatileBingo.isNew.should.be.false;
                                should.exist(volatileBingo.id);

                                done();
                            },
                            function(error) {
                                console.log("ERROR: " + error);
                            }
                        );
                    });
                }
            );
        });
    });
       
    describe("Numbers generation", function() {
        it("Can store and retrieve remaining and called numbers", function(done) {
            sampleBingo.calledNumbers.should.eql([]);
            sampleBingo.remainingNumbers.should.not.eql([]);
               
                        
            sampleBingo.remainingNumbers = [0,2,4,6,8];
            sampleBingo.calledNumbers = [1,3,5,7,9];
            sampleBingo.save().then(
                function () {
                    // Re fetch it from database and make sure we are instantiating it again.
                    // The latter is important as Bingo class has some tweaks in its constructor.
                    Bingo.filter({id: sampleBingo.id}).one().then(
                        function (anotherBingo) {
                            anotherBingo.remainingNumbers.should.eql([0,2,4,6,8]);
                            anotherBingo.calledNumbers.should.eql([1,3,5,7,9]);
                            done();
                        }
                    );
                }
            );
        });
        
        it("Generates numbers", function(done) {
            this.timeout(2000);
            var allNumbers = sampleBingo.remainingNumbers;
            sampleBingo.lapseBetweenNumbers = 0.3;
            sampleBingo.currentState = 30; // PAUSED
            var emittedNumbers = [];
            
            sampleBingo.addListener('numberCalled', function(number) {
                emittedNumbers.push(number);
            });
            
            sampleBingo.start().then(
                function() {
                    // Set a timeout to pause it in 1 second
                    setTimeout(function() {
                        sampleBingo.pause().then(
                            function() {
                                // Some numbers must have been called
                                sampleBingo.calledNumbers.length.should.be.above(0);
                                
                                // The union of called and remaining numbers must be equal to
                                // the original remaining numbers.
                                var resultNumbers = __.union(sampleBingo.calledNumbers, sampleBingo.remainingNumbers);
                                resultNumbers.should.eql(allNumbers);
                                
                                // Numbers received on our event listener must be also the same
                                emittedNumbers.should.eql(sampleBingo.calledNumbers);
                                
                                done();
                            }
                        );
                    }, 1000);
                }
            );
        });
        
    });

});
