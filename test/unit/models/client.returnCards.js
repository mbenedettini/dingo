// TODO: must be a better way to require utils without knowing the WHOLE path
var utils = require(__dirname + '/../../../lib/utils');
var patio = require('patio');
var comb = require('comb');
var should = require('should');
var fixtures = utils.loadFixtures();

var e = function(error) { 
    console.log("ERROR: " + error); 
    true.should.be.false;
};


describe("client.returnCards", function() {
    var models;
    var db;
        
    beforeEach(function(done) {
        this.timeout(10000);
        db = utils.getDbConnection();  // force db connection

        utils.getSyncedModels().then(
                function(m) {
                    models = m;
                    
                    /* Create a few sample things */
                    var c = new models.Client(fixtures.client[0]);
                    var b = new models.Bingo(fixtures.bingo[0]);
                    b.currentState = 10; // SELLING
                    
                    comb.serial([
                        comb.hitch(utils, "cleanDb"),
                        function() {
                            return models.Setting.save(fixtures.setting);
                        },
                        comb.hitch(c, "save"),
                        comb.hitch(b, "save"),
                        comb.hitch(b, "reload"),
                        function() {
                            var cards = [];
                            for (var i=1; i<=32; i++) {
                                var c = new models.Card(fixtures.card[0]);
                                c.number = i;
                                cards.push(c);
                            }
                            return models.Card.save(cards);
                        },
                        comb.hitch(c, "makeDeposit", 99.00, "Sample deposit"),
                        // Buy 12 single cards, 1 series (6 cards), two more single cards and another series
                        comb.hitch(c, "buyCards", b, 12, false),
                        comb.hitch(c, "buyCards", b, 1, true),
                        comb.hitch(c, "buyCards", b, 2, false),
                        comb.hitch(c, "buyCards", b, 1, true)
                    ]).then(
                        function() {
                            done(); 
                        },
                        function(error) {
                            console.log("ERROR on main serial: " + error);
                            done();
                        }
                    );
                },
                function(error) {
                    console.log("ERROR when syncing: " + error);
                    done();
                }
            );
    });
    
    describe("Cards return", function() {  
        it("Returns some cards", function(done) {
            this.timeout(4000);
            var sampleClient;
            var sampleCard;
            var sampleBingo;
            
            comb.serial([
                function() {
                    return models.Client.one().then(
                        function(client) {
                            sampleClient = client;
                        }
                    );
                },
                function() {
                    return models.Bingo.one().then(
                        function(bingo) {
                            sampleBingo = bingo;
                            }
                    );
                },
            ]).then(
                function() {
                    sampleClient.should.be.ok;

                    sampleClient.returnCards(sampleBingo, 2).then(
                        function(clientCards) {
                            clientCards.length.should.eql(2);
                            clientCards[0].number.should.eql(20);
                            clientCards[1].number.should.eql(19);

                            clientCards.forEach(function(c) {
                                c.isSeries.should.be.false;
                            });

                            // initial balance: 99. Bought 26 cards at  1.00 each. Returns 2.
                            sampleClient.balance.should.eql = 99 - 26 + 2;// 99-26+2

                            sampleClient.returnCards(sampleBingo, 2).then(
                                function(clientCards) {
                                    clientCards.length.should.eql(2);
                                    clientCards[0].number.should.eql(12);
                                    clientCards[1].number.should.eql(11);
                                    
                                    clientCards.forEach(function(c) {
                                        c.isSeries.should.be.false;
                                    });


                                    sampleClient.balance.should.eql = 99 - 26 + 2 + 2;

                                    done();
                                },
                                function(error) {
                                    e(error);
                                    done();
                                }
                            );
                        },
                        function(error) {
                            e(error);
                            done();
                        }
                    );
                },
                function(error) {
                    e(error);
                    done();
                }
            );
        });

        it("Returns series", function(done) {
            this.timeout(8000);
            var sampleClient;
            var sampleCard;
            var sampleBingo;
            
            comb.serial([
                function() {
                    return models.Client.one().then(
                        function(client) {
                            sampleClient = client;
                        }
                    );
                },
                function() {
                    return models.Bingo.one().then(
                        function(bingo) {
                            sampleBingo = bingo;
                            }
                    );
                },
            ]).then(
                function() {
                    sampleClient.should.be.ok;

                    sampleClient.returnCards(sampleBingo, 1, true).then(
                        function(clientCards) {
                            clientCards.length.should.eql(6);
                            clientCards[0].number.should.eql(30);
                            clientCards[1].number.should.eql(29);
                            clientCards[2].number.should.eql(28);
                            clientCards[3].number.should.eql(27);
                            clientCards[4].number.should.eql(26);
                            clientCards[5].number.should.eql(25);

                            clientCards.forEach(function(c) {
                                c.isSeries.should.be.true;
                            });

                            // initial balance: 99. Bought 26 cards at  1.00 each. Returns 6.
                            sampleClient.balance.should.eql = 99 - 26 + 6;

                            sampleClient.returnCards(sampleBingo, 1, true).then(
                                function(clientCards) {
                                    clientCards.length.should.eql(6);
                                    clientCards[0].number.should.eql(18);
                                    clientCards[1].number.should.eql(17);
                                    clientCards[2].number.should.eql(16);
                                    clientCards[3].number.should.eql(15);
                                    clientCards[4].number.should.eql(14);
                                    clientCards[5].number.should.eql(13);

                                    clientCards.forEach(function(c) {
                                        c.isSeries.should.be.true;
                                    });

                                    sampleClient.balance.should.eql = 99 - 26 + 6 + 6;

                                    done();
                                },
                                function(error) {
                                    e(error);
                                    done();
                                }
                            );
                        },
                        function(error) {
                            e(error);
                            done();
                        }
                    );
                },
                function(error) {
                    e(error);
                    done();
                }
            );
        });

        it("Returns two series at the same time", function(done) {
            this.timeout(8000);
            var sampleClient;
            var sampleCard;
            var sampleBingo;
            
            comb.serial([
                function() {
                    return models.Client.one().then(
                        function(client) {
                            sampleClient = client;
                        }
                    );
                },
                function() {
                    return models.Bingo.one().then(
                        function(bingo) {
                            sampleBingo = bingo;
                            }
                    );
                },
            ]).then(
                function() {
                    sampleClient.should.be.ok;

                    sampleClient.returnCards(sampleBingo, 2, true).then(
                        function(clientCards) {
                            clientCards.length.should.eql(12);
                            clientCards[0].number.should.eql(30);
                            clientCards[1].number.should.eql(29);
                            clientCards[2].number.should.eql(28);
                            clientCards[3].number.should.eql(27);
                            clientCards[4].number.should.eql(26);
                            clientCards[5].number.should.eql(25);

                            clientCards[6].number.should.eql(18);
                            clientCards[7].number.should.eql(17);
                            clientCards[8].number.should.eql(16);
                            clientCards[9].number.should.eql(15);
                            clientCards[10].number.should.eql(14);
                            clientCards[11].number.should.eql(13);


                            clientCards.forEach(function(c) {
                                c.isSeries.should.be.true;
                            });

                            sampleClient.balance.should.eql = 99 - 26 + 6 + 6;

                            done();
                        },
                        function(error) {
                            e(error);
                            done();
                        }
                    );
                },
                function(error) {
                    e(error);
                    done();
                }
            );
        });

    });
});
