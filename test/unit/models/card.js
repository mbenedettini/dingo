// TODO: must be a better way to require utils without knowing the WHOLE path
var utils   = require(__dirname + '/../../../lib/utils');
var patio   = require('patio');
var comb    = require('comb');
var __      = require('underscore');
var fixtures = utils.loadFixtures();

describe("Some card attributes", function() {
    var Card;
    var sampleCard;
    
    beforeEach(function(done) {
        utils.cleanDb().then(function() { done() });
    });
    
    beforeEach(function(done) {
        var db = utils.getDbConnection();  // force db connection
        
        // NOTE: this beforeEach (and the previous afterEach) gets executed
        // with every IT clause.
        
        // TODO: yes, we need a better way of importing and syncing models
        utils.getSyncedModels().then(
                function() {
                    Card = patio.getModel('card');
                    
                    patio.syncModels().then(
                        /* Create a sample bingo */
                        function() {
                            sampleCard = new Card(fixtures.card[0]);
                            sampleCard.save().then(function() { done(); });
                        },
                        function(error) {
                            console.log("ERROR when syncing: " + error);
                        }
                    );
                },
                function(error) {
                    console.log("ERROR when importing : " + error);
                }
            );
    });
    
    
    describe("Some Card attributes", function() {
        
        it("Must have lines getters and setters", function(done) {
            sampleCard.firstLineNumbers.should.eql([1,2,3,4,5]);
            sampleCard.secondLineNumbers.should.eql([6,7,8,9,10]);
            sampleCard.thirdLineNumbers.should.eql([11,12,13,14,15]);

            sampleCard.firstLine.should.eql([-1,1,-1,-1,2,3,-1,4,5]);
            sampleCard.secondLine.should.eql([-1,6,-1,-1,7,8,-1,9,10]);
            sampleCard.thirdLine.should.eql([11,-1,-1,12,13,-1,14,15,-1]);
            
            done();
        });
        
    });
});
