var utils   = require(__dirname + '/../../../lib/utils');
var patio   = require('patio');
var comb    = require('comb');
var sinon   = require('sinon');
var fixtures = utils.loadFixtures();

/*
 * THIS IS A NON-WORKING ATTEMPT OF A UNIT TEST FOR ClientCard.cancel();
 * */

describe("client_card.payout", function() {
    var models;
    var instances = {};
    
    beforeEach(function(done) {
        var db = utils.getDbConnection();  // force db connection
        
        comb.serial([
            comb.hitch(utils, "cleanDb"),
            function() {
                return utils.getSyncedModels().then(
                    function(m) {
                        models = m;
                    }
                );               
            },
            function create() {
                var bingo = new models.Bingo(fixtures.bingo[0]);
                bingo.linePrize = 567.89;
                bingo.bingoPrize = 123.45;
                var clientCard = new models.ClientCard(fixtures.client_card[0]);
                var card = new models.Card(fixtures.card[0]);
                var client = new models.Client(fixtures.client[0]);

                return comb.serial([                    
                    function() {
                        return models.Setting.save(fixtures.setting).then(function(settings) {
                            instances.settings = settings;
                        });
                    },
                    comb.hitch(bingo, "save"),
                    comb.hitch(card, "save"),
                    comb.hitch(client, "save"),
                    function() {
                        clientCard.bingo = bingo;
                        clientCard.client = client;
                        clientCard.card = card;
                        return clientCard.save();
                    },
                    function() {
                        instances.bingo = bingo;
                        instances.clientCard = clientCard;
                        instances.card = card;
                        instances.client = client;
                    }
                ]);

            },
            done
        ]);             
    });

    describe("Cancellation", function() {
        
        it("Cancels and refunds money", function(done) {

            var oldLinePrize = instances.bingo.linePrize;
            var oldBingoPrize = instances.bingo.bingoPrize;
            var accumulatedPrizeSetting = instances.settings.filter(function(s) { return s.key == 'accumulatedPrize'; }).shift();
            var oldAccumulatedPrize = accumulatedPrizeSetting.value;
            instances.client.balance.should.eql(0);

            instances.clientCard.cancel().then(
                function() {
                    console.log("Cancel done!");
                    instances.client.reload().then(function(client) {
                        client.balance.should.eql(instances.bingo.cardPrice);
                        client.transactions.then(function(txs) {
                            // ClientCard creation and refund
                            txs.length.should.eql(2);
                            txs[0].type.should.eql(6); // CARD_REFUND
                            txs[0].description.should.eql("Devolucion por carton " + instances.card.number);

                            instances.clientCard.reload().then(function(clientCard) {
                                clientCard.cancelled.should.be.true;
                                
                                instances.bingo.reload().then(function() {
                                    console.log("linePrize was " + oldLinePrize + " and now is " + instances.bingo.linePrize);
                                    instances.bingo.linePrize.should.be.below(oldLinePrize);
                                    instances.bingo.linePrize.should.eql(oldLinePrize - instances.bingo.cardPrice * instances.bingo.linePrizeRate);

                                    console.log("bingoPrize was " + oldBingoPrize + " and now is " + instances.bingo.bingoPrize);
                                    instances.bingo.bingoPrize.should.be.below(oldBingoPrize);
                                    instances.bingo.bingoPrize.should.eql(oldBingoPrize - instances.bingo.cardPrice * instances.bingo.bingoPrizeRate);

                                    console.log("accumulatedPrize was " + oldAccumulatedPrize + " and now is " + instances.bingo._accumulatedPrize);
                                    instances.bingo._accumulatedPrize.should.be.below(oldAccumulatedPrize);
                                    instances.bingo._accumulatedPrize.should.eql(oldAccumulatedPrize - instances.bingo.cardPrice * instances.bingo._accumulatedRatio);

                                    done();
                                });

                            });
                        });
                    });
                },
                function(error) {
                    console.log("Cancel ERROR: " + error);
                    true.should.be.false;
                    done();
                }
            );
        });
    });     
});