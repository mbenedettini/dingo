
var utils = require(__dirname + '/../../../lib/utils');
var patio = require('patio');
var comb = require('comb');
var should = require('should');
var fixtures = utils.loadFixtures();

describe("A client creation", function() {
    var Client;
    var Transaction;
    var sampleClient;
    var Cashier;
    
    before(function(done) {
        // Cleanup tables
        utils.cleanDb().then(function() { done(); });
    });

    beforeEach(function(done) {
        comb.serial([
            comb.hitch(Client, "remove"),
            comb.hitch(Cashier, "remove"),
            function() {
                /* Create a sample client */
                sampleClient = new Client(fixtures.client[0]);
                sampleClient.save().then(
                    function() { done(); },
                    function(error) {
                        console.log("ERROR when saving : " + error);
                        done();
                    }
                );
            },
        ]);
    });
    
    before(function(done) {
        var db = utils.getDbConnection();  // force db connection
        
        // TODO: yes, we need a better way of importing and syncing models
        utils.getSyncedModels().then(
                function() {
                    Client = patio.getModel('client');
                    Transaction = patio.getModel('transaction');
                    Cashier = patio.getModel('cashier');
                    
                    patio.syncModels().then(
                        function() {
                            done();
                        },
                        function(error) {
                            console.log("ERROR when syncing: " + error);
                        }
                    );
                },
                function(error) {
                    console.log("ERROR when importing : " + error);
                }
            );
    });
    
    describe("Client operations", function() {
        
        it("Creates a client", function(done) {
            var myClient = new Client( {'name': 'MeGusta', 'lastName': 'LaFafafa', 'username':'user2', 'password':'pass', 'email': 'narco@socialista.com'} );

            myClient.save().then(
                function() {
                    myClient.name.should.equal('MeGusta');
                    myClient.lastName.should.equal('LaFafafa');
                    myClient.username.should.equal('user2');
                    // password should be alway empty
                    myClient.password.should.not.equal('');
                    // Tell mocha we're done here.
                    done();
                },
                function(error) {
                    console.log("ERROR when creating user2 : " + error);
                    done();
                }
            );
        });
                
        it("Finds a previously created client", function(done) {
            Client.filter({id : sampleClient.id}).one().then(
                function(client) {
                    
                    client.id.should.equal(sampleClient.id);
                    client.name.should.equal('John');
                    client.username.should.equal('jdoe');
                    // password should be always empty
                    client.password.should.not.equal('');
                    
                    done();
                },
                done
            );
        });
        
        it("Finds all clients", function(done) {
            Client.all().then(
                function(clients) {
                    clients.length.should.equal(1);
                    done();
                },
                done
            );
        });
        
        it("Makes deposits", function(done) {
            sampleClient.makeDeposit(4.97, 'First deposit').then( function() {
                sampleClient.balance.should.equal(4.97);
                
                sampleClient.makeDeposit(5.02, 'Second deposit').then( function() {
                    sampleClient.balance.should.equal(9.99);
                    done();
                });
            }, function(error) {
                    console.log("ERROR when Makes deposits : " + error);
                    done();
            });
        });

        it("Makes deposits with cashier", function(done) {
            comb.serial([
                function setup() {
                    var ret = new comb.Promise();
                    var sampleCashier = new Cashier(fixtures.cashier[0]);
                    sampleCashier.save().then(
                        function() {
                            sampleClient.cashierId = sampleCashier.id;
                            sampleClient.save().then(
                                comb.hitch(ret, "callback")
                            );
                        }
                    );
                    return ret;
                },
                function testWrongCashierId() {
                    var ret = new comb.Promise();
                    sampleClient.makeDeposit(4.97, 'First deposit', 33333).then( 
                        function() {
                            console.log("BUH");
                        },
                        function(error) {
                            error.should.eql(new Error("El cajero no corresponde"));
                            sampleClient.balance.should.eql(0);
                            ret.callback();
                        }
                    );
                    return ret;
                },
                function testNoCashierId() {
                    /* It is allowed to make a deposit to a cashier-related client without
                     * providing a cashierId as it is the case of an admin making a deposit.
                     */
                    var ret = new comb.Promise();
                    sampleClient.makeDeposit(4.97, 'First deposit').then( 
                        function() {
                            sampleClient.balance.should.eql(4.97);
                            ret.callback();
                        },
                        function(error) {
                        }
                    );
                    return ret;
                },
                function testRightCashierId() {
                    var ret = new comb.Promise();
                    var cashierId = sampleClient.cashierId;

                    sampleClient.makeDeposit(7.73, 'Example Deposit', cashierId).then( 
                        function() {
                            sampleClient.balance.should.eql(4.97 + 7.73);
                            ret.callback();
                        },
                        function(error) {
                        }
                    );

                    return ret;
                }
            ]).then(
                function() {
                    done();
                }
            );
        });
                
        
        it("Makes withdrawals", function(done) {
            sampleClient.makeWithdrawal(6.66, 'First withdrawal').then(function() {
                sampleClient.balance.should.equal(-6.66);
                
                sampleClient.makeWithdrawal(3.32, 'Second withdrawal').then(function() {
                    sampleClient.balance.should.equal(-9.98);
                    done();
                });
            });
        });

        it("Makes withdrawals with cashier", function(done) {
            comb.serial([
                function setup() {
                    var ret = new comb.Promise();
                    var sampleCashier = new Cashier(fixtures.cashier[0]);
                    sampleCashier.save().then(
                        function() {
                            sampleClient.cashierId = sampleCashier.id;
                            sampleClient.save().then(
                                comb.hitch(ret, "callback")
                            );
                        }
                    );
                    return ret;
                },
                function testWrongCashierId() {
                    var ret = new comb.Promise();
                    sampleClient.makeWithdrawal(4.97, 'Example withdrawal', 33333).then( 
                        function() {
                            console.log("SUCCESS 0");
                        },
                        function(error) {
                            error.should.eql(new Error("El cajero no corresponde"));
                            sampleClient.balance.should.eql(0);
                            ret.callback();
                        }
                    );
                    return ret;
                },
                function testNoCashierId() {
                    var ret = new comb.Promise();
                    sampleClient.makeWithdrawal(4.97, 'Example withdrawal').then( 
                        function() {
                            sampleClient.balance.should.eql(-4.97);
                            ret.callback();
                        },
                        function(error) {
                        }
                    );
                    return ret;
                },
                function testRightCashierId() {
                    var ret = new comb.Promise();
                    var cashierId = sampleClient.cashierId;

                    sampleClient.makeWithdrawal(4.97, 'Example withdrawal', cashierId).then( 
                        function() {
                            // console.log("SUCCESS 2");
                            sampleClient.balance.should.eql(- 4.97 - 4.97);
                            ret.callback();
                        },
                        function(error) {
                        }
                    );

                    return ret;
                }
            ]).then(
                function() {
                    done();
                }
            );
        });

        
        it("Authenticate a client", function(done) {
            Client.filter({id : sampleClient.id}).one().then(
                function(client) {
                    console.log(client.password);
                    Client.authenticate(client.username, 'lalala').then(
                        function(client_back) {
                            client.id.should.equal(client_back.id);
                            done();
                        },
                        function(client_back){
                            // this must fail !
                            true.should.be.false
                            done();
                        }
                    );
                },
                done
            );
        });
        it("Authentication fail", function(done) {
            Client.filter({id : sampleClient.id}).one().then(
                function(client) {
                    Client.authenticate(client.username,'pass_mala').then(
                        function(){
                            // this must fail !
                            true.should.be.false;
                            done();
                        },
                        function(err){
                            err.should.exist;
                            done();
                        });
                },
                done
            );
        });

        it("Update password", function(done) {
            Client.filter({id : sampleClient.id}).one().then(
                function(client) {
                    console.log( "Cambiando password a " + client.username);
                    client.changePassword('lalala','pass_nueva').then( 
                        function () {
                            Client.authenticate(client.username,'pass_nueva').then(
                                function(client_back){
                                    client.id.should.equal(client_back.id);
                                    done();
                                },
                                function(err){
                                    // this must fail !
                                    true.should.be.false;
                                    done();
                                }
                            );
                        }, 
                        function(err){
                            // this must fail !
                            console.log("changePassword FAILED");
                            true.should.be.false;
                            done();
                        }
                    );
                }, 
                done           
            );
        });

        it("Update password fail", function(done) {
            Client.filter({id : sampleClient.id}).one().then(
                function(client) {
                    client.changePassword('pass','pass_nueva').then( 
                        function(){
                            // this must fail !
                            true.should.be.false
                            done();
                        },
                        function(err){
                            err.should.exist;
                            
                            Client.authenticate(client.username,'pass_nueva').then(
                                function(client_back){
                                    // this must fail !
                                    true.should.be.false;
                                    done();
                                }, 
                                function(err){
                                    err.should.exist;
                                    true.should.be.true;
                                    done();
                                }
                            );
                        }
                    );
                }, 
                done
            );
        });

        it("Update User, but not password ", function(done) {
            Client.filter({id : sampleClient.id}).one().then(
                function(client) {
                    client.email = "test@zaraza.com";
                    var old_password = client.password;
                    
                    client.save().then( 
                        function () {
                            // client email should be changed
                            client.email.should.equal("test@zaraza.com");
                            // password should be unchanged
                            client.password.should.equal(old_password);
                            done();
                        },
                        function(err){
                            // this must fail !
                            true.should.be.false
                            done();
                        }
                    );
                }, 
                done
            );
        });

        it("Reset password", function(done) {
            Client.filter({id : sampleClient.id}).one().then(
                function(client) {
                    client.resetPassword().then(function (new_pass) {
                        new_pass.should.exist;
                        new_pass.should.not.be.equal("");
                        Client.authenticate(client.username,new_pass).then(
                            function(client_back){
                                client.id.should.equal(client_back.id);
                                console.log("autenticado okay con password: " + new_pass);
                                done();
                            },
                            function(err){
                                console.log(err);
                                true.should.be.false;
                                done();
                            }
                        );
                    },function(err){
                        console.log(err);
                        true.should.be.false;
                        done();
                    });
                },
                function (err) {
                    done();
                }
            );
        });

        it("Can be suspended", function(done) {
            var password;
            comb.chain([
                function setUpPassword() {
                    return sampleClient.resetPassword();
                },
                function suspend(newPassword) {
                    password = newPassword;
                    return sampleClient.suspend();
                },
                function test() {
                    var ret = new comb.Promise();
                    
                    sampleClient.isBlocked.should.be.false;
                    sampleClient.isSuspended.should.be.true;

                    Client.authenticate(sampleClient.username, password).then(
                        function() {
                            true.should.be.false;
                        },
                        function(error) {
                            true.should.be.true;
                            ret.callback();
                        }
                    );

                    return ret;
                },
                function unsuspend() {
                    return sampleClient.unsuspend();
                },
                function test() {
                    var ret = new comb.Promise();
                    
                    sampleClient.isBlocked.should.be.false;
                    sampleClient.isSuspended.should.be.false;

                    Client.authenticate(sampleClient.username, password).then(
                        function() {
                            true.should.be.true;
                            ret.callback();
                        },
                        function(error) {
                            true.should.be.false;
                        }
                    );


                }
            ]).then(
                function() {
                    done();
                }
            );
            
        });

        it("Can be blocked and suspended", function(done) {
            var password;
            comb.chain([
                function test() {
                    sampleClient.isBlocked.should.be.false;
                    sampleClient.isSuspended.should.be.false;
                },
                function setUpPassword() {
                    return sampleClient.resetPassword();
                },
                function block(newPassword) {
                    password = newPassword;                    
                    return comb.serial([
                        function() {
                            var ret = new comb.Promise();
                            Client.authenticate(sampleClient.username, "wrong password").then(
                                function() {},
                                function() {
                                    ret.callback();
                                }
                            );
                            return ret;
                        },
                        function() {
                            var ret = new comb.Promise();
                            Client.authenticate(sampleClient.username, "wrong password").then(
                                function() {},
                                function() {
                                    ret.callback();
                                }
                            );
                            return ret;
                        },
                        function() {
                            var ret = new comb.Promise();
                            Client.authenticate(sampleClient.username, "wrong password").then(
                                function() {},
                                function() {
                                    ret.callback();
                                }
                            );
                            return ret;
                        },
                        comb.hitch(sampleClient, "reload")
                    ]);
                },
                function test() {
                    sampleClient.isBlocked.should.be.true;
                    sampleClient.isSuspended.should.be.false;
                    
                    return new comb.Promise().callback();
                },
                function suspend() {
                    return sampleClient.suspend();
                },
                function testAndAuthenticate() {
                    var ret = new comb.Promise();
                    
                    sampleClient.isBlocked.should.be.true;
                    sampleClient.isSuspended.should.be.true;

                    Client.authenticate(sampleClient.username, password).then(
                        function() {
                            true.should.be.false;
                        },
                        function(error) {
                            true.should.be.true;
                            ret.callback();
                        }
                    );

                    return ret;
                },
                function unsuspend() {
                    return sampleClient.unsuspend();
                },
                function testAndAuthenticate() {
                    var ret = new comb.Promise();
                    
                    sampleClient.isBlocked.should.be.true;
                    sampleClient.isSuspended.should.be.false;

                    Client.authenticate(sampleClient.username, password).then(
                        function() {
                            true.should.be.false
                        },
                        function(error) {
                            true.should.be.true;
                            ret.callback();
                        }
                    );
                    return ret;
                },
                function resetPassword() {
                    return sampleClient.resetPassword();
                },
                function testAndAuthenticate(newPassword) {
                    password = newPassword;

                    var ret = new comb.Promise();
                    
                    sampleClient.isBlocked.should.be.false;
                    sampleClient.isSuspended.should.be.false;

                    Client.authenticate(sampleClient.username, password).then(
                        function() {
                            true.should.be.true;
                            ret.callback();
                        },
                        function(error) {
                            true.should.be.false;
                        }
                    );
                    return ret;
                }
            ]).then(
                function() {
                    done();
                }
            );
            
        });

    });
    
});

