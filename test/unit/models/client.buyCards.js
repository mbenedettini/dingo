// TODO: must be a better way to require utils without knowing the WHOLE path
var utils = require(__dirname + '/../../../lib/utils');
var patio = require('patio');
var comb = require('comb');
var should = require('should');
var fixtures = utils.loadFixtures();

var e = function(error) { 
    console.log("ERROR: " + error); 
    true.should.be.false;
};


describe("client.buyCards", function() {
    var models;
    var db;
        
    beforeEach(function(done) {
        db = utils.getDbConnection();  // force db connection

        utils.getSyncedModels().then(
                function(m) {
                    models = m;
                    
                    /* Create a few sample things */
                    var c = new models.Client(fixtures.client[0]);
                    var b = new models.Bingo(fixtures.bingo[0]);
                    b.currentState = 10; // SELLING
                    var ca = new models.Card(fixtures.card[0]);
                    
                    comb.serial([
                        comb.hitch(utils, "cleanDb"),
                        comb.hitch(c, "save"),
                        comb.hitch(b, "save"),
                        comb.hitch(ca, "save"),
                        comb.hitch(c, "makeDeposit", 99.00, "Sample deposit")
                    ]).then(
                        function() {
                            done(); 
                        },
                        function(error) {
                            console.log("ERROR on main serial: " + error);
                            done();
                        }
                    );
                },
                function(error) {
                    console.log("ERROR when syncing: " + error);
                    done();
                }
            );
    });
    
    describe("Card purchasing", function() {  
        it("Buys a card out of a Bingo instance", function(done) {
            var sampleClient;
            var sampleCard;
            var sampleBingo;
            
            comb.serial([
                function() {
                    return models.Client.one().then(
                        function(client) {
                            sampleClient = client;
                        }
                    );
                },
                function() {
                    return models.Bingo.one().then(
                        function(bingo) {
                            sampleBingo = bingo;
                            }
                    );
                },
                function() {
                    return models.Card.one().then(
                        function(card) {
                            sampleCard = card;
                        }
                    );
                }
            ]).then(
                function() {
                    sampleClient.should.be.ok;
                    sampleCard.should.be.ok;
                    should.not.exist(sampleBingo.lastClientCardSold);

                    var bingo = sampleBingo;
                    sampleClient.buyCards(bingo, 1).then(
                        function(clientCards) {
                            clientCards.length.should.eql(1);
                            clientCards[0].id.should.be.ok;
                            clientCards[0].card.id.should.eql(sampleCard.id);
                            comb.serial([
                                function() {
                                    return models.Bingo.one().then(function(sameBingo) {
                                        sameBingo.id.should.eql(bingo.id);
                                        sameBingo.lastClientCardSold.id.should.eql(clientCards[0].id);
                                        sameBingo.lastClientCardSold.card.id.should.eql(sampleCard.id);
                                        sameBingo.clientsCount.should.eql(1);
                                    });
                                },
                                function() {
                                    return clientCards[0].client.then(function(client) {
                                        client.id.should.eql(sampleClient.id);
                                        client.balance.should.eql(98.00);
                                    });
                                },

                            ]).then(
                                function() { done(); },
                                function(error) {
                                    e(error);
                                    done();
                                }
                            );
                        },
                        function(error) {
                            e(error);
                            done();
                        }
                    );
                },
                function(error) {
                    e(error);
                    done();
                }
            );
        });

        it("Buys series", function (done) {
            this.timeout(4000);
            var sampleClient;
            var sampleCard;
            var sampleBingo;
            var cards = [];
            
            comb.serial([
                function() {
                    return models.Client.one().then(
                        function(client) {
                            sampleClient = client;
                        }
                    );
                },
                function() {
                    return models.Bingo.one().then(
                        function(bingo) {
                            sampleBingo = bingo;
                            }
                    );
                },
                function() {
                    return models.Card.one().then(
                        function(card) {
                            sampleCard = card;

                            cards = [sampleCard];
                            var i;
                            for (i=2; i<=24; i++) {
                                var c = new models.Card({
                                    series: 1, 
                                    number: i, 
                                    firstLine: sampleCard.firstLineNumbers,
                                    secondLine: sampleCard.secondLineNumbers,
                                    thirdLine: sampleCard.thirdLineNumbers
                                });
                                cards.push(c);
                            }
            
                        }
                    );
                },
                function () {
                    return models.Card.save(cards);
                }
            ]).then(
                function() {
                    sampleClient.should.be.ok;
                    sampleCard.should.be.ok;
                    should.not.exist(sampleBingo.lastClientCardSold);

                    var bingo = sampleBingo;
                    sampleClient.buyCards(bingo, 1, true).then(
                        function(clientCards) {
                            sampleBingo.reload().then(function() {

                                clientCards.length.should.eql(6);
                                
                                clientCards[0].card.number.should.eql(cards[0].number);
                                clientCards[0].card.id.should.eql(cards[0].id);
                                clientCards[5].card.number.should.eql(cards[5].number);
                                
                                sampleBingo.firstSoldCardNumber.should.eql(cards[0].number);
                                sampleBingo.lastSoldCardNumber.should.eql(cards[5].number);

                                sampleClient.reload().then(function() {
                                    sampleClient.balance.should.eql(93.00);

                                    sampleClient.buyCards(bingo, 2, true).then(
                                        function(clientCards) {
                                            clientCards.length.should.eql(12);
                                            clientCards[0].number.should.eql(cards[6].number);
                                            clientCards[11].number.should.eql(cards[17].number);

                                            sampleClient.reload().then(function() {
                                                sampleClient.balance.should.eql(81.00);

                                                done();
                                            });
                                        }
                                    );

                                });
                            });
                            
                        },
                        function(error) {
                            e(error);
                            done();
                        }
                    );
                },
                function(error) {
                    e(error);
                    done();
                }
            );
        });

        it("Cant buy if it leaves negative balance", function(done) {
            var sampleClient;
            var sampleCard;
            var sampleBingo;
            
            comb.serial([
                function() {
                    var ret = new comb.Promise();
                    models.Client.one().then(
                        function(client) {
                            sampleClient = client;
                            client.balance = 0.99;
                            client.save().then(comb.hitch(ret, "callback"), comb.hitch(ret, "errback"));
                        }
                    );
                    return ret;
                },
                function() {
                    return models.Bingo.one().then(
                        function(bingo) {
                            sampleBingo = bingo;
                            }
                    );
                },
                function() {
                    return models.Card.one().then(
                        function(card) {
                            sampleCard = card;
                        }
                    );
                }
            ]).then(
                function() {
                    sampleBingo.cardPrice.should.eql(1);
                    sampleClient.balance.should.eql(0.99);

                    sampleClient.buyCards(sampleBingo, 1).then(
                        function() {},
                        function(error) {
                            error.should.eql("No alcanza el saldo para realizar la compra");

                            sampleClient.reload().then(
                                function() {
                                    sampleClient.balance.should.eql(0.99);
                                    
                                    sampleBingo.reload().then(
                                        function() {
                                            sampleBingo.clientsCount.should.eql(0);
                                            done();
                                        }
                                    );
                                }
                            );
                        }
                    );
                }
            );
        });

        it("Should know how many clients bought cards", function(done) {
            var sampleClient;
            var anotherClient;
            var sampleCard;
            var sampleBingo;
            
            comb.serial([
                function() {
                    var ret = new comb.Promise();
                    models.Client.one().then(
                        function(client) {
                            sampleClient = client;
                            anotherClient = new models.Client(fixtures.client[0]);
                            anotherClient.balance = 10;
                            anotherClient.save().then(
                                function() {
                                    ret.callback();
                                }
                            );
                        }
                    );
                    return ret;
                },
                function() {
                    return models.Bingo.one().then(
                        function(bingo) {
                            sampleBingo = bingo;
                            }
                    );
                }
            ]).then(
                // TODO: these nested callbacks are a PITA to mantain. It's impossible
                // to distinguish the matching closing bracket.
                function() {
                    sampleBingo.clientsCount.should.eql(0);
                    sampleClient.buyCards(sampleBingo,1).then(
                        function(clientCards) {
                            sampleBingo.reload().then(function() {
                                sampleBingo.clientsCount.should.eql(1);
                                anotherClient.buyCards(sampleBingo, 1).then(
                                    function(clientCards) {
                                        sampleBingo.reload().then(function() {
                                            sampleBingo.clientsCount.should.eql(2);
                                            sampleClient.buyCards(sampleBingo, 1).then(
                                                function(clientCards) {
                                                    sampleBingo.reload().then(function() {
                                                        sampleBingo.clientsCount.should.eql(2);
                                                        anotherClient.buyCards(sampleBingo, 1).then(
                                                            function(clientCards) {
                                                                sampleBingo.reload().then(function() {
                                                                    sampleBingo.clientsCount.should.eql(2);
                                                                    done();
                                                                });
                                                            }
                                                        );
                                                    });
                                                }
                                            );
                                        });
                                    }
                                );
                            
                            });
                        }
                    );
                }
            );
        });
    });
});
