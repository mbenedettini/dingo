var should = require('should');

describe('Array', function(){
    before(function(){
        // ...
        console.log("Before");
    });

    describe('#indexOf()', function(){
        beforeEach(function(){
            console.log("BeforeEach");
        });
    
        it('should return -1 when not present', function(){
            console.log("1");
            [1,2,3].indexOf(4).should.equal(-1);
        });
        
        it('should return position when present', function(){
            console.log("2");
            [1,2,3,4].indexOf(4).should.equal(3);
        });
    
    });
  
    describe('Too good to be true', function(){
        it('Is always true', function(){
            console.log("always true");
            true.should.equal(true);
        });
    });
});
