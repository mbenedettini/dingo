#!/bin/bash
function run_test {
    OUTPUT=$(NODE_ENV='test' ./node_modules/mocha/bin/mocha "$1" 2>&1)
    if [ "$?" -eq "0" ]
    then
        echo "$1 OK!"
    else
        echo "ERROR ON $1: $OUTPUT"
    fi
}


TESTS=$(find ./test -name "*.js")	
echo "TESTS: $TESTS"
NODE_ENV='test' ./node_modules/mocha/bin/mocha $TESTS

# These tests must be run on its own
run_test "test/unit/models/client.payout.js.alone"
run_test test/unit/models/client_card.payout.js.alone
run_test test/unit/models/bingo.prizePayout.js.alone
run_test test/unit/models/bingo.cancel.js.alone 
