/**
 * Jake is a JavaScript build tool for Node.js
 * http://howtonode.org/intro-to-jake
 * https://github.com/mde/jake
 *
 * To find out the available tasks for Jake run:
 * jake -T
 *
 * To run a task do:
 * jake db:reset
 *
 * To run a task with params do:
 * jake db:populate[20]
 */
var colors          = require('colors'),
    faker           = require('./vendor/faker'),
    assetBuilder    = require('./lib/assetBuilder'),
    log             = console.log,
    ENV             = process.env.NODE_ENV || 'development',
    JK              = {};
    
// DB stuff
var patio   = require("patio"),
    comb    = require("comb"),
    when    = comb.when,
    serial  = comb.serial;

var utils = require('./lib/utils');

desc('Initialize stuff.');
task('init', [], function() {
    JK.utils = JK.utils || require('./lib/utils');
    // make sure the configs are loaded only once
    if (!JK.config) {
        JK.config = JK.utils.loadConfig(__dirname + '/config');
    }
});

namespace('app', function() {

  desc('Compress JS & CSS and make 1 JS && 1 CSS file. Run this before deploying to production.');
  task('assets', [], function(done) {
    assetBuilder(function() {
      log('- packed up JS & CSS files'.yellow);
      complete();
    });
  }, { async: true });

});

namespace('db', function() {

    desc('Connect to database');
    task('connect', ['init'], function() {
        log('- db:connecting'.yellow);
        if (!JK.db) {
            JK.db = JK.utils.getDbConnection();
            log('- db:connected!'.green);
        }
    });
    
    desc('Recreate database schema');
    task('recreateschema', ['db:connect'], function() {
        log('- db:recreating schema'.yellow);
        JK.utils.recreateSchema().then(
            function() {
                log('- db: schema successfully recreated'.green);
                complete();
            },
            function(error) {
                log(('- db: schema recreation ERROR: ' + error).red);
                log('- db: NOTE: Nevermind tables dropping errors: they are not taken into account.'.yellow);
                complete();
            }
        );
        
    }, {async: true});

    desc('Migrate database');
    task('migrate', ['db:connect'], function() {
        JK.utils.migrate().then(
            function() {
                log("migrations finished");
                complete();
            },
            function(error) {
                log(('- db: migration ERROR: ' + error).red);
                complete();
            }
        );
    }, {async: true});


    desc('Clean database without recreating schema');
    task('cleandb', ['db:connect'], function() {
        log('- db:cleaning tables'.yellow);
        JK.utils.cleanDb().then(
            function() {
                log('- db: tables successfully wiped'.green);
                patio.disconnect();
                complete();
            },
            function(error) {
                log(('- db: tables cleaning ERROR: ' + error).red);
                patio.disconnect();
                complete();
            }
        );
    }, {async: true});

    desc('Populate database with some values');
    task('populate', function() {
        var fixtures = utils.loadFixtures();

        utils.getSyncedModels().then(function(models) {
            var c = new models.Card(fixtures.card[0]);

            comb.serial([
                comb.hitch(c, "save")
            ]).then(
                function() {
                    log('- db: populate complete'.green);
                    patio.disconnect();
                    complete();
                },
                function(error){
                    log(('- db: populate ERROR: ' + error).red);
                    patio.disconnect();
                    complete();
                }
            );
        });
    }, {async: true});
});
