# DINGO
## The pragmatic Bingo engine

### Creating a client from [node-codein](https://github.com/ketamynx/node-codein)

```
var disconnect = global.patio.disconnect.bind(patio);
var disconnectError = function(err) {
    global.patio.logError(err);
    global.patio.disconnect();
};

var c = new Client({name: "Pedro", lastName: "Cucumelo"});
c.save();
c.save().then(function(client){
                console.log(format("%s %s's id is %d", client.name, client.lastName, client.id));
                disconnect();
            }, disconnectError);
            
```


### Retrieving a client, also from node-codein, and making a deposit to it:

```
var Client = global.app.settings.models.Client;
Client.filter({id : 1}).one().then(function(client) { 
    client.makeDeposit(4.27, 'lalala').then(
        function() {
            console.log("SUCCESS!");
        },
        function(error) {
            console.log("ERROR: " + error);
        }
    );
});
```


## Database indexes

### Table: setting
Index on columns: key

Example query: SELECT * FROM `setting` WHERE (`key` = 'increaseAccumulatedOrderEvery')

### Table: bingo
Index on columns: currentState, starts

Example query: SELECT * FROM `bingo` WHERE (starts <= '2012-12-27 10:27:15' AND currentState = 0) ORDER BY `starts` LIMIT 1

### Table: client_card
Index on columns: clientId, bingoId, cancelled, isSeries

Example query, all of the client cards of a client for a certain bingo: SELECT * FROM `client_card` WHERE ((`clientId` = 1) AND (`bingoId` = 169) AND (`cancelled` IS FALSE))

Another example, retrieving cards which are part of a series: SELECT * FROM `client_card` WHERE ((`bingoId` = 169) AND (`clientId` = 1) AND (`cancelled` IS FALSE) AND (`isSeries` IS FALSE)) ORDER BY `id` DESC LIMIT 1


### Table: card
Index on columns: number

Example query, get cards above a certain number: SELECT * FROM `card` WHERE (`number` > 6) ORDER BY `number`