var async   = require('async'),
    log     = console.log,
    sessionsHelper;

var patio   = require("patio"),
    comb    = require("comb"),
    when    = comb.when,
    serial  = comb.serial;

sessionsHelper = {
    requieresLogin: function(req,res,next) {
        if (req.session.user){
            console.log("session ok, seguimos querido " + req.session.user.username );
            next();
        } else {
          res.send('403 Forbidden', 403);
        }
    } 
};

module.exports = sessionsHelper;