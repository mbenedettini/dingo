var async   = require('async'),
    log     = console.log,
    mailerHelper;
    

var patio   = require("patio"),
    comb    = require("comb"),
    nodemailer = require("nodemailer"),
    when    = comb.when,
    serial  = comb.serial,
    utils   = require('../../lib/utils'),
    config  = utils.loadConfig(__dirname + "/../../config"),
    ENV     = utils.getENV(),
    mailerConfig  = config[ENV].mailer;

    
var CONFIRMATION_URL_BASE = mailerConfig["URL_BASE"] + "/api/clients/confirm/";

var transport = nodemailer.createTransport("SMTP", {
    service: mailerConfig["SERVICE"],
    /*auth: {
        user: mailerConfig["USER"],
        pass: mailerConfig["PASSWORD"]
    }*/
});

var mailerHelper = {
  
    sendConfirmationMail: function(recipient, tokenConfirmation) {
        var self = this;
        var subject = "Bingo Rosariocasino: Confirmacion de creacion de cuenta";
        var message = "Usted ha solicitado la creacion de una cuenta en Bingo Rosariocasino\n";
        message += " Haga clic en el siguiente enlace para confirmarla\n";
        message += CONFIRMATION_URL_BASE + tokenConfirmation + "\n\n";
        message += "Atentamente,\n";
        message += "Bingo Rosariocasino\n";
        return self.sendMail(recipient,subject,message);
    },

    sendNewPassword: function(recipient, newPassword) {
        var self = this;
        var subject = "Bingo Rosariocasino: Nueva contraseña";
        var message = "Su nueva contraseña para ingresar es " + newPassword + "\n";
        message += "\n\nAtentamente,\n";
        message += "Bingo Rosariocasino\n";
        return self.sendMail(recipient,subject,message);
    },
    
    sendMail: function(to,subject,message) {
        var self = this;
        var ret = new comb.Promise();

        var mailOptions = {
            from: "Bingo Rosariocasino <info@rosariocasino.com>", // sender address
            to: to, // list of receivers
            subject: subject, // Subject line
            text: message // plaintext body
            //html: "<b>Hello world ✔</b>" // html body
        };

        // send mail with defined transport object
        transport.sendMail(mailOptions, function(error, response){
            if(error){
                console.log(error);
                ret.errback(error);
            }else{
                console.log("Message sent: " + response.message);
                ret.callback(true);
            }
        });
        
        return ret;
    }
};

module.exports = mailerHelper; 
