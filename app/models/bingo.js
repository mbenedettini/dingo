var patio   = require('patio');
var comb    = require('comb');
var __      = require('underscore');

var STATES = {
    INACTIVE      : 0,
    SELLING       : 10,
    RUNNING       : 20,
    RUNNING_LIGHT_PAUSED : 25, // volatile state
    RUNNING_PAUSED: 30, // TODO: kind of stopped state
    FINISHED      : 40,
    CANCELLED     : 50
};

var DEFAULT_LAPSE = 5; // 5 seconds between numbers
var DEFAULT_SELLING_INTERVAL = 300; // sells cards for 5 minutes before starting
var START_DELAY = 5; // How long to wait between bingoStarted is emitted and the first number is called
var MIN_SHOWN_CLIENTS_COUNT = 3;  // min clients count to be shown on front client
var RATIO_SHOWN_CLIENTS_COUNT = 1.40; // shown clients count = totalClientsCount * ratio
var DEFAULT_ACCUMULATED_ORDER = 39;

var NUMBERS = [
    1,2,3,4,5,6,7,8,9,
    10,11,12,13,14,15,16,17,18,19,
    20,21,22,23,24,25,26,27,28,29,
    30,31,32,33,34,35,36,37,38,39,
    40,41,42,43,44,45,46,47,48,49,
    50,51,52,53,54,55,56,57,58,59,
    60,61,62,63,64,65,66,67,68,69,
    70,71,72,73,74,75,76,77,78,79,
    80,81,82,83,84,85,86,87,88,89,
    90
];

/* Mini state diagram:
 * 
 *                         Active
 *               |--------------------------------|       
 *                
 *         start()                                  bingo called
 * Inactive ----> Selling   ------------> Running  ----------> Finished
 *      ^             | pause()   pause()|     ^
 *      |             |                  |     |start()
 *      ---------------               Paused --
 * 
 * The lapse between each called (generated) number is lapseBetweenNumbers.
 * The lapse for which the Bingo will sell cards is intervalSelling.
 * At any time there can only be one active Bingo in the system.
 * 
 * Properties for each state:
 * 
 * Inactive (initial state)
 *  - isActive:   false
 *  - isRunning:  false
 *  - isSelling:  false
 *  - isFinished: false
 *  - isCancelled: false
 * 
 * Selling cards
 *  - isActive:   true
 *  - isRunning:  false
 *  - isSelling:  true
 *  - isFinished: false
 *  - isCancelled: false
 * 
 * Running (calling numbers)
 *  - isActive:   true
 *  - isRunning:  true
 *  - isSelling:  false
 *  - isFinished: false
 *  - isCancelled: false
 * 
 * Paused
 *  - isActive: true
 *  - isRunning: false
 *  - isSelling: false
 *  - isPaused:  true
 *  - isFinished: false
 *  - isCancelled: false
 * 
 * Finished (Bingo! has been called)
 *  - isActive:   false
 *  - isRunning:  false
 *  - isSelling:  false
 *  - isFinished: true
 *  - isCancelled: false
 * 
 *  * Cancelled
 *  - isActive:   false
 *  - isRunning:  false
 *  - isSelling:  false
 *  - isFinished: false
 *  - isCancelled: true

 * 
 * If paused and started again during selling time, it will become inactive it will have to 
 * start the whole selling interval again.
 * It will not continue from the moment it was paused. Example: selling interval is 2 minutes, Bingo 
 * is paused at 1:45 of the selling interval and started again: the selling interval will last for 
 * other 2 minutes
 * 
 * Light pause (volatile) just stops bingo from generating number. It is volatile (non persisted) and 
 * intended to hold on a few seconds while front clients perform visual and ear candy (animation, 
 * speeches, etc).
 * 
 */

var Bingo = patio.addModel('bingo', {
    plugins : [patio.plugins.TimeStampPlugin],
    
    static : {
        init : function() {
            this._super(arguments);
            this.timestamp();
            
            this.oneToMany("client_cards", {key: "bingoId"} );
            this.manyToOne("lastClientCardSold", {fetchType: this.fetchType.EAGER, model: "client_card", key: "lastClientCardSoldId"});

            this.setMaxListeners(0);          
        },

        _getDefaultRemainingNumbers: function(someRemainingNumbers) {
            /* TODO: is this complexity actually necessary? */

            /* All of these are equal for someRemainingNumbers argument:
             * null
             * empty array
             * array containing empty strings or zeroes
             * 
             */

            var defaultRemainingNumbers = NUMBERS;

            if (! (this.ENV == 'development')) {
                defaultRemainingNumbers = __.shuffle(defaultRemainingNumbers);
            }

            var remainingNumbers = defaultRemainingNumbers;

            /* someRemaining argument is only valid if all of its elements evaluate to true.
             * That is, none of its elements are zero.
             */
            if (someRemainingNumbers) {
                if (! comb.isEmpty(someRemainingNumbers)) {
                    // Only take into account someRemainingNumbers if all of their elements evaluate to true
                    var i;
                    for (i=0; i < someRemainingNumbers.length; i++) {
                        if (! someRemainingNumbers[i]) {
                            break;
                        }
                    }

                    if (i == someRemainingNumbers.length ) { // having completed the for(), i equals someRemainingNumbers.length;
                        remainingNumbers = someRemainingNumbers;
                    }
                }
            }

            return remainingNumbers;
        },

        getNextToStart : function () {
            return this.filter("starts <= ? AND currentState = 0", patio.timeStampToString(new Date())).order("starts").first();
        },

        getUpcoming : function () {
            return this.filter("starts BETWEEN ? AND ?", patio.timeStampToString(new Date()), patio.timeStampToString(comb.date.add(new Date(), "hours", 24))).order("starts").all();
        },

        setENV : function(env) {
            this.ENV = env;
        },

        calculateShownTotalClientsCount : function(totalClientsCount) {
            var shownClientsCount = totalClientsCount > MIN_SHOWN_CLIENTS_COUNT ? Math.round(totalClientsCount * RATIO_SHOWN_CLIENTS_COUNT) : MIN_SHOWN_CLIENTS_COUNT;

            return shownClientsCount;
        },
    },

    post : {
        load : function(next) {
            this._readAccumulatedSettings().then(function() {
                next();
            });
        },
        
        save : function(next) {
            this._readAccumulatedSettings().then(function() {
                next();
            });
        }
    },

    instance : {
        // Some non-persisted attributes
        _numbersGeneratorTimer : null,
        _sellingTimer : null,
        _sellingCancellationTimer : null,
        startDelay : START_DELAY,

        /* How many clients are now connected or viewing
         * this bingo instance, whether they purchased cards or not.
         */
        totalClientsCount : 0,

        /* # of clients to be shown on front client */
        shownTotalClientsCount : 0,

        lineWinnersCounter : 0,
        lineCallingStartedAt: null, // moment when the first line was called
        lineCallingFinishesAt: null, // when the bingo can continue calling numbers
        bingoCallingStartedAt: null,
        bingoCallingFinishesAt: null,
        
        /* Accumulated prize values cache */
        _accumulatedOrder : 0,
        _accumulatedPrize : 0,
        _accumulatedRatio : 0,
        _accumulatedEnabled : 0,
        
        constructor: function(options, fromDb) {
            options = options || {};
            this._super(arguments);

            this.setMaxListeners(0);
            
            if (! fromDb) {
                // Initialize remaining and called numbers
                this.remainingNumbers = this._static._getDefaultRemainingNumbers(options.remainingNumbers);
                this.calledNumbers = options.calledNumbers || [];
                this.lapseBetweenNumbers = options.lapseBetweenNumbers || DEFAULT_LAPSE;
                this.sellingInterval = options.sellingInterval || DEFAULT_SELLING_INTERVAL;
            }
        },

        getters : {
            canStart : function() {
                if (this.isFinished) { return false; };
                if (this.isSelling || this.isRunning) { return false; };
                if (this.isCancelled) return false;

                var startsHasArrived = (new Date() >= this.starts);
                
                // Bingo start time has arrived and is not already active
                return startsHasArrived;
            },
            isSelling : function () {
                return this.currentState == STATES.SELLING;
            },
            isRunning : function() {
                return this.currentState == STATES.RUNNING;
            },
            isPaused : function() {
                return this.currentState == STATES.RUNNING_PAUSED;
            },
            isLightPaused : function() {
                return this.currentState == STATES.RUNNING_LIGHT_PAUSED;
            },
            isFinished : function () {
                return this.currentState == STATES.FINISHED;
            },
            isActive : function () {
                return this.isSelling || this.isRunning || this.isPaused || this.isLightPaused;
            },
            isCancelled : function() {
                return this.currentState == STATES.CANCELLED;
            },
            visitorsCount : function () {
                var visitorsCount = this.totalClientsCount - this.clientsCount;
                // If something wrong happens, return 0
                visitorsCount = visitorsCount < 0 ? 0 : visitorsCount;
                return visitorsCount;
            },
            maxBall : function() {
                /* This is what is called "Bola max" in CityCenter Bingo: the next bingo prize to be called.
                 *  If calledNumbers.length <= accumulatedOrder, then maxBall is accumulatedOrder, because if Bingo is called it will win the accumulated prize.
                 *  If calledNumbers.length > accumulatedOrder, then maxBall is 90.
                 */

                /* 2012-11-23: this attribute is no longer needed. Consider definitely deprecating it. */
                if (this.calledNumbers.length <= this._accumulatedOrder) {
                    return this._accumulatedOrder;
                }
                else {
                    return NUMBERS.length;
                }
            }
        },

        _setTotalClientsCount : function(value) {
            this.shownTotalClientsCount = this._static.calculateShownTotalClientsCount(value);
            console.log("Setting shownTotalClientsCount to " + this.shownTotalClientsCount);
            return value;
        },
        
        /* Custom getters and setters */
        _getRemainingNumbers : function(value) {
            if (value) {
                var strRemainingNumbers = value;              
                return strRemainingNumbers.split(",").map(function(strNumber) { return parseInt(strNumber, 10); } );
            }
            else {
                return [];
            }
        },
        
        _getCalledNumbers : function(value) {
            if (value) {
                var strCalledNumbers = value;
                return strCalledNumbers.split(",").map(function(strNumber) { return parseInt(strNumber, 10); } );
            }
            else {
                return [];
            }
        },

        _setLinePrize : function(value) {
            return (Math.round((value) * 100) / 100);
        },

        _getLinePrize : function(value) {
            return (Math.round((value) * 100) / 100);
        },       

        /* end of custom getters and setters */

        persist: function() {
            var self = this;

            if (this.isNew) {
                var ret = new comb.Promise();

                console.log("Looking for last client card sold");
                this._lookForLastClientCardSold().then(
                    function(lastClientCardSold) {
                        console.log("Bingo: saving volatile instance on first purchase");
                        self.save().then(
                            function() {
                                self._startSelling().then( // Sets timers and other stuff
                                    function() {
                                        self.reload().then(
                                            comb.hitch(ret, "callback"),
                                            comb.hitch(ret, "errback")
                                        );
                                    },
                                    comb.hitch(ret, "errback")
                                );
                            },
                            comb.hitch(ret, "errback")
                        );
                    },
                    comb.hitch(ret, "errback")
                );
                return ret;
            }
            else {
                return new comb.Promise().errback(new Error("Trying to persist an already persisted bingo instance."));
            }
        },
        
        buyClientCards : function(quantity, series) {
            /* quantity is optional, defaulting to 1. series is also optional, defaulting to false.
             * If series is true, quantity becomes how many series you want to purchase (and not
             * how many cards). Example: series=true, quantity=2 means that two whole series will
             * be purchased, a total of 12 cards.  */
            var self = this;

            if (!this.isSelling) {
                return new comb.Promise().errback(new Error("ClientCards purchasing is not allowed at the moment."));
            }

            if (this.isNew) {
                return new comb.Promise().errback(new Error("Trying to purchase on a volatile bingo. First persist it."));
            }

            var ret = new comb.Promise();

            quantity = quantity || 1;
            series = series || false;

            if (series) {
                quantity = quantity * 6;
            }
            
            var ClientCard = patio.getModel('client_card');
            ClientCard.getAvailables(this.lastClientCardSold, quantity, series).then(
                function(availableClientCards) {
                    if (availableClientCards.length != quantity) {
                        ret.errback("There was an error purchasing cards: we asked for " + quantity + " cards and we've got " + availableClientCards.length);
                        return;
                    }

                    availableClientCards.forEach(function(cc) {
                        cc.bingo = self;
                        cc.price = self.cardPrice;
                    });

                    if (!self.firstSoldCardNumber) {
                        self.firstSoldCardNumber = availableClientCards[0].number;
                    }

                    var linePrizeIncrease = self.linePrizeRate * availableClientCards.length * self.cardPrice;
                    self.linePrize += linePrizeIncrease;

                    var bingoPrizeIncrease = self.bingoPrizeRate * availableClientCards.length * self.cardPrice;
                    self.bingoPrize += bingoPrizeIncrease;
                    
                    var newLastClientCardSold = availableClientCards[availableClientCards.length - 1 ];
                    newLastClientCardSold.lastSoldOf = self;
                    self.lastClientCardSold = newLastClientCardSold;
                    self.lastSoldCardNumber = newLastClientCardSold.number;

                    self.clientCardsCount += quantity;

                    var accumulatedPrizeIncrease = self._accumulatedRatio * availableClientCards.length * self.cardPrice;
                    if (accumulatedPrizeIncrease) { // safety check in case something wrong happens
                        self._increaseAccumulatedPrize(accumulatedPrizeIncrease).then(function(newAmount) {
                            // SUCCESS!
                            ret.callback(availableClientCards);
                        });
                    }
                    else {
                        ret.callback(availableClientCards);
                    }
                },
                function(error) {
                    ret.errback(error);
                }
            );

            return ret;
        },

        notifyCardPurchasing : function() {
            this.emit('cardsPurchased');
            this.setSellingTimerIfNecessary();
        },

        _readAccumulatedSettings : function() {
            /* Reads all settings related with the accumulated prize into our local attributes */
            var self = this;
            var ret = new comb.Promise();
            var Setting = patio.getModel('setting');
            
            var settings = Setting.getAll().then(function(settings) {
                var order = parseInt(settings.accumulatedOrder);
                var prize = parseFloat(settings.accumulatedPrize);
                /* TODO: I have fuckingly misnamed the accumulated rate. Instead of "rate"
                 * I named it "ratio". This should be homogenized by renaming either this
                 * one or the others.
                 */
                var ratio = parseFloat(settings.accumulatedRatio);
                var enabled = parseInt(settings.accumulatedEnabled); // 0-1

                self._accumulatedOrder = order;
                self._accumulatedPrize = prize;
                self._accumulatedRatio = ratio;
                self._accumulatedEnabled = enabled;

                ret.callback({
                    'accumulatedOrder': order,
                    'accumulatedPrize': prize,
                    'accumulatedRatio': ratio,
                    'accumulatedEnabled': enabled
                });
            });

            return ret;
        },

        _increaseAccumulatedPrize : function(amount) {
            /* Increases accumulatedPrize setting by amount */
            var self = this;
            var ret = new comb.Promise();
            var key = 'accumulatedPrize';

            var Setting = patio.getModel('setting');

            Setting.getValue(key, true).then(function(value) {
                value = parseFloat(value);
                var newValue = value + amount;

                Setting.setValue(key, newValue, true).then(function() {
                    self._accumulatedPrize = newValue;
                    ret.callback(newValue);
                });
            });

            return ret;
        },

        _resetAccumulatedPrize : function() {
            /* Resets accumulatedPrize to 0 and accumulatedOrder to DEFAULT_ACCUMULATED_ORDER*/
            var Setting = patio.getModel('setting');

            return comb.serial([Setting.setValue('accumulatedPrize', 0), Setting.setValue('accumulatedOrder', DEFAULT_ACCUMULATED_ORDER) ]);
        },
        
        start : function() {
            var self = this;

            if (this.isCancelled) {
                return new comb.Promise().errback(new Error("This Bingo instance cannot be started at the moment."));
            }

            return comb.serial([
                comb.hitch(self , "_readAccumulatedSettings"),
                function () {
                    if (!self.lastClientCardSold && !self.isNew) {
                        console.log("looking for last client card sold");
                        var ret = new comb.Promise();
                        self._lookForLastClientCardSold().then(
                            function(lastClientCardSold) {
                                
                                self.save().then(
                                    function() {
                                        self.start().then(
                                            comb.hitch(ret, "callback"),
                                            comb.hitch(ret, "errback")
                                        );
                                    }
                                );
                            },
                            function(error) {
                                console.log("Bingo.start(): error on _lookForLastClientCardSold: " + error);
                                ret.errback();
                            }
                        );
                        return ret;
                    }

                    if (self.isPaused) {
                        // If paused, continue running. Paused state is only allowed after running state has 
                        // been reached at least once.
                        return self._startRunning();
                    }
                    else if (self.isLightPaused) {
                        self.currentState = STATES.RUNNING;
                        self._generateNumber();
                        return (new comb.Promise()).callback();
                    }
                    else if (self.canStart) {
                        //~ console.log("Starting Bingo #" + this.id);
                        return self._startSelling();
                    }
                    else if (!self.isActive && self.isNew) {
                        // A volatile bingo. Allow it to start selling
                        console.log("Bingo: starting selling for a volatile bingo instance");
                        return self._startSelling();
                    }
                    else {
                        return new comb.Promise().errback(new Error("This Bingo instance cannot be started at the moment."));
                    }
                }
            ]).then(
                function() {},
                function(error) {
                    console.log("Serial error " + error);
                }
            );
        },

        _startRunning : function() {
            var self = this;
            /* Paranoid check! (shouldn't happen) */
            if (! (this.isSelling || this.isPaused)) {
                return new comb.Promise().errback(new Error("This Bingo instance cannot be put to run at the moment. It was not selling cards nor paused."));
            }

            this.currentState = STATES.RUNNING;
            clearTimeout(this._sellingCancellationTimer);

            console.log("Bingo #" + this.id + ": startRunning");

            var ret = new comb.Promise();
            this.bindToClientCards().then(
                function(clientCards) {
                    /* Accumulated check */

                    /* Only performed if 
                     *   the accumulated prize is disabled 
                     *   and when started for the first time 
                     *   and we actually we have an accumulated order defined
                     */
                    if ((! self._accumulatedEnabled) && self.calledNumbers.length == 0 && self._accumulatedOrder) { 
                        var weHaveAccumulatedWinner = true; // true to allow next while to run at least once

                        while (weHaveAccumulatedWinner) {
                            weHaveAccumulatedWinner = self._isAnyBingoWinner(self.remainingNumbers.slice(0, self._accumulatedOrder), clientCards);
                            if (weHaveAccumulatedWinner) {
                                // Get a new set of remaining numbers
                                console.log("Bingo #" + self.id + " getting a new set of remainingNumbers: accumulated disabled and it would be called");
                                self.remainingNumbers = self._static._getDefaultRemainingNumbers();
                            }
                        }
                    }

                    self.save().then(
                        function() {
                            self.emit('bingoStarted');
                            // Wait START_DELAY to give browsers the chance of playing a speech and start number generator
                            setTimeout(function() {
                                self._generateNumber();
                            }, self.startDelay * 1000);
                            ret.callback();
                        },
                        comb.hitch(ret, "errback")
                    );
                },
                comb.hitch(ret, "errback")
            );
            
            return ret;
        },

        _isAnyBingoWinner : function(calledNumbers, clientCards) {
            var winners = clientCards.filter(function(c) {
                return c.isBingoWinner(calledNumbers);
            });

            return winners.length > 0;
        },

        _startSelling : function() {
            var self = this;
            this.currentState = STATES.SELLING;
            var sellingStartedHasBeenEmitted = this.checkSellingCriteriaAndSetFinishesAttribute();

            var completeStartSelling = function() {
                if (!sellingStartedHasBeenEmitted) self.emit('sellingStarted');
                self.setSellingTimerIfNecessary();
                self._startSellingCancellationTimeout(); // only sets volatile values
            };

            if (this.isNew) {
                // Do not save bingo instance right now. It will be saved when the first purchase is made.
                completeStartSelling();
                return new comb.Promise().callback();
            }
            else {
                return this.save().then(
                    function() {
                        console.log("_startSelling: Bingo just saved");
                        completeStartSelling();
                    }
                );
            }
        },

        _startSellingCancellationTimeout : function() {
            /* Sets a timer that when fired will stop this bingo from selling cards and will cancel
             * it, refunding money.
             */
            var self = this;
            if (this._sellingCancellationTimer) {
                console.warn("A selling cancellation timer already exists for bingo #" + this.id + ". We are clearing it and setting a new one.");
                clearTimeout(this._sellingCancellationTimer);
            }

            if (this.sellingCancellationTimeout) {
                this._sellingCancellationTimer = setTimeout(function() {
                    if (self.isSelling) {
                        console.log("Selling Cancellation timeout");
                        if (self.isNew) {
                            self._cancel(); // _cancel() does not contain additional checks that cancel() does
                        }
                        else {
                            self.pause().then(
                                function() {
                                    self.cancel().then(
                                        function() {
                                            console.log("Successful cancellation of bingo #" + self.id);
                                        },
                                        function(error) {
                                            console.log("Bingo #" + self.id + " could not be cancelled: " + error);
                                        }
                                    );
                                }
                            );
                        }
                    }
                    else {
                        console.log("cancellationTimeout for bingo #" + self.id + " was fired but it was not active");
                    }
                }, this.sellingCancellationTimeout * 1000);
            }
        },

        checkSellingCriteriaAndSetFinishesAttribute : function() {
            /* For a bingo to start two conditions must be met: 
             *  clientsCount (how many clients purchased cards) must be at least minClientsCount
             *  totalClientsCount (how many clients are currently logged in) must be at least minTotalClientsCount
             * 
             * When those two conditions are met, the selling timer starts its countdown, at the end of
             * which the bingo will start.
             * 
             * This method checks those conditions and sets the selling timer. It should be called whenever any of clientsCount (a card is sold) or totalClientsCount (a client logs in no matter he purchases a card or not) change.
             */
            console.warn("Starting checkSellingCriteriaAndSetFinishesAttribute for bingo #" + this.id);
            console.warn("With clientsCount: " + this.clientsCount + " and totalClientsCount: " + this.totalClientsCount);
            // The attribute has been set. This makes us safe to be called several times.
            if (this.sellingFinishesAt) return;            

            if (this.clientsCount < this.minClientsCount) return;

            if (this.totalClientsCount < this.minTotalClientsCount) return;

            // An ultimate check which shouldnt happen.
            if (! this.isSelling) {
                console.error("_checkSellingCriteria: bingo #" + this.id + " met criteria but it is not selling");
                return;
            }

            this.sellingFinishesAt = new Date((new Date()).getTime() + this.sellingInterval * 1000);

            this.emit('sellingStarted');

            // All criteria passed.
            console.warn("All criteria passed. Will finish selling at " + new Date(this.sellingFinishesAt));

            return true;
        },

        setSellingTimerIfNecessary : function() {
            var self = this;
            
            // Do nothing if this attribute is not set
            if (! this.sellingFinishesAt) {
                console.log("Bingo.setSellingTimerIfNecessary: not setting timer as sellingFinishesAt is undefined");
                return;
            }

            // There is already a selling timer running
            if (this._sellingTimer) {
                console.log("Bingo.setSellingTimerIfNecessary: not setting timer as a selling timer (_sellingTimer) is already defined");
                return;
            }

            // Conditions for this bingo not to be cancelled have been met.
            clearTimeout(this._sellingCancellationTimer);
            this._sellingCancellationTimer = null; // helps testing         

            console.warn("Bingo.setSellingTimerIfNecessary: timer has been set for bingo #" + this.id + ". Will stop selling and start running at " + this.sellingFinishesAt.toLocaleString() + ". Selling interval is " + this.sellingInterval);

            //this.emit('sellingStarted');
            this._sellingTimer = setTimeout(comb.hitch(self, "_startRunning"), self.sellingInterval * 1000);
        },

        _lookForLastClientCardSold : function() {
            var self = this;
            var ClientCard = patio.getModel('client_card');
            var ret = new comb.Promise();

            // Look for the last finished bingo of the same cardPrice
            // SELECT * FROM bingo WHERE cardPrice = ? AND currentState = ? ORDER BY finished DESC LIMIT 1
            this._static.filter({cardPrice: this.cardPrice, currentState: STATES.FINISHED}).order("finished").reverse().one().then(
                function (bingo) {
                    if (bingo) {                        
                        self.lastClientCardSold = bingo.lastClientCardSold;
                        ret.callback(self.lastClientCardSold);
                    }
                    else {
                        ret.callback();
                    }
                },
                function(error) {
                    console.log("Bingo._lookForLastClientCardSold: Bingo Filter failed: " + error);
                    ret.errback();
                }
            );

            return ret;
        },
        
        pause : function(type) {
            var self = this;

            if (this.isCancelled) {
                return new comb.Promise().errback(new Error("This Bingo instance cannot be paused at the moment."));
            }
            
            // stop: stops the bingo. it is a persistent state.
            // light: just pauses number generation, without persisting anything
            type = type || 'stop';
            
            if (type == 'stop') {
                if (this.isRunning || this.isLightPaused) {
                    console.log("Bingo.pause(): Pausing bingo #" + this.id);
                    // First, stop generating numbers and set our desired state
                    //~ console.log("Clearing interval for bingo #" + this.id);
                    //~ console.log(this._numbersGeneratorTimer);
                    clearTimeout(this._numbersGeneratorTimer);
                    this.currentState = STATES.RUNNING_PAUSED;
                    
                    /* NOTE: The previous currentState setting could also
                     * be persisted by a numbers generation saving operation in progress.
                     * I also think that it wouldn't present a risk.* 
                     *  */
                    
                    
                    if (this._numbersGeneratorSaving) {
                        // There is a numbers generation saving operation in progress.
                        // We should chain another save operation to it.
                        console.log("Chaining pause save operation to a numbers generation already in progress");
                        this._numbersGeneratorSaving.addCallback(function() {
                            //~ console.log("Actually saving bingo pausing");
                            return self.save();
                        });
                        return this._numbersGeneratorSaving;
                    }
                    else {
                        // There is no operation in progress. This might happen if a bingo
                        // instance is immediately paused after started.
                        return this.save();
                    }
                    //~ console.log("Adding callback for " + this._savingNumbersGeneration);
                    
                }
                else if (this.isSelling) {
                    console.log("Bingo.pause(): Stop selling of Bingo #" + this.id );
                    this.sellingFinishesAt = null;
                    clearTimeout(this._sellingTimer);
                    clearTimeout(this._sellingCancellationTimer);
                    this._sellingTimer = null;
                    this._sellingCancellationTimer = null;
                    this.currentState = STATES.INACTIVE;
                    return this.save();
                }
                else {
                    return new comb.Promise().errback(new Error("This Bingo instance cannot be paused at the moment."));
                }
            }
            else {
                /* Default: light pause */
                if (this.isRunning) {
                    console.log("Bingo.pause(): Light Pausing bingo #" + this.id);
                    clearTimeout(this._numbersGeneratorTimer);
                    this.currentState = STATES.RUNNING_LIGHT_PAUSED;
                }
                else {
                    /* Light pause does not work in other states */
                    return new comb.Promise().errback(new Error("This Bingo instance cannot be paused at the moment."));
                }
            }
            
            
        },
        
        _finish : function() {
            var self = this;
            if (this.isRunning || this.isLightPaused) {
                console.log("Bingo._finish(): Finishing bingo #" + this.id);
                clearTimeout(this._numbersGeneratorTimer);
                this.currentState = STATES.FINISHED;
                this.finished = new Date();
                return this._numbersGeneratorSaving.addCallback(function() {
                    self.emit('finish');
                    return self.save();
                });
            }
            else if (this.isFinished) {
                console.log("Bingo._finish(): Bingo #" + this.id + " is already finished.");
                return (new comb.Promise()).callback();
            }
            else {
                return (new comb.Promise()).errback(new Error("This Bingo instance cannot be finished at the moment."));
            } 
        },

        cancel : function() {
            /* Cancels and refunds money */

            if (this.isCancelled) {
                return new comb.Promise().errback(new Error("This Bingo instance cannot be cancelled at the moment."));
            }

            var self = this;
            var ret = new comb.Promise();
            if (this.isActive) {
                if (this.isPaused) {
                    // It's paused. Cancel right away.
                    this._cancel().then(
                            comb.hitch(ret, "callback"),
                            comb.hitch(ret, "errback")
                    );
                }
                else if (this.isSelling || this.isRunning) {
                    // It is either selling or running. Pause before cancel.
                    this.pause().then(
                        function() {
                            self._cancel().then(
                                comb.hitch(ret, "callback"),
                                comb.hitch(ret, "errback")
                            );
                        },
                        comb.hitch(ret, "errback")
                    );
                }
                else {
                    var error = "ERROR: unexpected error when evaluating bingo #" + this.id + " state";
                    console.log(error);
                    ret.errback(error);
                }

            }
            else if (this.isFinished) {
                self._cancel().then(
                    comb.hitch(ret, "callback"),
                    comb.hitch(ret, "errback")
                );
            }
            else if (! this.isActive) {
                /* Classic situation: selling conditions have not been met yet and the cancellation selling timer has been fired */
                self._cancel().then(
                    comb.hitch(ret, "callback"),
                    comb.hitch(ret, "errback")
                );
            }
            else {
                console.log(this.currentState);
                return (new comb.Promise()).errback(new Error("This Bingo instance cannot be cancelled at this time."));
            }

            return ret;
        },

        _cancel : function() {
            /* Actually performs cancellation */
            var self = this;
            var ret = new comb.Promise();
            this.currentState = STATES.CANCELLED;
            
            // Notify before starting the operation
            this.emit('cancel');

            if (this.client_cards)
                this.client_cards.then(
                    function(clientCards) {
                        var allReady = clientCards.map(function(c) { return c.cancel(); });
                        (new comb.PromiseList(allReady)).promise().then(
                            function() {
                                // Final step: persist our new currentState
                                self.save().then(
                                    comb.hitch(ret, "callback"),
                                    comb.hitch(ret, "errback")
                                );
                            },
                            comb.hitch(ret, "errback")
                        );
                    },
                    comb.hitch(ret, "errback")
                );
            else {
                ret.callback();
            }
            
            return ret;
        },

        _generateNumber : function() {
            //~ console.log("Starting numbers generation for Bingo #" + this.id);
            
            // Might happen that we were triggered and actually this bingo is supposed to be paused.
            // Probably because of a race condition that I don't need to understand right now,
            // just make sure it doesn't run if bingo must be paused. MB.
            if (! this.isRunning) {
                //~ console.log("NOT generating number and exiting. Bingo paused.");
                return;
            }
            
            
            var self = this;
            var remainingNumbers = this.remainingNumbers;
            var calledNumbers = this.calledNumbers;

            if (!remainingNumbers.length) {
                /* Last-resource. This might happen if the bingo runs without any client card */
                this.pause().then(
                    function() {
                        self._finish();
                    }
                );
            }
            
            var nextNumber = remainingNumbers.shift();
            calledNumbers.push(nextNumber);
                        
            this.remainingNumbers = remainingNumbers;
            this.calledNumbers = calledNumbers;
            
            // Save current state and schedule another number generation by when it's done.
            this._numbersGeneratorSaving = this.save().addCallback(function() {
                var scheduleNextNumberGeneration = function () {
                    console.log("Emitting " + nextNumber + " for Bingo #" + self.id);
                    self.emit('numberCalled', nextNumber);
                    // Dunno exactly why (a matter of scope, though) this actually works with comb.hitch:
                    self._numbersGeneratorTimer = setTimeout(comb.hitch(self, "_generateNumber"), self.lapseBetweenNumbers * 1000);
                };
                
                // Did we just pass accumulatedOrder ?
                var justPassedAccumulatedOrder = self.calledNumbers.length == self._accumulatedOrder + 1;
                if (!justPassedAccumulatedOrder) {
                    scheduleNextNumberGeneration();
                }
                else {
                    console.log("Bingo #" + self.id + ": Just passed accumulatedOrder");
                    self.emit('justPassedAccumulatedOrder');
                    var delay = 6; // safety threshold + have_been_called
                    delay += 4; // number
                    delay += 3; // balls
                    delay += 4; // continue to bingo

                    if (self.dontDelayOnJustPassedAccumulatedOrder) delay = 0;

                    setTimeout(function() {
                        scheduleNextNumberGeneration();
                    }, delay * 1000);
                }

                
            });
        },
        
        bindToClientCards : function() {
            var self = this;
            var ret = new comb.Promise();
            if (comb.isPromiseLike(this.client_cards)) {
                // TODO: this part of this if must be merged with the other part
                this.client_cards.then(function(client_cards) {
                    client_cards.forEach(function(cc) { 
                        //console.log("Bingo.bindToClientCards(): Subscribing to notifications on ClientCard #" + cc.id);
                        cc.addListener('line', comb.hitch(self, "lineNotification"));
                        cc.addListener('bingo', comb.hitch(self, "bingoNotification"));
                        self.addListener('numberCalled', comb.hitch(cc, "pushNumber"));
                    });
                    
                    ret.callback(client_cards);
                });
            }
            else {
                if (this.client_cards) {
                    // TODO: this part of this if must be merged with the other part
                    this.client_cards.forEach(function(cc) {
                        //console.log("Bingo.bindToClientCards(): Subscribing to notifications on ClientCard #" + cc.id);
                        cc.addListener('line', comb.hitch(self, "lineNotification"));
                        cc.addListener('bingo', comb.hitch(self, "bingoNotification"));
                        self.addListener('numberCalled', comb.hitch(cc, "pushNumber"));
                    });
                    
                    ret.callback(this.client_cards);
                }
                else {
                    ret.callback();
                }
            }
            
            
            return ret;
        },
        
        lineNotification : function(clientCard) {
            console.log("Line notified for ClientCard #" + clientCard.id);
            
            this.payLines();
        },

        bingoNotification : function(clientCard) {
            var self = this;
            console.log("Bingo notified for ClientCard #" + clientCard.id);

            self.payBingos();
        },

        payLines : function(someClientCards) {
            /* Line notification handler. Also performs line payout.
             * 
             * Argument client_cards is a seam intended to be used from tests.
             * Although not used at the moment.
             * 
             */
            var self = this;

            // Another operation is in progress 
            if (this.lineCalled) { 
                console.log("Bingo #" + this.id + ": line has already been called");
                return this._payoutPromise;
            };
            
            this.lineCalled = true;
            this.lineCallingStartedAt = new Date();

            /* Stop numbers generation to let front clients perform eye and ear candy */
            this.pause('light');

            /* One SQL transaction will be used for each ClientCard winner. After all payouts
             * have been done, this.lineCalled will be persisted.
             * 
             */
            return this._payPrize('payLine', 'isLineWinner', 'linePrize', 'line', someClientCards).then(
                function(clientCards, winnerLines) {
                    // Calculate when to continue with the bingo
                    var delay = 3; // safety threshold
                    delay += 3; // line_called
                    winnerLines.forEach(function(line) {
                        delay += 4; // verifying line
                        line.forEach(function(number) {
                            /* Per-number delay.
                             * 
                             * All numbers are considered 2 seconds long, Except for those within
                             * range 60-79 which are 3 seconds long.
                             * 
                             * */
                            if ( (60 <= number) && (number <= 79) ) {
                                delay += 3;
                            }
                            else { 
                                delay += 2;
                            }
                        });
                    });
                    
                    delay += 4; // continue to bingo
                    
                    console.log("Calculated delay for line notifications: " + delay);

                    self.lineCallingFinishesAt = new Date(self.lineCallingStartedAt.getTime() + delay * 1000);
                    console.log("Will finish calling at " + self.lineCallingFinishesAt);
                    
                    var delayTimeout = self.lineCallingFinishesAt - self.lineCallingStartedAt;

                    if (self.dontRestartAfterLinePayout) return; // set by tests

                    console.log("Continuing bingo after line notification");

                    if (delayTimeout > 0) {
                        setTimeout(function() {
                            self.start();
                        }, delayTimeout);
                    }                    
                    else {
                        /* This was not intended to happen!! Start the bingo right away!! */
                        self.start();
                    }
                },
                function(error) {
                    // Log error and continue anyway
                    console.log("ERROR on payLine: " + error);
                    console.log("Continuing bingo anyway");
                }
            );
        },

        payBingos : function(someClientCards) {
            /* Performs bingo payout.
             * 
             * Argument client_cards is a seam intended to be used from tests.
             * 
             */
            var self = this;

            if (this.bingoCalled) { 
                console.log("Bingo #" + this.id + ": bingo has already been called");
                return this._payoutPromise;
            };
            
            this.bingoCalled = true;
            this.bingoCallingStartedAt = new Date();
            this.pause('light'); // stop number generation

            return comb.serial([
                function payPrize() {
                    return self._payPrize('payBingo', 'isBingoWinner', 'bingoPrize', 'bingo', someClientCards).then(
                        function(clientCards, winnerNumbers) {
                            // Calculate when to continue with the bingo
                            var delay = 5; // safety threshold
                            delay += 3; // bingo_called
                            winnerNumbers.forEach(function(aCardNumbers) {
                                delay += 4; // verifying bingo
                                aCardNumbers.forEach(function(number) {
                                    /* Per-number delay.
                                     * 
                                     * All numbers are considered 2 seconds long, Except for those within
                                     * range 60-79 which are 3 seconds long.
                                     * 
                                     * */
                                    if ( (60 <= number) && (number <= 79) ) {
                                        delay += 3;
                                    }
                                    else { 
                                        delay += 2;
                                    }
                                });
                            });
                            
                            delay += 4; // finishing bingo speech
                            
                            console.log("Calculated delay for bingo notifications: " + delay);
                            
                            self.bingoCallingFinishesAt = new Date(self.bingoCallingStartedAt.getTime() + delay * 1000);
                            console.log("Will finish at " + self.bingoCallingFinishesAt);
                            
                            var delayTimeout = self.bingoCallingFinishesAt - self.bingoCallingStartedAt;
                            if (self.dontDelayAfterBingoCalling) { // set by tests;
                                self._finish();
                                return;
                            }
                            
                            if (delayTimeout > 0) {
                                setTimeout(function() {
                                    // Actually finish the bingo
                                    self._finish();

                                }, delayTimeout);
                            }                    
                            else {
                                /* This should not happen. End the bingo right away! */
                                self._finish();
                            }
                        },
                        function(error) {
                            // Log error and continue anyway
                            console.log("ERROR on payBingo: " + error);
                            console.log("Finishing bingo anyway");
                        }
                    );
                },

                function accumulatedCheck() {
                    if (self.calledNumbers.length <= self._accumulatedOrder) {
                        /* WE HAVE ACCUMULATED BINGO! */
                        console.log("WE HAVE ACCUMULATED BINGO PRIZE ON #" + self.id + " !!! ");
                        
                        return comb.serial([
                            function lockAccumulatedPrize() {
                                // TODO : SQL lock accumulated prize
                            },
                            comb.hitch(self, "_payPrize", 'payAccumulatedBingo', 'isBingoWinner', '_accumulatedPrize', 'accumulated', someClientCards),
                            function decrementAndUnlockAccumulatedPrize() {
                                // TODO: SQL unlock accumulated prize
                                return self._resetAccumulatedPrize();
                            }
                        ]);
                    }
                }

            ]);

        },

        // increaseBingoWinnersCounter : function () {},

        _payPrize : function(payoutMethod, checkerMethod, prizeProperty, notificationName, someClientCards) {
            /* Pays out a prize by calling methodName() 
             * in the group of winners client cards.
             * 
             * Parameter client_cards is a seam intended to be used from tests;
             */
            var self = this;
            var ret = new comb.Promise();
            this._payoutPromise = ret;
            var calledNumbers = this.calledNumbers;

            var winners = []; // client cards
            var allWinnerData = []; // lines numbers (at the moment)
            this.client_cards.then(
                function(client_cards) {
                    /* SEAM! */
                    client_cards = someClientCards ? someClientCards : client_cards;
                    client_cards.forEach(function(cc) {
                        /* Asking each ClientCard passing calledNumbers as parameter ensures that we
                         * do not need to wait until each one marks the last called number. This way the
                         * line payout can be launched as soon as possible, without waiting for any
                         * ClientCard instance.
                         */
                        var winnerData;
                        eval("winnerData = cc." + checkerMethod + "(calledNumbers)");

                        /* If checkerMethod returns something, we consider it as a true value */
                        if (winnerData) {
                            winners.push(cc);
                            allWinnerData.push(winnerData);
                            
                            // Notify it along with the winnerData
                            self.emit(notificationName, cc, winnerData);
                        }
                    });

                    if (winners.length == 0) {
                        console.log("WARNING: bingo #" + self.id + " _payPrize() issued without any actual winner");
                        ret.callback();
                        return;
                    }

                    var eachCardPrize = eval("self." + prizeProperty) / winners.length;

                    var allReady = [];
                    winners.forEach(function(cc) { 
                        console.log("Bingo." + payoutMethod + "(): Paying out " + eachCardPrize + " to ClientCard #" + cc.id);
                        var cardPayoutReady = eval("cc." + payoutMethod + "(eachCardPrize)");
                        allReady.push(cardPayoutReady);
                    });

                    (new comb.PromiseList(allReady)).promise().then(
                        function() {
                            self.save().then(
                                function() {
                                    console.log("_payPrize() OK!");
                                    ret.callback(winners, allWinnerData);
                                },
                                function(error) {
                                    console.log("_payPrize() ERROR: " + error);
                                    ret.errback();
                                }
                            );
                        },
                        function(error) {
                            console.log("Error when paying out " + payoutMethod + " : " + error);
                            ret.errback(error);
                        }
                    );
                },
                comb.hitch(ret, "errback")
            );
            
            return ret;
        },

        registerNewClient : function() {
            /* Increments clientsCount and sets attribute sellingFinishesAt if necessary */
            this.clientsCount++;
            this.checkSellingCriteriaAndSetFinishesAttribute();
            // this._checkSellingCriteriaAndSetTimeout();
        },
        
        toJSON : function () {
            var json = this._super(arguments);
            
            // Add some calculated attributes
            json['isRunning'] = this.isRunning;
            json['isSelling'] = this.isSelling;
            json['isActive'] = this.isActive;
            json['isPaused'] = this.isPaused;
            json['isLightPaused'] = this.isLightPaused;
            json['isFinished'] = this.isFinished;
            json['isCancelled'] = this.isCancelled;
            json['canStart'] = this.canStart;
            //json['totalClientsCount'] = this.totalClientsCount;
            json['shownTotalClientsCount'] = this.shownTotalClientsCount;
            json['visitorsCount'] = this.visitorsCount;
            json['accumulatedPrize'] = this._accumulatedPrize;
            json['accumulatedOrder'] = this._accumulatedOrder;
            json['maxBall'] = this.maxBall;

            /* Patio hack to overcome getters issue. Other way, values
             * are retrieved without having them converted by getters.
             */
            json['calledNumbers'] = this.calledNumbers;
            delete json['remainingNumbers'];
            
            return json;
        },

        toJSONFull : function () {
            /* Adds to JSON some attributes that must not be undisclosed to Clients */
            var json = this.toJSON();
            json['remainingNumbers'] = this.remainingNumbers;
            json['totalClientsCount'] = this.totalClientsCount;

            return json;
        },

        setTotalClientsCount : function(count) {
            var self = this;
            var ret = new comb.Promise();
            console.log("Setting total clients count to " + count);
            this.totalClientsCount = count;

            var dontSave = this.isNew;

            comb.serial([
                function() {
                    if (dontSave) {
                        ret.callback();
                    }
                    else {
                        self.checkSellingCriteriaAndSetFinishesAttribute();
                        self.save().then(function() {
                            self.setSellingTimerIfNecessary();
                            ret.callback();
                        });
                    }
                }
            ]);

            return ret;
        },

        cancelOneClientCard : function() {
            var self = this;
            /* Decrease line prize, bingo prize and accumulated prize */
            var linePrizeDecrement = this.linePrizeRate * this.cardPrice;
            this.linePrize -= linePrizeDecrement;
            
            var bingoPrizeDecrement = this.bingoPrizeRate * this.cardPrice;
            this.bingoPrize -= bingoPrizeDecrement;
            
            this.clientCardsCount -= 1;
            
            var accumulatedPrizeDecrement = -1 * this._accumulatedRatio * this.cardPrice;

            return comb.serial([
                comb.hitch(self, "_increaseAccumulatedPrize", accumulatedPrizeDecrement),
                comb.hitch(self, "save", {transaction: false})
            ]);
        }
    }
}).as(module);
