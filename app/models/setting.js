var patio = require('patio');
var comb = require('comb');

var Setting = patio.addModel('setting', {
    plugins : [patio.plugins.TimeStampPlugin],
    static :  {
        init : function() {
            this._super(arguments);
            this.timestamp();
        },

        getValue : function(key, forUpdate) {
            var ret = new comb.Promise();
            
            var ds = this.filter({key:key});
            if (forUpdate) {
                ds = ds.forUpdate();
            }
            ds.one().then(
                function(setting) {
                    if (setting) {
                        ret.callback(setting.value);
                    }
                    else {
                        ret.errback();
                    }
                },
                comb.hitch(ret, "errback")
            );

            return ret;
        },

        getAll : function(forUpdate) {
            var ret = new comb.Promise();
            
            var ds = this;

            if (forUpdate) {
                ds = this.forUpdate();
            }
            ds.all().then(
                function(settings) {
                    var res = {};
                    settings.forEach(function(s) {
                        res[s.key] = s.value;
                    });
                    ret.callback(res);
                },
                comb.hitch(ret, "errback")
            );

            return ret;
        },

        setValue : function(key, value, forUpdate) {
            var self = this;
            var ret = new comb.Promise();
            
            var ds = this.filter({key:key});
            if (forUpdate) {
                ds = ds.forUpdate();
            }

            ds.one().then(
                function(setting) {
                    if (setting) {
                        setting.value = value;
                        setting.save().then(
                            comb.hitch(ret, "callback"),
                            comb.hitch(ret, "errback")
                        );
                    }
                    else {
                        var s = new self({key: key, value: value});
                        s.save().then(
                            comb.hitch(ret, "callback"),
                            comb.hitch(ret, "errback")
                        );
                    }
                },
                comb.hitch(ret, "errback")

            );

            return ret;
        }
    },

    instance : {
    }

}).as(module);