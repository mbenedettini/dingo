var patio = require('patio');
var comb = require('comb');
var utils = require('../../lib/utils');

var STATUS = {
    ACTIVE:   0,
    BLOCKED:  1,
    SUSPENDED: 2
};

var MAX_BAD_LOGINS_COUNT = 3;

var Cashier = patio.addModel('cashier', {
    plugins : [patio.plugins.TimeStampPlugin, patio.plugins.ValidatorPlugin],

    static : {
        init : function() {
            this._super(arguments);
            this.timestamp();

            this.oneToMany("clients", {key: "cashierId" });
            this.oneToMany("transactions", {key: "cashierId" });
        },

        authenticate : function(username, password) {
            var ret = new comb.Promise();
            this.filter({username: username}).first().then(
                function(cashier) {
                    if (cashier) {
                        cashier.authenticate(password).then(
                            function(authOk) {
                                if (authOk) {
                                    ret.callback(cashier);
                                }
                                else {
                                    ret.callback(false);
                                }
                            },
                            comb.hitch(ret, "errback")
                        );
                    }
                    else {
                        ret.callback(false);
                    }
                },
                comb.hitch(ret, "errback")
            );

            return ret;
        }
    },

    instance : {
        getters: {
            isActive: function() {
                return this.status == STATUS.ACTIVE;
            },
            isBlocked: function()  {
                return this.status == STATUS.BLOCKED;
            },
            isSuspended: function() {
                return this.status == STATUS.SUSPENDED;
            },
            isNotSuspended: function() {
                return this.status != STATUS.SUSPENDED;
            }
        },
        generatePassword : function() {
            var self = this;
            var ret = new comb.Promise();

            if (this.isNew) {
                return new comb.Promise().errback(new Error("Cannot generate a password for a Cashier that has not been saved yet"));
            }

            var newPassword = utils.generateRandomPassword();

            this._static.db.run("UPDATE cashier SET password = PASSWORD('" + newPassword + "'), status = " + STATUS.ACTIVE + ", badLoginsCount = 0 WHERE id = " + this.id + "  ;").then( 
                function() {
                    // Refresh updates made by the previous statement
                    self.reload().then(
                        comb.hitch(ret, "callback", newPassword),
                        comb.hitch(ret, "errback")
                    );
                },
                comb.hitch(ret, "errback")
            );

            return ret;
        },

        suspend : function() {
            if (this.status == STATUS.SUSPENDED) {
                return new comb.Promise().errback(new Error("Este cajero ya esta suspendido"));
            }

            this.status = STATUS.SUSPENDED;

            return this._suspendOrUnsuspend("isSuspended", "suspend");
        },

        unsuspend : function() {
            if (!this.status == STATUS.SUSPENDED) {
                return new comb.Promise().errback(new Error("Este cajero no esta suspendido"));
            }

            this.status = STATUS.ACTIVE;

            return this._suspendOrUnsuspend("isNotSuspended", "unsuspend");
        },

        _suspendOrUnsuspend : function(property, method) {
            /* Generic suspend/unsuspend method. */
            var self = this;
            var ret = new comb.Promise();

            var alreadySuspendedClients = []; // no use at the moment
            var clientsSuspensionReady = [];

            /* TODO: SQL transaction */
            this.clients.then(
                function(clients) {
                    clients.forEach(function(c) {
                        if (eval("c." + property)) {
                            alreadySuspendedClients.push(c);
                        }
                        else {
                            clientsSuspensionReady.push(eval("c." + method + "()"));
                        }
                    });

                    comb.serial([
                        function() {
                            return comb.when(clientsSuspensionReady);
                        },
                        comb.hitch(self, "save")
                    ]).then(
                        comb.hitch(ret, "callback"),
                        comb.hitch(ret, "errback")
                    );
                }
            );

            return ret;
        },

        authenticate : function(password) {
            /* Returns either true or false on the success callback whether authentication
             * was successful or not.
             * 
             * Error callback is left for other errors and problems.
             */
            var self = this;
            var ret = new comb.Promise();

            this._static.db.run("SELECT COUNT(1) AS auth FROM cashier WHERE id = " + this.id + " AND status = " + STATUS.ACTIVE + " AND password = PASSWORD('" + password + "')").then(
                function(auth_result) {
                    if (auth_result[0]["auth"] == 1) {
                        // SUCCESS
                        if (self.badLoginsCount > 0) {
                            self.badLoginsCount = 0;
                        }

                        self.modified = new Date();

                        self.save().then(
                            function() {
                                ret.callback(true);
                            },
                            comb.hitch(ret, "errback")
                        );
                    }
                    else {
                        // FAILURE
                        // Increase Bad logins count
                        if (self.isActive) {
                            self.badLoginsCount++;
                            if (self.badLoginsCount == MAX_BAD_LOGINS_COUNT) {
                                self.status = STATUS.BLOCKED;
                            }
                            self.save().then(
                                function() {
                                    ret.callback(false);
                                },
                                comb.hitch(ret, "errback")
                            );
                        }
                        else {
                            // Already blocked, nothing to do
                            ret.callback(false);
                        }
                        
                    }
                },
                comb.hitch(ret, "errback")
            );

            return ret;
        },

        toJSON : function() {
            var json = this._super(arguments);

            delete json['password'];
            json['isBlocked'] = this.isBlocked;
            json['isSuspended'] = this.isSuspended;

            return json;
        }
    }
}).as(module);

Cashier.validate(function(validate) {
    validate("username").isNotNull();
    validate("email").isEmail();
});
