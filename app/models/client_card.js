var patio   = require('patio');
var comb    = require('comb');

var ClientCard = patio.addModel('client_card', {
    plugins : [patio.plugins.TimeStampPlugin],
    static : {
        init : function() {
            this._super(arguments);
            this.timestamp();
            
            this.manyToOne("client", { key: "clientId" });
            this.manyToOne("bingo", { key: "bingoId" });
            this.manyToOne("card", { key: "cardId", fetchType: this.fetchType.EAGER });
            this.oneToMany("lastSoldOf", { model: "bingo", key: "lastClientCardSoldId" });
            this.oneToMany("transactions", { key: "clientCardId" });
        },
        
        createPurchase : function(price, client, card, bingo) {
            var c = this.create({
                'price'     : price,
                'client'    : client,
                'card'      : card,
                'bingo'     : bingo
            });
            
            return c;
        },

        getAvailables : function(lastClientCardSold, quantity, series) {
            var self = this;
            var Card = patio.getModel('card');
            var ret = new comb.Promise();

            series = series || false;

            if (series && quantity % 6) return (new comb.Promise()).errback("We'been asked for series and quantity " + quantity + " is not a multiple of six");

            if (this.ENV == 'development') {
                console.log("ClientCard.getAvailables(): development mode");
                Card.filter({number: 0}).all().then(
                    function(cards) {
                        // TODO: clone the single card returned until quantity
                        self._foundCardsToSell(ret, cards, series);
                    },
                    function(error) {
                        console.log("ERROR: ClientCard.getAvailables(): " + error);
                    }
                );
                return ret;
            }

            if (lastClientCardSold) {
                // console.log("Searching with non-null lastClientCardSold");
                // console.log(lastClientCardSold);
                if (!lastClientCardSold.card) return (new comb.Promise()).errback("Inconsistence error: client card #" + lastClientCardSold.id + " is not related with a card instance");
                // Card.filter(function() { return this.number.gt(lastClientCardSold.card.number) }).order("number").limit(quantity).all().then(

                var lastNumber = lastClientCardSold.card.number;

                if (series) {
                    if ( lastNumber % 6 ) {
                        // lastNumber is not the sixth card of a series, let's calculate it
                        lastNumber += (6 - lastNumber % 6);
                    }
                }

                this._findCards(lastNumber, quantity, series).then(
                    comb.hitch(ret, "callback"),
                    comb.hitch(ret, "errback")
                );
            }
            else {
                // This should not happen. It is a special situation that could arise, for instance,
                // for a bingo of a certain price that has never been sold before.
                console.log("ClientCard.getAvailables(): Searching with NULL lastClientCardSold");
                // 
                self._findCardsFromTop(quantity).then(
                    function(cards) {
                        self._foundCardsToSell(ret, cards, series);
                    },
                    comb.hitch(ret, "errback")
                );
            }
                
            return ret;
        },

        _findCards : function(lastSoldNumber, quantity, isSeries) {
            var self = this;
            var Card = patio.getModel('card');
            var ret = new comb.Promise();

            Card.filter({ number : { gt: lastSoldNumber }}).order("number").limit(quantity).all().then(
                function(cards) {
                    if (cards) {
                        // console.log("Non-null search found some cards: " + cards.length);
                        /* Doest the quantity found satisfy the quantity requested? */
                        if (cards.length < quantity) {
                            /* We should retrieve more cards from the beginning */
                            self._findCardsFromTop(quantity - cards.length).then(
                                function(moreCards) {
                                    self._foundCardsToSell(ret, cards.concat(moreCards), isSeries);
                                },
                                comb.hitch(ret, "errback")
                            );      
                        }
                        else {
                            /* We're good to go */
                            self._foundCardsToSell(ret, cards, isSeries);
                        }
                    }
                    else {
                        // console.log("Non-null search didnt found any card. Retrieving from the beginning.");
                        /* We are at bottom of the cards. Start again by selling the first ones. */
                        self._findCardsFromTop(quantity).then(
                            function(cards) {
                                self._foundCardsToSell(ret, cards, isSeries);
                            },
                            comb.hitch(ret, "errback")
                        );
                    }
                },
                comb.hitch(ret, "errback")
            );

            return ret;
        },

        _findCardsFromTop : function(quantity) {
            /* Gets a quantity of cards from the beginning */
            var Card = patio.getModel('card');
            return Card.order("number").limit(quantity).all();
        },

        _foundCardsToSell : function(ret, cards, isSeries) {
            /* Final handler to be called from getAvailables() when all of
             * the requested cards have been found.
             */
            isSeries = isSeries || false;
            if (cards) {
                var clientCards = [];
                cards.forEach(function(card) {
                    var c = new ClientCard({card: card, isSeries: isSeries });
                    clientCards.push(c);
                });
                ret.callback(clientCards);
            }
            else {
                ret.errback();
            }
        },

        setENV : function(env) {
            this.ENV = env;
        }

    },
    
    pre : {
        "save" : function(next) {
            var Transaction = patio.getModel('transaction');
            var t = Transaction.createCardPurchase(this.price ,"Bingo information");
            if (comb.isPromiseLike(this.client)) {
                this.client.then(function(client) {
                    t.client = client;
                    t.save(null, {transaction: false} ).then(
                        function() {
                            next();
                        },
                        function(error) {
                            console.log("ERROR on ClientCard.preSave : " + error);
                            next();
                        }
                    );
                });
            }
            else {
                t.client = this.client;
                t.save(null, {transaction: false} ).then(
                        function() {
                            next();
                        },
                        function(error) {
                            console.log("ERROR on ClientCard.preSave : " + error);
                            next();
                        }
                    );
            }
        }
    },
    
    instance : {
        constructor: function(options, fromDb) {
            options = options || {};
            this._super(arguments);
            
            if (! fromDb) {
                // Initialize remaining and called numbers
                this.firstLineCalled = [];
                this.secondLineCalled = [];
                this.thirdLineCalled = [];
            }
        },
        
        /* Called numbers on each line */
        _getFirstLineCalled : function(value) {
            if (value) {
                return value.split(",").map(function(strNumber) { return parseInt(strNumber, 10); } );
            }
            else {
                return [];
            }
        },
        
        _getSecondLineCalled : function(value) {
            if (value) {
                return value.split(",").map(function(strNumber) { return parseInt(strNumber, 10); } );
            }
            else {
                return [];
            }
        },
        
        _getThirdLineCalled : function(value) {
            if (value) {
                return value.split(",").map(function(strNumber) { return parseInt(strNumber, 10); } );
            }
            else {
                return [];
            }
        },
        
        getters : { 
            /* Each line of this card, borrowed from this.card */
            firstLine : function() {
                // If not yet completely initialized, this.card might be null.
                return this.card ? this.card.firstLine : null;
            },
            secondLine : function() {
                return this.card ? this.card.secondLine : null;
            },
            thirdLine : function() {
                return this.card ? this.card.thirdLine : null;
            },

            firstLineNumbers : function() {
                return this.card ? this.card.firstLineNumbers : null;
            },
            secondLineNumbers : function() {
                return this.card ? this.card.secondLineNumbers : null;
            },
            thirdLineNumbers : function() {
                return this.card ? this.card.thirdLineNumbers : null;
            },

            number : function() {
                return this.card ? this.card.number : null;
            }

        },
        
        pushNumber : function(number) {
            var self = this;

            // Should not happen: a big big problem
            if (!this.card || ! this.id) {
                console.log("ClientCard.pushNumber(): Likely a scope problem here!");
                return;
            }

            // console.log("clientCard #" + this.id + " : receiving number " + number);
            var lines = [
                { lineName: 'first', numbers: 'firstLineNumbers', calledNumbers: 'firstLineCalled' },
                { lineName: 'second', numbers: 'secondLineNumbers', calledNumbers: 'secondLineCalled' },
                { lineName: 'third', numbers: 'thirdLineNumbers', calledNumbers: 'thirdLineCalled' }
            ];
            
            var i;
            var completeLines = 0;
            var saveMe = false;
            var lineCalled = null;
            
            for (i in lines) {
                var line = lines[i];
                
                var numbers = eval("this." + line.numbers);
                if (!numbers) { console.log("UNDEFINED line.numbers"); }

                var calledNumbers = eval("this." + line.calledNumbers);
                if (!calledNumbers) { console.log("UNDEFINED line.calledNumbers"); }
                
                // Already complete line
                if (calledNumbers.length == numbers.length) {
                    completeLines++;
                    continue;
                }
                
                if ((numbers.indexOf(number) >= 0) &&  (calledNumbers.indexOf(number) == -1 )) {
                    // We have a number to mark!
                    //~ console.log("Marking number " + number);
                    calledNumbers.push(number);
                    
                    // Add the number to the called numbers of this line
                    // this.{lineName}LineCalled
                    eval("this." + line.calledNumbers + " = calledNumbers");
                    
                    // Mark the flag so that we are persisted
                    saveMe = true;
                    
                    // Check if this line has been completed
                    //~ calledNumbers = eval(line.calledNumbers);
                    if (calledNumbers.length == numbers.length) {
                        // Tell everybody we have a line here!
                        lineCalled = calledNumbers;
                        completeLines++;
                        console.log("Line #" + i + " completed on ClientCard #" + self.id);
                    }
                    
                }
                
            }
            
            if (saveMe) {
                this.save().then(function() {
                    if (completeLines == lines.length) {
                        // BINGO!
                        console.log("BINGO! on ClientCard#" + self.id);
                        self.emit("bingo", self);
                        return;
                    }
                    else if (lineCalled) {
                        console.log("LINE! on ClientCard#" + self.id + " with numbers " + lineCalled);
                        self.emit("line", self, lineCalled);
                    }
                });
            }
        },
        
        payLine : function(amount) {
            var self = this;
            var ret = new comb.Promise();

            console.log("paying out line to cc #" + this.id);

            // Do not pay again
            if (this.wonLine) {
                return ret.callback();
            }

            this.db.transaction(function() {
                self.reload().then(
                    function() {
                        self.wonLine = true;
                        self.client.then(
                            function(client) {
                                comb.serial([
                                    comb.hitch(client, "payLine", amount, self),
                                    comb.hitch(self, "save", null, {transaction: false} ),
                                    comb.hitch(client, "save", null, {transaction: false} )
                                ]).then(comb.hitch(ret, "callback"), comb.hitch(ret, "errback"));
                            },
                            comb.hitch(ret, "errback")
                        );
                    },
                    comb.hitch(ret, "errback")
                );
                
                return ret;
            });

            return ret;
        },

        payBingo : function(amount) {
            var self = this;
            var ret = new comb.Promise();

            console.log("paying out bingo to cc #" + this.id);

            // Do not pay again
            if (this.wonBingo) {
                return ret.callback();
            }

            this.db.transaction(function() {
                self.reload().then(
                    function() {
                        self.wonBingo = true;
                        self.client.then(
                            function(client) {
                                comb.serial([
                                    comb.hitch(client, "payBingo", amount, self),
                                    comb.hitch(self, "save", null, {transaction: false} ),
                                    comb.hitch(client, "save", null, {transaction: false} )
                                ]).then(comb.hitch(ret, "callback"), comb.hitch(ret, "errback"));
                            },
                            comb.hitch(ret, "errback")
                        );
                    },
                    comb.hitch(ret, "errback")
                );
                
                return ret;
            });

            return ret;
        },

        payAccumulatedBingo : function(amount) {
            var self = this;
            var ret = new comb.Promise();

            console.log("Paying out accumulated bingo to cc #" + this.id);

            // Do not pay again
            if (this.wonAccumulated) {
                return ret.callback();
            }

            this.db.transaction(function() {
                self.reload().then(
                    function() {
                        self.wonAccumulated = true;
                        self.client.then(
                            function(client) {
                                comb.serial([
                                    comb.hitch(client, "payAccumulatedBingo", amount, self),
                                    comb.hitch(self, "save", null, {transaction: false} ),
                                    comb.hitch(client, "save", null, {transaction: false} )
                                ]).then(comb.hitch(ret, "callback"), comb.hitch(ret, "errback"));
                            },
                            comb.hitch(ret, "errback")
                        );
                    },
                    comb.hitch(ret, "errback")
                );
                
                return ret;
            });

            return ret;
        },



        isLineWinner : function(someCalledNumbers) {
            /* Returns either true or false if it is a line winner (any of its line is complete)
             * given a certain array of called numbers.
             */
            var winnerData = null;
            var calledNumbers = someCalledNumbers.slice().sort(); // slice() copies the array
            [this.firstLineNumbers, this.secondLineNumbers, this.thirdLineNumbers].forEach(function(line) {
                var i;
                var markedNumbersCounter = 0;
                for(i=0; i < line.length; i++) {
                    if (calledNumbers.indexOf(line[i]) >= 0) {
                        markedNumbersCounter++;

                        if (markedNumbersCounter == line.length) {
                            // console.log("CC #" + self.id + ": line '" + l + "' is winner with called numbers " + calledNumbers);
                            winnerData = line;
                            break;
                        }
                    }
                }
            });

            return winnerData;
        },

        isBingoWinner : function(someCalledNumbers) {
            /* Returns either true or false if it is a bingo winner (all of these nombers have been
             * called) given a certain calledNumbers array.
             */
            var markedNumbersCounter = 0;
            var calledNumbers = someCalledNumbers.slice().sort();
            var totalNumbers = this.firstLineNumbers.length + this.secondLineNumbers.length + this.thirdLineNumbers.length;

            // Shortcut
            if (calledNumbers.length < totalNumbers.length) {
                return null;
            }

            [this.firstLineNumbers, this.secondLineNumbers, this.thirdLineNumbers].forEach(function(l) {
                var line = l; 
                line.sort(); // just in case
                var i;
                for(i=0; i < line.length; i++) {
                    if (calledNumbers.indexOf(line[i]) >= 0) {
                        markedNumbersCounter++;
                    }
                }
            });

            var winnerData = null;

            if (markedNumbersCounter == totalNumbers) {
                // We have bingo!
                winnerData = this.firstLineNumbers.concat(this.secondLineNumbers, this.thirdLineNumbers);
            }

            return winnerData;
        },

        cancel : function () {
            var self = this;
            if (this.cancelled) return (new comb.Promise()).callback();
            var Client = patio.getModel('client');
            var Transaction = patio.getModel('transaction');
            var Bingo = patio.getModel('bingo');

            var amount = this.price;
            var ret = new comb.Promise();


            /* SQL TX */
            this.db.transaction({isolation: "serializable"}, function() {
                var reloadClient = Client.forUpdate().first({id: self.clientId});
                var reloadClientCard = self._static.forUpdate().first({id: self.id});
                var reloadBingo = Bingo.forUpdate().first({id: self.bingoId});
                new comb.PromiseList([reloadClient, reloadClientCard, reloadBingo]).then(
                    function(args) {
                        /* I figured out args structure by looking at comb.PromiseList tests */
                        var client = args[0][1];
                        var clientCard = args[1][1];
                        var bingo = args[2][1];
                        clientCard.cancelled = true;
                        var t = Transaction.createCardRefund(amount, "Devolucion por carton " + self.number);
                        t.client = client;
                        /* Refund client */
                        client.refund(amount);

                        console.log("Cancelling client card #" + clientCard.id);
                        comb.when(
                            t.save(null, {transaction: false}),
                            client.save(null, {transaction: false}),
                            clientCard.save(null, {transaction: false}),
                            bingo.cancelOneClientCard()
                            /* bingo.processClientCardCancellation() takes care of saving the bingo instance.
                             * We can't do it here because it requires a serial operation between
                             * decreasing the accumulated prize and actually saving the instance
                             * and doing that here would be to add to ClientCard class the burden of
                             * knowing some Bingo class internals.
                             * 
                             * TODO: Following that principle, client.refund() should take care
                             * of saving the client instance.
                             */
                        ).then(
                            comb.hitch(ret, "callback"),
                            comb.hitch(ret, "errback")
                        );
                    },
                    function(error) {
                        console.log("ERROR on ClientCard.cancel(): " + error);
                        ret.errback(error);
                    }
                );
                return ret;
            });
            /* END OF SQL TX */

            return ret;
        },

        toJSON : function () {
            var json = this._super(arguments);

            /* Patio toJSON() doesn't use getters. Instead uses
             * internal values. Hack until fixed. Otherwise these attributes
             * do not appear in the JSON representation.
             */
            
            json['firstLine'] = this.firstLine;
            json['secondLine'] = this.secondLine;
            json['thirdLine'] = this.thirdLine;

            json['firstLineNumbers'] = this.firstLineNumbers;
            json['secondLineNumbers'] = this.secondLineNumbers;
            json['thirdLineNumbers'] = this.thirdLineNumbers;

            json['number'] = this.number;

            json['firstLineCalled'] = this.firstLineCalled;
            json['secondLineCalled'] = this.secondLineCalled;
            json['thirdLineCalled'] = this.thirdLineCalled;
            
            
            return json;
        }
    }
    
}).as(module);
