var patio = require('patio');

var TYPES = {
    DEPOSIT         : 1,
    WITHDRAWAL      : 2,
    CARD_PURCHASE   : 3,
    LINE_PAYOUT     : 4,
    BINGO_PAYOUT    : 5,
    CARD_REFUND     : 6,
    ACCUMULATED_PAYOUT: 7
};

var Transaction = patio.addModel('transaction', {
    plugins : [patio.plugins.TimeStampPlugin],
    static : {
        init : function() {
            this._super(arguments);
            
            this.timestamp();
            this.manyToOne("client", { key: "clientId" });
            this.manyToOne("client_card", { key: "clientCardId" });
            this.manyToOne("cashier", {key: "cashierId" });
        },

        createDeposit: function(amount, description) {
            var t = this.create({
                'description'   : description,
                'amount'        : amount,
                'type'          : TYPES.DEPOSIT
            });
            
            return t;
        },

        createWithdrawal: function(amount, description) {
            var t = this.create({
                'description'   : description,
                'amount'        : amount,
                'type'          : TYPES.WITHDRAWAL
            });
            
            return t;
        },

        createCardPurchase: function(amount, description) {
            var t = this.create({
                'description'   : description,
                'amount'        : amount,
                'type'          : TYPES.CARD_PURCHASE
                //~ 'client'        : client
            });
            
            return t;
        },

        createCardRefund: function(amount, description) {
            var t = this.create({
                'description'   : description,
                'amount'        : amount,
                'type'          : TYPES.CARD_REFUND
            });
            
            return t;
        },
        
        createLinePayout: function(amount, clientCard) {
            return this.create({
                'description' : 'Pago de linea de carton ' + clientCard.number,
                'amount'      : amount,
                'type'        : TYPES.LINE_PAYOUT,
                'clientCard'  : clientCard
            });
        },

        createBingoPayout: function(amount, clientCard) {
            return this.create({
                'description' : 'Pago de bingo de carton ' + clientCard.number,
                'amount'      : amount,
                'type'        : TYPES.BINGO_PAYOUT,
                'clientCard'  : clientCard
            });
        },

        createAccumulatedBingoPayout: function(amount, clientCard) {
            return this.create({
                'description' : 'Pago de bingo acumulado de carton ' + clientCard.number,
                'amount'      : amount,
                'type'        : TYPES.ACCUMULATED_PAYOUT,
                'clientCard'  : clientCard
            });
        }
        
    },
}).as(module);

