var patio = require('patio');
var comb = require('comb');

var clientTable = function () {
    this.primaryKey("id");
    this.name(String, {allowNull: false});
    this.lastName(String, {allowNull: false});
    this.balance(patio.sql.Float, {allowNull: false, default: 0});
    //
    this.created(patio.sql.TimeStamp, {allowNull:false, default: '0000-00-00 00:00:00' });
    this.updated(patio.sql.TimeStamp, {allowNull:true});
};


var transactionTable = function () {
    this.primaryKey("id");
    this.description(String, {allowNull: false, default: ""});
    this.amount(patio.sql.Float, {allowNull: false});
    this.type(Number, {allowNull: false});
    this.foreignKey("clientId", "client", {key:"id"});
    this.foreignKey("clientCardId", "client_card", {key:"id"});
    //
    this.created(patio.sql.TimeStamp, {allowNull:false, default: '0000-00-00 00:00:00' });
    this.updated(patio.sql.TimeStamp, {allowNull:true});
};

var bingoTable = function () {
    this.primaryKey("id");
    this.description(String, {allowNull: false, default: ""});
    this.starts(patio.sql.TimeStamp, {allowNull:true, default: null });
    this.cardPrice(patio.sql.Decimal, {allowNull: false, default: 0});
    this.cardColor(Number, {allowNull: false});
    this.currentState(Number, {allowNull: false, default: 0});
    this.lapseBetweenNumbers(Number, {allowNull: false, default: 5});
    this.linePrize("numeric(10,2)", {allowNull: false, default: 0});
    this.linePrizeRate(patio.sql.Float, {allowNull: false, default: 0});
    this.lineCalled(Boolean, {allowNull: false, default: false});
    this.bingoPrize("numeric(10,2)", {allowNull: false, default: 0});
    this.bingoPrizeRate(patio.sql.Float, {allowNull: false, default: 0});
    this.calledNumbers("TEXT", {allowNull: false, default: ""});
    this.remainingNumbers("TEXT", {allowNull: false, default: ""});
    this.foreignKey("lastClientCardSoldId", "client_card", {key:"id"});
    //
    this.created(patio.sql.TimeStamp, {allowNull:false, default: '0000-00-00 00:00:00' });
    this.updated(patio.sql.TimeStamp, {allowNull:true});
};

var cardTable = function() {
    this.primaryKey("id");
    this.series(Number, {allowNull: false});
    this.number(Number, {allowNull: false});
    this.firstLine(String, {allowNull: false});
    this.secondLine(String, {allowNull: false});
    this.thirdLine(String, {allowNull: false});
    //
    this.created(patio.sql.TimeStamp, {allowNull:false, default: '0000-00-00 00:00:00' });
    this.updated(patio.sql.TimeStamp, {allowNull:true});
};

var clientCardTable = function() {
    this.primaryKey("id");
    this.price(patio.sql.Decimal, {allowNull: false, default: 0});
    this.firstLineCalled(String, {allowNull: false, default: ""});
    this.secondLineCalled(String, {allowNull: false, default: ""});
    this.thirdLineCalled(String, {allowNull: false, default: ""});
    this.foreignKey("clientId", "client", {key:"id"});
    this.foreignKey("bingoId", "bingo", {key:"id"});
    this.foreignKey("cardId", "card", {key:"id"});
    //
    this.created(patio.sql.TimeStamp, {allowNull:false, default: '0000-00-00 00:00:00' });
    this.updated(patio.sql.TimeStamp, {allowNull:true});
};

exports.clientTable         = clientTable;
exports.transactionTable    = transactionTable;
exports.bingoTable          = bingoTable;
exports.cardTable           = cardTable;
exports.clientCardTable     = clientCardTable;
