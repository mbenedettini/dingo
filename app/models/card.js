var patio   = require('patio');
var comb    = require('comb');

/*
 * Development card:
 * 
 * insert into card values (3841, 0, 0, '1,2,3,4,5,-1-1-1-1', '6,7,8,9,10,-1,-1,-1,-1', '11,12,13,14,15,-1,-1,-1,-1', now(), null);
 */

var Card = patio.addModel('card', {
    plugins : [patio.plugins.TimeStampPlugin],
    static : {
        init : function() {
            this._super(arguments);
            this.timestamp();
            
            this.oneToMany("client_cards", {key: "cardId"} );
        }
    },
    
    instance : {
        /* Full line getters: include -1 which are blank spaces */
        _getFirstLine : function(value) {
            if (value) {
                return value.split(",").map(function(strNumber) { return parseInt(strNumber, 10); } );    
            }
            else {
                return [];
            }
        },

        _getSecondLine : function(value) {
            if (value) {
                return value.split(",").map(function(strNumber) { return parseInt(strNumber, 10); } );
            }
            else {
                return [];
            }
        },

        _getThirdLine : function(value) {
            if (value) {
                return value.split(",").map(function(strNumber) { return parseInt(strNumber, 10); } );
            }
            else {
                return [];
            }
        },

        getters : {
            /* ____LineNumbers getters do not include negative numbers, as they are blank spaces */
            firstLineNumbers : function() {
                return this.firstLine.filter(function(n) { return n >= 0; });
            },

            secondLineNumbers : function() {
                return this.secondLine.filter(function(n) { return n >= 0; });
            },

            thirdLineNumbers : function() {
                return this.thirdLine.filter(function(n) { return n >= 0; });
            }
        }
    }
    
}).as(module);
