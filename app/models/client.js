var patio = require('patio');
var comb = require('comb');
var utils = require('../../lib/utils');

var MAX_LOGIN_TRY = 3;

var STATUS = {
    ACTIVE:   0,
    BLOCKED:  1,
    SUSPENDED: 2
};

var Client = patio.addModel('client', {
    plugins : [patio.plugins.TimeStampPlugin, patio.plugins.ValidatorPlugin],

    pre : {
        "save" : function(next){
            var self = this;

            if (this.isNew) {
                self._encryptPassword(next);
            }
            else {
                next();
            }
        }
    },
    
    static : {
        init : function() {
            this._super(arguments);
            this.timestamp();
            this.oneToMany("transactions", {key: "clientId", order: [patio.sql.identifier("id").desc()]} );
            this.oneToMany("client_cards", {key: "clientId", order: [patio.sql.identifier("created").desc()]} );
            this.manyToOne("cashier", {key: "cashierId"});
        },
        
        authenticate : function(username,password) {
            var self = this;
            var ret = new comb.Promise();
            Client.db.run("select COUNT(*) as auth, id FROM client WHERE username = '"+ username +"' AND password = PASSWORD('" + password + "') AND confirmed = true AND status = " + STATUS.ACTIVE).then( 
                function(auth_result){
                  if (auth_result[0]["auth"] == 1) {
                      Client.filter({id : auth_result[0]["id"]}).one().then(
                          function(client){
                              if (client) {
                                  client.loginSuccess().then(
                                      function() {
                                          ret.callback(client);
                                      }
                                  );
                              }
                              else {
                                  console.log("NOT Found!. Something really wrong happen! We authenticate a user but we could not retreive the mofel object");
                                  ret.errback("Something really wrong happen! We authenticate a user but we could not retreive the mofel object");
                              }
                          }
                      )
                  } else {
                      // por alguna razon no fuimos autenticados. si el usuario existe, tomar medidas de seguridad. Sino, tirar un warn o cosa asi ...
                      Client.filter({username: username}).one().then(
                          function(client) {
                              if (client) {
                              // se trato de acceder con un cliente existente
                                  client.loginFail().then(
                                      function() {
                                          console.log("Wrong username or password");
                                          ret.errback("Wrong username or password");
                                      }
                                  );
                              } else {
                                  console.log("Wrong username or password");
                                  ret.errback("Wrong username or password");                                
                              }
                          },
                          function(error) {
                              console.log("Wrong username or password 2");
                              ret.errback("Wrong username or password");
                          }
                      );
                  }
                },
                function(err){
                  console.log("FATAL ERROR! " + err );
                  ret.errback("Fatal Error" + err);
                }
            );
            return ret
        }
    },
    instance : {
        makeDeposit : function(amount, description, cashierId) {
            var Transaction = patio.getModel('transaction');
            var ret = new comb.Promise();
            var self = this;

            if (this.cashierId && cashierId && this.cashierId != cashierId) {
                return new comb.Promise().errback(new Error("El cajero no corresponde"));
            }

            this.db.transaction(function() {
                self.reload().then(
                    function() {
                        var t = Transaction.createDeposit(amount, description);
                        t.client = self;
                        if (cashierId) t.cashierId = cashierId;
                        self.balance += amount;
                        
                        comb.serial([
                            comb.hitch(t, "save", null, {transaction: false}),
                            comb.hitch(self, "save", null, {transaction: false})
                        ]).then(comb.hitch(ret, "callback", t), comb.hitch(ret, "errback"));
                    },
                    comb.hitch(ret, "errback")
                );
                
                // This return is extremely important: a promise that will be
                // fulfilled when we are done with what we want to do within the transaction.
                return ret;
            });
            
            return ret;
        },
        
        makeWithdrawal : function(amount, description, cashierId) {
            var Transaction = patio.getModel('transaction');
            var ret = new comb.Promise();
            var self = this;
            
            if (this.cashierId && cashierId && this.cashierId != cashierId) {
                return new comb.Promise().errback(new Error("El cajero no corresponde"));
            }

            this.db.transaction(function() {
                self.reload().then(
                    function() {
                        var t = Transaction.createWithdrawal(amount, description);
                        t.client = self;
                        if (cashierId) t.cashierId = cashierId;
                        self.balance -= amount;
                        
                        comb.serial([
                            comb.hitch(self, "save", null, {transaction: false}),
                            comb.hitch(t, "save", null, {transaction: false})
                        ]).then(comb.hitch(ret, "callback", t), comb.hitch(ret, "errback"));
                    },
                    comb.hitch(ret, "errback")
                );
                
                return ret;
            });
            
            return ret;
        },

        refund : function(amount) {
            /* Just refunds the passed amount. No tx of any type is created here. */
            this.balance += amount;
        },

        buyCards : function(bingo, quantity, series) {
            /* Buys a determined quantity of cards for a bingo */
            /* quantity is optional, defaults to 1 */
            var self = this;
            var ret = new comb.Promise();
            
            if (bingo.isNew) {
                console.log("Client.buyCards(): Persisting bingo");
                bingo.persist().then(
                    function() {
                        console.log("Client.buyCards(): Actually buying cards");
                        self._buyCards(bingo, quantity, series).then(
                            comb.hitch(ret, "callback"),
                            comb.hitch(ret, "errback")
                        );
                    },
                    comb.hitch(ret, "errback")
                );
            }
            else {
                self._buyCards(bingo, quantity, series).then(
                    comb.hitch(ret, "callback"),
                    comb.hitch(ret, "errback")
                );
            }

            return ret;
        },
        
        _buyCards: function(bingo, quantity, series) {
            var self = this;
            var ret = new comb.Promise();
            var txReady = new comb.Promise();
            
            quantity = quantity || 1;
            series = series || false;

            var freshBingo;
            var freshClient;

            this.db.transaction({isolation:"serializable"}, function() {
                comb.serial([
                    /* Reload both client and bingo with FOR UPDATE  so that we get an exclusive
                     * lock on them. Bingo will get a few attributes updated (lastClientCardSold, 
                     * clientCardsCount, etc.) and Client mainly its balance.
                     */
                    function() {
                        var ret = new comb.Promise();
                        //console.log("Client #" + self.id + " : acquiring exclusive lock on ourselves");
                        self._static.forUpdate().filter({id: self.id}).one().then(
                            function(aClient) {
                                freshClient = aClient;
				ret.callback();
                            }
                        );

                        return ret;
                    },
                    function() {
                        var ret = new comb.Promise();
                        var Bingo = patio.getModel('bingo');
                        //console.log("Client #" + self.id + " : acquiring exclusive lock on bingo");
                        Bingo.forUpdate().filter({id: bingo.id}).one().then(
                            function(aBingo) {
                                console.log("Client #" + self.id + " : exclusive lock on bingo has been acquired!");
                                freshBingo = aBingo;
				ret.callback();
                            }
                        );

                        return ret;
                    },

                    function() {
                        /* Determine if we have already bought cards for this bingo 
                        /* If not, tell bingo so that it can be aware.
                         */
                        var countIncreaseReady = new comb.Promise();
                        var ClientCard = patio.getModel('client_card');
                        //console.log("Client #" + self.id + " : determining if we already purchased on this bingo");
                        ClientCard.filter({clientId: self.id, bingoId: bingo.id}).count().then(
                            function(count) {
                                if (!count) {
                                    freshBingo.registerNewClient();
                                }
                                countIncreaseReady.callback();
                            },
                            comb.hitch(countIncreaseReady, "errback")
                        );
                        return countIncreaseReady;
                    },
                    function() {
                        /* Actually buy the cards */
                        var purchaseReady = new comb.Promise();
                        //console.log("Client #" + self.id + " : actually purchasing cards");
                        freshBingo.buyClientCards(quantity, series).then(
                            function(clientCards) {
                                clientCards.forEach(function(c) {
                                    c.client = freshClient;
                                });
                                
                                var toExecute = [];
                                var totalAmount = 0;

                                clientCards.map(function(c) { 
                                    totalAmount += c.price ;
                                    toExecute.push(comb.hitch(c, "save", null, {transaction: false}));
                                });
                                freshClient.balance -= totalAmount;
                                
                                
                                // This check could be done before, but the Client would have to know how
                                // to calculate the total amount before asking for cards
                                if (freshClient.balance < 0) {
                                    purchaseReady.errback("No alcanza el saldo para realizar la compra");
                                    return;
                                }

                                toExecute.push(comb.hitch(freshClient, "save", null, {transaction: false}));
                                toExecute.push(comb.hitch(freshBingo, "save", null, {transaction: false}));
                                
                                comb.serial(toExecute).then(
                                    comb.hitch(purchaseReady, "callback", clientCards),
                                    comb.hitch(purchaseReady, "errback")
                                );
                            },
                            comb.hitch(purchaseReady, "errback")
                        );
                        return purchaseReady;
                    }
                ]).then(

                    function(results) {
                        var clientCards = results.pop(); // Last item is our wanted clientCards
                        txReady.callback(clientCards);
                    },
                    comb.hitch(txReady, "errback")
                );

                /* Returning this promise is important as it will finish the sql transaction. */
                return txReady;
            });

            txReady.then(
                function(clientCards) {
                    //console.log("Client #" + self.id + " : finished sql transaction. reloading bingo.");
                    bingo.reload().then(function() {
                        bingo.notifyCardPurchasing();
                        ret.callback(clientCards);
                    });
                },
                comb.hitch(ret, "errback")
            );

            return ret;
        },
        
        _setBalance : function(newBalance) {
            return (Math.round((newBalance) * 100) / 100);
        },
        
        _getBalance : function(balance) {
            return (Math.round((balance) * 100) / 100);
        },

        buyCard : function(card, bingo) {
            /* This method is not quite useful at the moment. Consider deprecating it. */
            var self = this;
            var Transaction = patio.getModel('transaction');
            var ClientCard = patio.getModel('client_card');
            var ret = new comb.Promise();
            
            if (! card) {
                console.log("Card is undefined!");
                return new comb.Promise().errback(new Error("card is undefined"));
            }
            
            this.db.transaction(function() {
                self.reload().then(
                    function() {
                        var clientCard = ClientCard.createPurchase(bingo.cardPrice, self, card, bingo);
                        self.balance -= bingo.cardPrice;
                    
                        comb.serial([
                            clientCard.save(null, {transaction: false} ),
                            self.save(null, {transaction: false} )
                        ]).then(comb.hitch(ret, "callback", clientCard), comb.hitch(ret, "errback"));
                    },
                    comb.hitch(ret, "errback")
                );
                
                return ret;
            });
            
            return ret;
        },

        payLine : function(amount, clientCard) {
            /* Pays out amount to this client. Creates a transaction, which is
             * associated to the clientCard, to reflect the operation.
             */

            /*
             * TODO: MAKE SURE WE ARE INSIDE AN SQL TRANSACTION
             */
            var Transaction = patio.getModel('transaction');
            var t = Transaction.createLinePayout(amount, clientCard);
            t.client = this;
            this.balance += amount;

            return t.save(null, {transaction: false});
        },

        payBingo : function(amount, clientCard) {
            /* Pays out a desired amount in concept of bingo winnings */
            /*
             * TODO: MAKE SURE WE ARE INSIDE AN SQL TRANSACTION
             */
            var Transaction = patio.getModel('transaction');
            var t = Transaction.createBingoPayout(amount, clientCard);
            t.client = this;
            this.balance += amount;

            return t.save(null, {transaction: false});
        },

        payAccumulatedBingo : function(amount, clientCard) {
            /* Pays out amount to this client in concept of accumulated prize. 
             * Creates a transaction.
             */

            /*
             * TODO: MAKE SURE WE ARE INSIDE AN SQL TRANSACTION
             */
            var Transaction = patio.getModel('transaction');
            var t = Transaction.createAccumulatedBingoPayout(amount, clientCard);
            t.client = this;
            this.balance += amount;

            return t.save(null, {transaction: false});
        },

        returnCards : function(bingo, howMany, series) {
            /* This method could be part of a Facade pattern, avoiding the bingo
             * instance as parameter, decoupling Bingo from Client.
             * 
             * Anyway, at the moment there are other points of coupling between them.
             */
            var self = this;
            var ret = new comb.Promise();
            var ClientCard = patio.getModel('client_card');

            series = series || false;
            if (series) howMany = howMany * 6;

            /* SELECT * FROM client_card WHERE bingoId = ? AND clientId = ? AND cancelled = false AND isSeries = ? ORDER BY id DESC LIMIT ? */
            ClientCard.filter({bingoId: bingo.id, clientId: this.id, cancelled: false, isSeries: series }).order("id").reverse().limit(howMany).all().then(
                function(clientCards) {
                    console.log("Cancelling the last " + clientCards.length + " client cards for client #" + self.id + " and bingo #" + bingo.id);
                    var cancelAll = [];
                    clientCards.forEach(function(c) {
                        cancelAll.push(comb.hitch(c, "cancel"));
                    });
                    
                    comb.serial(cancelAll).then(
                        comb.hitch(ret, "callback", clientCards),
                        comb.hitch(ret, "errback")
                    );
                },
                comb.hitch(ret, "errback")
            );

            return ret;
        },

        loginFail : function(){
            var self = this;
            var ret = new comb.Promise();          
            
            this.try += 1;

            if (this.try >= MAX_LOGIN_TRY) {
                this._blockIfUnblocked();
            }

            this.save().then( function () {
                ret.callback( true );
            });
            
            return ret;
        },

        loginSuccess : function(){
            var self = this;
            var ret = new comb.Promise();          
            
            this.try = 0;
            this._unblockIfBlocked();

            this.save().then(function () {
                ret.callback( true );
            });
            
            return ret;
        },

        setConfirmationToken: function() {
            var self = this;        

            self.confirmed = false;
            var token = utils.generateRandomPassword();
            self.confirmationToken = token;

            return token;
        },
        
        confirm: function (){
            var self = this;
            var ret = new comb.Promise();          
            
            this.confirmed = true;
            this.confirmationToken = "";

            this.save().then( function () {
                ret.callback( true );
            });
            
            return ret;          
        },
        
        changePassword : function(old_password, new_password){
            var self = this;
            var ret = new comb.Promise();
            
            if (! old_password || ! new_password ) {
                console.log("old password or new password is undefined!");
                return new comb.Promise().errback(new Error("Old or new password is undefined"));
            }
            
            comb.serial([ 
                function () {
                    return Client.authenticate(self.username,old_password);
                },
                function(){
                    var passwordReady = new comb.Promise();
                    self._storePassword(new_password).then( 
                        function() {
                            self._manualReload(self).then(
                                comb.hitch(passwordReady, "callback"),
                                comb.hitch(passwordReady, "errback")
                            );
                    });
                    return passwordReady;
                }
            ]).then(
                function(results){             
                    ret.callback(results);
                },
                function(error) {
                    console.log("Client.changePassword error: " + error);
                    ret.errback(error);
                }
            );
            
            return ret;
        },
        
        resetPassword : function(){
            var self = this;
            var ret = new comb.Promise();
            
            var new_password = utils.generateRandomPassword();

            self._storePassword(new_password).then( 
                function() {
                    self._manualReload().then(
                        function() {
                            
                            self.try = 0;
                            self._unblockIfBlocked();

                            self.save().then(
                                function() {
                                    ret.callback(new_password);
                                },
                                comb.hitch(ret, "errback")
                            );
                        },
                        comb.hitch(ret, "errback")
                    );
                },
                function (err) {
                    ret.errback(err);
              }
            );
            return ret;
        },
        
        _storePassword : function(new_password){
            var self = this;
            var ret = new comb.Promise();
            // console.log("_storePassword called");
            
            Client.db.run("UPDATE client set password = PASSWORD('" + new_password + "') where id = " + self.id + "  ;").then( function(){
                    ret.callback(new_password);
                }, 
                function(err){
                    console.log("Critical Error. Password could not be stored. " + err);
                    ret.errback("Critical Error. Password could not be stored. " + err);               
            });
            
            return ret;
        },

        _manualReload : function (attr) {
            var self = this;
            var ret = new comb.Promise();
            Client.db.run("select password as pass_crypt from client where id = " + self.id + ";").then( 
                function(pass_crypt){
                    self.password = pass_crypt[0]['pass_crypt'];
                    ret.callback(attr);
                },
                function(err) {
                    console.log("Problems during password encryption " + err );
                    ret.errback(err);
                }
            );
            return ret;
        },
        
        _encryptPassword : function(next){
            var self = this;
            Client.db.run("select PASSWORD('" + self.password + "') as pass_crypt ;").then( 
                function(pass_crypt){
                  // CABECEADAAAAAAAAAAAAAAAAAAAAA!!!!!!!!!
                  self.password = pass_crypt[0]["pass_crypt"];
                  next();
                },
                function(err){
                  console.log("Problems during password encryption " + err );
                  // TODO: shouldn't we throw an error?
                  next();
                }
            );
        },
        
        toJSON : function() {
            var json = this._super(arguments);

            delete json['password'];

            json['isBlocked'] = this.isBlocked;
            json['isSuspended'] = this.isSuspended;

            return json;
        },

        suspend : function() {
            if (this.isSuspended) {
                return new comb.Promise().errback(new Error("El cliente ya está suspendido"));
            }
            else {
                this.status += STATUS.SUSPENDED;
                return this.save();
            }
        },

        unsuspend : function() {
            if (this.isSuspended) {
                this.status -= STATUS.SUSPENDED;
                return this.save();
            }
            else {
                return (new comb.Promise()).errback(new Error("El cliente ya esta suspendido"));
            }
        },

        _unblockIfBlocked : function() {
            if (this.isBlocked) {
                this.status -= STATUS.BLOCKED;
            }
        },

        _blockIfUnblocked : function() {
            if (!this.isBlocked) {
                this.status += STATUS.BLOCKED;
            }
        },

        getters : {
            isBlocked: function() {
                return this.status & STATUS.BLOCKED ? true : false;
            },
            isSuspended: function() {
                return this.status & STATUS.SUSPENDED ? true : false;
            }
        }

    }
}).as(module);

Client.validate(function(validate){
     validate("username").isNotNull();
     //Ensures that the emailAddress column is a valid email address.
     validate("email").isEmail();
});
