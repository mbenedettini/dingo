var apiBaseUrl = '/api',
    utils    = require('../../../lib/utils'),
    _        = require('underscore'),
    NotFound = utils.NotFound,
    checkErr = utils.checkErr,
    log      = console.log,
    ClientsController,
    Client;

var patio = require('patio');
var comb = require('comb');

var sessionsHelper = require('../../helpers/sessions_helper');
var mailerHelper = require('../../helpers/mailer_helper');

var disconnect = patio.disconnect.bind(patio);
var disconnectError = function(err) {
    patio.logError(err);
    patio.disconnect();
};

ClientsController = function(app, config) {
    Client = app.settings.models.Client;

    app.get(apiBaseUrl + '/clients', sessionsHelper.requieresLogin, function index(req, res, next) {
        var currentCashier = req.session.user;
        var allClientsJSON = [];
        Client.filter({cashierId: currentCashier.id}).forEach(function(c) {
            var clientJSON = c.toJSON();
            clientJSON['cashierUsername'] = currentCashier.username;
            allClientsJSON.push(clientJSON);
            
            return new comb.Promise().callback();
            
        }).then(
            function() {
                res.json(allClientsJSON);
            }
        );
    });

    app.get(apiBaseUrl + '/clients/:id', sessionsHelper.requieresLogin, function show(req, res, next) {
        var id = parseInt(req.params.id, 10);
        var currentCashier = req.session.user;
        Client.filter({id : id, cashierId: currentCashier.id}).one().then(
            function(client){
                if (client) {
                    console.log("Found!");
                    res.json(client);
                }
                else {
                    console.log("NOT Found!");
                    res.json(new NotFound('json'), 404);
                    // next(new NotFound());
                }
            },
            disconnectError
        );
        
    });
    
    app.post(apiBaseUrl + '/clients', sessionsHelper.requieresLogin, function create(req, res, next) {
        var currentCashier = req.session.user;
        var client = new Client(_.pick(req.body, 'name', 'lastName','username','password','email', 'telephone'));
        client.cashierId = currentCashier.id;
        client.confirmed = true;
        client.save().then(
            function() {
                var loc = '/clients/' + client.id;
                res.setHeader('Location', loc);
                res.json(client, 201);
            },
            disconnectError
        );
    });
    
    app.put(apiBaseUrl + '/clients/:id', sessionsHelper.requieresLogin, function update(req, res, next) {
        var id = parseInt(req.params.id, 10);
        var currentCashier = req.session.user;
        Client.filter({id : id, cashierId: currentCashier.id}).one().then(
            function(client){
                if (client) {
                    console.log("Found!");
                    // Update client
                    // modify resource with allowed attributes
                    var newAttributes = _.pick(req.body, 'name', 'lastName','email', 'telephone');
                    client = _.extend(client, newAttributes);
                    client.save().then(
                        function(client) {
                            console.log("SUCCESSFULLY AND MOTHERFUCKINGLY UPDATED");
                            
                            console.log("Sending update via socket.io");
                            app.settings.socket.back.emit('updateClient', { 'Client': client });
                            
                            res.json(client);
                        },
                        function(error) {
                            console.log("DAMN ERROR: " + error);
                        });
                } else {
                    console.log("NOT Found!");
                    res.json(new NotFound('json'));
                    next(new NotFound());
                }
            },
            disconnectError
        );
    });
    
    app.post(apiBaseUrl + '/clients/:id/deposits', sessionsHelper.requieresLogin, function makeDeposit(req, res, next) {
        var clientId = parseInt(req.params.id, 10);
        var amount = parseFloat(req.body.amount);
        var description = req.body.description;
        var currentCashier = req.session.user;
        Client.filter({id : clientId, cashierId: currentCashier.id}).one().then(
            function(client){
                if (client) {
                    console.log("Found!");
                    client.makeDeposit(amount, description, currentCashier.id).then(
                        function(t) {
                            console.log("DEPOSIT SUCCESS!: " + client.balance);
                            
                            console.log("Sending update via socket.io");
                            app.settings.socket.back.emit('updateClientBalance', { 'balance': client.balance });
                            
                            res.json(t);
                        },
                        function(error) {
                            console.log("ERROR: " + error);
                        }
                    );
                }
                else {
                    res.json(new NotFound('json'));
                }
            },
            disconnectError
        );
    });
    
    app.post(apiBaseUrl + '/clients/:id/withdrawals', sessionsHelper.requieresLogin, function makeWithdrawal(req, res, next) {
        var clientId = parseInt(req.params.id, 10);
        var amount = parseFloat(req.body.amount);
        var description = req.body.description;
        var currentCashier = req.session.user;
        Client.filter({id : clientId, cashierId: currentCashier.id}).one().then(
            function(client){
                if (client) {
                    client.makeWithdrawal(amount, description, currentCashier.id).then(
                        function(t) {
                            app.settings.socket.back.emit('updateClientBalance', { 'balance': client.balance });
                            
                            res.json(t);
                        },
                        function(error) {
                            console.log("ERROR: " + error);
                        }
                    );
                }
                else {
                    res.json(new NotFound('json'));
                }
            },
            disconnectError
        );
    });
    
    app.get(apiBaseUrl + '/clients/:id/transactions', sessionsHelper.requieresLogin, function listTransactions(req, res, next) {
        var clientId = parseInt(req.params.id, 10);
        var currentCashier = req.session.user;
        Client.filter({id : clientId, cashierId: currentCashier.id}).one().then(
            function(client){
                if (client) {
                    client.transactions.then(function(transactions) {
                        res.json(transactions);
                    });
                }
            }
        );
    });

    app.post(apiBaseUrl + '/clients/:id/password', sessionsHelper.requieresLogin, function resetPassword(req, res, next) {
        var id = parseInt(req.params.id, 10);
        var currentCashier = req.session.user;
        Client.filter({id : id, cashierId: currentCashier.id}).one().then(
            function(client){
                if (client) {
                    client.resetPassword().then(
                        function(newPassword) {
                            mailerHelper.sendNewPassword(client.email, newPassword);
                            res.json();
                        }
                    );
                } else {
                    console.log("NOT Found!");
                    res.json(new NotFound('json'));
                    next(new NotFound());
                }
            },
            disconnectError
        );
    });

    app.post(apiBaseUrl + '/clients/:id/suspend', function suspend(req, res, next) {
        var id = parseInt(req.params.id, 10);
        var currentCashier = req.session.user;
        Client.filter({id : id, cashierId: currentCashier.id}).one().then(
            function(client){
                if (client) {
                    client.suspend().then(
                        function() {
                            res.json(client);
                        },
                        function(error) {
                            res.json(new Error(error));
                        }
                    );
                } else {
                    console.log("NOT Found!");
                    res.json(new NotFound('json'));
                    next(new NotFound());
                }
            },
            disconnectError
        );
    });

    app.post(apiBaseUrl + '/clients/:id/unsuspend', function unsuspend(req, res, next) {
        var id = parseInt(req.params.id, 10);
        var currentCashier = req.session.user;
        Client.filter({id : id, cashierId: currentCashier.id}).one().then(
            function(client){
                if (client) {
                    client.unsuspend().then(
                        function() {
                            res.json(client);
                        },
                        function(error) {
                            res.json(new Error(error));
                        }
                    );
                } else {
                    console.log("NOT Found!");
                    res.json(new NotFound('json'));
                    next(new NotFound());
                }
            },
            disconnectError
        );
    });

};

module.exports = ClientsController;
