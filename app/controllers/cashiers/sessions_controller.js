var apiBaseUrl = '/api',
    utils    = require('../../../lib/utils'),
    _        = require('underscore'),
    NotFound = utils.NotFound,
    checkErr = utils.checkErr,
    log      = console.log,
    sessionsHelper = require('../../helpers/sessions_helper'),
    SessionsController,
    Cashier;
    
var patio = require('patio');

var disconnect = patio.disconnect.bind(patio);
var disconnectError = function(err) {
    patio.logError(err);
    patio.disconnect();
};

SessionsController = function(app, config) {
    Cashier = app.settings.models.Cashier;

    
    // Check if a conection still has valid session
    app.get(apiBaseUrl + '/sessions', sessionsHelper.requieresLogin, function index(req, res, next) {
        var cashier = req.session.user;
        res.json({ username : cashier.username, user_id : cashier.id});
    });
    
    // Create a Session
    app.post(apiBaseUrl + '/sessions', function login(req, res, next) {
        var username = req.body.username;
        var password = req.body.password;
        //console.log("username: " + username + " password: " + password);
        Cashier.authenticate( username,password).then(
            function(cashier){
                if (cashier) {
                    console.log("Authentication SUCCESS!: " + cashier.username);
                    // Save data into session.
                    req.session.user = cashier;
                    // console.log("session %j " , req.session );
                    res.json({ username : cashier.username, user_id: cashier.id});
                }
                else {
                    console.log("Authentication FAILED!: " + cashier.username);
                    res.json(false);
                }
            },
            function () {
                res.json(false);
                disconnectError;
            }
        );
    });
    
    // Logout or session destroy
    app.get(apiBaseUrl + '/sessions/destroy', sessionsHelper.requieresLogin, function logout(req, res, next) {
        if (req.session) {
            req.session.destroy();
        }
        res.json(true);
    });
}

module.exports = SessionsController;
