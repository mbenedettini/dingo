var apiBaseUrl = '/api',
    _        = require('underscore');
var patio = require('patio');

var sessionsHelper = require('../../helpers/sessions_helper');

// hardcoded to GMT-3
var TIMEZONE_SUFFIX = ' GMT-0300';

var ReportsController = function(app, config) {
    var Reports = app.settings.Reports;

    // dateFrom and dateTo both in format YYYY-MM-DD
    app.get(apiBaseUrl + 
            '/reports/cashier/:id/:dateFrom([0-9]{4}-[0-9]{2}-[0-9]{2})?/:dateTo([0-9]{4}-[0-9]{2}-[0-9]{2})?', 
            sessionsHelper.requieresLogin,
    function cashierReport(req, res, next) {
        var currentCashier = req.session.user;
        var id = parseInt(req.params.id, 10);
        var dateFrom = req.params.dateFrom ? new Date(req.params.dateFrom + TIMEZONE_SUFFIX) : null;
        var dateTo = req.params.dateTo ? new Date(req.params.dateTo + TIMEZONE_SUFFIX) : null;

        if (currentCashier.id != id) {
            res.json('Cajero incorrecto', 500);
            return;
        }

        Reports.getStatsCashier(id, dateFrom, dateTo).then(function(report){
            res.json(report);
        });
    });

};

module.exports = ReportsController;
