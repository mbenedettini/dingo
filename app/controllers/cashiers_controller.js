var apiBaseUrl = '/api',
    utils    = require('../../lib/utils'),
    _        = require('underscore'),
    NotFound = utils.NotFound,
    checkErr = utils.checkErr;

var mailerHelper = require('../helpers/mailer_helper');
    
var patio = require('patio');

var disconnect = patio.disconnect.bind(patio);
var disconnectError = function(err) {
    patio.logError(err);
    patio.disconnect();
};

var CashiersController = function(app, config) {
    var Cashier = patio.getModel('cashier');

    app.get(apiBaseUrl + '/cashiers', function index(req, res, next) {
        Cashier.all().then(
            function(cashiers) { res.json(cashiers); },
            function() { console.log("BUUUH!"); }
        );
    });

    app.get(apiBaseUrl + '/cashiers/:id', function show(req, res, next) {
        var id = parseInt(req.params.id, 10);
        Cashier.filter({id : id}).one().then(
            function(cashier){
                if (cashier) {
                    res.json(cashier);
                }
                else {
                    console.log("CashiersControllers: Cashier NOT Found!");
                    res.json(new NotFound('json'), 404);
                    // next(new NotFound());
                }
            },
            disconnectError
        );
        
    });
    
    app.post(apiBaseUrl + '/cashiers', function create(req, res, next) {
        var cashier = new Cashier(_.pick(req.body, 'username', 'email','name'));
        cashier.save().then(
            function() {
                console.log("CashiersController: created cashier #" + cashier.id);

                cashier.generatePassword().then(
                    function(password) {
                        var cashierJSON = cashier.toJSON();
                        cashierJSON.password = password;
                        var loc = '/cashiers/' + cashier.id;
                        res.setHeader('Location', loc);
                        res.json(cashierJSON, 201);

                        mailerHelper.sendNewPassword(cashier.email, password);
                    },
                    disconnectError
                );
            }
        );
    });
    
    app.put(apiBaseUrl + '/cashiers/:id', function update(req, res, next) {
        var id = parseInt(req.params.id, 10);
        Cashier.filter({id : id}).one().then(
            function(cashier){
                if (cashier) {
                    var newAttributes = _.pick(req.body, 'name', 'email', 'username');
                    cashier = _.extend(cashier, newAttributes);
                    cashier.save().then(
                        function(client) {
                            res.json(cashier);
                        },
                        function(error) {
                            console.log("DAMN ERROR: " + error);
                        });
                } else {
                    res.json(new NotFound('json'));
                }
            },
            disconnectError
        );
    });
    
    app.post(apiBaseUrl + '/cashiers/:id/password', function resetPassword(req, res, next) {
        var id = parseInt(req.params.id, 10);
        Cashier.filter({id : id}).one().then(
            function(cashier){
                if (cashier) {
                    cashier.generatePassword().then(
                        function(newPassword) {
                            mailerHelper.sendNewPassword(cashier.email, newPassword);
                            res.json();
                        }
                    );
                } else {
                    res.json(new NotFound('json'));
                    next(new NotFound());
                }
            },
            disconnectError
        );
    });

    app.post(apiBaseUrl + '/cashiers/:id/suspend', function suspend(req, res, next) {
        var id = parseInt(req.params.id, 10);
        Cashier.filter({id : id}).one().then(
            function(cashier){
                if (cashier) {
                    cashier.suspend().then(
                        function() {
                            res.json(cashier);
                        },
                        function(error) {
                            res.json(new Error(error));
                        }
                    );
                } else {
                    console.log("NOT Found!");
                    res.json(new NotFound('json'));
                    next(new NotFound());
                }
            },
            disconnectError
        );
    });

    app.post(apiBaseUrl + '/cashiers/:id/unsuspend', function unsuspend(req, res, next) {
        var id = parseInt(req.params.id, 10);
        Cashier.filter({id : id}).one().then(
            function(cashier){
                if (cashier) {
                    cashier.unsuspend().then(
                        function() {
                            res.json(cashier);
                        },
                        function(error) {
                            res.json(new Error(error));
                        }
                    );
                } else {
                    console.log("NOT Found!");
                    res.json(new NotFound('json'));
                    next(new NotFound());
                }
            },
            disconnectError
        );
    });

    app.get(apiBaseUrl + '/cashiers/:id/clients', function listClients(req, res, next) {
        var cashierId = parseInt(req.params.id, 10);
        Cashier.filter({id : cashierId}).one().then(
            function(cashier){
                if (cashier) {
                    cashier.clients.then(function(clients) {
                        res.json(clients);
                    });
                }
            }
        );
    });
};

module.exports = CashiersController;
