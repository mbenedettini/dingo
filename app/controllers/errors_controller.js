var utils    = require('../../lib/utils'),
    NotFound = utils.NotFound,
    ErrorsController;

ErrorsController = function(app, config) {

  // only for test environment
  if (config.ENV === 'test') {
    app.get('/test_500_page', function(req, res, next) {
      next(new Error('test'));
    });
  }

  app.all('*', function(req, res, next) {
      console.log("Not found!");
      console.log(req);
      throw new NotFound();
   });


  app.error(function(err, req, res, next) {
      if (err instanceof NotFound) {
          if (err.msg && err.msg === 'json') {
              res.json(null, 404);
          } else {
              res.send('404 - Page Not Found', 404);
          }
      } else {
          console.log("Returning Internal Server Error");
          console.log(err);
          res.send('500 - Internal Server Error', 500);
      }
  });


};

module.exports = ErrorsController;
