var apiBaseUrl = '/api',
    utils    = require('../../../lib/utils'),
    _        = require('underscore'),
    NotFound = utils.NotFound,
    checkErr = utils.checkErr;
        
var patio = require('patio');

var sessionsHelper = require('../../helpers/sessions_helper');
var mailerHelper = require('../../helpers/mailer_helper');

var disconnect = patio.disconnect.bind(patio);
var disconnectError = function(err) {
    patio.logError(err);
    patio.disconnect();
};

var ClientsController = function(app, config) {
    var Client = app.settings.models.Client;
    
    app.get(apiBaseUrl + '/clients/:id', sessionsHelper.requieresLogin, function getCurrent(req, res, next) {
        if (req.params.id != req.session.user.id) {
            res.json({error: {message: "Error: esta intentando recuperar un cliente que no es el que esta logueado actualmente."}});
            return;
        }

        Client.filter({id: req.session.user.id}).one().then(
            function(client) {         
                res.json(client);
            },
            function(error) {
                console.log("ClientsController.getCurrent: ERROR retrieving current client : " + error);
                res.json({error: {message: "Error retrieving current client"}});
            }
        );
    });
    
    app.post(apiBaseUrl + '/clients', function create(req, res, next) {
        var client = new Client(_.pick(req.body, 'name', 'lastName','username','password','email', 'telephone', 'cashierId'));
        
        var token = client.setConfirmationToken();
        
        client.save().then(
            function() {
                console.log("FUCKING created client #" + client.id + " con token: " + token);
                mailerHelper.sendConfirmationMail(client.email, token);
                res.json(client, 201);
            },
            function(error){
                console.log("ClientsController.postClient: ERROR creating client : " + error);
                res.json({error: {message: "Error creando client"}},412);   
                disconnectError;
            }
        );
    });
    
    app.get(apiBaseUrl + '/clients/confirm/:confirmationToken', function confimClient(req, res, next) {
        Client.filter({confirmationToken: req.params.confirmationToken}).one().then(
            function(client) {
                if (client != null) {
                    client.confirm().then(
                        function () { 
                          var loc = '#login/confirmed';
                          res.redirect(loc);                          
                        }
                    )
                } else {
                  res.json({error: {message: "Error retrieving client"}});
                  disconnectError;                  
                }
            },
            function(error) {
                console.log("ClientsController.vonfirm: ERROR retrieving client : " + error);
                res.json({error: {message: "Error retrieving client"}});
                disconnectError;
            }
        );
    });
    
    app.post(apiBaseUrl + '/clients/checkUsername', function checkUsername(req, res, next) {
        Client.filter({username: req.body["username"]}).one().then(
            function(client) {
                if (client == null) {
                  res.json({username: "valid"});
                } else {
                  res.json({error: {message: "Username Invalid"}},412);
                  disconnectError;
                }
            },
            function(error) {
                console.log("ClientsController.checkUsername: ERROR checking username : " + error);
                res.json({error: {message: "Error checking username"}},412);
                disconnectError;
            }
        );
    });

    app.get(apiBaseUrl + '/clients/:id/transactions', sessionsHelper.requieresLogin, function listTransactions(req, res, next) {
        var clientId = parseInt(req.params.id, 10);
        Client.filter({id : clientId}).one().then(
            function(client){
                if (client) {
                    client.transactions.then(function(transactions) {
                        res.json(transactions);
                    })
                }
            }
        );
    });
};

module.exports = ClientsController;
