var apiBaseUrl = '/api',
    utils    = require('../../../lib/utils'),
    _        = require('underscore'),
    NotFound = utils.NotFound,
    checkErr = utils.checkErr,
    log      = console.log;
        
var patio = require('patio');
var sessionsHelper = require('../../helpers/sessions_helper');

var disconnect = patio.disconnect.bind(patio);
var disconnectError = function(err) {
    patio.logError(err);
    patio.disconnect();
};

var MAX_QUANTITY_CARDS_PURCHASE = 30;

var ClientCardsController = function(app, config) {
    var Client = app.settings.models.Client;
    var Bingo = app.settings.models.Bingo;
    var ClientCard = app.settings.models.ClientCard;
    
    app.post(apiBaseUrl + '/client_cards', sessionsHelper.requieresLogin, function buyCards(req, res, next) {
        var currentBingo = app.settings.globals.currentBingo;
        var currentClient = req.session.user;

        if (!currentBingo) {
            var errorMsg = "No hay bingo en juego en este momento.";
            res.json({error: {message: errorMsg}});
            return;
        }

        var quantity = parseInt(req.body.quantity, 10) || 1;
        var series = req.body.series ? true : false;

        var totalQuantity = series ? quantity * 6 : quantity;
        if (totalQuantity > MAX_QUANTITY_CARDS_PURCHASE) {
            res.json({"error": {"message": "Disculpe, pero el máximo de cartones que se pueden comprar de una sola vez es " + MAX_QUANTITY_CARDS_PURCHASE }});
            return;
        }

        Client.filter({id: currentClient.id}).one().then(
            function(client) {
                client.buyCards(currentBingo, quantity, series).then(
                    function(clientCards) {
                        currentBingo.reload().then(function() {
                            app.settings.socket.back.emit('bingoUpdated', {bingo: currentBingo.toJSONFull()});
                            app.settings.socket.front.emit('bingoUpdated', {bingo: currentBingo.toJSON()});
                            res.json({model: clientCards});
                        });
                    },
                    function(error) {
                        console.log("ERROR on ClientCardsController.buyCards: " + error);
                        console.log(currentBingo.currentState);
                        res.json({"error": {"message": error }});
                    }
                );
            },
            function(error) {
                console.log("ERROR on ClientCardsController.buyCards: " + error);
                res.json({"error": {"message": error}});
            }
        );
    });

    app.post(apiBaseUrl + '/client_cards/return', sessionsHelper.requieresLogin, function returnCards(req, res, next) {
        var currentBingo = app.settings.globals.currentBingo;
        var currentClient = req.session.user;

        if (!currentBingo) {
            var errorMsg = "No hay bingo en juego en este momento.";
            res.json({error: {message: errorMsg}});
            return;
        }

        var quantity = parseInt(req.body.quantity, 10) || 1;
        var series = req.body.series ? true : false;

        Client.filter({id: currentClient.id}).one().then(
            function(client) {
                client.returnCards(currentBingo, quantity, series).then(
                    function(clientCards) {
                        currentBingo.reload().then(function() {
                            app.settings.socket.back.emit('bingoUpdated', {bingo: currentBingo.toJSONFull()});
                            app.settings.socket.front.emit('bingoUpdated', {bingo: currentBingo.toJSON()});
                            res.json({model: clientCards});
                        });
                    },
                    function(error) {
                        console.log("ERROR on ClientCardsController.returnCards: " + error);
                        console.log(currentBingo.currentState);
                        res.json({"error": {"message": error }});
                    }
                );
            },
            function(error) {
                console.log("ERROR on ClientCardsController.returnCards: " + error);
                res.json({"error": {"message": error}});
            }
        );
    });

    app.get(apiBaseUrl + '/client_cards', sessionsHelper.requieresLogin, function getCards(req, res, next) {
        var currentBingo = app.settings.globals.currentBingo;
        var currentClient = req.session.user;

        if (!currentBingo) {
            var errorMsg = "No hay bingo en juego en este momento.";
            //res.json({error: {message: errorMsg}});
            res.json([]);
            return;
        }

        if (!currentBingo.id) {
            res.json([]);
            return;
        }

        console.log("Retrieving cards for client #" + currentClient.id + " and bingo #" + currentBingo.id);
        ClientCard.filter({clientId: currentClient.id, bingoId: currentBingo.id, cancelled: false}).all().then(
            function(clientCards) {
                res.json(clientCards);
            }
        );
    });

};

module.exports = ClientCardsController;
