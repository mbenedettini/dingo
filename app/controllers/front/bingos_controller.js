var apiBaseUrl = '/api',
    utils    = require('../../../lib/utils'),
    _        = require('underscore'),
    NotFound = utils.NotFound,
    checkErr = utils.checkErr,
    log      = console.log;
        
var patio = require('patio');

var disconnect = patio.disconnect.bind(patio);
var disconnectError = function(err) {
    patio.logError(err);
    patio.disconnect();
};

var BingosController = function(app, config) {
    var Bingo = app.settings.models.Bingo;
    
    app.get(apiBaseUrl + '/bingos', function index(req, res, next) {
        var currentBingo = app.settings.globals.currentBingo;

        if (req.query.currentBingo) {
            console.log("BingosController: currentBingo requested");
            if (currentBingo) {
                if (currentBingo.id) {
                    console.log("Responding with bingo #" + currentBingo.id);
                    Bingo.filter({id: currentBingo.id}).first().then(function(bingo) {
                        res.json(bingo);
                    });
                }
                else {
                    // Volatile bingo
                    console.log("Responding with a volatile bingo");
                    res.json(currentBingo);
                }
            }
            else {
                console.log("but currentBingo is null");
                res.json(null);
            }
            
        }
        else if (req.query.upcomingBingos) {
            Bingo.getUpcoming().then(
                function(bingos) {
                    console.log(bingos.length + " upcoming bingos");
                    res.json(bingos);
                },
                function(error) {
                    console.log("ERROR Bingo.getUpcoming : " + error);
                    res.json({error: {message: "Error al recuperar los bingos"}});
                }
            );
        }
        else {
            res.json({error: {message: "Frontend can only request the current bingo"}});
        }

    });

};

module.exports = BingosController;
