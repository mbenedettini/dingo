var apiBaseUrl = '/api',
    utils    = require('../../../lib/utils'),
    _        = require('underscore'),
    NotFound = utils.NotFound,
    checkErr = utils.checkErr,
    log      = console.log,
    sessionsHelper = require('../../helpers/sessions_helper'),
    SessionsController,
    Client;
    
var patio = require('patio');

var disconnect = patio.disconnect.bind(patio);
var disconnectError = function(err) {
    patio.logError(err);
    patio.disconnect();
};

SessionsController = function(app, config) {
    Client = app.settings.models.Client;

    
    // Check if a conection still has valid session
    app.get(apiBaseUrl + '/sessions', sessionsHelper.requieresLogin, function index(req, res, next) {
        var client = req.session.user;
        res.json({ username : client.username, balance: client.balance, user_id : client.id});
    });
    
    // Create a Session
    app.post(apiBaseUrl + '/sessions', function login(req, res, next) {
        var username = req.body.username;
        var password = req.body.password;
        //console.log("username: " + username + " password: " + password);
        Client.authenticate( username,password).then(
            function(client){
                if (client) {
                    console.log("Authentication SUCCESS!: " + client.username);
                    // Save data into session.
                    req.session.user = client;
                    // console.log("session %j " , req.session );
                    res.json({ username : client.username, balance: client.balance, user_id : client.id});
                }
                else {
                    console.log("ERROR: " + error);
                    console.log("Authentication FAILED!: " + client.username);
                    res.json(false);
                }
            },
            function () {
                res.json(false);
                disconnectError;
            }
        );
    });
    
    // Logout or session destroy
    app.get(apiBaseUrl + '/sessions/destroy', sessionsHelper.requieresLogin, function logout(req, res, next) {
        if (req.session) {
            req.session.destroy();
        }
        res.json(true);
    });
}

module.exports = SessionsController;
