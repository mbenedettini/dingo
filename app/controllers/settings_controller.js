var apiBaseUrl = '/api',
    utils    = require('../../lib/utils'),
    _        = require('underscore'),
    NotFound = utils.NotFound,
    checkErr = utils.checkErr;
    
var patio = require('patio');

var disconnect = patio.disconnect.bind(patio);
var disconnectError = function(err) {
    patio.logError(err);
    patio.disconnect();
};

var SettingsController = function(app, config) {
    var Setting = app.settings.models.Setting;

    app.get(apiBaseUrl + '/settings', function index(req, res, next) {
        Setting.all().then(
            function(settings) { res.json(settings); },
            function(error) { 
                console.log("ERROR: " + error); 
                res.json("ERROR");
            }
        );
    });

    app.put(apiBaseUrl + '/settings/:key', function update(req, res, next) {
        var key = req.params.key;
        var value = req.body.value;
        Setting.setValue(key, value).then(
            function() {
                res.json("OK");
            },
            function(error) {
                console.log("ERROR when updating setting " + key + " : " + error);
                res.json("ERROR");
            }
        );
    });
    

};

module.exports = SettingsController;
