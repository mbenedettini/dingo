var apiBaseUrl = '/api',
    utils    = require('../../lib/utils'),
    _        = require('underscore'),
    NotFound = utils.NotFound,
    checkErr = utils.checkErr,
    ClientsController,
    Client,
    Cashier;

var mailerHelper = require('../helpers/mailer_helper');
    
var patio = require('patio');
var comb = require('comb');

var disconnect = patio.disconnect.bind(patio);
var disconnectError = function(err) {
    patio.logError(err);
    patio.disconnect();
};

ClientsController = function(app, config) {
    Client = app.settings.models.Client;

    app.get(apiBaseUrl + '/clients', function index(req, res, next) {
        // TODO: what to do if no client is found? shall we return the empty array?
        var allClientsJSON = [];
        Client.forEach(function(c) {
            var ret = new comb.Promise();
            c.cashier.then(
                function (cashier) {
                    var clientJSON = c.toJSON();
                    if (cashier) {
                        clientJSON['cashierUsername'] = cashier.username;
                    }
                    else {
                        clientJSON['cashierUsername'] = '';
                    }
                    allClientsJSON.push(clientJSON);
                    ret.callback(); 
                },
                function(error) {
                    console.log("ERror loading cashier: " + error);
                }
            );
            return ret;
        }).then(
            function() {
                res.json(allClientsJSON);
            }
        );
    });
    
    app.get(apiBaseUrl + '/clients/:id', function show(req, res, next) {
        var id = parseInt(req.params.id, 10);
        console.log("Finding client #" + id);
        Client.filter({id : id}).one().then(
            function(client){
                if (client) {
                    client.cashier.then(
                        function(cashier) {
                            var out = client.toJSON();
                            out.cashierUsername = cashier ? cashier.username : '-';
                            res.json(out);
                        }
                    );
                }
                else {
                    console.log("NOT Found!");
                    res.json(new NotFound('json'), 404);
                    // next(new NotFound());
                }
            },
            disconnectError
        );
        
    });
    
    app.post(apiBaseUrl + '/clients', function create(req, res, next) {
        var client = new Client(_.pick(req.body, 'name', 'lastName','username','password','email', 'telephone'));
        client.confirmed = true;
        client.save().then(
            function() {
                console.log("FUCKING created client #" + client.id);
                var loc = '/clients/' + client.id;
                res.setHeader('Location', loc);
                res.json(client, 201);
            },
            disconnectError
        );
    });
    
    app.put(apiBaseUrl + '/clients/:id', function update(req, res, next) {
        var id = parseInt(req.params.id, 10);
        console.log("Finding client #" + id + " for update");
        Client.filter({id : id}).one().then(
            function(client){
                if (client) {
                    console.log("Found!");
                    // Update client
                    // modify resource with allowed attributes
                    var newAttributes = _.pick(req.body, 'name', 'lastName','email', 'telephone', 'cashierId');

                    // special cashierId == -1 means that no cashier must be associated
                    if (newAttributes.cashierId == -1) {
                        newAttributes.cashierId = undefined;
                    }
                    client = _.extend(client, newAttributes);
                    client.save().then(
                        function(client) {
                            console.log("SUCCESSFULLY AND MOTHERFUCKINGLY UPDATED");
                            
                            console.log("Sending update via socket.io");
                            app.settings.socket.back.emit('updateClient', { 'Client': client });
                            
                            res.json(client);
                        },
                        function(error) {
                            console.log("DAMN ERROR: " + error);
                        });
                } else {
                    console.log("NOT Found!");
                    res.json(new NotFound('json'));
                    next(new NotFound());
                }
            },
            disconnectError
        );
    });
    
    app.post(apiBaseUrl + '/clients/:id/deposits', function makeDeposit(req, res, next) {
        var clientId = parseInt(req.params.id, 10);
        var amount = parseFloat(req.body.amount);
        var description = req.body.description;
        Client.filter({id : clientId}).one().then(
            function(client){
                if (client) {
                    console.log("Found!");
                    client.makeDeposit(amount, description).then(
                        function(t) {
                            console.log("DEPOSIT SUCCESS!: " + client.balance);
                            
                            console.log("Sending update via socket.io");
                            app.settings.socket.back.emit('updateClientBalance', { 'balance': client.balance });
                            
                            res.json(t);
                        },
                        function(error) {
                            console.log("ERROR: " + error);
                        }
                    );
                }
                else {
                }
            },
            disconnectError
        );
    });
    
    app.post(apiBaseUrl + '/clients/:id/withdrawals', function makeWithdrawal(req, res, next) {
        var clientId = parseInt(req.params.id, 10);
        var amount = parseFloat(req.body.amount);
        var description = req.body.description;
        Client.filter({id : clientId}).one().then(
            function(client){
                if (client) {
                    client.makeWithdrawal(amount, description).then(
                        function(t) {
                            console.log("WITHDRAWAL SUCCESS!: " + client.balance);
                            
                            console.log("Sending update via socket.io");
                            app.settings.socket.back.emit('updateClientBalance', { 'balance': client.balance });
                            
                            res.json(t);
                        },
                        function(error) {
                            console.log("ERROR: " + error);
                        }
                    );
                }
                else {
                }
            },
            disconnectError
        );
    });
    
    app.get(apiBaseUrl + '/clients/:id/transactions', function listTransactions(req, res, next) {
        var clientId = parseInt(req.params.id, 10);
        Client.filter({id : clientId}).one().then(
            function(client){
                if (client) {
                    client.transactions.then(function(transactions) {
                        res.json(transactions);
                    })
                }
            }
        );
    });

    app.post(apiBaseUrl + '/clients/:id/password', function resetPassword(req, res, next) {
        var id = parseInt(req.params.id, 10);
        Client.filter({id : id}).one().then(
            function(client){
                if (client) {
                    client.resetPassword().then(
                        function(newPassword) {
                            mailerHelper.sendNewPassword(client.email, newPassword);
                            res.json();
                        }
                    );
                } else {
                    console.log("NOT Found!");
                    res.json(new NotFound('json'));
                    next(new NotFound());
                }
            },
            disconnectError
        );
    });

    app.post(apiBaseUrl + '/clients/:id/suspend', function suspend(req, res, next) {
        var id = parseInt(req.params.id, 10);
        Client.filter({id : id}).one().then(
            function(client){
                if (client) {
                    client.suspend().then(
                        function() {
                            app.settings.socket.back.emit('updateClient', client.toJSON());
                            res.json(client);
                        },
                        function(error) {
                            res.json(new Error(error));
                        }
                    );
                } else {
                    console.log("NOT Found!");
                    res.json(new NotFound('json'));
                    next(new NotFound());
                }
            },
            disconnectError
        );
    });

    app.post(apiBaseUrl + '/clients/:id/unsuspend', function unsuspend(req, res, next) {
        var id = parseInt(req.params.id, 10);
        Client.filter({id : id}).one().then(
            function(client){
                if (client) {
                    client.unsuspend().then(
                        function() {
                            app.settings.socket.back.emit('updateClient', client.toJSON());
                            res.json(client);
                        },
                        function(error) {
                            res.json(new Error(error));
                        }
                    );
                } else {
                    console.log("NOT Found!");
                    res.json(new NotFound('json'));
                    next(new NotFound());
                }
            },
            disconnectError
        );
    });
};

module.exports = ClientsController;
