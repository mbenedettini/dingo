var apiBaseUrl = '/api',
    utils    = require('../../lib/utils'),
    _        = require('underscore'),
    NotFound = utils.NotFound,
    checkErr = utils.checkErr;
    
var patio = require('patio');

var disconnect = patio.disconnect.bind(patio);
var disconnectError = function(err) {
    patio.logError(err);
    patio.disconnect();
};

// hardcoded to GMT-3
var TIMEZONE_SUFFIX = ' GMT-0300';

var ReportsController = function(app, config) {
    var Reports = app.settings.Reports;

    // dateFrom and dateTo both in format YYYY-MM-DD
    app.get(apiBaseUrl + '/reports/cashiers/:dateFrom([0-9]{4}-[0-9]{2}-[0-9]{2})?/:dateTo([0-9]{4}-[0-9]{2}-[0-9]{2})?', function allCashiersReport(req, res, next) {
        /* When instantiating a date object, we must tell it what timezone we're dealing with.
         * Otherwise, it will assume that the parsed date string is UTC, building a date object
         * with 3 hours less than desired.
         * 
         * Both examples:
         * 
         * > new Date('2013-02-15')
         * Thu Feb 14 2013 21:00:00 GMT-0300 (ART)
         * > new Date('2013-02-15 GMT-0300')
         * Fri Feb 15 2013 00:00:00 GMT-0300 (ART)
         */
        var dateFrom = req.params.dateFrom ? new Date(req.params.dateFrom + TIMEZONE_SUFFIX) : null;
        var dateTo = req.params.dateTo ? new Date(req.params.dateTo + TIMEZONE_SUFFIX) : null;

        Reports.getStatsAllCashiers(dateFrom, dateTo).then(function(report){
            res.json(report);
        });
    });

};

module.exports = ReportsController;
