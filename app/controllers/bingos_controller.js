var apiBaseUrl = '/api',
    utils    = require('../../lib/utils'),
    _        = require('underscore'),
    NotFound = utils.NotFound,
    checkErr = utils.checkErr;
        
var patio = require('patio');
var comb = require('comb');

var disconnect = patio.disconnect.bind(patio);
var disconnectError = function(err) {
    patio.logError(err);
    patio.disconnect();
};

var BINGO_ATTRIBUTES = [ 'cardPrice', 'cardColor', 'lapseBetweenNumbers', 'sellingInterval', 'linePrize', 'linePrizeRate', 'bingoPrize', 'bingoPrizeRate', 'remainingNumbers', 'calledNumbers', 'starts', 'minClientsCount', 'minTotalClientsCount', 'sellingCancellationTimeout'];

var BingosController = function(app, config) {
    var Bingo = app.settings.models.Bingo;
    
    app.get(apiBaseUrl + '/bingos', function index(req, res, next) {
        Bingo.filter({currentState: [0,10,20,25,30,40]}).order('id').reverse().all().then(
            function(bingos) { res.json(bingos); },
            function() { console.log("BUUUH!"); }
        );
    });

    app.get(apiBaseUrl + '/bingos/:id', function show(req, res, next) {
        var id = parseInt(req.params.id, 10);
        var currentBingo = app.settings.globals.currentBingo;

        if (currentBingo && currentBingo.id == id) {
            console.log("Returning currentBingo");
            res.json(currentBingo.toJSONFull());
        }
        else {
            console.log("Retrieving bingo #" + id);
            Bingo.filter({id : id}).one().then(
                function(bingo){
                    if (bingo) {
                        console.log("Found!");
                        res.json(bingo.toJSONFull());
                    }
                    else {
                        console.log("NOT Found!");
                        res.json(new NotFound('json'), 404);
                        // next(new NotFound());
                    }
                },
                disconnectError
            );
        }
        
    });
    
    app.post(apiBaseUrl + '/bingos', function create(req, res, next) {
        console.log(req.body);
        console.log(_.pick(req.body, BINGO_ATTRIBUTES));

        var bingo = new Bingo(_.pick(req.body, BINGO_ATTRIBUTES));

        bingo.save().then(
            function() {
                console.log("FUCKING created bingo #" + bingo.id);
                var loc = '/bingos/' + bingo.id;
                res.setHeader('Location', loc);
                res.json(bingo.toJSONFull(), 201);
            },
            disconnectError
        );
    });
    
    app.put(apiBaseUrl + '/bingos/:id', function update(req, res, next) {
        var id = parseInt(req.params.id, 10);
        console.log("Finding bingo #" + id + " for update");
        Bingo.filter({id : id}).one().then(
            function(bingo){
                if (bingo) {
                    var updatedAttributes = _.pick(req.body, BINGO_ATTRIBUTES);
                    bingo = _.extend(bingo, updatedAttributes);
                    bingo.save().then(
                        function(bingo) {
                            console.log("SUCCESSFULLY AND MOTHERFUCKINGLY UPDATED");
                            
                            console.log("Sending update via socket.io");
                            app.settings.socket.back.emit('bingoUpdated', {bingo: bingo.toJSONFull()});
                            app.settings.socket.front.emit('bingoUpdated', {bingo: bingo.toJSON()});
                            
                            res.json(bingo.toJSONFull());
                        },
                        function(error) {
                            console.log("DAMN ERROR: " + error);
                        });
                } else {
                    console.log("NOT Found!");
                    res.json(new NotFound('json'));
                    next(new NotFound());
                }
            },
            disconnectError
        );
    });
    
    app.post(apiBaseUrl + '/bingos/:id/start', function start(req, res, next) {
        var self = this;
        var id = parseInt(req.params.id, 10);
        var currentBingo = app.settings.globals.currentBingo;

       if (currentBingo && currentBingo.isRunning) {
            res.json({error: {message : "No se puede iniciar el bingo solicitado (#" + id + "). El bingo #" + currentBingo.id + " ya se encuentra corriendo"}});
            return;
        }


        if (currentBingo && currentBingo.id == id) {
            console.log("Starting currentBingo");
            comb.serial([
                function() {
                    return currentBingo.setTotalClientsCount(app.settings.globals.getConnectedClientsCount());
                },
                function() {
                    return currentBingo.reload();
                },
                function() {
                    return currentBingo.start().then(
                        function() {
                            console.log("Bingo has been started");
                            app.settings.socket.back.emit('bingoUpdated', {bingo: currentBingo.toJSONFull()});
                            //app.settings.socket.front.emit('bingoUpdated', {bingo: bingo.toJSON()});                            
                            res.json(currentBingo);
                        }
                    );
                }
            ]);
        }
        else {
            console.log("Retrieving and starting bingo #" + id);
            Bingo.filter({id:id}).one().then(
                function(bingo) {
                    if (bingo) {
                        /* Listeners must be added *before* the start() operation to ensure we don't miss any event that might be emitted just after it */
                        console.log("Adding listeners to bingo");
                        app.settings.helpers.addBingoListeners(app, bingo);
                        

                        comb.serial([
                            function() {
                                return bingo.setTotalClientsCount(app.settings.globals.getConnectedClientsCount());
                            },
                            function() {
                                return bingo.reload();
                            },
                            function() {
                                return bingo.start().then(
                                    function() {
                                        console.log("Bingo #" + bingo.id + " was just started and set as currentBingo");
                                        app.settings.globals.currentBingo = bingo;                                
                                        console.log("Bingo started with " + bingo.totalClientsCount + " clients and " + bingo.visitorsCount + "visitors");
                                    }
                                );
                            },
                            function() {
                                /* this reload() overcomes a bug in patio that leaves bingo.lastClientCardSold as a Promise (and it shouldnt, as it has fetchType.EAGER) */
                                return bingo.reload().then(
                                    function() {
                                        // TODO: is this notification actually necessary?
                                        app.settings.socket.back.emit('bingoUpdated', {bingo: bingo.toJSONFull()});                            
                                    }
                                );
                            }
                         ]).then(
                             function() {
                                 res.json(bingo);
                             },
                             function(error) {
                                 var errorMsg = "Error when starting bingo: " + error;
                                 console.log(errorMsg);
                                 res.json({"error": {message: errorMsg}});
                             }
                         );
                    }
                    else {
                        // TODO: what to do?
                        var errorMsg = "Bingo not found: #" + id;
                        console.log(errorMsg);
                        res.json({"error": {message: errorMsg}});
                    }
                },
                function(error) {
                    console.log(error);
                    res.json({"error": {message: error}});
                }
            );
        }
    });

    app.post(apiBaseUrl + '/bingos/:id/pause', function pause(req, res, next) {
        var self = this;
        var id = parseInt(req.params.id, 10);
        var currentBingo = app.settings.globals.currentBingo;
                 
        if (currentBingo && currentBingo.id == id) {
            currentBingo.pause().then(
                function() {
                    console.log("Bingo #" + currentBingo.id + " was just paused");
                    app.settings.globals.currentBingo = null;
                    app.settings.socket.back.emit('bingoUpdated', {bingo: currentBingo.toJSONFull()});
                    app.settings.socket.front.emit('bingoUpdated', {bingo: currentBingo.toJSON() } );
                    res.json(currentBingo);
                },
                function(error) {
                    res.json({"error": {message: error}});
                }
            );
        }
        else  {
            console.log("currentBingo is " + (currentBingo ? "#" + currentBingo.id : "null" ) + " , retrieving and pausing bingo #" + id);
            Bingo.filter({id: id}).one().then(
                function(bingo) {
                    bingo.pause().then(
                        function() {
                            res.json(bingo);
                        },
                        function(error) {
                            res.json({"error": {message: error}});
                        }
                    );
                }
            );
        }
    });

    app.post(apiBaseUrl + '/bingos/:id/pause-light', function pause(req, res, next) {
        var self = this;
        var id = parseInt(req.params.id, 10);
        var currentBingo = app.settings.globals.currentBingo;
                 
        if (currentBingo && currentBingo.id == id) {
            // TODO: try-catch this
            currentBingo.pause('light');
            console.log("Bingo #" + currentBingo.id + " was just light paused");
            app.settings.socket.back.emit('bingoUpdated', {bingo: currentBingo.toJSONFull()});
            app.settings.socket.front.emit('bingoUpdated', {bingo: currentBingo.toJSON() } );
            res.json(currentBingo);
        }
        else  {
            console.log("currentBingo is " + (currentBingo ? "#" + currentBingo.id : "null" ) + " and differs from the bingo you are asking to light pause. Nothing has been done.");
        }
    });
    

    
};

module.exports = BingosController;
