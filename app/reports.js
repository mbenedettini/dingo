var patio = require('patio');
var comb = require('comb');
var _ = require('underscore');

var Reports = comb.define(null, {
    static : {
        getStatsAllCashiers : function(dateFrom, dateTo) {
            var self = this;
            var ret = new comb.Promise();
            var db = patio.defaultDatabase;

            dateFrom = dateFrom || new Date();
            dateTo = dateTo || new Date();

            var query = "SELECT t.cashierId, t.type, c.username, SUM(t.amount) AS totalAmount"; 
            query += " FROM transaction t LEFT JOIN cashier c ON t.cashierId = c.id"; 
            query += " WHERE DATE(t.created) BETWEEN ? AND ?"; 
            query += " GROUP BY t.cashierId, t.type"; 
            query += " ORDER BY t.cashierId, t.type";

            db.fetch(query, dateFrom, dateTo).all().then(function(rows) {
                // row.cashierId row.username, row.type row.totalAmount

                // Get every distinct cashier
                var cashiers = _.uniq(rows.map(function(row) { return row.username; }));

                // For every cashier build the report
                var report = [];
                cashiers.forEach(function(username) {
                    var cashierRows = rows.filter(function(row) { return row.username == username; });
                    var cashierReport = self._buildCashierReport(cashierRows);
                    cashierReport[username] = username;

                    report.push(cashierReport);
                });

                ret.callback(report);
 
            });

            return ret;
        },

        getStatsCashier : function(cashierId, dateFrom, dateTo) {
            var self = this;
            var ret = new comb.Promise();
            var db = patio.defaultDatabase;

            dateFrom = dateFrom || new Date();
            dateTo = dateTo || new Date();

            var query = "SELECT t.type, SUM(t.amount) AS totalAmount"; 
            query += " FROM transaction t"; 
            query += " WHERE cashierId = ? AND DATE(t.created) BETWEEN ? AND ?"; 
            query += " GROUP BY t.type"; 
            query += " ORDER BY t.type";

            db.fetch(query, cashierId, dateFrom, dateTo).all().then(function(rows) {
                // row.type row.totalAmount
                var report = self._buildCashierReport(rows);

                ret.callback(report);
 
            });

            return ret;
        },

        _buildCashierReport: function(cashierRows) {
            // cashierRow.type cashierRow.totalAmount
            var cashierReport = {};
            var cashierRowsByType = {};
            cashierRows.forEach(function(row) { 
                // Damn patio returns decimal columns as string
                cashierRowsByType[row.type] = parseFloat(row.totalAmount, 10);
            });

            // When there aren't transactions, fill in with zero
            _.defaults(cashierRowsByType, {1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0});

            // Financial flow is just deposits and withdrawals
            cashierReport['deposit'] = cashierRowsByType[1];
            cashierReport['withdrawal'] = cashierRowsByType[2];
            cashierReport['financialMargin'] = cashierReport['deposit'] - cashierReport['withdrawal'];
            
            // Game flow
            cashierReport['cardPurchase'] = cashierRowsByType[3];
            cashierReport['linePayout'] = cashierRowsByType[4];
            cashierReport['bingoPayout'] = cashierRowsByType[5];
            cashierReport['accumulatedPayout'] = cashierRowsByType[7];
            cashierReport['cardRefund'] = cashierRowsByType[6];
            cashierReport['totalClientWin'] = cashierReport['linePayout'] + cashierReport['bingoPayout'] + cashierReport['accumulatedPayout'];
            cashierReport['siteMargin'] = cashierReport['cardPurchase'] - cashierReport['cardRefund'] - cashierReport['totalClientWin'];

            return cashierReport;
        }
    },

    instance: {}
});

module.exports = Reports;