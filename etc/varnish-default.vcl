backend backoffice {
      .host = "127.0.0.1";
      .port = "3333";
}

backend front {
      .host = "127.0.0.1";
      .port = "3334";
}

backend cashiers {
      .host = "127.0.0.1";
      .port = "3335";
}

# websockets
sub vcl_pipe {
    if (req.http.upgrade) {
       set bereq.http.upgrade = req.http.upgrade;
    }

    set bereq.http.Connection = "close";
    return (pipe);
}
# /websockets

import std;

sub vcl_recv {

    
    if (req.http.host == "bingo.rosariocasino.com") {
       set req.backend = front;
    } else if (req.http.host == "adminbingo.rosariocasino.com") {
       set req.backend = backoffice;
       std.log("Backend Cookie es " + req.http.Cookie);
    } else if (req.http.host == "cajerobingo.rosariocasino.com") {
       set req.backend = cashiers;
    }


    if (req.restarts == 0) {
	if (req.http.x-forwarded-for) {
	    set req.http.X-Forwarded-For =
		req.http.X-Forwarded-For + ", " + client.ip;
	} else {
	    set req.http.X-Forwarded-For = client.ip;
	}
    }
    if (req.request != "GET" &&
      req.request != "HEAD" &&
      req.request != "PUT" &&
      req.request != "POST" &&
      req.request != "TRACE" &&
      req.request != "OPTIONS" &&
      req.request != "DELETE") {
        /* Non-RFC2616 or CONNECT which is weird. */
        return (pipe);
    }
    if (req.request != "GET" && req.request != "HEAD") {
        /* We only deal with GET and HEAD by default */
	std.log("We only cache GET and HEAD");
        return (pass);
    }
    if (req.http.Authorization || req.http.Cookie) {
        /* Not cacheable by default */
        std.log("Not cacheable!");
        return (pass);
    }
    
    if (req.url ~ "^/api/") {
        return (pass);
    }

    if (req.url ~ "^/mp3/") {
       return (pass);
    }

    # websockets
    if (req.http.Upgrade ~ "(?i)websocket") {
       return (pipe);
    }
    # /websockets


    return (pass);

}

sub vcl_pass {
    return (pass);
}

sub vcl_hash {
    hash_data(req.url);
    if (req.http.host) {
        hash_data(req.http.host);
    } else {
        hash_data(server.ip);
    }
    return (hash);
}

sub vcl_hit {
    return (deliver);
}

sub vcl_miss {
    return (fetch);
}

sub vcl_fetch {
    if (beresp.ttl <= 0s ||
        beresp.http.Set-Cookie ||
        beresp.http.Vary == "*") {
		/*
		 * Mark as "Hit-For-Pass" for the next 2 minutes
		 */
		set beresp.ttl = 120 s;
		return (hit_for_pass);
    }
    return (deliver);
}

sub vcl_deliver {
    return (deliver);
}

sub vcl_error {
    set obj.http.Content-Type = "text/html; charset=utf-8";
    set obj.http.Retry-After = "5";
    synthetic {"
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
  <head>
    <title>"} + obj.status + " " + obj.response + {"</title>
  </head>
  <body>
    <h1>Error "} + obj.status + " " + obj.response + {"</h1>
    <p>"} + obj.response + {"</p>
    <h3>Guru Meditation:</h3>
    <p>XID: "} + req.xid + {"</p>
    <hr>
    <p>Varnish cache server</p>
  </body>
</html>
"};
    return (deliver);
}

sub vcl_init {
	return (ok);
}

sub vcl_fini {
	return (ok);
}

