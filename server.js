require("node-codein");

var utils       = require('./lib/utils'),
    config      = utils.loadConfig(__dirname + "/config"),
    express     = require('express'),
    gzippo      = require('gzippo'),
    appBack     = express.createServer(),
    appFront    = express.createServer(),
    appCashiers = express.createServer(),
    ENV         = utils.getENV(),
    connect     = require('connect'),
    helpers     = require('./server-helpers'),
    cookie      = require('cookie'),
    apiBaseUrl = '/api';

// Patio & Comb
var patio   = require("patio"),
    comb    = require("comb"),
    when    = comb.when,
    serial  = comb.serial;
    
// patio configuration
patio.identifierOutputMethod = null;
patio.identifierInputMethod = null;
var patioLoggingConfig = {
    "patio" : {
        appenders : [
            {
                type : "RollingFileAppender",
                file : "patio.log"
            }
        ]
    },
    "patio.Database" : {
        appenders : [
            {
                type : "RollingFileAppender",
                file : "patio.log"
            }
        ]
    },
    "patio.Dataset" : {
        appenders : [
            {
                type : "RollingFileAppender",
                file : "patio.log"
            }
        ]
    }
};
if (ENV == 'production') {
    patioLoggingConfig["patio.Database"]["level"] = "WARN";
    patioLoggingConfig["patio.Dataset"]["level"] = "WARN";
}
patio.configureLogging(patioLoggingConfig);


// Early using of basicAuth middleware so that all content is protected
// (static '/public' included )
// TODO: move user/password to a separate file
appBack.configure('production', function() {
    appBack.use(connect.basicAuth('adminbingo', 'C73nR67x'));
});

var configBack = config[ENV].back;
var configFront = config[ENV].front;
var configCashiers = config[ENV].cashiers;
var allConfigs = [configBack, configFront, configCashiers];
var allApps = [appBack, appFront, appCashiers];

var cookieSid = '6a02953d73b2439c8a314b2b6ebe9c6b096b3d97';

var io  = require('socket.io');
//io.settings.logger.level = 2;

// Register socket.io instance into app
var frontIo = io.listen(appFront);
var backIo = io.listen(appBack);
// Log level: 1 -> warn
backIo.settings["log level"] = 1;
//frontIo.settings["log level"] = 1;
frontIo.settings["heartbeat timeout"] = 30;
frontIo.settings["hearbeat interval"] = 15;
frontIo.settings["close timeout"] = 45;


var sockets = {back: backIo.sockets, front: frontIo.sockets};
appBack.set('socket', sockets);
appFront.set('socket', sockets);
appCashiers.set('socket', sockets);

// Expose some things globally so that we can access them from node-codein console.
global.app = appBack;
global.io = io;
global.frontIo = frontIo;
global.backIo = backIo;

// Save ENV into config for later use (e.g. in controllers)
configBack['ENV']  = ENV;
configFront['ENV']  = ENV;

// Session Store definition TODO: maybe move?
var SessionStore = require('connect-redis')(express);
var sessionStore = new SessionStore({ reapInterval: 6000 * 10 });

var backStatics = '/public/back';
var frontStatics = '/public/front';
var cashiersStatics = '/public/cashiers';
if (ENV == 'production') {
    console.log("Using production statics");
    backStatics = '/public/back-built';
    frontStatics = '/public/front-built';
    cashiersStatics = '/public/cashiers-built';
}

appBack.use(express.static(__dirname + backStatics));
appFront.use(express.static(__dirname + frontStatics));
appCashiers.use(express.static(__dirname + cashiersStatics));

allApps.forEach(function(app) { 
    app.configure(function() {
        app.use(function(req, res, next) {            
            res.removeHeader("X-Powered-By");
            next();
        });

        app.use(express.favicon());
       
        app.use(express.bodyParser());
        app.use(express.methodOverride());
        
        // sessions stuffs TODO: make some real secret string ...
        app.use(express.cookieParser());
        app.use(express.session({
            store: sessionStore,
            secret: cookieSid
        }));
        
        // this must be after session definitions ... otherwise, req.session get undefined
        app.use(app.router);
        
        app.enable('jsonp callback', true);
        
    });

    app.configure('production', function() {
        app.use(gzippo.compress());
    });

    app.get(apiBaseUrl + '/*', function(req, res, next) {
        res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        next();
    });

});


[frontIo, backIo].forEach(function(io) {
    io.configure('production', function() {
        io.enable('browser client minification');  // send minified client
        io.enable('browser client gzip');  // gzip it
    });
});

frontIo.set('authorization', function (data, accept) {
    // check if there's a cookie header
    if (data.headers.cookie) {
        // if there is, parse the cookie
        var userCookie = cookie.parse(data.headers.cookie);
        sessionStore.get(userCookie['connect.sid'], function (err, session) {
            if (err || !session) {
                // if we cannot grab a session, turn down the connection
                //console.log("Socket.IO: client NOT authorized. Invalid Cookie");
                return accept('Error', false);
            } else {
                // save the session data and accept the connection
                data.session = session;
                //console.log("Socket.IO: client authorized");
                return accept(null, true);
            }
        });
    } else {
        // if there isn't, turn down the connection with a message
        // and leave the function.
        //console.log("Socket.IO: client not authorized");
        return accept('No cookie transmitted.', false);
    }
});
helpers.handleChatOn(frontIo);

    
// Connect to the db
var db = utils.getDbConnection();

// register models
var models = require(__dirname + "/app/models");

// other modules
var Reports = require(__dirname + "/app/reports");
[appBack, appCashiers].forEach(function(app) {
    app.set('Reports', Reports);
});

var currentVirtualClientsCount;

patio.syncModels().then(
    function() {
        // Set development mode for some models
        models.ClientCard.setENV(utils.getBINGO_ENV());
        models.Bingo.setENV(utils.getBINGO_ENV());

        // Register controller, do not put 'errors' here, since it got a cachall
        var back_controllers = ['clients', 'bingos', 'settings', 'cashiers', 'reports'];
        var front_controllers = ['client_cards','bingos','sessions', 'clients'];
        var cashiers_controllers = ['sessions', 'clients', 'reports'];

        // add 'errors' controler to backend controllers
        // back_controllers.push('errors');
        
        appBack.set('models', models);
        appFront.set('models', models);
        appCashiers.set('models', models);

        // globals is a way to mantain some references between both apps
        var globals = {
            currentBingo: null,
            getConnectedClientsCount: null,
            runScheduler: false
        };
        appBack.set('globals', globals);
        appFront.set('globals', globals);
        appCashiers.set('globals', globals);

        appBack.get('/js/config.json', function(req, res){
            var output = config[ENV];            
            models.Setting.getAll().then(
                function(settings) {
                    output['defaults'] = settings;
	            res.json(output);
                },
	        function(error) {
                    console.log("ERROR when getting Settings: " + error);
                    res.json(output);
                }
            );
        });

        appFront.get('/js/config.json', function(req, res) { 
            res.json(config[ENV]); 
        });

        appCashiers.get('/js/config.json', function(req, res) { 
            res.json(config[ENV]); 
        });


        /* Clients Connections count handling */
        var updateConnectedClients = function() {
            var ret = new comb.Promise();
            var connectedClientsCount = globals.getConnectedClientsCount() + currentVirtualClientsCount;
            if (globals.currentBingo) {
                globals.currentBingo.setTotalClientsCount(connectedClientsCount).then(function() {
                    helpers.notifyBingoUpdateVisitorsCount(frontIo, globals.currentBingo);
                    ret.callback();
                });
            }
            else {
                var shownTotalClientsCount = models.Bingo.calculateShownTotalClientsCount(connectedClientsCount);

                var data = {
                    'totalClientsCount': connectedClientsCount,
                    'shownTotalClientsCount': shownTotalClientsCount
                };
                helpers.notifyUpdateVisitorsCount(frontIo, data);
                ret.callback();
            }

            return ret;
        };

        

        /* Returns the count of authorized clients at the moment */
        globals.getConnectedClientsCount = function() {
            var clients = [];
            Object.keys(frontIo.handshaken).forEach(function(k) {
                if (frontIo.handshaken[k].session && frontIo.handshaken[k].session.user) {
                    var username = frontIo.handshaken[k].session.user.username;
                    if (clients.indexOf(username) < 0) {
                        clients.push(username);
                    }
                }
            });

            return clients.length;
        };

        frontIo.sockets.on('connection', function (socket) {
            console.log("Connection!");
            updateConnectedClients(); // is async but no need to take care of that here

            // Send it how much the accumulated prize is
            /* TODO: isn't this much of a performance overkill ? */
            models.Setting.getAll().then(function(settings) {
                var accumulatedSettings = {
                    'accumulatedPrize': parseFloat(settings.accumulatedPrize),
                    'accumulatedOrder': parseFloat(settings.accumulatedOrder)
                };
                helpers.notifyAccumulatedPrize(frontIo, accumulatedSettings);
            });

            socket.on('disconnect', function() {
                updateConnectedClients(); // is async but no need to take care of that here
            });
        });
        /* End of Clients count handling */


        // Register back controllers
        back_controllers.forEach(function(controller) {
            require('./app/controllers/' + controller + '_controller')(appBack, configBack);
            console.log("Registered controller: " + controller);
        });

        // Register front controllers
        front_controllers.forEach(function(controller) {
            require('./app/controllers/front/' + controller + '_controller')(appFront, configFront);
            console.log("Registered Front controller: " + controller);
        });

        // Cashiers controllers
        cashiers_controllers.forEach(function(controller) {
            require('./app/controllers/cashiers/' + controller + '_controller')(appCashiers, configCashiers);
            console.log("Registered Cashiers controller: " + controller);
        });
        
        appBack.listen(configBack.PORT);
        console.log('Backoffice app listening on port %d, environment: %s', configBack.PORT, appBack.settings.env);
        
        appFront.listen(configFront.PORT);
        console.log('Frontend app listening on port %d, environment: %s', configFront.PORT, appFront.settings.env);

        appCashiers.listen(configCashiers.PORT);
        console.log('Cashiers app listening on port %d, environment: %s', configCashiers.PORT, appCashiers.settings.env);

        appBack.set('helpers', helpers);

        var bingoScheduler = function() {
            /*if (! globals.runScheduler ) {
                console.log("Scheduler not active");
                return;
            }*/
            
            comb.serial([
                function increaseAccumulatedOrder() {
                    /* Increase accumulatedOrder if needed */
                    var ret = new comb.Promise();
                    models.Setting.getValue('increaseAccumulatedOrderEvery').then(
                        function(increaseAccumulatedOrderEvery) {
                            increaseAccumulatedOrderEvery = parseInt(increaseAccumulatedOrderEvery, 10);
                            if (increaseAccumulatedOrderEvery) {
                                models.Setting.filter({key: 'accumulatedOrder'}).one().then(
                                    function(a) {
                                        var timeSpan = (new Date()) - a.updated;
                                        if ((timeSpan / 1000 / 3600) >= increaseAccumulatedOrderEvery) {
                                            var oldAccumulatedOrder = parseInt(a.value, 10);
                                            var newAccumulatedOrder = oldAccumulatedOrder + 1;
                                            console.log("Server: Increasing accumulatedOrder to " + newAccumulatedOrder);
                                            models.Setting.setValue('accumulatedOrder', newAccumulatedOrder).then(
                                                comb.hitch(ret, "callback"),
                                                comb.hitch(ret, "errback")
                                            );
                                        }
                                        else {
                                            ret.callback();
                                        }
                                    }
                                );
                            }
                            else {
                                console.log("Bingo scheduler: automatic accumulated order increasing disabled");
                                ret.callback();
                            }
                        }
                    );
                    return ret;
                },
                function scheduleAndRunBingo() {
                    var ret = new comb.Promise();
                    var scheduleAndRunBingo = false;
                    if (globals.currentBingo) {
                        var bingo = globals.currentBingo;
                        
                        if (! bingo.isActive) {
                            scheduleAndRunBingo = true;
                        }
                        else {
                            // console.log("Bingo scheduler: #" + bingo.id + " has not finished yet");
                        }
                    }
                    else {
                        scheduleAndRunBingo = true;
                    }
                    
                    if (scheduleAndRunBingo) {
                        // First, check if there is any bingo already scheduled to run
                        models.Bingo.getNextToStart().then(
                            function(bingo) {
                                if (bingo) {
                                    globals.currentBingo = bingo;
                                    console.log("Bingo scheduler: Starting #" + bingo.id);
                                    helpers.addBingoListeners(appBack, bingo);
                                    bingo.setTotalClientsCount(globals.getConnectedClientsCount()).then(function() {
                                    
                                        // As soon as it finishes, run the scheduler
                                        bingo.addListener('finish', function() {
                                            bingoScheduler();
                                        });
                                        
                                        bingo.start();
                                        ret.callback();
                                    });
                                }
                                else {
                                    console.log("Bingo scheduler: no bingo scheduled at the moment");
                                    // See if we have to create an automatic Bingo instance
                                    models.Setting.getValue('autoBingoEnabled').then(function(autoBingoEnabled) {
                                        autoBingoEnabled = parseInt(autoBingoEnabled);
                                        console.log("AutoBingoEnabled: " + autoBingoEnabled);
                                        if (autoBingoEnabled) {
                                            console.log("Bingo scheduler: auto bingo enabled. Instantiating and starting a Bingo according to current settings");
                                            models.Setting.getAll().then(function(settings) {
                                                var autoBingo = new models.Bingo({
                                                    cardPrice:           settings.cardPrice,
                                                    cardColor:           settings.cardColor,
                                                    lapseBetweenNumbers: settings.lapseBetweenNumbers,
                                                    sellingInterval:     settings.sellingInterval,
                                                    linePrizeRate:       settings.linePrizeRate,
                                                    bingoPrizeRate:      settings.bingoPrizeRate,
                                                    minClientsCount:     settings.minClientsCount,
                                                    minTotalClientsCount:settings.minTotalClientsCount,
                                                    sellingCancellationTimeout:settings.sellingCancellationTimeout,
                                                    clientsCount: 0,
                                                    clientCardsCount: 0                                                    
                                                });

                                                globals.currentBingo = autoBingo;
                                                helpers.addBingoListeners(appBack, autoBingo);
                                                var virtualClientsId = settings.virtualClientsId.split(",").map(function(n) { return parseInt(n, 10); });
                                                
                                                currentVirtualClientsCount = helpers.virtualClientsManager(autoBingo, virtualClientsId);
                                                var totalClientsCount = globals.getConnectedClientsCount() + currentVirtualClientsCount;
                                                // Initial totalClientsCount
                                                autoBingo.setTotalClientsCount(totalClientsCount).then(function() {
                                                    // As soon as it finishes, run the scheduler
                                                    autoBingo.addListener('finish', function() {
                                                        bingoScheduler();
                                                    });
                                                    
                                                    autoBingo.start();
                                                    ret.callback();
                                                });
                                            });
                                        }
                                        else {
                                            // console.log("Bingo scheduler: auto bingo disabled");
                                            ret.callback();
                                        }
                                    });
                                }
                            },
                            function(error) {
                                console.log("Bingo scheduler error on Bingo.getNextToStart(): " + error);
                                ret.errback(error);
                            }
                        );
                    }
                    return ret;
                } // end of scheduleAndRunBingo()
            ]);

        };

        // scheduler runs every 60 seconds
        setInterval(function() {
            bingoScheduler();
        }, 10 * 1000); // 60 seconds

        // After all initialization, configure logging and redefine console.log
        var dingoLogger = comb.logger("dingoLogger").addAppender("RollingFileAppender", {file:'dingo.log'});
        console.log = function(msg) {
            dingoLogger.log("debug", comb.argsToArray(arguments));
        }; 
        console.warn = function(msg) { 
            dingoLogger.log("warn", comb.argsToArray(arguments));
        };
        

    },
    function(error) {
        console.log("ERROR when syncing models: " + error);
    }
);

