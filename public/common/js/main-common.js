requirejs.config({
    baseUrl: 'js',
    paths: {
        'utils'             : 'common/utils',
	'text'              : 'common/lib/text',
        'config'            : '/js/config.json?callback=define',
        'json'              : 'common/lib/json',
        'jquery'            : 'common/lib/jquery',
	'bootstrap'         : 'common/lib/bootstrap',
        'bootstrap-inputmask' : 'common/lib/bootstrap-inputmask',
	'moment'            : 'common/lib/moment',
	'moment-es'         : 'common/lib/moment-es',
	'underscore'        : 'common/lib/underscore',
        'backbone'          : 'common/lib/backbone',
        'rivets'            : 'common/lib/rivets',
	'socket.io'         : 'common/lib/socket.io',
        'jqueryCookie'      : 'common/lib/jquery.cookie',
        'jqueryUI'          : 'common/lib/jquery-ui-1.9.0.custom',
	'Mediator'          : 'common/lib/mediator',
	'ClientModel'       : 'common/models/client',
	'ClientCollection'  : 'common/collections/clients',
	'DepositModel'      : 'common/models/deposit',
	'WithdrawalModel'   : 'common/models/withdrawal',
	'TransactionModel'  : 'common/models/transaction',
	'TransactionCollection' :   'common/collections/transactions',
        'ClientPassword'    :       'common/models/client_password',
        'BingoModel'        :       'common/models/bingo',
        'BingoCollection'   :       'common/collections/bingos',
        'ClientCardModel'   :       'common/models/client_card',
        'ClientCardCollection' :    'common/collections/client_cards',
        'ClientCardBulkCreation' :  'common/models/client_card_bulk_creation',
        'ClientCardBulkReturning' : 'common/models/client_card_bulk_returning',
        'SettingModel'      :       'common/models/setting',
        'SettingCollection' :       'common/collections/settings',
        'CashierModel'      : 'common/models/cashier',
        'CashierPassword'   : 'common/models/cashier_password',
        'CashierCollection' : 'common/collections/cashiers'

    },

    shim: {
	'underscore': {
	    exports: '_'
	},
	'backbone': {
	    deps: ['underscore', 'jquery'],
	    exports: 'Backbone'
	},
	'bootstrap': {
	    deps: ['jquery' ],
	    exports: 'bootstrap'
	},
	'socket.io' : {
	    deps: [],
	    exports: 'io'
	},
        'rivets' : {
            deps: ['backbone'],
            exports: 'rivets'
        },
        'bootstrap-inputmask' : {
            deps: ['bootstrap']
        },
        'jqueryUI' : {
            deps: ['jquery']
        },
        'jqueryCookie' : {
            deps: ['jquery']
        }
    },

    waitSeconds: 15
	                
});



