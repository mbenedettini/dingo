define(function() {
    return {
        debug: function(message) {
            if (typeof(console) != "undefined" && console.log != undefined) {
                console.log("[" +  new Date().toLocaleTimeString() + "]: " + message);
                // console.log(message);
            }
        }
    };
});