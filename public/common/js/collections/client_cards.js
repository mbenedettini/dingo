define('ClientCardCollection', [
    'jquery',
    'underscore',
    'backbone',
    'ClientCardModel'
], function($, _, Backbone, ClientCard) {
    var ClientCardCollection = Backbone.Collection.extend({
        model : ClientCard,
        url: 'api/client_cards'
    });

    return ClientCardCollection;
});
