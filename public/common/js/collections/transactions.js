define('TransactionCollection', [
  'jquery',
  'underscore',
  'backbone',
  'TransactionModel'
], function($, _, Backbone, Transaction) {
  var TransactionCollection;

  TransactionCollection = Backbone.Collection.extend({
      model : Transaction
      //~ url   : "api/clients"
  });

  return TransactionCollection;
});
