define('BingoCollection', [
    'jquery',
    'underscore',
    'backbone',
    'BingoModel'
], function($, _, Backbone, Bingo) {
    var BingoCollection = Backbone.Collection.extend({
        model : Bingo,
        url   : "api/bingos"
    });

    return BingoCollection;
});
