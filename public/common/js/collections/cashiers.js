define('CashierCollection', [
  'jquery',
  'underscore',
  'backbone',
  'CashierModel'
], function($, _, Backbone, Cashier) {
  var CashierCollection;

  CashierCollection = Backbone.Collection.extend({
    model : Cashier,
    url   : "api/cashiers"
  });

  return CashierCollection;
});
