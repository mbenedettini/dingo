define('SettingCollection', [
  'jquery',
  'underscore',
  'backbone',
  'SettingModel'
], function($, _, Backbone, Setting) {

  var SettingCollection = Backbone.Collection.extend({
    model : Setting,
    url   : "api/settings"
  });

  return SettingCollection;
});
