define('ClientCardBulkReturning', [
    'jquery',
    'underscore',
    'backbone',
    'ClientCardCollection'
], function($, _, Backbone, ClientCardCollection) {
    var ClientCardBulkReturning = Backbone.Model.extend({
        urlRoot:  "/api/client_cards/return",

        defaults : {
            quantity: 1,
            series : false
        },

        getCollection: function() {
            var model = this.get('model');
            return new ClientCardCollection(model);
        }

    });

    return ClientCardBulkReturning;
});
