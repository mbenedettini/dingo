define([
    'jquery',
    'underscore',
    'backbone'
], function($, _, Backbone) {
    var CashierPassword = Backbone.Model.extend({
        urlRoot: function() { return "/api/cashiers/" + this.get("cashierId") + "/password"; },

        defaults: {
            cashierId: 0,
            newPassword: ''
        },

    });

    return CashierPassword;
});
