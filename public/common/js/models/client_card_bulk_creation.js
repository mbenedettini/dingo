define('ClientCardBulkCreation', [
    'jquery',
    'underscore',
    'backbone',
    'ClientCardCollection'
], function($, _, Backbone, ClientCardCollection) {
    var ClientCardBulkCreation = Backbone.Model.extend({
        urlRoot:  "/api/client_cards",

        defaults : {
            quantity: 1,
            series : false
        },

        getCollection: function() {
            var model = this.get('model');
            return new ClientCardCollection(model);
        }

    });

    return ClientCardBulkCreation;
});
