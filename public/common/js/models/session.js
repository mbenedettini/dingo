define([
    'utils',
    'jquery',
    'underscore',
    'backbone',
    'config',
    'jqueryCookie'
], function(utils, $, _, Backbone, Config, JqueryCookie) {

    var Session = Backbone.Model.extend({

        // TODO: Still need validation
        // validate: {},

        urlRoot: '/api/sessions',

        defaults: {
            userId: null,
            username: null,
            balance: 0,

            // Status of the session
            loggedIn: false,
        },
        
        isAuthorized: function(callback) {
            utils.debug("@@@ isAuthorized?");
            var that = this;
            if (that.get('loggedIn')) {
                utils.debug("@@@ found!");
                callback(true);
            } else {
                utils.debug("@@@ NOT found .. chequing cookies");
                if (that.readCookie() ){
                    utils.debug("@@@ found!, checking server side...");
                    that.checkIfImLogin(callback);
                }
                else {
                    callback(false);
                }
            }
        },
        
        setUsername: function(username) {
            this.set({'username': username});
        },

        setUserId: function(userId) {
            this.set({'userId': userId});
        },

        getUsername: function(){
            return this.get('username');
        },
        
        login: function(attrs) {
            // POST to /session            
            var that = this;
            
            this.save(attrs).success(
                function(data) { 
                    utils.debug("Login ...", data);
                    that._loginStuff(data);
                    that.createCookie(true);
                }
            ).error(
                function(json) {
                    alert("Error al hacer la request", json);
                    $(".ajax-loading").css("display", "none");
                }
            );
        },
        
        checkIfImLogin: function(callback) {
            var that = this;
            utils.debug("checkIfImLogin ...");
            $.get(that.urlRoot).success(
                function(data) {
                    $(".ajax-loading").css("display", "none");
                    utils.debug("Si ... %j ", data );
                    utils.debug("Veamos...%j", data);
                    if (data) {
                        that.setUsername(data.username);
                        that.setUserId(data.user_id);
                        that.set('balance', data.balance);
                        that.set('loggedIn', true);
                        callback(true);
                    }
                    else {
                        utils.debug("NOP, NO estabamos logeados ...");
                        that.logout();
                        callback(false);
                    }
                }
            ).error(
                function(json) {
                    utils.debug("Error !!");
                    that.logout();
                    callback(false);
                }
            );
        },
        
        logout: function(){
            utils.debug("Session logout. Destroying model...");
            var that = this;
            
            that.destroyCookie();
            
            $.get(this.urlRoot + "/destroy").success(function() {
                that.clear();
                utils.debug("logout success...", that.toJSON());
            });
        },
        
        _loginStuff: function(data){
            var that = this;
            $(".ajax-loading").css("display", "none");
            utils.debug("Veamos...", data);
            if (data) {
                that.setUsername(data.username);
                that.setUserId(data.user_id);
                that.set({'loggedIn': true});
            }
            else {
                alert("Usuario o contraseña incorrectos");
                that.logout();
            }          
        },
        
        createCookie: function() {
            utils.debug("#### creando cookie");
            $.cookie("loggedIn", true);
            utils.debug("#### listo cookie, " + $.cookie("loggedIn"));
        },
        
        readCookie: function(){
            utils.debug("#### Reading cookie: " + $.cookie("loggedIn") );
            return $.cookie("loggedIn");
        },
        
        destroyCookie: function(){
            utils.debug("#### Delete cookie");
            $.cookie("loggedIn", null);
        }
    });
    
    return Session;
});
