define('CashierModel', [
    'utils',
    'jquery',
    'underscore',
    'backbone',
    'CashierPassword',
    'ClientCollection'
], function(utils, $, _, Backbone, CashierPassword, ClientCollection) {
  var Cashier;

  Cashier = Backbone.Model.extend({
      idAttribute: "id",
      urlRoot: "/api/cashiers",
      initialize: function() {
          this.clients = new ClientCollection();
          this.clients.url = '/api/cashiers/' + this.id + '/clients';

          this.on('change:isBlocked change:isSuspended', this.updateStatusLabel, this);
          this.updateStatusLabel();
      },
      // set defaults for checking existance in the template for the new model
      defaults: {
          name      : '',
          username  : '',
          password  : '',
          email     : '',
          status    : 0,
          isBlocked : false,
          isSuspended: false,
          statusLabel: '',
          statusClass: '',
          statusButton: ''
      },
      validate: function(attrs) {
          var requiredFields, i, len, nameLen, lastNameLen, compLen, errors = {};

          /**
           * HACK: don't validate when silent is passed as an attribute
           * Useful when fetching model from server only by id
           */
          if (!attrs._silent) {
              // check required fields
              requiredFields = {
                  'username': 'Nombre de usuario o nick',
                  'name': 'Nombre', 
                  // 'password': 'Contraseña', 
                  'email': 'E-mail',
              };

              _.each(_.keys(requiredFields), function(f) {
                    if (! f in attrs || attrs[f] === '') {
                        errors[f] = requiredFields[f] + ' es requerido';
                    }
                });

              // check valid name
              nameLen = (attrs.name) ? attrs.name.length : null;
              if (nameLen < 2 || nameLen > 100) {
                  errors.name = "nombre inválido";
              }
              
              var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
              if (! re.test(attrs.email)){
                  errors.email = "Email inválido";
              }

              /*
              var telephoneLen = (attrs.telephone) ? attrs.telephone.length : null;
              if (telephoneLen < 5) {
                  errors.telephone = "teléfono inválido";
              }
               */

              if (_.keys(errors).length) {
                  return {
                      errors: errors
                  };
              }
          }

          // utils.debug("Validate OK");
          return;
      },

      resetPassword: function(callback, errback) {
          var newPassword = new CashierPassword({'cashierId': this.get('id')});
          newPassword.save(null, {
              success: function(model, res) {
                  callback();
              },
              error:  function(model, res) {
                  utils.debug("newPassword error:");
                  utils.debug(res);
                  errback();
              }
          });
      },

      suspend : function(opts) {
          var model = this;
          var url = model.url() + '/suspend';

          var callback = opts.success;
          var errback = opts.error;
          delete opts['success'];
          delete opts['error'];

          var options = {
              url: url,
              type: 'POST',
              success: function() {
                  console.log("CashierModel suspend success");
                  model.set('isSuspended', true);

                  if (callback) callback();
              },
              error: function() {}
          };

          _.extend(options, opts);
          
          return (this.sync || Backbone.sync).call(this, null, this, options);
      },

      unsuspend : function(opts) {
          var model = this;
          var url = model.url() + '/unsuspend';

          var callback = opts.success;
          var errback = opts.error;
          delete opts['success'];
          delete opts['error'];

          var options = {
              url: url,
              type: 'POST',
              success: function() {
                  console.log("CashierModel unsuspend success");
                  model.set('isSuspended', false);

                  if (callback) callback();
              },
              error: function() {}
          };

          _.extend(options, opts);
          
          return (this.sync || Backbone.sync).call(this, null, this, options);
      },

      updateStatusLabel : function() {
          var statusLabel = 'Activo';
          var statusClass = '';
          var statusButtonClass = 'btn-danger';
          var statusButtonId = 'suspend-btn';
          var statusButtonLabel = 'Suspender';

          if (this.get('isSuspended')) {
              statusLabel = 'Suspendido';
              statusClass = 'label-important';
              statusButtonClass = 'btn-info';
              statusButtonId = 'unsuspend-btn';
              statusButtonLabel = 'Reactivar';
          }
          else if (this.get('isBlocked')) {
              statusLabel = 'Bloqueado';
              statusClass = 'label-warning';
              statusButtonClass = 'btn-danger';
              statusButtonId = 'suspend-btn';
              statusButtonLabel = 'Suspender';
          }
          
          // utils.debug("Client #" + this.get('id') + " : updating status label to " + statusLabel);
          this.set('statusLabel', statusLabel);
          this.set('statusClass', statusClass);
          this.set('statusButton', '<button id="' + statusButtonId + '" type="button" class="btn ' + statusButtonClass + '">' + statusButtonLabel + '</button>');
      },

  });

  return Cashier;
});
