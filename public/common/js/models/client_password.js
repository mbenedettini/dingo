define([
    'jquery',
    'underscore',
    'backbone'
], function($, _, Backbone) {
    var ClientPassword = Backbone.Model.extend({
        urlRoot: function() { return "/api/clients/" + this.get("clientId") + "/password"; },

        defaults: {
            clientId: 0,
            newPassword: ''
        },

    });

    return ClientPassword;
});
