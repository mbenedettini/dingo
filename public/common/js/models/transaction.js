define('TransactionModel', [
  'jquery',
  'underscore',
  'backbone'
], function($, _, Backbone) {
    var Transaction;

    Transaction = Backbone.Model.extend({
        idAttribute: "id",
        urlRoot: function() { return "/api/clients/" + this.get("clientId") + "/transactions"; },
        // set defaults for checking existance in the template for the new model
        defaults: {
            clientId:   0,
            amount:     0.00,
            description: '',
        },
        
        initialize: function() {
            var type = this.get("type");
            var typeDescription = "";
            
            if (type == 1) typeDescription = "Depósito";
            if (type == 2) typeDescription = "Extracción";
            if (type == 3) typeDescription = "Compra de cartón";
            if (type == 4) typeDescription = "Pago de línea";
            if (type == 5) typeDescription = "Pago de bingo";
            if (type == 6) typeDescription = "Devolución";
            if (type == 7) typeDescription = "Pago de bingo acumulado";
            
            
            this.set("typeDescription", typeDescription);
        },
    });

    return Transaction;
});
