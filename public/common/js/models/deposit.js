define('DepositModel', [
  'jquery',
  'underscore',
  'backbone'
], function($, _, Backbone) {
  var Deposit;

  Deposit = Backbone.Model.extend({
    idAttribute: "id",
    urlRoot: function() { return "/api/clients/" + this.get("clientId") + "/deposits"; },
    // set defaults for checking existance in the template for the new model
    defaults: {
        clientId:   0,
        amount:     0.00,
        description: '',
    },
    validate: function(attrs) {
      var fields, i, len, nameLen, compLen, errors = {};

      /**
       * HACK: don't validate when silent is passed as an attribute
       * Useful when fetching model from server only by id
       */
      if (!attrs._silent) {
        // check required fields
        fields = ['amount'];

        // Check valid amount
        if (! _.isNumber(attrs.amount)) {
            errors.amount = "monto inválido";
        }

        if (_.keys(errors).length) {
          return {
            errors: errors
          };
        }
      }

    }
  });

  return Deposit;
});
