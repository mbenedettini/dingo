define('ClientCardModel', [
    'utils',
    'jquery',
    'underscore',
    'backbone'
], function(utils, $, _, Backbone) {
    var ClientCardModel = Backbone.Model.extend({
        idAttribute: "id",

        markNumber: function(number) {
            var self = this;
            
            var lineNames = ['first','second','third'];
            
            var i;
            for (i=0; i < lineNames.length; i++) {
                var lineName = lineNames[i];
                var lineAttribute = lineName + 'Line';
                var lineCalledAttribute = lineAttribute + 'Called';

                // utils.debug(lineAttribute);
                // utils.debug(lineCalledAttribute);

                var line = this.get(lineAttribute);
                var called = this.get(lineCalledAttribute);

                // utils.debug(line);
                // utils.debug(called);

                // Shortcut if the number has already been called
                if (called && _.indexOf(called, number) >= 0) { return true; }

                if (_.indexOf(line, number) >= 0) {
                    called.push(number);
                    self.set(lineCalledAttribute, called);

                    return true;
                }

            }

            return false;
        },
        
    });

    return ClientCardModel;
});
