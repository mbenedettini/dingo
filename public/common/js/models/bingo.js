define('BingoModel', [
    'utils',
    'jquery',
    'underscore',
    'backbone',
    'moment',
    'config'
], function(utils, $, _, Backbone, moment, config) {
    var Bingo = Backbone.Model.extend({
        idAttribute: "id",
        urlRoot: "/api/bingos",
        initialize: function() {
            this.on('change:isSelling', function() {
                utils.debug("BingoModel: isSelling has changed to " + this.get('isSelling'));
                if (this.get('isSelling')) {
                    this.startSelling();
                }
                else {
                    this.stopSelling();
                }
            }, this);

            this.on('change:sellingFinishesAt', function() {
                utils.debug("BingoModel: sellingFinishesAt has changed to " + this.get('sellingFinishesAt'));
                if (this.get('isSelling')) {
                    this.startSelling();
                }
                else {
                    this.stopSelling();
                }
            }, this);

            this.on('change:cardColor', function() {
                var htmlColor = this.decodeCardColor(this.get('cardColor'));
                this.set('cardHtmlColor', htmlColor);
            }, this);

            this.on('change:calledNumbers', function() {
                this.set('calledNumbersCount', this.get('calledNumbers').length);
            });

            this.set('cardHtmlColor', this.decodeCardColor(this.get('cardColor')));

            if (this.get('isSelling')) {
                this.startSelling();
            }
        },
                                          
        // set defaults for checking existance in the template for the new model
        defaults: {
            cardPrice: 0.00,
            cardColor: 1,
            cardHtmlColor: null,
            linePrize: 0.00,
            bingoPrize: 0.00,
            /* TODO: these are hacks for the front. I think there must be two different models:
             * one for back and another for front. One extending the other, though.
             */
            linePrizeRate: config.defaults ? config.defaults.linePrizeRate : 0.0001,
            bingoPrizeRate: config.defaults ? config.defaults.bingoPrizeRate : 0.0001,
            lapseBetweenNumbers: config.defaults ? config.defaults.lapseBetweenNumbers : 5,
            sellingInterval: config.defaults ? config.defaults.sellingInterval : 300,
            calledNumbers: [],
            remainingNumbers: [],
            sellingIntervalTimer: null,
            sellingFinishesAt: null,
            starts: null,
            calledNumbersCount: 0,
            cardPurchaseQuantity: 1,
            clientsCount: 0, // how many clients have purchased cards for this bingo
            totalClientsCount: 0, // how many clients are logged in or connected at the moment
            shownTotalClientsCount: 0, // how many connected clients to show
            visitorsCount: 0, // how many clients are connected but haven't purchased (totalClientsCount - clientsCount)
            minClientsCount: config.defaults ? config.defaults.minClientsCount : 0,
            minTotalClientsCount: config.defaults ? config.defaults.minTotalClientsCount : 0,
            sellingCancellationTimeout: config.defaults ? config.defaults.sellingCancellationTimeout : 0,
            clientCardsCount: 0, // how many cards have been sold,
            accumulatedPrize: 0,
            accumulatedOrder: 0,
            maxBall: 0,
            currentClientCardsCount: 0 // how many cards the current client has purchased
        },

        decodeCardColor: function(cardColor) {
            var htmlColor = "#FAA7A7"; // Default: RED
            if (cardColor == 1) {
                // RED
            }
            else if (cardColor == 2) {
                htmlColor = "#B1A8FF"; // BLUE
            }
            else if (cardColor == 3) {
                htmlColor = "#77ED84"; // GREEN
            }
            else if (cardColor == 4) {
                htmlColor = "#FFB3E4"; // PINK
            }
            else if (cardColor == 5) {
                htmlColor = "#FFC387"; // ORANGE
            }

            return htmlColor;

        },

        updateSellingIntervalTimer: function() {
            //utils.debug("updateSellingIntervalTimer");
            var dueDate = moment(this.get('sellingFinishesAt'), "YYYY-MM-DD HH:mm:ss");
            //utils.debug(dueDate);
            var span = moment.duration(dueDate - (new Date()).getTime() );
            // utils.debug("span is: " + span);
            if (span < 1000) {
                span = 0;
                this.stopSelling();
            }
            this.set('sellingIntervalTimer', span);
        },

        getSellingIntervalTimer: function() {
            var timer = this.get('sellingIntervalTimer');
            if (timer) {
                return timer.minutes() + ":" + timer.seconds();
            }
            else {
                return "00:00";
            }
        },

        start: function(opts) {
            var model = this,
            url = model.url() + '/start',
            
            // $.ajax() options
            options = {
                url: url,
                type: 'POST'
            };
            
            // add any additional options, e.g. a "success" callback or data
            _.extend(options, opts);
            
            return (this.sync || Backbone.sync).call(this, null, this, options);
        },
                                          
        pause: function(opts) {
            /* Pause is actually a light pause */
            var model = this,
            url = model.url() + '/pause-light',
            
            options = {
                url: url,
                type: 'POST'
            };
            
            _.extend(options, opts);
            
            return (this.sync || Backbone.sync).call(this, null, this, options);
        },

        stop: function(opts) {
            /* Stop is a pause operation */
            var model = this,
            url = model.url() + '/pause',
            
            options = {
                url: url,
                type: 'POST'
            };
            
            _.extend(options, opts);
            
            return (this.sync || Backbone.sync).call(this, null, this, options);
        },
                                          
        startSelling: function() {
            /* This method can be safely called anytime */
            var self = this;
            utils.debug("startSelling");
            
            // Safety shorcut
            if (!this.get('isSelling')) return;

            this.updateSellingIntervalTimer();
            if (!this._sellingUpdateInterval) {
                this._sellingUpdateInterval = setInterval(function() {
                    self.updateSellingIntervalTimer();
                }, 1000);
                utils.debug("Timer Interval ID : " + this._sellingUpdateInterval);

                this._killSellingUpdateInterval = setTimeout(function() {
                    self.stopSelling();
                }, this.get('sellingInterval') * 1000);
            }
        },

        stopSelling: function() {
            utils.debug("stopSelling");
            utils.debug(this._sellingUpdateInterval);
            clearInterval(this._sellingUpdateInterval);
            this._sellingUpdateInterval = null;
        },

        validate: function(attrs) {
            var requiredFields, errors = {};

            /**
             * HACK: don't validate when silent is passed as an attribute
             * Useful when fetching model from server only by id
             */
            if (!attrs._silent) {
                //utils.debug("Validating!");
                //utils.debug(attrs);

                // check required fields
                requiredFields = {
                    'cardPrice': 'Precio de carton', 
                    'cardColor': 'Color de carton',
                    'lapseBetweenNumbers': 'Tiempo entre numeros',
                    'sellingInterval': 'Duración de la venta',
                    'linePrize': 'Premio inicial de Linea',
                    'linePrizeRate': 'Porcentaje a Linea', 
                    'bingoPrize': 'Premio inicial de Bingo', 
                    'bingoPrizeRate': 'Porcentaje a Bingo',
                    'minClientsCount': 'Cantidad minima de compradores',
                    'minTotalClientsCount': 'Cantidad minima de jugadores conectados'
                    // 'remainingNumbers': 'Numeros por salir',
                    // 'calledNumbers': 'Numeros que salieron'
                    // 'starts': 'Hora de comienzo'
                };

                //for (var f in Object.keys(requiredFields)) {
                _.each(_.keys(requiredFields), function(f) {
                    if (! f in attrs || attrs[f] === '') {
                        errors[f] = requiredFields[f] + ' es requerido';
                    }
                });

                
                // check fields content
                if (isNaN(parseInt(attrs.cardPrice)) ) {
                    errors.cardPrice = "Precio de cartón inválido";
                }
                if (isNaN(parseInt(attrs.cardColor)) ) {
                    errors.cardColor = "Color de cartón inválido";
                }
                if (! parseInt(attrs.lapseBetweenNumbers) ) {
                    errors.lapseBetweenNumbers = "Tiempo entre números inválido";
                }
                if (! parseInt(attrs.sellingInterval) ) {
                    errors.sellingInterval = "Duración de la venta inválido";
                }

                if (isNaN(parseFloat(attrs.linePrize)) ) {
                    utils.debug("linePrize");
                    utils.debug(attrs.linePrize);
                    errors.linePrize = "Premio para Linea inválido";
                }
                if (! parseFloat(attrs.linePrizeRate)) { // Cannot be zero
                    errors.linePrizeRate = "Porcentaje para Linea inválido";
                }
                if (isNaN(parseFloat(attrs.bingoPrize)) ) {
                    errors.bingoPrize = "Premio para Bingo inválido";
                }
                if (! parseFloat(attrs.bingoPrizeRate) ) { // Cannot be zero
                    errors.bingoPrizeRate = "Porcentaje para Bingo inválido";
                }
                if (isNaN(parseInt(attrs.minClientsCount)) ) {
                    errors.minClientsCount = "Cantidad mínima de compradores de cartones inválida";
                }
                if (isNaN(parseInt(attrs.minTotalClientsCount)) ) {
                    errors.minTotalClientsCount = "Cantidad mínima de jugadores online inválido";
                }
                if (isNaN(parseInt(attrs.sellingCancellationTimeout)) ) {
                    errors.sellingCancellationTimeout = "Duración máxima de venta es inválida";
                }

                if (attrs.starts && ! moment(attrs.starts).isValid()) {
                    errors.starts = "Hora y fecha de comienzo inválidos";
                }

                if (attrs.remainingNumbers && _.any(attrs.remainingNumbers)) {
                    if (! _.all(attrs.remainingNumbers, function(n) { return parseInt(n);})) {
                        errors.remainingNumbers = "Numeros por salir inválidos";
                    }
                }
                if (attrs.calledNumbers && _.any(attrs.calledNumbers)) {
                    if (! _.all(attrs.calledNumbers, function(n) { return parseInt(n);})) {
                        errors.calledNumbers = "Numeros que salieron inválidos";
                    }
                }

                if (_.keys(errors).length) {
                    utils.debug("Returning with error!");
                    utils.debug(errors);
                    return {
                        errors: errors
                    };
                }
            }

        },

        save: function(attributes, options) {
            attributes || (attributes = {});
            attributes['starts'] = attributes.starts ? attributes.starts.format() : null;
            Backbone.Model.prototype.save.call(this, attributes, options);
        },

        parse: function(response) {
            if (response) {
                response.starts = moment(response.starts, "YYYY-MM-DD HH:mm:ss");
                response.created = moment(response.created, "YYYY-MM-DD HH:mm:ss");
                response.updated = moment(response.updated, "YYYY-MM-DD HH:mm:ss");
            }

            return response;
        }
    });

    return Bingo;
});
