define('ClientModel', [
    'utils',
    'jquery',
    'underscore',
    'backbone',
    'TransactionCollection',
    'ClientCardCollection',
    'ClientPassword'
], function(utils, $, _, Backbone, TransactionCollection, ClientCardCollection, ClientPassword) {
  var Client;

  Client = Backbone.Model.extend({
      idAttribute: "id",
      urlRoot: "/api/clients",
      initialize: function() {
          this.transactions = new TransactionCollection;
          this.transactions.url = '/api/clients/' + this.id + '/transactions';

          this.client_cards = new ClientCardCollection;
          this.client_cards.url = '/api/clients/' + this.id + '/client_cards';

          this.on('change:isBlocked change:isSuspended', this.updateStatusLabel, this);
          this.updateStatusLabel();
      },
      // set defaults for checking existance in the template for the new model
      defaults: {
          name      : '',
          lastName  : '',
          username  : '',
          password  : '',
          email     : '',
          telephone : '',
          balance   : 0,
          isBlocked : false,
          isSuspended: false,
          statusLabel: '',
          statusClass: '',
          statusButton: ''
      },

      validate: function(attrs) {
          var requiredFields, i, len, nameLen, lastNameLen, compLen, errors = {};

          /**
           * HACK: don't validate when silent is passed as an attribute
           * Useful when fetching model from server only by id
           */
          utils.debug("Validating");
          utils.debug(attrs);
          if (!attrs._silent) {
              // check required fields
              requiredFields = {
                  'username': 'Nombre de usuario o nick',
                  'name': 'Nombre', 
                  // 'password': 'Contraseña', 
                  'lastName': 'Apellido',
                  'email': 'E-mail',
                  // 'telephone': 'Teléfono'
              };

              _.each(_.keys(requiredFields), function(f) {
                    if (! f in attrs || attrs[f] === '') {
                        errors[f] = requiredFields[f] + ' es requerido';
                    }
                });

              // check valid name
              nameLen = (attrs.name) ? attrs.name.length : null;
              if (nameLen < 2 || nameLen > 100) {
                  errors.name = "nombre inválido";
              }

              // check valid lastName
              lastNameLen = (attrs.lastName) ? attrs.lastName.length : null;
              if (!lastNameLen || (lastNameLen < 2 || lastNameLen > 100)) {
                  errors.lastNameLen = "apellido inválido";
              }
              
              var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
              if (! re.test(attrs.email)){
                  errors.email = "Email inválido";
              }

              /*
              var telephoneLen = (attrs.telephone) ? attrs.telephone.length : null;
              if (telephoneLen < 5) {
                  errors.telephone = "teléfono inválido";
              }
               */

              if (_.keys(errors).length) {
                  return {
                      errors: errors
                  };
              }
          }

          utils.debug("Validate OK");
          return;
      },

      decreaseBalance: function(amount) {
          var oldBalance = this.get('balance');
          this.set('balance', oldBalance - amount);
      },

      increaseBalance: function(amount) {
          var oldBalance = this.get('balance');
          this.set('balance', oldBalance + amount);
      },

      resetPassword: function(callback, errback) {
          var newPassword = new ClientPassword({'clientId': this.get('id')});
          newPassword.save(null, {
              success: function(model, res) {
                  utils.debug("newPassword response:");
                  utils.debug(model);
                  callback();
              },
              error:  function(model, res) {
                  utils.debug("newPassword error:");
                  utils.debug(res);
                  errback();
              }
          });
      },

      updateStatusLabel : function() {
          var statusLabel = 'Activo';
          var statusClass = '';
          var statusButtonClass = 'btn-danger';
          var statusButtonId = 'suspend-btn';
          var statusButtonLabel = 'Suspender';

          if (this.get('isSuspended')) {
              statusLabel = 'Suspendido';
              statusClass = 'label-important';
              statusButtonClass = 'btn-info';
              statusButtonId = 'unsuspend-btn';
              statusButtonLabel = 'Reactivar';
          }
          else if (this.get('isBlocked')) {
              statusLabel = 'Bloqueado';
              statusClass = 'label-warning';
              statusButtonClass = 'btn-danger';
              statusButtonId = 'suspend-btn';
              statusButtonLabel = 'Suspender';
          }
          
          // utils.debug("Client #" + this.get('id') + " : updating status label to " + statusLabel);
          this.set('statusLabel', statusLabel);
          this.set('statusClass', statusClass);
          this.set('statusButton', '<button id="' + statusButtonId + '" type="button" class="btn ' + statusButtonClass + '">' + statusButtonLabel + '</button>');
      },

      suspend : function(opts) {
          var model = this;
          var url = model.url() + '/suspend';

          var options = {
              url: url,
              type: 'POST'
          };

          _.extend(options, opts);
          
          return (this.sync || Backbone.sync).call(this, null, this, options);
      },

      unsuspend : function(opts) {
          var model = this;
          var url = model.url() + '/unsuspend';

          var options = {
              url: url,
              type: 'POST'
          };

          _.extend(options, opts);
          
          return (this.sync || Backbone.sync).call(this, null, this, options);
      }
  });

  return Client;
});
