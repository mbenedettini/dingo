define('SettingModel', [
    'jquery',
    'underscore',
    'backbone'
], function($, _, Backbone) {

  var Setting = Backbone.Model.extend({
    idAttribute: "key",
    urlRoot: "/api/settings",

    // set defaults for checking existance in the template for the new model
    defaults: {
      name      : '',
      lastName  : '',
    }

  });

  return Setting;
});
