define(
    [
  'jquery',
  'underscore',
  'backbone',
  './router',
  'config', 
  'socket.io', 
  'bootstrap'
], function($, _, Backbone, Router, config, io, bootstrap) {

    function initialize() {
        var app = new Router();
                
        Backbone.history.start();
    }

  // TODO: error handling with window.onerror
  // http://www.slideshare.net/nzakas/enterprise-javascript-error-handling-presentation

  return {
    initialize: initialize
  };
});
