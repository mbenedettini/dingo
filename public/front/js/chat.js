var socket;

$(document).ready(function() {

    $('#myModal').modal('show');

    // Give focus to the nickname input when the page loads
    $("#nick-input").focus();

    // Create a connection to the server
    socket = io.connect(document.URL);
    
    /*
     Handle submission of the join form, try to join the chat-room
     */
    //$("#join-form").submit(function(ev) {
    $('#button').on('click', function(e) {
        e.preventDefault();
        doLogin();
    });

    $('#join-form').on('keypress', function(e) {
        if ( e.which == 13 ) {
            e.preventDefault();
            doLogin();
        }
    });
});


var doLogin = function() {
    console.log("Submitting form");

    // Prevent the browser from submitting the form via HTTP
    //ev.preventDefault();

    // Check that a nickname has been entered
    var nick = $("#nick-input").val();
    if (nick) {

        // Send a request to join the chat-room
        // console.log("Trying to join");
        socket.emit("join", nick, function(successful, users) {
            // console.log("Join response: " + successful);

            if (successful) {

                // Hide the join form and show the main chat interface
                $('#myModal').modal('hide');
                //$("#chat").show();

                // Display a welcome message
                var message = "Bienvenido/a, " + nick;
                $("#systemMessageTemplate").tmpl({message: message}).appendTo("#messages");

                // Display the list of connected users on the page
                $.each(users, function(i, user) {
                    $("#userTemplate").tmpl({user: user}).appendTo("#users");
                });

                // Give focus to the message input so the user can start entering a message
                $("#message-input").focus();


                /*
                 Send chat messages
                 ------------------
                 */
                $("#message-form").submit(function(ev) {
                    // Prevent the browser from submitting the form via HTTP
                    ev.preventDefault();

                    // Send the message to the server
                    socket.emit("chat", $("#message-input").val());

                    // Clear the text-box
                    $("#message-input").val("");
                });


                /*
                 Handle chat messages
                 --------------------
                 */
                socket.on("chat", function(message) {

                    // Inject the message into the DOM
                    $("#chatMessageTemplate").tmpl(message).appendTo("#messages");

                    // Auto-scroll the message pane
                    $("#messages").scrollTop($("#messages").prop("scrollHeight") - $("#messages").height());

                });


                /*
                 Handle users joining
                 --------------------
                 */
                socket.on("user-joined", function(user) {

                    // Add to the on-page list of users
                    $("#userTemplate").tmpl({user: user}).appendTo("#users");

                    // Display a message in the main chat stream
                    var message = user + " acaba de entrar";
                    $("#systemMessageTemplate").tmpl({message: message}).appendTo("#messages");

                    // Auto-scroll the message pane
                    $("#messages").scrollTop($("#messages").prop("scrollHeight") - $("#messages").height());

                });


                /*
                 Handle users leaving
                 --------------------
                 */
                socket.on("user-left", function(user) {

                    // Remove from the on-page list of users
                    $("#user-" + user).remove();

                    // Display a message in the main chat stream
                    var message = user + " se fue";
                    $("#systemMessageTemplate").tmpl({message: message}).appendTo("#messages");

                    // Auto-scroll the message pane
                    $("#messages").scrollTop($("#messages").prop("scrollHeight") - $("#messages").height());

                });


                // If the request to join was rejected, show an error message
            } else {
                $("#nick-error").show();
            }
        });
    }
};