define([
    'utils',
    'jquery', 
    'underscore',
    'backbone',
    'text!templates/account.html'
     ], function(utils, $, _, Backbone, tplAccount) {

    var AccountView = Backbone.View.extend({

        events: {
            "click #logout-btn" : "logout",
            "click #refresh-btn": "refresh",
            "click #myaccount-btn": "myaccount",
            "click #chat-btn": "openChat"
        },

        initialize: function() {
            _.bindAll(this, 'render');
            this.template = _.template(tplAccount);

        },

        render: function() {

            $(this.el).html(this.template());


            $(this.el).find('a.myAccount').hover(
                function(){ $(this).addClass('hover'); },
                function(){ $(this).removeClass('hover'); }
            );

            return this;
        },

        logout: function(e) {            
            e.preventDefault();
            this.trigger('logout');
            
        },

        refresh: function(e) {
            e.preventDefault();
            utils.debug("Triggering refresh event");
            this.trigger('refresh');
        },

        myaccount: function(e) {
            e.preventDefault();

            this.trigger('myaccount');
        },

        openChat: function(e) {
            e.preventDefault();

            window.open('/chat-client.html', 'Rosario Bingo Chat', 'height=700,width=1000,menubar=no,location=no');
        }
    });

    return AccountView;

});
 
 
