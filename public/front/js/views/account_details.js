define([
    'utils',
    'jquery', 
    'underscore',
    'backbone',
    'text!templates/account_details.html'
     ], function(utils, $, _, Backbone, tplAccountDetails) {

    var AccountDetailsView = Backbone.View.extend({

        initialize: function(options) {
            _.bindAll(this, 'render');
            this.template = _.template(tplAccountDetails);

            utils.debug("AccountDetailsView.initialize");
            utils.debug(options.client);
            utils.debug(options.transactions);
            this.client = options.client;
            this.transactions = options.transactions;

        },

        render: function() {

            $(this.el).html(this.template({client: this.client.toJSON(), transactions: this.transactions.toJSON()}));

            return this;
        },

    });

    return AccountDetailsView;

});
 
 
