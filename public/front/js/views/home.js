define([
    'utils',
    'jquery',
    'underscore',
    'backbone',
    'rivets',
    'text!templates/home.html',
    'text!templates/calledNumbers.html',
    'text!templates/cardsRow.html',
    'text!templates/card.html',
    'text!templates/upcomingBingos.html',
    './login',
    './account',
    './account_details',
    'ClientCardBulkCreation',
    'ClientCardBulkReturning',
    'ClientCardCollection',
    '../common/models/session',
    'BingoModel',
    'ClientModel',
    'BingoCollection',
    './durations',
    'jqueryUI'
], function(utils, $, _, Backbone, rivets,tplHome, tplCalledNumbers, tplCardsRow, tplCard, tplUpcomingBingos, LoginView, AccountView, AccountDetailsView,
            ClientCardBulkCreation, ClientCardBulkReturning, ClientCardCollection, Session, Bingo, Client, BingoCollection, durations) {

    var SPEECH = {
        BINGO_STARTS      : 'bingo_starts',
        CONTINUE_TO_BINGO : 'continue_to_bingo',
        BINGO_FINISHES    : 'bingo_finishes',
        BINGO_CALLED      : 'bingo_called',
        LINE_CALLED       : 'line_called',
        VERIFY_BINGO      : 'verify_bingo',
        VERIFY_LINE       : 'verify_line',
        BALLS             : 'balls',
        HAVE_BEEN_CALLED  : 'have_been_called'
    };

    var getNumberSpeech = function(number, nonClarified) {
        var numberSpeech = number;
        if (nonClarified && number >= 60 && number <= 79) {
            return 'non-clarified/' + numberSpeech;
        }
        else {
            return numberSpeech;
        }
    };

    var HomeView;

    HomeView = Backbone.View.extend({
        initialize: function(options) {
            var self = this;
            this.template = _.template(tplHome);
            this.templateCalledNumbers = _.template(tplCalledNumbers);
            this.cardsRowTemplate = _.template(tplCardsRow);
            this.cardTemplate = _.template(tplCard);
            this.upcomingBingosTemplate = _.template(tplUpcomingBingos);

            this.session = options.session || null;;
            this.bingo = options.bingo || this._getBingoFake();

            this.lineNotificationsQueue = [];
            this.bingoNotificationsQueue = [];
            
            rivets.configure({
                adapter: {
                    subscribe: function(obj, keypath, callback) {
                        obj.on('change:' + keypath, callback);
                    },
                    unsubscribe: function(obj, keypath, callback) {
                        
                        obj.off('change:' + keypath, callback);
                    },
                    read: function(obj, keypath) {
                        
                        return obj.get(keypath);
                    },
                    publish: function(obj, keypath, value) {
                        
                        obj.set(keypath, value);
                    }
                }
            });

            rivets.binders.backgroundcolor = function(el, value) {
                if (! value) {
                    return;
                }
                //utils.debug("Setting backgroundColor to " + value);
                el.style.backgroundColor = value;
            };

            rivets.formatters.latestCalledNumbers = function(value) {
                /* At the moment returns the last 5 called numbers */
                var latestCalledNumbers = value.slice(-5).reverse();

                return latestCalledNumbers;

            };

            rivets.formatters.money = function(value){
                return value.toFixed(2);
            };

            rivets.formatters.time = function(value) {
                /* Formats hours and minutes in a format of hh:mm. Argument must be two
                 * numbers separated by semicolon.
                 */
                var pad = "00";
                var parts = value.split(":");
                var output = [];
                _.each(parts, function(p) {
                    output.push(pad.substring(0, pad.length - p.length) + p);
                });

                return output.join(':');
            };

            window.homeView = this;

        },

        events: {
            "click #buyCards-btn" : "buyCards",
            "click #buySeries-btn" : "buySeries",
            "click #returnCards-btn" : "returnCards",
            "click #returnSeries-btn" : "returnSeries",
            "click #upcomingBingos-btn": "showModalUpcomingBingos",
            "click #notificationsBanner" : "hideBanner",
            "click #mainContainer" : "hideBanner",
            
            /* Testing */
            "click #animate" : "exampleAnimate",
            "click #resetAnimation" : "resetAnimation",
            "click #line": "exampleLineNotification",
            "click #bingo": "exampleBingoNotification",
            "click #justPassedAccumulatedOrder": "exampleJustPassedAccumulatedOrder"
        },

        _getBingoFake: function() {
            return new Bingo({
                    isActive: false,
                    isRunning: false,
                    isSelling: false,
                    isFinished: false,
                    calledNumbers: []
            });
        },


        render: function() {
            var self = this;
            utils.debug("HomeView.render()");

            $(this.el).html(this.template());

            $(this.el).find('a#buySeries-btn').hover(
                function(){ $(this).addClass('plus-sign-hover'); },
                function(){ $(this).removeClass('plus-sign-hover'); }
            );
            $(this.el).find('a#returnSeries-btn').hover(
                function(){ $(this).addClass('minus-sign-hover'); },
                function(){ $(this).removeClass('minus-sign-hover'); }
            );

            $(this.el).find('a#buyCards-btn').hover(
                function(){ $(this).addClass('plus-sign-hover'); },
                function(){ $(this).removeClass('plus-sign-hover'); }
            );
            $(this.el).find('a#returnCards-btn').hover(
                function(){ $(this).addClass('minus-sign-hover'); },
                function(){ $(this).removeClass('minus-sign-hover'); }
            );

            return this;
        },
        
        renderBingo: function(bingo) {
            var self = this;
            /* Renders everything related to the bingo instance:
             * numbers, card price, card color, etc.
             */
            utils.debug("HomeView.renderBingo()");

            if (bingo) {
                utils.debug("renderBingo: Received bingo as parameter: " + bingo);
               
                // Don't leave any running timer
                if (this.bingo) {
                    this.bingo.stopSelling();
                }

                this.bingo = bingo;
            }

            /* At the moment is difficult to render everything without a bingo instance.
             * Therefore, we create a fake.
             */
            if (!this.bingo) {
                // Fake bingo
                this.bingo = this._getBingoFake();
            }

            var calledNumbers = this.bingo.get('calledNumbers');
            this.renderCalledNumbers(calledNumbers);

            // Fetch and render ClientCards
            this.refreshClientCards();

            // Refresh upcomingBingos if appropriate
            if (! this.bingo.get('isActive')) {               
                utils.debug("HomeView: Bingo is NOT active. Rendering upcoming bingos");
                utils.debug(this.bingo.get('id'));
                utils.debug(this.bingo.get('isActive'));
                this.renderUpcomingBingos();
            }

            /* Dunno if it's better to route this event right on the view.
             * For now we'll keep it here, to see how beautiful (or not) this
             * approach is.
             */

            /* TODO: refreshClientCards makes a request and this is not always the right
             * solution. Most of times we can just render the cards received as parameters
             * by these events listeners, avoiding the request and avoiding re-rendering
             * all of the cards.
             */
            this.on('cardsBought', function(cards) {
                utils.debug("Router: 'cardsBought' event");
                utils.debug("Refreshing clientCards");
                var newScrollTop = $('.dingoCardsMainContainer').prop("scrollHeight");
                utils.debug("New scrollTop: " + newScrollTop);
                self.refreshClientCards(function() {               
                    // Scroll div to bottom
                    $('.dingoCardsMainContainer').animate({scrollTop: newScrollTop }, 500);
                });
            });

            this.on('cardsReturned', function(cards) {
                utils.debug("Router: 'cardsReturned' event");
                utils.debug("Refreshing clientCards");
                self.refreshClientCards();
            });
   

            /* RIVETS bindings */
            
            /* TODO: Ok, 5 bindings, perhaps it is time to bind only ONCE for the whole view */

            var bingoBind = rivets.bind($('#bingoInfoRight'), {bingo: this.bingo});
            this._bindBingoInfoRight = bingoBind;
            
            var bingoTopBind = rivets.bind($('#bingoInfoTop'), {bingo: this.bingo});
            this._bindBingoTop = bingoTopBind;
            
            var currentBingoBind = rivets.bind($('#currentBingo'), {bingo: this.bingo});
            this._bindCurrentBingo = currentBingoBind;

            var mainContainerBind = rivets.bind($('#mainContainer'), {bingo: this.bingo});
            this._bindMainContainer = mainContainerBind;

            var calledNumbersCountBind = rivets.bind($('#calledNumbersCount'), {bingo: this.bingo});
            this._bindCalledNumbersCount = calledNumbersCountBind;

            /* /RIVETS */

            /* Effects */
            $('a.upcomingBingos').hover(
                function(){ $(this).addClass('hover'); },
                function(){ $(this).removeClass('hover'); }
            );
        },


        refreshClientCards: function(callback) {
            /* Takes care of retrieving and rendering client cards */            
            var self = this;
            var clientCards = new ClientCardCollection();
            clientCards.fetch({
                success: function(model) {
                    utils.debug("Refreshed clientCards and got: ");
                    utils.debug(model.length);
                    self.renderClientCards(model, callback);
                },
                error: function(model, res) {
                    utils.debug(res);
                }
            });

        },

        renderUpcomingBingos : function() {
            var self = this;
            var bingos = new BingoCollection();
            bingos.fetch({
                data: {upcomingBingos: true},
                success: function(model) {
                    var container = $('#upcomingBingos');
                    utils.debug("upcomingBingos");
                    utils.debug(model);
                    container.html(self.upcomingBingosTemplate({bingos: model.toJSON()}));
                },
                error: function() {}
            });
        },

        showModalUpcomingBingos : function(e) {
            utils.debug("showModalUpcomingBingos");
            e.preventDefault();
            
            var self = this;
            var bingos = new BingoCollection();
            bingos.fetch({
                data: {upcomingBingos: true},
                success: function(model) {
                    utils.debug("Alright, trying to show up the modal");
                    var container = $('#upcomingBingosModalContent');
                    container.html(self.upcomingBingosTemplate({bingos: model.toJSON()}));
                    $('#upcomingBingosModal').modal('show');
                },
                error: function() {}
            });
        },

        showModalMyAccount : function() {
            utils.debug("showModalMyAccount");
            
            var self = this;

            self.client.transactions.fetch({
                success: function(transactions) {
                    var accountDetailsView = new AccountDetailsView({'client': self.client, 'transactions': transactions});
                    $('#myAccountModal').html(accountDetailsView.render().el);
                    $('#myAccountModal').modal('show');
                },
                error: function(transactions, res) {
                    // TODO: .... ?
                }
                
            });
            
        },

        renderClientCards: function(clientCards, callback) {
            this.clientCards = clientCards;
            this.bingo.set('currentClientCardsCount', clientCards.length);
            clientCards = clientCards.toArray();

            //var cardsContainer = $(this.el).find('.cardsContainerContent');
            var cardsContainer = $(this.el).find('.dingoCardsContainer');
            // Clear previous content before begin
            cardsContainer.html('');

            var i;
            var currentCardsRow;
            var currentCC = clientCards.shift();
            var renderedItems = 0;

            while (currentCC) {
                /* And the magic begins! */
                
                if (renderedItems % 2 == 0) {
                    // Even cards. They are placed at left.
                    
                    // A new cardsRow must be placed.
                    currentCardsRow = $(this.cardsRowTemplate());

                    // Let's insert the card
                    utils.debug("Appending card #" + currentCC.get('number') + " on the left");
                    var card = this.renderCard(currentCC);
                    card.addClass('leftDingoCard');
                    card.css('backgroundColor', this.bingo.get('cardHtmlColor'));
                    card.css('borderColor', this.bingo.get('cardHtmlColor'));
                    
                    currentCardsRow.append(card);
                    currentCC = clientCards.shift();                    
                    renderedItems++;
                    
                    cardsContainer.append(currentCardsRow);

                }
                else {
                    // Odd
                    utils.debug("Trying to append card #" + currentCC.get('number') + " on the right");
                    
                    /* Is this card the beginning of a series? If so, a blank space
                     * must be rendered here and let the card be shown on the left.
                     * 
                     * Card number modulus 6 must equal 1 and have 5 consecutive cards
                     * following it.
                     */
                    var remainingCards = [currentCC].concat(clientCards);
                    if (parseInt(currentCC.get('number')) % 6 == 1 // first card number
                        && clientCards.slice(0,5).length == 5 // there must be at least other 5 cards
                        && remainingCards.slice(1,6).every(function(c) { return c.get('number') == this[_.indexOf(this, c) - 1].get('number') + 1; }, remainingCards) ) { // consecutiveness
                        // Add a blank space
                        utils.debug("Instead we will add a blank space");
                        currentCardsRow.append($('<div>&nbsp;</div>').addClass('dingoCard rightDingoCard'));
                        renderedItems++;
                    }  
                    else {
                        utils.debug("Appending card #" + currentCC.get('number') + " on the right");
                        var card = this.renderCard(currentCC);
                        card.addClass('rightDingoCard');
                        card.css('backgroundColor', this.bingo.get('cardHtmlColor'));
                        card.css('borderColor', this.bingo.get('cardHtmlColor'));
                        
                        currentCardsRow.append(card);
                        currentCC = clientCards.shift();
                        renderedItems++;
                    }
                }


            }

            if (callback) {
                callback();
            }
        },

        renderCard: function(card) {
            var renderedCard = $(this.cardTemplate());
            /* Possible TD classes:
             * number, blank.
             * 
             * Called number is achieved via an additional div which holds an 'X'.
             */
            var rows = [];

            var calledNumbers = [].concat(card.get('firstLineCalled'), card.get('secondLineCalled'), card.get('thirdLineCalled'));

            _.each([card.get('firstLine'), card.get('secondLine'), card.get('thirdLine')], function(line) {
                var row = '<tr>';
                _.each(line, function (number) {
                    var tdClass = number < 0 ? 'blank' : 'number';
                    var box = '<td id="cardNumber' + number + '" class="' + tdClass + '"><span id="cardNumber' + number + '">' + (number < 0 ? '' : number) + '</span></td>';
                    if (_.indexOf(calledNumbers, number) >= 0) { // is a called number
                        /* The dummy div wrapper is a trick so after we can get not
                         * only td's content but the td itself.
                         */
                        box = $('<div>').append($(box).clone().append('<div class="calledNumber">X</div>')).html();
                    }
                    row += box;
                });

                row += '</tr>';
                rows.push(row);
            });

            var table = $('<table class="dingoCard"></table>').append(rows.join(''));
            renderedCard.append(table);

            var cardNumber = $('<div class="cardNumber"><span>Carton ' + card.get('number') + '</span></div>');
            renderedCard.append(cardNumber);

            return renderedCard;
        },

        renderCalledNumbers: function(calledNumbers) {
            /* Renders the whole numbers grid */
            var self = this;
            var calledNumbersHtml = this.templateCalledNumbers();
            var numbersContainer = $(this.el).find('#calledNumbers');
            numbersContainer.html(calledNumbersHtml);
            
            _.each(calledNumbers, function(number) {
                self.markNumberOnGrid(number);
            });
        },

        markNumberOnGrid: function(number) {
            /* marks a single number on the grid */
            $('#gridNumber' + number).addClass('marked');
        },
        
        renderAccount: function(){
             /* render Account view */
            var self = this;
            // Retrieve and render client
            var client = new Client({id: self.session.get('userId')});
            client.fetch({
                silent: true,
                success: function(model) {
                    //_.bind(self._renderAccount, self)(model);
                    self.client = model;
                    var accountView = new AccountView();
                    var clientAccount = $(self.el).find('#clientAccount');

                    utils.debug("Rendering accountView.render()");
                    clientAccount.empty().html(accountView.render().el);

                    self.clientAccountBind = rivets.bind(clientAccount, {client: self.client, bingo: self.bingo});

                    accountView.on('logout', function() {
                        self.session.logout();
                    });

                    accountView.on('refresh', function() {
                        utils.debug("HomeView: refreshing this.client");
                        var client = new Client({id: self.client.get('id')});
                        client.fetch({
                            success: function(model) {
                                self.client.set(model);
                            },
                            error: function() {}
                        });
                    });

                    accountView.on('myaccount', function() {
                        self.showModalMyAccount();
                    });

                },
                error: function(err) { 
                    utils.debug("Error al renderar accountView ");
                }
            });


        },

        updateCalledNumbers: function(calledNumbers, cb) {
            /* - Updates the grid with the new called numbers. 
             * - Marks them in the client cards.
             * - Executes the ball animation.
             * - Updates the latest five called numbers.
             * 
             * The argument is an array containing all of the called numbers.
             * What to update is decided here.
             *  
             */
            var self = this;
            var originalCalledNumbers = this.bingo.get('calledNumbers');
            this.bingo.set('calledNumbers', calledNumbers);

            // If more than two numbers have to be updated, refresh the whole thing.
            if (calledNumbers.length > originalCalledNumbers + 2) {
                utils.debug("updateCalledNumbers: more than two numbers to update, refreshing");
                this.renderCalledNumbers(calledNumbers);
                cb();
                // TODO: refresh cards, just in case
            }
            else {
                var newNumbers = _.difference(calledNumbers, originalCalledNumbers);
                utils.debug("New numbers to be rendered: " + newNumbers);
                /*utils.debug(calledNumbers);
                utils.debug(originalCalledNumbers);*/
                var lastNumber = newNumbers.slice(newNumbers.length - 1);

                var animationReady = new $.Deferred();
                var speechReady = new $.Deferred();

                // Only animate the last one (we can't animate several numbers at the same time)
                this.animateNumberCalling(lastNumber, function() {
                    // Update the latest called numbers when the animation is done.
                    self.updateLatestCalledNumbers(calledNumbers);

                    // Render the grid. Except the last one, which will be animated.
                    _.each(newNumbers.slice(0, newNumbers.length - 1), function(number) {
                        self.markNumberOnGrid(number);
                    });

                    utils.debug("updateCalledNumbers: Animation ready");
                    animationReady.resolve();
                });

                // speech
                this.playNumber(lastNumber, function() {
                    utils.debug("updateCalledNumbers: Speech Ready");
                    speechReady.resolve();
                });
                
                // Mark numbers on the cards
                _.each(newNumbers, function(number) {
                    self.markNumberOnCard(number);
                });

                // $.when(animationReady, speechReady).done(
                $.when(animationReady).done(
                    function() {
                        cb();
                    }
                );
                
            }
        },

        playNumber: function(number, cb, nonClarified) {
            this.playSpeech(getNumberSpeech(number, nonClarified), cb);
        },

        playSpeech: function(speech, callback) {
            var self = this;
            utils.debug("playSpeech: " + speech);

            var speechesPlayer = window.speechesPlayer;
            if (!speechesPlayer) {
                utils.debug("NO speechesPlayer: not playing speech!");
                if (callback) callback(false);
                return;
            }
            
            var from = durations[speech][0];
            var duration = durations[speech][1];

            if (isNaN(from) || isNaN(duration)) {
                utils.debug("HomeView.playSpeech: no duration for speech " + speech);
                return;
            }

            // to milliseconds
            from = from * 1000;
            duration = duration * 1000;
            to = from + duration;

            speechesPlayer.play({
                from: from,
                to: to,
                onstop: function() {
                    soundManager._writeDebug('sound stopped at position ' + this.position);
                    if (callback) callback();
                    // note that the "to" target may be over-shot by 200+ msec, depending on polling and other factors.
                }
            });
        },

        playSeveralSpeeches: function(someSpeeches, callback) {
            /* Plays someSpeeches and then invokes callback, if passed. Argument
             * someSpeeches must be an array of values suitables for playSpeech(). */
            var self = this;

            // Copy the argument as we are going to modify it
            var speeches = someSpeeches;

            var firstSpeech = speeches.shift();

            var recursion;
            recursion = function() {
                var nextSpeech = speeches.shift();
                if (nextSpeech) {
                    utils.debug("playSeveralSpeeches: playing next speech");
                    self.playSpeech(nextSpeech, recursion);
                }
                else {
                    utils.debug("playSeveralSpeeches: invoking callback");
                    if (callback) callback();
                }
            };
                    
            this.playSpeech(firstSpeech, recursion);
        },

        updateLatestCalledNumbers: function(calledNumbers) {
            /* Updates (renders) the latest called numbers */

            // The 5 latest
            var latestCalledNumbers = calledNumbers.slice(-5);
            utils.debug("Rendering latestCalledNumbers: " + latestCalledNumbers);

            var contents = "";
            _.each(latestCalledNumbers, function(number) {
                var numberRendered = '<div class="dingoSingleLatestCalledNumber sprite-balls small' + number + '"></div>';
                contents += numberRendered;
            });
            
            $('div.dingoLatestCalledNumbers').fadeOut(250, function() {
                $(this).html(contents).fadeIn(250);
            });
        },

        buyCards: function(e) {
            var self = this;
            e.preventDefault();

            if (this.processingPurchase) return false;
            this.processingPurchase = true;

            var quantity = $.trim($('#buyCards-input').val());
            
            $(this.el).find('a#buyCards-btn').addClass('plus-sign-inactive');
            this._buyCards(quantity, false, function() {
                self.processingPurchase = false;
                $(self.el).find('a#buyCards-btn').removeClass('plus-sign-inactive');
            });

        },

        returnCards: function(e) {
            var self = this;
            e.preventDefault();

            if (this.processingPurchase) return false;
            this.processingPurchase = true;

            var quantity = $.trim($('#buyCards-input').val());
            
            $(this.el).find('a#returnCards-btn').addClass('minus-sign-inactive'); 
            this._returnCards(quantity, false, function() {
                self.processingPurchase = false;
                $(self.el).find('a#returnCards-btn').removeClass('minus-sign-inactive'); 
            });

        },

        buySeries: function(e) {
            var self = this;
            e.preventDefault();

            if (this.processingPurchase) return false;
            this.processingPurchase = true;

            var quantity = $.trim($('#buySeries-input').val());

            $(this.el).find('a#buySeries-btn').addClass('plus-sign-inactive');
            this._buyCards(quantity, true, function() {
                self.processingPurchase = false;
                $(self.el).find('a#buySeries-btn').removeClass('plus-sign-inactive');
            });
        },

        returnSeries: function(e) {
            var self = this;
            e.preventDefault();

            if (this.processingPurchase) return false;
            this.processingPurchase = true;

            var quantity = $.trim($('#buySeries-input').val());

            $(this.el).find('a#returnSeries-btn').addClass('minus-sign-inactive');
            this._returnCards(quantity, true, function() {
                self.processingPurchase = false;
                $(self.el).find('a#returnSeries-btn').removeClass('minus-sign-inactive');
            });
        },

        _buyCards: function(quantity, series, callback) {
            var self = this;
            // TODO: An event can be triggered here and this logic moved to the router
            var ccBulkCreation = new ClientCardBulkCreation({
                quantity: quantity || 1,
                series : series
            });

            ccBulkCreation.save(null, {
                success : function(ccBC, res) {
                    console.log(ccBC);
                    console.log(res);
                    if (!res.error) {
                        var clientCardCollection = ccBC.getCollection();
                        self.client.decreaseBalance(clientCardCollection.length * self.bingo.get('cardPrice'));
                        self.trigger('cardsBought', clientCardCollection);
                        callback(true);
                    } 
                    else {                        
                        $('#purchaseErrors').html('<div class="alert alert-error alert-block"><button type="button" class="close" data-dismiss="alert">&times;</button>' + res.error.message + '</div>');
                        callback(false);

                    }
                    
                },
                error: function(m, res) {
                    if (res && res.errors) {
                        utils.debug(res.errors);
                    }
                    callback();
                }
            });
        },

        _returnCards: function(quantity, series, callback) {
            var self = this;

            var ccBulkReturning = new ClientCardBulkReturning({
                quantity: quantity || 1,
                series : series
            });

            ccBulkReturning.save(null, {
                success : function(ccBC, res) {
                    var clientCardCollection = ccBC.getCollection();
                    self.client.increaseBalance(clientCardCollection.length * self.bingo.get('cardPrice'));
                    self.trigger('cardsReturned', clientCardCollection);
                    callback();
                },
                error: function(m, res) {
                    if (res && res.errors) {
                        utils.debug(res.errors);
                    }
                    callback();
                }
            });

        },

        markNumberOnCard: function(number) {
            /* Checks if number exists in our cards and marks it appropiately */
            // utils.debug("Checking if we have on cards the #" + number);
            // this.ClientCards is a Backbone Collection
	    if (!this.clientCards) {
		utils.debug("HomeView.markNumberOnCard: this.clientCards is null. Not marking anything");
		return;
	    }

            _.each(this.clientCards.toArray(), function(card) {
                // utils.debug(card);
                if (card.markNumber(number)) {
                    // Do the fucking drawing
                    // utils.debug("We have the number " + number + " !!!! ");
                    $('div.dingoCardsContainer').find('td#cardNumber' + number).append('<div class="calledNumber">X</div>');
                }
                else {
                    // utils.debug("We don't have " + number + " :( on this card");
                }
            });
        },

        /* Number calling animation */
        animateNumberCalling: function(number, callback) {
            /* Animates a ball with the number passed as argument and marks it on grid. 
             * Executes callback when the animation finishes. */
            var self = this;
            utils.debug("animateNumberCalling: starting");

            var duration = 2000; // milliseconds

            if (this.numberAnimationInProgress) {
                utils.debug("animateNumberCalling: Animation already in progress. Not executing current animation.");
                callback();
            }
            this.numberAnimationInProgress = true;

            // Remove previously called number highlighting
            //$('#gridNumber' + this._lastAnimatedNumber).removeClass('highlight');
            utils.debug("animateNumberCalling: unhighlighting");
            if (this._lastAnimatedNumber) this.unHighlightNumbers([this._lastAnimatedNumber]);
            utils.debug("animateNumberCalling: unhighlighting DONE");

            // Ball animation
            //utils.debug("Resetting animation");
            utils.debug("animateNumberCalling: resetAnimation()");
            this.resetAnimation();
            utils.debug("animateNumberCalling: resetAnimation() DONE");
            //utils.debug("Setting sprite classes for number " + number);
            $('div#ballSprite').removeClass().addClass('sprite-balls').addClass('normal' + number);
            utils.debug("animateNumberCalling: animateBall()");
            this.animateBall(duration, function() {
                self.numberAnimationInProgress = false;
                utils.debug("animateNumberCalling: animateBall DONE()");
                if (callback) callback();               
            });

            // Grid highlighting
            this.markNumberOnGrid(number);
            //$('#gridNumber' + number).addClass('highlight').animate('fast');
            this.highlightNumbers([number]);

            this._lastAnimatedNumber = number;

            utils.debug("animateNumberCalling: FINISHED");
        },

        exampleAnimate: function(e) {
            var self = this;
            e.preventDefault();
            var number = 66;
            this.animateNumberCalling(number, function() { 
                $('#gridNumber' + number).removeClass();
                self.resetAnimation();
                alert("Animate finished"); 
            });
            
        },

        animateBall: function(totalDuration, callback) {
            var fadeInDuration = totalDuration * 0.20;
            var stayBottomDuration = totalDuration * 0.10;
            var moveRightDuration = totalDuration * 0.70;
            var rotationDuration = totalDuration * 0.40;
            function fadeIn(cb) {
                $("div#dingoBall").fadeIn(fadeInDuration, cb);
            }

            function moveRight(cb) {
                $("div#dingoBall").animate({left : "+=490"}, moveRightDuration, cb);
            }
 

            function rotation() {   
                $('div#ballSprite').rotate({
                    angle:0,
                    animateTo: 360,
                    duration: rotationDuration,            
                    callback: rotation,
                    easing: function(x, t, b, c, d) { return (t/d)*c ; }
                });
            }

            fadeIn(function() {
                setTimeout(function() {
                    rotation();
                    moveRight(function() {
                        $('div#ballSprite').stopRotate();
                        $('div#dingoBall').css({display: "none"});
                        if (callback) callback();
                    });  
                }, stayBottomDuration);
            });
        },

        resetAnimation: function(e) {
            if (e) e.preventDefault();

            $("div#dingoBall").css({top: "-10px", left: "15px", display: "none"});
            $("div#ballSprite").rotate({angle: 0});
        },
        /* /BALL ANIMATION */

        exampleLineNotification: function(e) {
            e.preventDefault();
            var self = this;
            utils.debug("Displaying #dingoInformationRight");
            $('#dingoInformationRight').show();
            utils.debug("Faking line notification processing");
            var numbers = [1,2,3,4,5].map(function(n) { 
                return n * (self.lineNotificationsQueue.length + 1);
            });
            this.processUpdateLineCalled({clientCard: {number: 1234}, lineNumbers: numbers}, function() {});
        },

        exampleBingoNotification: function(e) {
            e.preventDefault();
            utils.debug("Displaying #dingoInformationRight");
            $('#dingoInformationRight').show();
            utils.debug("Faking bingo notification processing");
            this.processUpdateBingoCalled({clientCard: {number: 5678, firstLineNumbers: [1,2,3,4,5], secondLineNumbers: [11,12,13,14,15], thirdLineNumbers: [21,22,23,24,25]} }, function() {});
        },

        exampleJustPassedAccumulatedOrder: function(e) {
            e.preventDefault();
            utils.debug("Testing justPassedAccumulatedOrder");

            if (!this.bingo) {
                this.bingo = this._getBingoFake();
            }

            this.bingo.set('accumulatedOrder', 62);

            this.processUpdateJustPassedAccumulatedOrder({}, function() {});
        },
        
        highlightNumbers: function(numbers, cb) {
            /* Highlights numbers on grid */
            _.each(numbers, function(n) {
                $('#gridNumber' + n).addClass('highlight').animate('fast');
            });
            if (cb) cb();
        },

        unHighlightNumbers: function(numbers, cb) {
            _.each(numbers, function(n) {
                $('#gridNumber' + n).removeClass('highlight').animate('fast');
            });
            if (cb) cb();
        },
        
        /* Updates handlers */
        processUpdateNumberCalled: function(data, cb) {
            // data.calledNumbers
            /* Updates handler to process new called numbers */
            var calledNumbers = data.calledNumbers;
            this.updateCalledNumbers(calledNumbers, function() {
                cb();
            });
        },

        processUpdateSellingStarted: function(data, cb) {
            // data.bingo
            var bingo = new Bingo(data.bingo);
            this.renderBingo(bingo);
            
            // Re-delegate events as the buy button is now being shown
            this.delegateEvents();

            cb();
        },

        processUpdateBingoStarted: function(data, cb) {
            // data.bingo
            var bingo = new Bingo(data.bingo);

            utils.debug("processUpdateBingoStarted: rendering bingo");
            this.renderBingo(bingo);

            utils.debug("processUpdateBingoStarted: Playing speech bingo started");
            this.playSpeech(SPEECH.BINGO_STARTS, function() {
                cb();
            });
        },

        processUpdateBingoUpdated: function(data, cb) {
            // data.bingo
            
            if (!this.bingo) {
                var bingo = new Bingo(data.bingo);
                this.renderBingo(bingo);
            }
            else if (this.bingo && this.bingo.get('id') && this.bingo.get('id') == data.bingo.id) {
                // If there is an existant bingo, update it only if it is the same
                utils.debug("processUpdateBingoUpdated: updating bingo instance");
                //this.renderBingo(bingo);
                this.bingo.set(data.bingo);
            }
            else {
                utils.debug("processUpdateBingoUpdated: not processing because there is another bingo in progress");
            }

            cb();
        },

        processUpdateLineCalled: function(data, cb) {
            // data.clientCard
            // data.lineNumbers
            var self = this;

            if (data) { 
                utils.debug("Queueing line notification for card number " + data.clientCard.number);
                this.lineNotificationsQueue.push([data.clientCard, data.lineNumbers]);
                if (this.lineNotificationsQueue.length == 1) {
                    utils.debug("Running lineNotificationsQueue for the first time");
                    this.runLineNotificationsQueue();
                }
            }

            // We are handling our own queue so we can call back immediately
            cb();

        },

        processUpdateBingoCalled: function(data, cb) {
            // data.clientCard
            var self = this;

            if (data) { 
                this.bingoNotificationsQueue.push(data.clientCard);
                if (this.bingoNotificationsQueue.length == 1) {
                    this.runBingoNotificationsQueue();
                }
            }

            cb();
        },

        processUpdateBalanceUpdated: function(data, cb) {
            // data.id
            // data.balance
            if (this.client.get('id') == data.id) {
                this.client.set('balance', data.balance);
            }

            cb();
        },

        processUpdateVisitorsCount: function(data, cb) {
            // data.bingoId
            // data.visitorsCount
            // data.shownTotalClientsCount
            if (!this.bingo) {
                this.bingo = this._getBingoFake();
            }

            utils.debug('processUpdateVisitorsCount');

            this.bingo.set('visitorsCount', data.visitorsCount);
            this.bingo.set('shownTotalClientsCount', data.shownTotalClientsCount);

            cb();
        },

        processUpdateBingoCancelled: function(data, cb) {
            // data.bingo
            if (this.bingo && this.bingo.get('id') == data.bingo.id) {
                /* Display a notification */
                var bingoCancelledNotification = '<div><span class="bold">BINGO CANCELADO. &nbsp;</span><span>Estimado Cliente: informamos que la partida ha sido cancelada. El importe correspondiente a los cartones comprados será devuelto.</span></div>';
                
                utils.debug("Showing bingo cancelled notification");

                $('#dingoNotifications').append(bingoCancelledNotification).show(1500, function() {
                    $('#dingoNotifications').animate({scrollTop: $('#dingoNotifications').prop("scrollHeight")}, 500);
                });
                
            }

            cb();
        },

        processUpdateAccumulatedPrize: function(data, cb) {
            // data.accumulatedPrize
            // data.accumulatedOrder
            if (!this.bingo) {
                this.bingo = this._getBingoFake();
            }

            this.bingo.set('accumulatedPrize', parseFloat(data.accumulatedPrize));
            this.bingo.set('accumulatedOrder', parseFloat(data.accumulatedOrder));

            cb();
        },

        processUpdateJustPassedAccumulatedOrder: function(data, cb) {
            // no data at the moment

            utils.debug("HomeView.processUpdateJustPassedAccumulatedOrder");

            var accumulatedOrder = this.bingo.get('accumulatedOrder');

            var speeches = [SPEECH.HAVE_BEEN_CALLED, getNumberSpeech(accumulatedOrder, true), SPEECH.BALLS, SPEECH.CONTINUE_TO_BINGO ];
            this.playSeveralSpeeches(speeches, function() {
                cb();
            });

        },
        /* End of updates handlers */

        /* Intended to be used by processUpdateBingoCalled */
        runBingoNotificationsQueue: function() {
            var self = this;
            var currentClientCard = this.bingoNotificationsQueue[0];
            
            if (!currentClientCard) return;

            var animationReady = new $.Deferred();
            var speechReady = new $.Deferred();

            /* Banner */
            var bannerReady = this.showBanner([
                '¡BINGO!',
                'Cartón ' + currentClientCard.number
            ]);

            /* Notification on information box */
            var bingoNotification = '<div>';
            bingoNotification += '<p><span class="bold">¡HAN CANTADO BINGO!</span><span>El cartón </span>';
            bingoNotification += $('<span id="cardNumber"></span>').text(currentClientCard.number).html();
            bingoNotification += '<span> ha ganado bingo</span></p></div>';

            utils.debug("Showing bingo notification for card number #" + currentClientCard.number);

            $('#dingoNotifications').append(bingoNotification).show(1500,
                function() {
                    $('#dingoNotifications').animate({scrollTop: $('#dingoNotifications').prop("scrollHeight")}, 500);
                    animationReady.resolve();

            });

            /* Speech */
            var numbers = currentClientCard.firstLineNumbers.concat(currentClientCard.secondLineNumbers, currentClientCard.thirdLineNumbers);
            var speeches = [];          
            if (! this.bingo.bingoCalledSpeechAlreadyPlayed) {
                speeches.push(SPEECH.BINGO_CALLED);
                this.bingo.bingoCalledSpeechAlreadyPlayed = true;
            }
            speeches.push(SPEECH.VERIFY_BINGO);
            _.each(numbers, function(n) {
                speeches.push(getNumberSpeech(n));
            });
            
            this.playSeveralSpeeches(speeches, function() {
                speechReady.resolve();
            });

            /* Highlighting on grid */
            this.highlightNumbers(numbers);


            $.when(animationReady, speechReady, bannerReady).done(function() {
                utils.debug("Ready: bingo notification animation and speech");
                self.bingoNotificationsQueue.shift(); // Remove it on success
                self.hideBanner();
                if (self.bingoNotificationsQueue.length) {
                    // Unhighlight and then run the queue
                    self.unHighlightNumbers(numbers, function() {
                        self.runBingoNotificationsQueue();
                    });
                }
                else {
                    // End of bingo notifications queue
                    self.playSpeech(SPEECH.BINGO_FINISHES);
                }
            });

        },

        /* Intended to be used by processUpdateLineCalled() */
        runLineNotificationsQueue: function() {
            var self = this;
            utils.debug("runLineNotificationsQueue");
            
            if (this.lineNotificationsQueue.length == 0) return;

            var currentNotification = this.lineNotificationsQueue[0];
            
            var clientCard = currentNotification[0];
            var lineNumbers = currentNotification[1];

            /* Banner */
            var bannerReady = this.showBanner([
                '¡LINEA!',
                'Cartón ' + clientCard.number
            ]);

            /* Notification on information box */
            var lineNotification = '<div>';
            lineNotification += '<p><span class="bold">¡HAN CANTADO LINEA!</span><span>El cartón </span>';
            lineNotification += $('<span id="cardNumber"></span>').text(clientCard.number).html();
            lineNotification += '<span> ha ganado línea</span></p></div>';
            
            utils.debug("Showing line notification for card number #" + clientCard.number);

            var animationReady = new $.Deferred();
            var speechReady = new $.Deferred();

            $('#dingoNotifications').append(lineNotification).show(1000,
                function() {
                    $('#dingoNotifications').animate({scrollTop: $('#dingoNotifications').prop("scrollHeight")}, 500);
                    animationReady.resolve();
            });

            /* Speeches */
            var speeches = [];          
            if (! this.bingo.lineCalledSpeechAlreadyPlayed) {
                this.bingo.lineCalledSpeechAlreadyPlayed = true;
                speeches.push(SPEECH.LINE_CALLED);
            }
            speeches.push(SPEECH.VERIFY_LINE);
            _.each(lineNumbers, function(n) {
                speeches.push(getNumberSpeech(n));
            });
            

            this.playSeveralSpeeches(speeches, function() {
                speechReady.resolve();
            });

            /* Highlighting on grid */
            this.highlightNumbers(lineNumbers);

            $.when(animationReady, speechReady, bannerReady).done(function() {
                utils.debug("Ready: line notification animation and speech");
                self.lineNotificationsQueue.shift(); // Remove it on success
                self.hideBanner();
                self.unHighlightNumbers(lineNumbers, function() {
                    if (self.lineNotificationsQueue.length > 0) {
                        utils.debug("Running again line notifications queue");
                        self.runLineNotificationsQueue();
                    }
                    else {
                        // End of line notifications queue
                        utils.debug("Playing speech continue to bingo");
                        self.playSpeech(SPEECH.CONTINUE_TO_BINGO);
                    }
                });
            });
        },

        showBanner: function(lines) {
            /* lines is an array which containes the two lines to be displayed */
            var bannerReady = new $.Deferred();
            
            var bannerContainer = $('div#notificationsBanner');
            bannerContainer.find('#line0').text(lines[0]);
            bannerContainer.find('#line1').text(lines[1]);

            bannerContainer.fadeIn('slow', function() {
                bannerReady.resolve();
            });

            return bannerReady;
        },

        hideBanner: function(e) {
            if (e) e.preventDefault();

            $('div#notificationsBanner').hide();
        }
      
    });

    return HomeView;
});
