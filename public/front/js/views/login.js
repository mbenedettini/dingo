define([
    'utils',
    'jquery',
    'underscore',
    'backbone',
    'text!templates/login.html',
    'ClientModel'
], function(utils, $, _, Backbone, tplLogin, Client) {
  
    var bindingsHash = {
        "name"            : "#name",
        "lastName"        : "#lastName",
        "username"        : "#username",
        "email"           : "#mail",
        "password"        : "#password",
        "passwordConfirm" : "#passwordConfirm",
        "telephone"       : "#telephone"
    };

    var LoginView = Backbone.View.extend({

        events: {
            "click #login" : "login",
            "click .save" : "createUser",
            "keypress #loginForm" : "checkKeypress",
            "change #usernameInput" : "checkUsername"
        },

        initialize: function(options) {
            var self = this;
            _.bindAll(this, 'render');
            this.template = _.template(tplLogin);

            window.loginView = this;

            // Give jplayer 3 seconds to be ready
            // $(document).ready(function() {
            //     // utils.debug("Dom ready. In 3 seconds we'll check if jplayer is ready.");
            //     setTimeout(function() {
            //         self.checkForRequiredPlugins();
            //     }, 3000);
            // });

        },

        render: function() {
            var self = this;
            utils.debug("LoginView: Rendering login with cashierId: " + this.options.cashierId);            

            $(this.el).html(this.template({cashierId: this.options.cashierId}));

            // if cashierId has been passed along show the registration modal, as this
            // behaviour is intended to have clients associated with a cashier during
            // registration process
            if (this.options.cashierId) {
                $(this.el).find('#signUpModal').modal('show');
            }

            return this;
        },
        
        checkKeypress: function(e) {
            if ( e.which == 13 ) {
                e.preventDefault();
                this.login();
            }
        },

        checkUsername: function(e){
            var self = this;
            var attr = {username : $("#usernameInput").val()};
            $.post("api/clients/checkUsername",  attr , "json").
            success(function(res){ self.usernameOk() }).
            error(function(){ self.usernameFail() });
        },
        
        usernameOk: function(){
          var selector =  "#usernameGroup";
          var selectorSpanHelp = "#usernameHelp";
          var signUpButton = "#signUpButton";
          
          $(selector).tooltip('show').closest('.control-group').removeClass('error').addClass('success');
          utils.debug(selectorSpanHelp);
          $(selectorSpanHelp).hide();
          $(signUpButton).removeClass('disabled');
        },

        usernameFail: function(){
          var selector =  "#usernameGroup";
          var selectorSpanHelp = "#usernameHelp";
          var signUpButton = "#signUpButton";
          
          $(selector).tooltip('show').closest('.control-group').removeClass('success').addClass('error');
          utils.debug(selectorSpanHelp);
          $(selectorSpanHelp).show();
          $(signUpButton).addClass('disabled');
        },

        login: function(e) {
            var self = this;
            if (e) e.preventDefault();

            utils.debug("login");
            $(".ajax-loading").css("display", "block");
            
            var inputUsername = $('input[name=username]');
            var inputPassword = $('input[name=password]');
            var attrs = {username:inputUsername.val(),password:inputPassword.val()};
            
            self.model.login(attrs);
            
        },

        createUser: function(){
            var self = this;
            utils.debug("createUser");
            
            if ( ! $("#signUpButton").hasClass('disabled')) {
            
                var errors = {};
                
                var username = $('input[name=usernameInput]');
                var password = $('input[name=passwordInput]');
                var passwordConfirm = $('input[name=passwordConfirmInput]');
                var name = $('input[name=nameInput]');
                var lastName = $('input[name=lastNameInput]');
                var email = $('input[name=emailInput]');
                var telephone = $('input[name=telephoneInput]');
                var cashierId = $('input[name=cashierIdInput]');
                
              
              if (passwordConfirm.val() == password.val()) {
                  
                  var attrs = {   
                      username:username.val(),
                      password:password.val(),
                      name: name.val(),
                      lastName: lastName.val(),
                      email: email.val(),
                      telephone: telephone.val(),
                      cashierId: cashierId.val()
                  };
                      
                  
                  var newClient = new Client(attrs);
                  newClient.save({},{
                      success: function() {
                        
                          $('#signUpForm').hide();
                          $('#signUpFooter').hide();
                          
                          $("#errorMessage").hide();  
                          $("#successMessage").show();
                          
                          utils.debug("OK");
                      },
                      error: function(model,err) {
                        utils.debug("Error" + err.errors);
                        if (err.hasOwnProperty("errors")) {
                          self.showSignUpFormError(null,err);
                        }else {
                          $("#errorMessage").show();
                        }
                      }
                  });
              } else {
                  utils.debug("Password missmatch");
                  errors.errors = {"password" : "Las password ingresadas no coinciden",
                    "passwordConfirm" : "Las password ingresadas no coinciden"
                  };
                  self.showSignUpFormError(null,errors);
              }
            }/*else {
                alert("El formulario contiene errores. Soluciones los problemas antes de continuar");
            }*/
        },
        
        showSignUpFormError : function (model,err){
            var msgs = [];
            var self = this;
            utils.debug(err)
            self.removeErrMsg();
            
            $.each(err.errors,function(key,value){
                var selectorBaseName = bindingsHash[key];
                utils.debug("key:" + key + " selectorBase:" + selectorBaseName);
                self.addErrMsg(selectorBaseName);
            });
            
        },

        addErrMsg: function(name){
          var selector =  name + "Group"
          var selectorSpanHelp = name + "Help"
          $(selector).tooltip('show').closest('.control-group').addClass('error');
          utils.debug(selectorSpanHelp);
          $(selectorSpanHelp).show();
        },
        
        // TODO Cambiar esto
        removeErrMsg: function() {
          $(this.el).find('.error').removeClass("error");
          $(this.el).find('.help-inline').hide();
        }
        
        
    });

    return LoginView;
});
 
