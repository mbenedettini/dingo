define([
    'utils',
    'jquery',
    'underscore',
    'backbone',
    'rivets',
    './views/home',
    './views/login',
    'BingoModel',
    'ClientCardCollection',
    './common/models/session',
    'socket.io',
    'config'
], function(utils, $, _, Backbone, rivets, HomeView, LoginView, Bingo, ClientCardCollection, Session, io, config) {

    var AppRouter, initialize;

    AppRouter = Backbone.Router.extend({
        routes: {
            ''       : 'home',
            'login'  : 'login',
            'login/:confirmed'  : 'login',
            'register/cid:cashierId' : 'registerWithCid',
            'home'   : 'home'
        },
        
        initialize: function() {
            this.session = new Session();
            this.session.on('change:loggedIn', function() {
                utils.debug("change:loggedIn");
                if (this.session.get('loggedIn')) {
                    this.navigate('home', {trigger: true});
                }
                else {
                    if (this.socket) {
                        utils.debug("Disconnecting SOCKET.IO");
                        this.socket.disconnect();
                    }
                    
                    /* Workaround for SOCKET.IO. For some unknown reason, a new socket cannot be
                     * established after logging out unless the page is reloaded.
                     * 
                     * When solved, should be replaced by
                     * this.navigate('login', {trigger: true});
                     */
                    window.location.reload();
                }
            }, this);

            this.notificationsQueue = [];


            this.elms = {
                'page-content': $('.page-content')
            };

        },
        
/*        confirm: function() {
            this.elms['page-content'].html((new ConfirmView()).render().el);
        },
        */

        login: function(confirmed) {
            // TODO: add logout route
            
            confirmed = ( confirmed == "confirmed" )? true : false;
            
            this.elms['page-content'].html((new LoginView({model:this.session})).render().el);
            
            if (confirmed) {
                $('#clientConfirmed').alert().show();
            } else {
                $('#clientConfirmed').alert().hide();
            }
        },

        registerWithCid: function(cashierId) {
            this.elms['page-content'].html((new LoginView({model:this.session, cashierId: cashierId })).render().el);
        },
        
        home: function() {
            /* Home View setup */
            utils.debug("home view...");
            var self = this;
            if (this.session) {
                this.session.isAuthorized(function(authorized) {
                    if (authorized) {
                        // Clean up notifications queue
                        self.notificationsQueue = [];

                        /* SOCKET.IO */
                        // Keep only one instance
                        if (!self.socket) {
                            var ioUrl = config.front.HTTPS ? 'https://' : 'http://';
                            ioUrl += config.front.HOST + ":" + (config.front.PUBLIC_PORT ? config.front.PUBLIC_PORT : config.front.PORT);
                            utils.debug("SOCKET.IO: connecting to " + ioUrl);
                            self.socket = io.connect();
                            
                            self.socket.on('connect', function () {
                                utils.debug("SOCKET.IO Connected!");
                            });
                            
                            var notifications = ['numberCalled', 'bingoUpdated', 'sellingStarted', 'bingoStarted', 'line', 'bingo', 'balanceUpdated', 'updateVisitorsCount', 'bingoCancelled', 'updateAccumulatedPrize', 'justPassedAccumulatedOrder'];
                            _.each(notifications, function(n) {
                                self.socket.on(n, function(data) {
                                    utils.debug("Router: received notification " + n);
                                    utils.debug(data);
                                    self.processUpdate(n, data);
                                });
                            });
                        }
       
                        /* /SOCKET.IO */


                        // Fetch the current bingo and render it
                        var bingo = new Bingo();
                        bingo.fetch({
                            data: {currentBingo: true},
                            success: function(model) {
                                if (model.error) {
                                    utils.debug("Error when retrieving current bingo or no currentBingo");
                                    if (model.error) utils.debug(model.error);
                                    _.bind(self._renderHome, self)();
                                }
                                else {
                                    if (model.get('id')) {
                                        utils.debug("Router: Retrieved bingo: #" + model.get('id'));
                                    }
                                    else {
                                        utils.debug("Router: Retrieved a volatile bingo");
                                    }
                                    utils.debug("Router: Binding and executing _renderHome");
                                    utils.debug(model);
                                    _.bind(self._renderHome, self)(model);

                                }
                            },
                            error: function() {}
                        });
                    }
                    else {
                        utils.debug("User is not logged in");
                        self.navigate('login', {trigger: true});
                    }

                });
            }
            else {
                utils.debug("this.client does not exist");
            }

        },

        _renderHome: function(bingo) {
            var self = this;
            
            utils.debug("_renderHome, instantiating new view with bingo");
            utils.debug(bingo);
            this.homeView = new HomeView({session: this.session, bingo: bingo});
            this.elms['page-content'].html(this.homeView.render().el);
            this.homeView.renderAccount();
            this.homeView.renderBingo();

        },

                                         
        processUpdate: function(name, args) {
            utils.debug("Router: queueing notification " + name);
            this.notificationsQueue.push([name, args]);

            utils.debug("Router: notifications queue length now is ");
            utils.debug(this.notificationsQueue.length);

            this.runNotificationsQueue();
        },

        runNotificationsQueue : function() {
            var self = this;
            if (this.notificationsQueue.length < 1 ) {
                utils.debug("Not recursing notifications queue. Length = " + this.notificationsQueue.length);
                return;
            }

            if (this.runningQueue) {
                utils.debug("Router: there is already a queue runner. Not running the queue");
                return;
            }

            utils.debug("Router: Processing next item on notifications queue");
            this.runningQueue = true;
            var current = this.notificationsQueue[0];
            var name = current[0];
            var args = current[1];

            var updatesHandlers = {
                'numberCalled': function(args, cb) { 
                    this.homeView.processUpdateNumberCalled(args, cb);
                },
                'sellingStarted': function (args, cb) {
                    // args.bingo
                    utils.debug(args.bingo);
                    if (!args.bingo.sellingFinishesAt) {
                        // If it is a new bingo, cleanup and render again the whole view
                        utils.debug("Router: sellingStarted without defined attribute sellingFinishesAt: rendering home view again");
                        this.home();
                        if (cb) cb();
                    }
                    else {
                        // Instead, it could be an update of sellingFinishesAt
                        utils.debug("Router: sellingStarted with sellingFinishesAt: sending update to the home view");
                        this.homeView.processUpdateSellingStarted(args, cb);
                    }
                },
                'bingoStarted': function(args, cb) {
                    this.homeView.processUpdateBingoStarted(args, cb);
                },
                'bingoUpdated': function(args, cb) {
                    this.homeView.processUpdateBingoUpdated(args, cb);
                },
                'line': function(args, cb) {
                    utils.debug("Router: processUpdate -> line");
                    this.homeView.processUpdateLineCalled(args, cb);
                },
                'bingo': function(args, cb) {
                    this.homeView.processUpdateBingoCalled(args, cb);
                },
                'balanceUpdated': function(args, cb) {
                    this.homeView.processUpdateBalanceUpdated(args, cb);
                },
                'updateVisitorsCount': function(args, cb) {
                    this.homeView.processUpdateVisitorsCount(args, cb);
                },
                'bingoCancelled' : function(args, cb) {
                    this.homeView.processUpdateBingoCancelled(args, cb);
                },
                'updateAccumulatedPrize' : function(args, cb) {
                    this.homeView.processUpdateAccumulatedPrize(args, cb);
                },
                'justPassedAccumulatedOrder' : function(args, cb) {
                    this.homeView.processUpdateJustPassedAccumulatedOrder(args, cb);
                }
            };

            utils.debug("Router: processing update: " + name);

	    var finishNotificationProcessing = function() {
	    };

            var handler = updatesHandlers[name];

            if (this.homeView) {
                if (handler) {
                    var f = _.bind(handler, this);
                    try {
			f(args, function() {
			    // Callback
                            utils.debug("Router: sucessfully finished processing update: " + name);
			});
		    } catch (e) {
			utils.debug("Router: ERROR when processing update: " + name);
			utils.debug(e);
		    } finally {
			self.notificationsQueue.shift();
			self.runningQueue = false;
			self.runNotificationsQueue();
		    }
                }
                else {
                    utils.debug("No handler for update " + name);
                }
            }
            else  {
                utils.debug("No homeView at the moment. Will retry in half a second.");
                self.runningQueue = false;
                setTimeout(function() {
                    self.runNotificationsQueue();
                }, 500);
            }
        }

    });

    return AppRouter;
});
