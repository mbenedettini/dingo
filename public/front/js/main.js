require(['./common/main-common'], function(common) {
    require(['./app', 'utils', 'jquery'], function(app, utils, $) {

	
        app.initialize();

        var hideSpinner = function() {
            if (window.spinner) {
                utils.debug("App ready. Stopping spinner");
                window.spinner.stop();
            }
        
            $('div#spin-container').hide();
            $('div#container').show();
        };

        if (window.speechesPlayer) {
            if (window.speechesPlayer.readyState == 3) { // load is ready
                utils.debug("MAIN: speeches have been already loaded.");
                hideSpinner();
            }
            else if (window.speechesPlayer.readyState == 2) { // failed
                utils.debug("MAIN: speechesPlayer loading has failed");
                hideSpinner();
                $('div#flashAlert').removeClass('hidden');
            }
            else {
                utils.debug("MAIN: speeches are not ready yet. Leaving a callback to be fired");
                window.speechesPlayerOnLoad = function(success) {
                    utils.debug("MAIN: speechesPlayerOnLoad");
                    hideSpinner();

                    if (!success) {
                        $('div#flashAlert').removeClass('hidden');
                    }
                };
            }
        }
        else {
            if (window.soundManagerTimeoutOccurred) {
                // soundManager didn't load, probably because of a Flash failure
                utils.debug("MAIN: soundManager is not available");
                hideSpinner();
                $('div#flashAlert').removeClass('hidden');
            }
            else {
                // soundManager hasnt finished loading yet, set a callback in case its fails
                window.soundManagerOnTimeout = function() {
                    utils.debug("MAIN: soundManagerOnTimeout");
                    hideSpinner();
                    $('div#flashAlert').removeClass('hidden');
                };
            }
        }
    });
    
}, function(err) {    
    // console.log("ERROR");
    // console.log(err);
    // On any error, just reload
    window.location.reload();
    
});



