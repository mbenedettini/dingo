#!/bin/bash
rm output.*

FILES="1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 non-clarified/60 non-clarified/61 non-clarified/62 non-clarified/63 non-clarified/64 non-clarified/65 non-clarified/66 non-clarified/67 non-clarified/68 non-clarified/69 non-clarified/70 non-clarified/71 non-clarified/72 non-clarified/73 non-clarified/74 non-clarified/75 non-clarified/76 non-clarified/77 non-clarified/78 non-clarified/79 balls bingo_called bingo_finishes bingo_starts continue_to_bingo have_been_called line_called verify_bingo verify_line"

CMD=""

for f in $FILES; do
    mpg123 -w $f.wav $f.mp3
    CMD="$CMD $f.wav silence.wav"
done

sox $CMD output.wav

# bitrate: 24
lame -h -b 24 output.wav output.mp3
