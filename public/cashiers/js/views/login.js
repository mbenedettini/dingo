define([
    'utils',
    'jquery',
    'underscore',
    'backbone',
    'text!templates/login.html',
    'CashierModel'
], function(utils, $, _, Backbone, tplLogin, CashierModel) {
  
    var bindingsHash = {
        "name"            : "#name",
        "username"        : "#username",
        "email"           : "#mail",
        "password"        : "#password"
    };

    var LoginView = Backbone.View.extend({

        events: {
            "click #login" : "login",
            "keypress #loginForm" : "checkKeypress"
        },

        initialize: function() {
            var self = this;
            _.bindAll(this, 'render');
            this.template = _.template(tplLogin);

            window.loginView = this;
        },

        render: function() {
            var self = this;
            utils.debug("confirmed = " + this.options.confirmed);
            $(this.el).html(this.template());

            return this;
        },
        
        checkKeypress: function(e) {
            if ( e.which == 13 ) {
                e.preventDefault();
                this.login();
            }
        },

        login: function(e) {
            var self = this;
            if (e) e.preventDefault();

            utils.debug("LoginView: logging in...");
            $(".ajax-loading").css("display", "block");
                
            var inputUsername = $('input[name=username]');
            var inputPassword = $('input[name=password]');
            var attrs = {username:inputUsername.val(),password:inputPassword.val()};
                
            self.model.login(attrs);
            
        },

        addErrMsg: function(name){
            var selector =  name + "Group";
            var selectorSpanHelp = name + "Help";
            $(selector).tooltip('show').closest('.control-group').addClass('error');
            utils.debug(selectorSpanHelp);
            $(selectorSpanHelp).show();
        },
        
        // TODO Cambiar esto
        removeErrMsg: function() {
          $(this.el).find('.error').removeClass("error");
          $(this.el).find('.help-inline').hide();
        }
        
        
    });

    return LoginView;
});
 
