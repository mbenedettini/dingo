define([
  'jquery',
  'underscore',
  'backbone',
  'moment',
  'text!templates/clients/index.html',
  'ClientCollection'
], function($, _, Backbone, moment, tpl, ClientCollection) {
      var ClientListView;

    ClientListView = Backbone.View.extend({
        initialize: function() {
            var clientList;

            this.template = _.template(tpl);
            this.collection; // = new ClientCollection();
        },
        render: function() {
            var that = this, tmpl;
            var clients = this.collection.toJSON();
            tmpl = that.template({ 'clients': clients });
            $(that.el).html(tmpl);
            
            return this;
        }
    });

    return ClientListView;
});
