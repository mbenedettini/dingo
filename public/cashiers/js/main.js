require(['common/main-common'], function(common) {

    // Require moment spanish language. Moment itself is needed to be required
    // before the language file: that is enforced here.
    require(['./app', 'moment', 'moment-es', 'socket.io', 'utils', 'jquery'], function(app, moment, momentes, io, utils, $) {

        utils.debug("About to initialize APP");
        //require(['app'], function(app) {
            app.initialize();
            utils.debug("APP READY");
        //});
    });
});

