define(
    [
        'utils',
        'jquery',
        'underscore',
        'backbone',
        './views/home',
        './views/header',
        './views/login',
        './views/clients/index',
        './views/clients/show',
        './views/clients/edit',
        './views/clients/new',
        './views/reports/cashier',
        'ClientModel',
        'ClientCollection',
        './common/models/session'
    ], function(utils, $, _, Backbone,
        HomeView,
        HeaderView,
        LoginView,
        ClientListView,
        ClientView,
        ClientEditView,
        ClientNewView,
        ReportCashierView,
        Client,
        ClientCollection,
        Session
        ) {
            var AppRouter, initialize;

            AppRouter = Backbone.Router.extend({

                routes: {
                    ''                  : 'home',
                    'home'              : 'home',
                    'login'             : 'login',
                    'logout'            : 'logout',

                    // clients
                    'clients'           : 'listClients',
                    'clients/new'       : 'addClient',
                    'clients/:id'       : 'showClient',
                    'clients/:id/edit'  : 'editClient',

                    // reports
                    'reports/cashier'   :  'cashierReport',

                    // any other action defaults to the following handler
                    '*actions'          : 'defaultAction'
                },
        
                initialize: function() {
                    var self = this;
                    this.session = new Session();
                    this.session.on('change:loggedIn', function() {
                        utils.debug("change:loggedIn");
                        if (this.session.get('loggedIn')) {
                            this.navigate('home', {trigger: true});
                        }
                        else {
                            this.navigate('login', {trigger: true});
                        }
                    }, this);

                    this.headerView = new HeaderView();
                    // cached elements
                    this.elms = {
                        'page-content': $('.page-content')
                    };
                    $('header').hide().html(self.headerView.render().el);
                    $('footer').fadeIn('slow');

                },

                login: function() {
                    $('header').hide();
                    this.elms['page-content'].html((new LoginView({model:this.session})).render().el);
                },

                logout: function() {
                    this.session.logout();
                    this.navigate('login', {trigger: true});
                },

                checkSession: function(cb) {
                    var self = this;
                    if (this.session) {
                        this.session.isAuthorized(function(authorized) {
                            if (authorized) {
                                cb();
                            }
                            else {
                                utils.debug("User is not logged in");
                                self.navigate('login', {trigger: true});
                            }
                        });
                    }
                    else {
                        utils.debug("this.session is undefined");
                        self.navigate('login', {trigger: true});
                    }
                },
        
                home: function() {
                    var self = this;
                    
                    this.checkSession(function() {
                        $('header').fadeIn('slow');
                        self.headerView.select('home-menu');
                        
                        if (!self.homeView) {
                            self.homeView = new HomeView();
                        }
                        self.elms['page-content'].html(self.homeView.render().el);
                    });
                },

                listClients: function() {
                    var self = this;

                    this.checkSession(function() {
                        self.headerView.select('clients-menu');
                        var collection = new ClientCollection;
                    
                        collection.fetch({
                            success: function(clientsCollection) {
                                var view = new ClientListView({ collection: clientsCollection });
                                self.elms['page-content'].html(view.render().el);
                            },
                            error: function(coll, res) {
                                if (res.status === 404) {
                                    // TODO: handle 404 Not Found
                                } else if (res.status === 500) {
                                    // TODO: handle 500 Internal Server Error
                                }
                            }
                        });
                    });
                },
                                                   
                showClient: function(id) {
                    var self = this;

                    this.checkSession(function() {
                        self.headerView.select('clients-menu');
                        
                        /* At the moment I have chained transactions fetching to the client fetching.
                         * There might be a better way, such as triggering transactions fetching when
                         * the user clicks the "Transactions" tab (but this would require a spin to alert
                         * the user self there is an operation in progress).
                         * 
                         * */
                        
                        // pass _silent to bypass validation to be able to fetch the model
                        model = new Client({ id: id, _silent: true });
                        model.fetch({
                            success : function(model) {
                                model.unset('_silent');
                                self.currentClient = model;
                                
                                model.transactions.fetch({
                                    success: function(transactions) {
                                        self.clientView = new ClientView({ model: model });
                                        self.elms['page-content'].html(self.clientView.render().el);
                                    },
                                    error: function(transactions, res) {
                                        // TODO: .... ?
                                    }
                                    
                                });
                                
                            },
                            error   : function(model, res) {
                                if (res.status === 404) {
                                    // TODO: handle 404 Not Found
                                } else if (res.status === 500) {
                                    // TODO: handle 500 Internal Server Error
                                }
                            }
                        });
                    });
                },
                                                   
                addClient: function() {
                    var self = this, model, view;

                    this.checkSession(function() {
                        self.headerView.select('clients-menu');

                        model = new Client();
                        view  = new ClientNewView({ model: model });

                        self.elms['page-content'].html(view.render().el);
                        view.on('back', function() {
                            delete view;
                            self.navigate('#/clients', { trigger: true });
                        });
                        view.model.on('save-success', function(id) {
                            delete view;
                            self.navigate('#/clients/' + id, { trigger: true });
                        });
                    });
                },

                editClient: function(id) {
                    var self = this, model, view;

                    this.checkSession(function() {
                        self.headerView.select('clients-menu');

                        // pass _silent to bypass validation to be able to fetch the model
                        model = new Client({ id: id, _silent: true });
                        model.fetch({
                            success : function(model) {
                                model.unset('_silent');
                                // Create & render view only after model has been fetched
                                view = new ClientEditView({ model: model });
                                self.elms['page-content'].html(view.render().el);
                                view.on('back', function() {
                                    delete view;
                                    self.navigate('#/clients/' + id, { trigger: true });
                                });
                                view.model.on('save-success', function() {
                                    delete view;
                                    self.navigate('#/clients/' + id, { trigger: true });
                                });
                            },
                            error   : function(model, res) {
                                if (res.status === 404) {
                                    // TODO: handle 404 Not Found
                                } else if (res.status === 500) {
                                    // TODO: handle 500 Internal Server Error
                                }
                            }
                        });
                    });
                },

                cashierReport: function() {
                    var self = this;

                    this.checkSession(function() {
                        self.headerView.select('reports-menu');
                    
                        var reportCashierView = new ReportCashierView({cashierId: self.session.get('userId') });
                        self.elms['page-content'].html(reportCashierView.render().el);
                    });
                },
                                                   
                defaultAction: function(actions) {
                    // No matching route, log the route?
                    utils.debug("Non defined route: " + actions);
                }

            });

    return AppRouter;
});
