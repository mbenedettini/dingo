define('app', [
    'jquery',
    'underscore',
    'backbone',
    'config',
    'socket.io',
    'bootstrap',
    'router',
    'utils'
 ], function($, _, Backbone, config, io, bootstrap, Router, utils) {

    function initialize() {
        var app = new Router();
        
        utils.debug("Config loaded is");
        utils.debug(config.cashiers);

        Backbone.history.start();
    }

  // TODO: error handling with window.onerror
  // http://www.slideshare.net/nzakas/enterprise-javascript-error-handling-presentation

  return {
      initialize: initialize
  };
});
