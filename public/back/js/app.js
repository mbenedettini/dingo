define('app', [
    'jquery',
    'underscore',
    'backbone',
    'config',
    'socket.io',
    'bootstrap',
    'router',
    'utils'
 ], function($, _, Backbone, config, io, bootstrap, Router, utils) {

    function initialize() {
        var app = new Router();
        
        /* SOCKET.IO initialization */
        var ioUrl = config.back.HTTPS ? 'https://' : 'http://';
        ioUrl += config.back.HOST + ":" + (config.back.PUBLIC_PORT ? config.back.PUBLIC_PORT : config.back.PORT);
        app.socket = io.connect(ioUrl);
        
        app.socket.on('connect', function () {
            utils.debug("SOCKET.IO Connected to " + ioUrl);
        });

        var notifications = ['updateClient', 'updateClientBalance', 'numberCalled', 'bingoUpdated', 'sellingStarted', 'bingoStarted'];
        
        _.each(notifications, function(n) {
            app.socket.on(n, function(data) {
                utils.debug("App: received notification " + n);
                app.processUpdate(n, data);
            });
        });
        
        /* end of SOCKET.IO initialization */

        Backbone.history.start();
    }

  // TODO: error handling with window.onerror
  // http://www.slideshare.net/nzakas/enterprise-javascript-error-handling-presentation

  return {
      initialize: initialize
  };
});
