define(
    [
        'utils',
        'jquery',
        'underscore',
        'backbone',
        './views/home',
        './views/header',
        './views/clients/index',
        './views/clients/show',
        './views/clients/edit',
        './views/clients/new',
        './views/bingos/index',
        './views/bingos/show',
        './views/bingos/edit',
        './views/cashiers/index',
        './views/cashiers/new',
        './views/cashiers/show',
        './views/settings/index',
        './views/reports/cashiers',
        'ClientModel',
        'ClientCollection',
        'BingoModel',
        'BingoCollection',
        'SettingModel',
        'SettingCollection',
        'CashierModel',
        'CashierCollection'
    ], function(utils, $, _, Backbone,
        HomeView,
        HeaderView,
        ClientListView,
        ClientShowView,
        ClientEditView,
        ClientNewView,
        BingoListView,
        BingoShowView,
        BingoEditView,
        CashierListView,
        CashierNewView,
        CashierShowView,
        SettingListView,
        ReportCashiersView,
        Client,
        ClientCollection,
        Bingo,
        BingoCollection,
        Setting,
        SettingCollection,
        Cashier,
        CashierCollection
        ) {
            var AppRouter, initialize;

            AppRouter = Backbone.Router.extend({

                routes: {
                    ''                  : 'home',
                    'home'              : 'home',
                    // clients
                    'clients'           : 'listClients',
                    'clients/new'       : 'addClient',
                    'clients/:id'       : 'showClient',
                    'clients/:id/edit'  : 'editClient',

                    // bingos
                    'bingos'     : 'listBingos',
                    'bingos/new' : 'addBingo',
                    'bingos/:id' : 'showBingo',
                    'bingos/:id/edit': 'editBingo',

                    // cashiers
                    'cashiers'           : 'listCashiers',
                    'cashiers/new'       : 'addCashier',
                    'cashiers/:id'       : 'showCashier',
                    'cashiers/:id/edit'  : 'editCashier',

                    // reports
                    'reports/cashiers'   : 'reportCashiers',

                    // Settings
                    'settings'   : 'listSettings',

                    
                    // any other action defaults to the following handler
                    '*actions'          : 'defaultAction'
                },
        
                initialize: function() {
                    this.clientShowView = null;
                    this.clientEditView = null;
                    this.headerView = new HeaderView();
                    // cached elements
                    this.elms = {
                        'page-content': $('.page-content')
                    };
                    $('header').hide().html(this.headerView.render().el).fadeIn('slow');
                    $('footer').fadeIn('slow');
                },
        
                home: function() {
                    this.headerView.select('home-menu');

                    if (!this.homeView) {
                        this.homeView = new HomeView();
                    }
                    this.elms['page-content'].html(this.homeView.render().el);
                },

                listClients: function() {

                    this.headerView.select('list-clients-menu');

                    var that = this;
                    var collection = new ClientCollection;
                    
                    collection.fetch({
                        success: function(clientsCollection) {
                            var view = new ClientListView({ collection: clientsCollection });
                            that.elms['page-content'].html(view.render().el)
                        },
                        error: function(coll, res) {
                            if (res.status === 404) {
                                // TODO: handle 404 Not Found
                            } else if (res.status === 500) {
                                // TODO: handle 500 Internal Server Error
                            }
                        }
                     });
                },
                                                   
                showClient: function(id) {
                    var that = this;

                    this.headerView.select();
                    
                    /* At the moment the client and its transactions are fetched in a parallel fashion.
                     * There might be a better way, such as triggering transactions fetching when
                     * the user clicks the "Transactions" tab (but this would require a spin to alert
                     * the user that there is an operation in progress).
                     * 
                     * */
                    
                    // pass _silent to bypass validation to be able to fetch the model
                    var model = new Client({ id: id, _silent: true });
                    var modelReady = model.fetch({
                        success : function(models) {
                            model.unset('_silent');
                            this.currentClient = models.client;
                            
                            
                                    
                        },
                        error   : function(model, res) {
                            if (res.status === 404) {
                                // TODO: handle 404 Not Found
                            } else if (res.status === 500) {
                                // TODO: handle 500 Internal Server Error
                            }
                        }
                    });

                    var transactions;
                    var transactionsReady = model.transactions.fetch({
                        success: function(txs) {
                            transactions = txs;
                            
                        },
                        error: function(transactions, res) {
                            // TODO: .... ?
                        }
                        
                    });

                    $.when(modelReady, transactionsReady).done(function() {
                        that.clientShowView = new ClientShowView({ model: model});
                        that.elms['page-content'].html(that.clientShowView.render().el);
                    });
                },
                                                   
                addClient: function() {
                    var that = this, model, view;

                    this.headerView.select('new-client-menu');

                    model = new Client();
                    view  = new ClientNewView({ model: model });

                    this.elms['page-content'].html(view.render().el);
                    view.on('back', function() {
                        delete view;
                        that.navigate('#/clients', { trigger: true });
                    });
                    view.model.on('save-success', function(id) {
                        delete view;
                        that.navigate('#/clients/' + id, { trigger: true });
                    });
                },

                editClient: function(id) {
                    var that = this, model, view;

                    this.headerView.select();

                    // pass _silent to bypass validation to be able to fetch the model
                    model = new Client({ id: id, _silent: true });
                    var modelReady = model.fetch({
                        success : function(model) {
                            model.unset('_silent');                            
                        },
                        error   : function(model, res) {
                            if (res.status === 404) {
                                // TODO: handle 404 Not Found
                            } else if (res.status === 500) {
                                // TODO: handle 500 Internal Server Error
                            }
                        }
                    });

                    var cashiers;
                    var cashiersReady = new CashierCollection().fetch({
                        success: function(cs) {
                            cashiers = cs;
                        },
                        error : function(cs, res) {}
                    });
                    
                    $.when(modelReady, cashiersReady).done(function() {
                        // Create & render view only after model has been fetched
                        view = new ClientEditView({ model: model, cashiers: cashiers });
                        that.elms['page-content'].html(view.render().el);
                        view.on('back', function() {
                            delete view;
                            that.navigate('#/clients/' + id, { trigger: true });
                        });
                        view.model.on('save-success', function() {
                            delete view;
                            that.navigate('#/clients/' + id, { trigger: true });
                        });
                    });

                },
                                                   
                listBingos: function() {
                    
                    utils.debug("Listing bingos");

                    this.headerView.select('list-bingos-menu');

                    var that = this;
                    var collection = new BingoCollection;
                    
                    collection.fetch({
                        success: function(bingosCollection) {
                            var view = new BingoListView({ collection: bingosCollection });
                            that.elms['page-content'].html(view.render().el);
                            this.bingoListView = view;
                        },
                        error: function(coll, res) {
                            if (res.status === 404) {
                                // TODO: handle 404 Not Found
                            } else if (res.status === 500) {
                                // TODO: handle 500 Internal Server Error
                            }
                        }
                     });
                },
                

                showBingo: function(id) {
                    var that = this;

                    this.headerView.select();
                    
                    // pass _silent to bypass validation to be able to fetch the model
                    var model = new Bingo({ id: id, _silent: true });
                    model.fetch({
                        success : function(bingo) {
                            bingo.unset('_silent');
                            this.currentBingo = bingo;
                            that.bingoShowView = new BingoShowView({ model: bingo });
                            that.elms['page-content'].html(that.bingoShowView.render().el);
                            
                            var rivetsView = rivets.bind($('#bingoContainer'), { bingo: bingo });

                        },
                        error : function(model, res) {
                            if (res.status === 404) {
                                // TODO: handle 404 Not Found
                            } else if (res.status === 500) {
                                // TODO: handle 500 Internal Server Error
                            }
                        }
                    });
                },
                                                   
                addBingo: function() {
                    var that = this, model, view;
                    
                    this.headerView.select('new-bingo-menu');

                    model = new Bingo();
                    view  = new BingoEditView({ model: model });

                    this.elms['page-content'].html(view.render().el);
                    view.on('back', function() {
                        delete view;
                        that.navigate('#/bingos', { trigger: true });
                    });
                    view.model.on('save-success', function(id) {
                        delete view;
                        that.navigate('#/bingos/' + id, { trigger: true });
                    });
                },

                editBingo: function(id) {
                    var self = this, model, view;

                    this.headerView.select();

                    // pass _silent to bypass validation to be able to fetch the model
                    model = new Bingo({ id: id, _silent: true });
                    model.fetch({
                        success : function(model) {
                            model.unset('_silent');
                            // Create & render view only after model has been fetched
                            view = new BingoEditView({ model: model });
                            self.elms['page-content'].html(view.render().el);
                            view.on('back', function() {
                                delete view;
                                self.navigate('#/bingos/' + id, { trigger: true });
                            });
                            view.model.on('save-success', function() {
                                delete view;
                                self.navigate('#/bingos/' + id, { trigger: true });
                            });
                        },
                        error   : function(model, res) {
                            if (res.status === 404) {
                                // TODO: handle 404 Not Found
                            } else if (res.status === 500) {
                                // TODO: handle 500 Internal Server Error
                            }
                        }
                    });

                },


                listSettings: function() {
                    
                    this.headerView.select('list-settings-menu');

                    var that = this;
                    var collection = new SettingCollection;
                    
                    collection.fetch({
                        success: function(settings) {
                            var view = new SettingListView({ collection: settings });
                            that.elms['page-content'].html(view.render().el);
                        },
                        error: function(coll, res) {
                            if (res.status === 404) {
                                // TODO: handle 404 Not Found
                            } else if (res.status === 500) {
                                // TODO: handle 500 Internal Server Error
                            }
                        }
                     });
                },

                listCashiers: function() {
                    var self = this;
                    this.headerView.select('list-cashiers-menu');

                    var collection = new CashierCollection;
                    
                    collection.fetch({
                        success: function(cashiers) {
                            var view = new CashierListView({ collection: cashiers });
                            self.elms['page-content'].html(view.render().el);
                        },
                        error: function(coll, res) {
                            if (res.status === 404) {
                                // TODO: handle 404 Not Found
                            } else if (res.status === 500) {
                                // TODO: handle 500 Internal Server Error
                            }
                        }
                     });
                },

                addCashier: function() {
                    var self = this, model, view;

                    this.headerView.select('list-cashiers-menu');

                    model = new Cashier();
                    view  = new CashierNewView({ model: model });

                    this.elms['page-content'].html(view.render().el);
                    view.on('back', function() {
                        delete view;
                        self.navigate('#/cashiers', { trigger: true });
                    });
                    view.model.on('save-success', function(id) {
                        delete view;
                        self.navigate('#/cashiers/' + id, { trigger: true });
                    });
                },

                showCashier: function(id) {
                    var self = this;

                    this.headerView.select();
                    
                    // pass _silent to bypass validation to be able to fetch the model
                    model = new Cashier({ id: id, _silent: true });
                    model.fetch({
                        success : function(model) {
                            model.unset('_silent');
                            this.currentCashier = model;
                            
                            self.cashierShowView = new CashierShowView({ model: model });
                            self.elms['page-content'].html(self.cashierShowView.render().el);
                        },
                        error   : function(model, res) {
                            if (res.status === 404) {
                                // TODO: handle 404 Not Found
                            } else if (res.status === 500) {
                                // TODO: handle 500 Internal Server Error
                            }
                        }
                    });
                },

                editCashier: function() {},

                reportCashiers: function() {
                    var self = this;

                    var reportCashiersView = new ReportCashiersView();
                    self.elms['page-content'].html(reportCashiersView.render().el);
                },


                defaultAction: function(actions) {
                    // No matching route, log the route?
                    utils.debug("Non defined route: " + actions);
                },

                processUpdate: function(name, args) {
                    var updatesHandlers = {
                        'numberCalled': function(args) { 
                            this.bingoShowView && this.bingoShowView.processUpdateNumberCalled(args);
                        },

                        'updateClient': function(args) { 
                            this.clientShowView && this.clientShowView.processUpdateClient(args); 
                        },

                        'updateClientBalance': function(args) { 
                            this.clientShowView && this.clientShowView.processUpdateClientBalance(args); 
                        },

                        'bingoUpdated' : function(args) {
                            this.bingoShowView && this.bingoShowView.processUpdateBingoUpdated(args);
                        },

                        'sellingStarted' : function(args) {
                            this.bingoShowView && this.bingoShowView.processUpdateSellingStarted(args);
                        },

                        'bingoStarted' : function(args) {
                            this.bingoShowView && this.bingoShowView.processUpdateBingoStarted(args);
                        }

                    };

                    utils.debug("Received update: " + name);

                    var handler = updatesHandlers[name];

                    if (handler) {
                        var f = _.bind(handler, this);
                        f(args);
                    }
                    else {
                        utils.debug("No handler for update " + name);
                    }
                }
                                                   

        });

    return AppRouter;
});
