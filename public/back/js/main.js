require(['common/main-common'], function(common) {

    // Require moment spanish language. Moment itself is needed to be required
    // before the language file: that is enforced here.
    require(['moment', 'moment-es', 'socket.io', 'utils'], function(moment, momentes, io, utils) {

        require(['app'], function(app) {
            app.initialize();
            utils.debug("APP READY");
        });
    });
});

