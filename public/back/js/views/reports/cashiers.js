define( [
    'utils',
    'jquery',
    'underscore',
    'backbone',
    'moment',
    'text!templates/reports/cashiers.html',
    'text!templates/reports/cashiers-report.html'
], function(utils, $, _, Backbone, moment, tpl, reportTpl) {
    var ReportCashiersView = Backbone.View.extend({
        initialize: function() {
            this.template = _.template(tpl);
        },

        events: {
            "click button" : "showReport"
        },

        render: function() {
            var self = this;
            
            var template = self.template();
            $(self.el).html(template);

            return this;
        },

        showReport: function(e) {
            var self = this;
            if (e) e.preventDefault();

            var rawDateFrom = $.trim($('#dateFrom-input').val()) || null;
            var rawDateTo = $.trim($('#dateTo-input').val()) || null;

            var dateFrom = rawDateFrom ? moment(rawDateFrom, "DD/MM/YYYY").format("YYYY-MM-DD") : null;
            var dateTo = rawDateTo ? moment(rawDateTo, "DD/MM/YYYY").format("YYYY-MM-DD"): null;
            
            var period = rawDateFrom ? "" + rawDateFrom : "Hoy";
            period += rawDateTo ? " a " + rawDateTo : (rawDateFrom ? " a hoy" : "");

            var url = '/api/reports/cashiers';
            if (dateFrom) url += '/' + dateFrom;
            if (dateTo) url += '/' + dateTo;

            utils.debug("ReportCashiersView url is " + url);

            $.ajax({
                url: url                 
            }).done(function(data) {
                // utils.debug("ReportCashiersView data received");

                var template = _.template(reportTpl, {data: data, period: period});
                $(self.el).find('div#report-container').html(template);
            });
        }

    });

    return ReportCashiersView;
});


