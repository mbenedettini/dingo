define([
    'utils',
    'jquery',
    'underscore',
    'backbone',
    'moment',
    'text!templates/cashiers/show.html',
    'text!templates/cashiers/clients.html',
    'CashierModel'
], function(utils, $, _, Backbone, moment, tpl, clientsTpl, Cashier) {
  var CashierShowView;

  CashierShowView = Backbone.View.extend({
    
      initialize: function() {
          this.template = _.template(tpl);
      },
      
      events: {
          "click #resetPassword-btn":   "resetPassword",

          "click #suspend-btn": "suspend",
          "click #unsuspend-btn": "unsuspend",
          
          'show #cashierTabs a[href="#tab2"]': "renderClients"
      },
      
      render: function(argModel) {
          var that = this, tmpl;
          
          this.model = argModel ? argModel : this.model;

          var modelJSON = this.model.toJSON();
          
          tmpl = that.template({ 'cashier': modelJSON  });
          $(that.el).html(tmpl);
          
          return this;
      },
      
      renderClients: function(e) {
          //~ e.target; // activated tab
          //~ e.relatedTarget; // previous tab
          utils.debug("Rendering clients!");
          
          this.model.clients.fetch({
              success: function(clients, res) {
                  $('#tab2').html(_.template(clientsTpl, {clients: clients.toJSON()} ));
              },
              error: function(clients, res) {}
          });
      },
      
      resetPassword: function(e) {
          var self = this;
          e.preventDefault();

          this.model.resetPassword(
              function callback() {
                  self.alertMsg({
                      msg: "El nuevo password ha sido enviado al usuario"
                  });
              }, 
              function errback() {
                  
              }
          );
      },

      suspend: function(e) {
          var self = this;
          e.preventDefault();
          
          this.model.suspend({
              success: function() {
                  console.log("CashierShowView suspend success");
                  self.render();
              }
          });
      },

      unsuspend: function(e) {
          var self = this;
          e.preventDefault();
          
          this.model.unsuspend({
              success: function() {
                  console.log("CashierShowView unsuspend success");
                  self.render();
              }
          });
      },

      alertMsg: function(options) {
          var msg = options.msg;
          
          // Type must be info||success||error. Default: info.
          var type = options.type || 'info';
          
          this.removeAlertMsg();

          var alertTpl  = '<div class="span4">';
          alertTpl += '<div id="alertMsg" class="alert alert-' + type + '">';
          alertTpl += '<button type="button" class="close" data-dismiss="alert">x</button>';
          alertTpl += '<%- msg %>';
          alertTpl += '</div>';
          alertTpl += '</div>';
          alertTpl = _.template(alertTpl);
          
          $(this.el).find('#alerts').html(alertTpl({ msg: msg }));
      },
      
      removeAlertMsg: function() {
          $(this.el).find('#alertMsg').remove();
      }
  });

  return CashierShowView;
});
