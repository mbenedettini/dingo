define([
  'jquery',
  'underscore',
  'backbone',
  'moment',
  'text!templates/cashiers/index.html',
  'CashierCollection'
], function($, _, Backbone, moment, tpl, CashierCollection) {
    var CashierListView;

    CashierListView = Backbone.View.extend({
        initialize: function() {
            this.template = _.template(tpl);
        },
        render: function() {
            var that = this, tmpl;
            var cashiers = this.collection.toJSON();
            tmpl = that.template({ 'cashiers': cashiers });
            $(that.el).html(tmpl);
            
            return this;
        }
    });

    return CashierListView;
});
