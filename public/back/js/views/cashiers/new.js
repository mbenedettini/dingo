define( [
    'utils',
    'jquery',
    'underscore',
    'backbone',
    'moment',
    'text!templates/cashiers/new.html',
    'CashierModel'
], function(utils, $, _, Backbone, moment, tpl, Cashier) {
  var CashierNewView;

  CashierNewView = Backbone.View.extend({
      initialize: function() {
          this.template = _.template(tpl);

          this.errTmpl  = '<div class="span4">';
          this.errTmpl += '<div class="alert alert-error">';
          this.errTmpl += '<button type="button" class="close" data-dismiss="alert">x</button>';
          this.errTmpl += '<%- msg %>';
          this.errTmpl += '</div>';
          this.errTmpl += '</div>';
          this.errTmpl = _.template(this.errTmpl);
      },
      events: {
          "focus .input-prepend input" : "removeErrMsg",
          "click .btn-primary"            : "saveCashier",
          "click .btn-back"            : "goBack"
      },
      render: function() {
          var tmpl = '';

          tmpl = this.template({ cashier: this.model.toJSON() });
          $(this.el).html(tmpl);

          return this;
      },

      goBack: function(e) {
          e.preventDefault();
          this.trigger('back');
      },

      saveCashier: function(e) {
          var self = this;
          var name, username, email;

          e.preventDefault();

          name    = $.trim($('#name-input').val());
          email   = $.trim($('#email-input').val());
          username = $.trim($('#username-input').val());

          this.model.save({
              name        : name,
              email       : email,
              username    : username,
          }, {
              silent  : false,
              sync    : true,
              success : function(model, res) {
                  if (res && res.errors) {
                      self.renderErrMsg(res.errors);
                  } else {
                      model.trigger('save-success', model.get('id'));
                  }
              },
              error: function(model, res) {
                  if (res && res.errors) {
                      self.renderErrMsg(res.errors);
                  } else if (res.status === 404) {
                      // TODO: handle 404 Not Found
                  } else if (res.status === 500) {
                      // TODO: handle 500 Internal Server Error
                  }
              }
          });
      },

      renderErrMsg: function(err) {
          var msgs = [];

          this.removeErrMsg();

          if (_.isString(err)) {
              msgs.push(err);
          } else {
              if (err.general) {
                  msgs.push(err.general);
                  delete err.general;
              }
              if (_.keys(err).length) {
                  msgs.push(_.keys(err).join(', ') + ' son inválidos');
              }
          }
          msgs = _.map(msgs, function(string) {
              // uppercase first letter
              return string.charAt(0).toUpperCase() + string.slice(1);
          }).join('.');
          $(this.el).find('form').after(this.errTmpl({ msg: msgs }));
      },

      removeErrMsg: function() {
          $(this.el).find('.alert-error').remove();
      }

  });

  return CashierNewView;
});
