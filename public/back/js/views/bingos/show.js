define( [
    'utils',
    'jquery',
    'underscore',
    'backbone',
    'moment',
    'rivets',
    'text!templates/bingos/show.html',
    'BingoModel'
       ], function(utils, $, _, Backbone, moment, rivets, tpl, Bingo) {
           var BingoShowView = Backbone.View.extend({                                                    
               initialize: function() {
                   this.template = _.template(tpl);
                   
               },
           
               events: {
                   "click #start-btn": "start",
                   "click #pause-btn": "pause",
                   "click #stop-btn": "stop"
               },
           
               render: function(argModel) {
                   var that = this, tmpl;
                   
                   this.model = argModel ? argModel : this.model;

                   var modelJSON = this.model.toJSON();
                   
                   tmpl = that.template({ 'bingo': modelJSON  });
                   $(that.el).html(tmpl);
                   
                   rivets.configure({
                       adapter: {
                           subscribe: function(obj, keypath, callback) {
                               obj.on('change:' + keypath, callback);
                           },
                           unsubscribe: function(obj, keypath, callback) {
                               
                               obj.off('change:' + keypath, callback);
                           },
                           read: function(obj, keypath) {
                               
                               return obj.get(keypath);
                           },
                           publish: function(obj, keypath, value) {
                               
                               obj.set(keypath, value);
                           }
                       }
                   });

                   rivets.formatters.money = function(value){
                       return value.toFixed(2);
                   };

                   rivets.formatters.time = function(value) {
                       /* Formats hours and minutes in a format of hh:mm. Argument must be two
                        * numbers separated by semicolon.
                        */
                       var pad = "00";
                       var parts = value.split(":");
                       var output = [];
                       _.each(parts, function(p) {
                           output.push(pad.substring(0, pad.length - p.length) + p);
                       });
                       return output.join(':');
                   };

                   return this;
               },
                                                    
               pause: function(e) {
                   e.preventDefault();
                   this.model.pause();
               },
                                                    
               start: function(e) {
                   e.preventDefault();
                   this.model.start();
               },

               stop: function(e) {
                   e.preventDefault();
                   this.model.stop();
               },

               removeErrorMessages: function() {
               },

               /* Updates handlers */
               processUpdateNumberCalled: function(args) {
                   // args.bingoId
                   // args.calledNumbers
                   // args.remainingNumbers
                   this.model.set("calledNumbers", args.calledNumbers);
                   this.model.set("remainingNumbers", args.remainingNumbers);
               },

               processUpdateBingoUpdated: function(args) {
                   var bingo = args.bingo;
                   if (this.model && this.model.get('id') && this.model.get('id') == bingo.id) {
                       utils.debug("Bingo updated. New Bingo:");
                       utils.debug(bingo);
                       this.model.set(bingo);
                   }

               },

               processUpdateSellingStarted: function(args) {
                   var bingo = args.bingo;
                   utils.debug("BingoShowView: update sellingFinishesAt");
                   utils.debug(args.bingo);
                   this.model.set('sellingFinishesAt', bingo.sellingFinishesAt);
                   this.model.set('isSelling', bingo.isSelling);
               },

               processUpdateBingoStarted: function(args) {
                   var bingo = args.bingo;
                   this.model && this.model.set(bingo);
               }
               /* End of updates handlers */

          });

           return BingoShowView;
       });
