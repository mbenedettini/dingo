define( [
    'utils',
    'jquery',
    'underscore',
    'backbone',
    'moment',
    'text!templates/bingos/edit.html',
    'BingoModel',
    'bootstrap-inputmask'
       ], 
function(utils, $, _, Backbone, moment, tpl, Bingo) {
    var BingoNewView = Backbone.View.extend({
        initialize: function() {
            this.template = _.template(tpl);
            //$('.inputmask').inputmask();
            // data-mask="99:99 99/99/9999"

        },
        events: {
            "click .btn-primary"         : "saveBingo",
            "click .btn-back"            : "goBack",
            "focus #starts-input"        : "onFocusStartsInput"
        },

        render: function() {
            var tmpl = '';

            tmpl = this.template({ bingo: this.model.toJSON() });
            $(this.el).html(tmpl);

            utils.debug("Setting mask!");
            $(this.el).find('#starts-input').inputmask({mask: "99:99 99/99/9999" });

            var cardColorInput = $(this.el).find('#cardColor-input option[value="' + this.model.get('cardColor') + '"]');
            cardColorInput.attr("selected", true);

            return this;
        },

        goBack: function(e) {
            e.preventDefault();
            this.trigger('back');
        },

        saveBingo: function(e) {
            var self = this;
            e.preventDefault();

            // Restore all error states
            $('.control-group').removeClass('error');
            //$(".alert").alert("close");

            var data = {
                cardPrice: $.trim($('#cardPrice-input').val()),
                cardColor: $.trim($('#cardColor-input').val()),
                lapseBetweenNumbers: $.trim($('#lapseBetweenNumbers-input').val()),
                sellingInterval: $.trim($('#sellingInterval-input').val()),
                linePrize: $.trim($('#linePrize-input').val()),
                linePrizeRate: $.trim($('#linePrizeRate-input').val()),
                bingoPrize: $.trim($('#bingoPrize-input').val()),
                bingoPrizeRate: $.trim($('#bingoPrizeRate-input').val()),
                remainingNumbers: $.trim($('#remainingNumbers-input').val()).split(","),
                calledNumbers: $.trim($('#calledNumbers-input').val()).split(","),
                starts: moment($.trim($('#starts-input').val()), "HH:mm DD/MM/YYYY"),
                minClientsCount: $.trim($('#minClientsCount-input').val()),
                minTotalClientsCount: $.trim($('#minTotalClientsCount-input').val()),
                sellingCancellationTimeout: $.trim($('#sellingCancellationTimeout-input').val())
            };

            this.model.save(data, {
                silent  : false,
                sync    : true,
                success : function(model, res) {
                    if (res && res.errors) {
                        self.renderErrMsg(res.errors);
                    } else {
                        model.trigger('save-success', model.get('id'));
                    }
                },
                error: function(model, res) {
                    if (res && res.errors) {
                        self.renderErrMsg(res.errors);
                    } else if (res.status === 404) {
                        // TODO: handle 404 Not Found
                    } else if (res.status === 500) {
                        // TODO: handle 500 Internal Server Error
                    }
                }
            });
        },

        onFocusStartsInput : function(e) {
            e.preventDefault();

            if (! $('#starts-input').val() ) {
                $('#starts-input').val(moment().format("HH:mm DD/MM/YYYY"));
            }
        },

        renderErrMsg: function(err) {
            var msgs = [];


            if (_.isString(err)) {
                msgs.push(err);
            } else {
                if (err.general) {
                    msgs.push(err.general);
                    delete err.general;
                }

                _.each(_.keys(err), function(field) {
                    msgs.push(err[field]);
                    $('div.control-group#' + field ).addClass('error');
                });
            }
            var errorMsg = _.map(msgs, function(string) {
                // uppercase first letter
                return string.charAt(0).toUpperCase() + string.slice(1);
            }).join('. ');
            
            $("#alert-container").html('<div class="alert fade in"><button type="button" class="close" data-dismiss="alert">×</button><strong>Errores&nbsp;</strong><span></span></div>');
            $(".alert").find("span").text(errorMsg);
            $(".alert").alert();
        },

    });

    return BingoNewView;
});
