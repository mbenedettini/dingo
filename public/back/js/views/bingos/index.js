define( [
  'jquery',
  'underscore',
  'backbone',
  'moment',
  'text!templates/bingos/index.html',
  'BingoCollection'
], function($, _, Backbone, moment, tpl, BingoCollection) {
    var BingoListView = Backbone.View.extend({
        initialize: function() {
            var bingoList;

            this.template = _.template(tpl);
            this.collection; 
        },
        render: function() {
            var that = this, tmpl;
            var bingos = this.collection.toJSON();
            tmpl = that.template({ 'bingos': bingos });
            $(that.el).html(tmpl);
            
            return this;
        }
    });

    return BingoListView;
});
