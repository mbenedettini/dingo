define( [
    'utils',
    'jquery',
    'underscore',
    'backbone',
    'moment',
    'text!templates/settings/index.html',
    'SettingModel',
    'SettingCollection'
], function(utils, $, _, Backbone, moment, tpl, Setting, SettingCollection) {
    var SettingListView = Backbone.View.extend({
        initialize: function() {
            var bingoList;

            this.template = _.template(tpl);
        },

        events: {
            "click button" : "save"
        },

        save: function(e){
            e.preventDefault();

            var key = e.currentTarget.id;

            var model = this.collection.get(key);

            /* Input exceptions where
             *   key: input id
             *   value: input property to be evaluated
             * 
             * Default input property: val()
             */
            var exceptions = {
                'accumulatedEnabled': "is(':checked') ? 1 : 0",
                'autoBingoEnabled': "is(':checked') ? 1 : 0"
            };

            var input = $('#' + key + '-input');
            var value;
            if (!exceptions[key]) {
                value = input.val();
            }
            else {
                eval('value = input.' + exceptions[key]);
            }
            
            model.save({value: value}, {
                success: function(model, res) {},
                error : function(model, res) {}

            });

            utils.debug("value: " + value);
        }, 

        render: function() {
            var that = this, tmpl;
            var settingsByKey = {};
            this.collection.each(function(s) {
                settingsByKey[s.get("key")] = { description: s.get("description"), value: s.get("value") };
            });
            tmpl = that.template({ 'settings': settingsByKey });
            $(that.el).html(tmpl);

            $(that.el).find('#cardColor-input option[value="' + settingsByKey.cardColor.value + '"]').attr("selected", true);
            
            return this;
        }
    });

    return SettingListView;
});
