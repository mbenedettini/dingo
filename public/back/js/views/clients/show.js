define([
    'utils',
    'jquery',
    'underscore',
    'backbone',
    'moment',
    'text!templates/clients/show.html',
    'text!templates/clients/transactions.html',
    'ClientModel',
    'DepositModel',
    'WithdrawalModel'
], function(utils, $, _, Backbone, moment, tpl, txsTpl, Client, Deposit, Withdrawal) {
  var ClientView;

  ClientView = Backbone.View.extend({
    
      initialize: function() {
          this.template = _.template(tpl);
      },
      
      events: {
          "click #makeDeposit-btn" :    "makeDeposit",
          "submit #depositForm" :       "makeDeposit",
          "focus #depositForm"  :       "removeErrorMessages",
          
          "click #makeWithdrawal-btn":  "makeWithdrawal",
          "submit #withdrawalForm":     "makeWithdrawal",
          "focus #withdrawalForm" :     "removeErrorMessages",

          "click #resetPassword-btn":   "resetPassword",
          
          'show #clientTabs a[href="#tab2"]': "renderTransactions",

          "click #suspend-btn": "suspend",
          "click #unsuspend-btn": "unsuspend"
      },
      
      render: function(argModel) {
          var that = this, tmpl;
          
          this.model = argModel ? argModel : this.model;

          var modelJSON = this.model.toJSON();
          
          tmpl = that.template({ 'client': modelJSON });
          $(that.el).html(tmpl);
          
          return this;
      },
      
      renderTransactions: function() {
          //~ e.target; // activated tab
          //~ e.relatedTarget; // previous tab
          utils.debug("Rendering transactions!");
          
          // TODO: do not render them every time the tab is shown
          
          var transactionsJSON = this.model.transactions.toJSON();
          var compiledTxsTemplate = _.template(txsTpl, {transactions: transactionsJSON});
          $('#tab2').html(compiledTxsTemplate);
      },
      
      removeClient: function(e) {
          e.preventDefault();

          this.model.destroy({
              sync: true,
              success: function(model) {
                  model.trigger('delete-success');
              },
              error: function(model, res) {
                  if (res.status === 404) {
                      // TODO: handle 404 Not Found
                  } else if (res.status === 500) {
                      // TODO: handle 500 Internal Server Error
                  }
              }
          });
      },
      
      makeDeposit: function(e) {
          e.preventDefault();
          
          var self = this;

          // Parse input and allow both decimal separators
          var amount = parseFloat(
              ($.trim(
                  $('#depositAmount-input').val()
              )).
                  replace(",", ".")
          );
          if (amount) {
              //~ alert(amount);
              
              utils.debug("Making deposit for " + amount);
              $('#depositModal').modal('hide');
              //~ this.model.makeDeposit(amount);
              var deposit = new Deposit({'clientId': this.model.id, 'amount':amount});
              
              deposit.save(null, {
                  silent  : false,
                  sync    : true,
                  success : function(tx, res) {
                      if (res && res.errors) {
                          //~ that.renderErrMsg(res.errors);
                          utils.debug("DAMN DEPOSIT");
                      } else {
                          //model.trigger('save-success', model.get('id'));
                          $('#depositForm').each (function() { this.reset(); });
                          utils.debug("Successful deposit!");
                      }
                  },
                  error: function(model, res) {
                      if (res && res.errors) {
                          that.renderErrMsg(res.errors);
                      } else if (res.status === 404) {
                          // TODO: handle 404 Not Found
                      } else if (res.status === 500) {
                          // TODO: handle 500 Internal Server Error
                      }
                  }
              });
          }
          else {
              //~ $('#myModal').modal('show');
              $('#amount-group').addClass('error');
              $('#depositAlert').text("El monto ingresado no es correcto.").show();
          }
          
          //~ return false;
      },
      
      makeWithdrawal: function(e) {
          e.preventDefault();
          
          var self = this;
          
          var amount = parseFloat(
              ($.trim(
                  $('#withdrawalAmount-input').val()
              )).
                  replace(",", ".")
          );
          if (amount) {
              //~ alert(amount);
              
              utils.debug("Making withdrawal for " + amount);
              $('#withdrawalModal').modal('hide');
              //~ this.model.makeDeposit(amount);
              var withdrawal = new Withdrawal({'clientId': this.model.id, 'amount':amount});
              
              withdrawal.save(null, {
                  silent  : false,
                  sync    : true,
                  success : function(tx, res) {
                      if (res && res.errors) {
                          //~ that.renderErrMsg(res.errors);
                          utils.debug("DAMN WITHDRAWAL");
                      } else {
                          // Clean up form if everything went right
                          $('#withdrawalForm').each (function() { this.reset(); });
                          utils.debug("Successful withdrawal!");
                      }
                  },
                  error: function(model, res) {
                      if (res && res.errors) {
                          that.renderErrMsg(res.errors);
                      } else if (res.status === 404) {
                          // TODO: handle 404 Not Found
                      } else if (res.status === 500) {
                          // TODO: handle 500 Internal Server Error
                      }
                  }
              });
          }
          else {
              //~ $('#myModal').modal('show');
              $('#withdrawalAmount-group').addClass('error');
              $('#withdrawalAlert').text("El monto ingresado no es correcto.").show();
          }
          
          //~ return false;
      },
      
      resetPassword: function(e) {
          var self = this;
          e.preventDefault();

          this.model.resetPassword(
              function callback() {
                  self.alertMsg({
                      msg: "El nuevo password ha sido enviado al usuario"
                  });
              }, 
              function errback() {
                  
              }
          );
      },

      suspend: function(e) {
          var self = this;
          e.preventDefault();

          this.model.suspend({
              success: function(model, res) {
                  utils.debug("SUCCESSFUL suspend!");
                  utils.debug(model);
                  utils.debug(res);
              },
              error: function(model, res) {}
          });
      },

      unsuspend: function(e) {
          var self = this;
          e.preventDefault();

          this.model.unsuspend({
              success: function(model, res) {
                  utils.debug("SUCCESSFUL unsuspend!");
                  utils.debug(model);
                  utils.debug(res);
              },
              error: function(model, res) {}
          });
      },


      processUpdateClient: function(model) {
          utils.debug("Update client");
          console.log(model);
          this.render(new Client(model));
      },
      
      processUpdateClientBalance: function(args) {
          $('#clientBalance').fadeOut(200, function() {
              $(this).text(args.balance).fadeIn(500);
          });
      },
      
      removeErrorMessages: function() {
          $('#depositAmount-group').removeClass('error');
          $('#withdrawalAmount-group').removeClass('error');
          $('#depositAlert').text("").hide();
          $('#withdrawalAlert').text("").hide();
      },

      alertMsg: function(options) {
          var msg = options.msg;
          
          // Type must be info||success||error. Default: info.
          var type = options.type || 'info';
          
          this.removeAlertMsg();

          var alertTpl  = '<div class="span4">';
          alertTpl += '<div id="alertMsg" class="alert alert-' + type + '">';
          alertTpl += '<button type="button" class="close" data-dismiss="alert">x</button>';
          alertTpl += '<%- msg %>';
          alertTpl += '</div>';
          alertTpl += '</div>';
          alertTpl = _.template(alertTpl);
          
          $(this.el).find('#alerts').html(alertTpl({ msg: msg }));
      },
      
      removeAlertMsg: function() {
          $(this.el).find('#alertMsg').remove();
      }
  });

  return ClientView;
});
