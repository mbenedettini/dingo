define( [
    'utils',
    'jquery',
    'underscore',
    'backbone',
    'moment',
    'text!templates/clients/new.html',
    'ClientModel'
], function(utils, $, _, Backbone, moment, tpl, Client) {
  var ClientNewView;

  ClientNewView = Backbone.View.extend({
    initialize: function() {
      this.template = _.template(tpl);

      this.errTmpl  = '<div class="span4">';
      this.errTmpl += '<div class="alert alert-error">';
      this.errTmpl += '<button type="button" class="close" data-dismiss="alert">x</button>';
      this.errTmpl += '<%- msg %>';
      this.errTmpl += '</div>';
      this.errTmpl += '</div>';
      this.errTmpl = _.template(this.errTmpl);
    },
    events: {
      "focus .input-prepend input" : "removeErrMsg",
      "click .btn-primary"            : "saveClient",
      "click .btn-back"            : "goBack"
    },
    render: function() {
      //~ var tmpl, formattedDate = ' ', bornAttr;
      var tmpl = '';

      //~ bornAttr = this.model.get('born');
      //~ formattedDate = moment(new Date(bornAttr)).format("MM/DD/YYYY");

      //~ tmpl = this.template({ client: this.model.toJSON(), formattedDate: formattedDate });
      tmpl = this.template({ client: this.model.toJSON() });
      $(this.el).html(tmpl);

      return this;
    },

    goBack: function(e) {
      e.preventDefault();
      this.trigger('back');
    },

    saveClient: function(e) {        
        var self = this;
        e.preventDefault();

        var name, lastName, email, username, password, telephone;
        
        name     = $.trim($('#name-input').val());
        lastName = $.trim($('#lastName-input').val());
        email    = $.trim($('#email-input').val());
        username = $.trim($('#username-input').val());
        password = $.trim($('#password-input').val());
        telephone = $.trim($('#telephone-input').val());

        this.model.save({
            name        : name,
            lastName    : lastName,
            email       : email,
            username    : username,
            password    : password, // ?? TODO: check that this should be here!!
            telephone   : telephone
        }, {
            silent  : false,
            sync    : true,
            success : function(model, res) {
                if (res && res.errors) {
                    that.renderErrMsg(res.errors);
                } else {
                    model.trigger('save-success', model.get('id'));
                }
            },
            error: function(model, res) {
                if (res && res.errors) {
                    that.renderErrMsg(res.errors);
                } else if (res.status === 404) {
                    // TODO: handle 404 Not Found
                } else if (res.status === 500) {
                    // TODO: handle 500 Internal Server Error
                }
            }
        });
    },

    renderErrMsg: function(err) {
      var msgs = [];

      this.removeErrMsg();

      if (_.isString(err)) {
        msgs.push(err);
      } else {
        if (err.general) {
          msgs.push(err.general);
          delete err.general;
        }
        if (_.keys(err).length) {
          msgs.push(_.keys(err).join(', ') + ' son inválidos');
        }
      }
      msgs = _.map(msgs, function(string) {
        // uppercase first letter
        return string.charAt(0).toUpperCase() + string.slice(1);
      }).join('.');
      $(this.el).find('form').after(this.errTmpl({ msg: msgs }));
    },

    removeErrMsg: function() {
      $(this.el).find('.alert-error').remove();
    }

  });

  return ClientNewView;
});
