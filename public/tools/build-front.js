/* How to run it:
 * 
 * mariano@macondo:~/work/dingo/public/tools$ ../../node_modules/requirejs/bin/r.js -o build-front.js
 * 
 * It will generate a minified version of public/front in public/back-front.
 * 
 * 
 */
({
    appDir: '../front',
    baseUrl: './js',
    mainConfigFile: '../common/js/main-common.js',
    dir: '../front-built',
    findNestedDependencies: true,
    // optimize: 'none',
    
    modules: [
        //First set up the common build layer.
        {
            //module names are relative to baseUrl
            name: 'common/main-common',
            //List common dependencies here. Only need to list
            //top level dependencies, "include" will find
            //nested dependencies.
            include: [
                'jquery',
                'bootstrap',
                'backbone',
                'rivets',
                'socket.io',
                'moment',
                'moment-es',
                'jqueryCookie',
                'jqueryUI'
            ],
        },

        {
            // module names are relative to baseUrl/paths config
            name: 'main',
            include: ['app', 'router', 'views/home', 'views/login', 'views/account' ],
            exclude: ['common/main-common'],
        },
    ]
})