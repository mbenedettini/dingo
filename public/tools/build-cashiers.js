/* How to run it:
 * 
 * mariano@macondo:~/work/dingo/public/tools$ ../../node_modules/requirejs/bin/r.js -o build-cashiers.js
 * 
 * It will generate a minified version of public/cashiers in public/cashiers-built
 * 
 * 
 */
({
    appDir: '../cashiers',
    baseUrl: 'js',
    mainConfigFile: '../back/js/common/main-common.js',
    dir: '../cashiers-built',
    findNestedDependencies: true,
    // optimize: 'none',
    
    modules: [
        //First set up the common build layer.
        {
            //module names are relative to baseUrl
            name: 'common/main-common',
            //List common dependencies here. Only need to list
            //top level dependencies, "include" will find
            //nested dependencies.
            include: [
                'jquery',
                'bootstrap',
                'backbone',
                'rivets',
                'socket.io',
                'moment',
                'moment-es',
                'bootstrap-inputmask',
                'jqueryCookie'
            ]
        },

        {
            // module names are relative to baseUrl/paths config
            name: 'main',
            include: ['app'],
            exclude: ['common/main-common']
        }
    ]
})