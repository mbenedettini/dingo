var async   = require('async'),
    fs      = require('fs'),
    ENV     = process.env.NODE_ENV || 'development',
    _       = require('underscore'),
    colors  = require('colors'),
    sinon   = require('sinon'),
    log     = console.log,
    tables  = require('../app/models/tables'),
    utils;

var patio   = require("patio"),
    comb    = require("comb"),
    when    = comb.when,
    serial  = comb.serial;

var TABLES = ["cashier", "setting", "transaction", "client_card", "card", "bingo", "client"];

utils = {
    getENV: function() {
        return process.env.NODE_ENV || 'development';
    },

    getBINGO_ENV: function() {
        return process.env.BINGO_ENV || 'production';
    },

    syncedModels: null,
    
    loadJson: function(filePath) {
        var data = fs.readFileSync(filePath, 'UTF-8');

        return JSON.parse(data);
    },
    
    walkDir: function(dir) {
        var results = [];

        var list = fs.readdirSync(dir);

        var pending = list.length;

        if (!pending) { return null };

        list.forEach(function(file) {
            file = dir + '/' + file;
            //~ console.log("Mhh: " + file);
            var stat = fs.statSync(file);
            
            if (stat && stat.isDirectory()) {
                var res = utils.walkDir(file);
                results = results.concat(res);
            } else {
                results.push(file);
            }
        });
    
    return results;
    },
    
    loadFixtures: function() {
        var data = {};
        
        var files = utils.walkDir(__dirname + '/../test/fixtures' );
        
        files.forEach(function(filePath) {
            var name = filePath.match(/.*\/(\w+)\.js/)[1];
            var d = utils.loadJson(filePath);
            data[name] = d;
        });
        
        return data;
    },
    
    /*
    * Recursively search the directory for all JSON files, parse them
    * and trigger a callback with the contents
    */
    loadConfig: function(directory) {
        var files = utils.walkDir(directory);
        
        var pending = files.length,
          configs = {},
          ENV     = process.env.NODE_ENV || 'development';
          
        //~ console.log('Loading config for ' + ENV);

        files.forEach(function(filePath) {
        
            var data = utils.loadJson(filePath);
            
            var filename = filePath.split('/').pop().replace('.json', '');

            configs[filename] = data;

            if (!--pending) {
                configs.site_url  = (configs[ENV].HTTPS) ? 'https://' : 'http://';
                configs.site_url += configs[ENV].HOST + ':' + configs[ENV].PORT;
            }
        });
    
        return configs;
    },

    getDbConnection: function(uri) {
        if (! uri) {
            var config = this.loadConfig(__dirname + "/../config");
            var ENV = this.getENV();
            uri = config.db[ENV].main;
        }
        console.log("==> Connecting to DB!");
        return patio.createConnection(uri);
    },

    cleanDb : function() {
        var ret = new comb.Promise();
        var db = utils.getDbConnection();
        var toExecute = [comb.hitch(db, "run", "SET foreign_key_checks = 0")];
        TABLES.forEach(function(t) {
            toExecute.push(comb.hitch(db, "run", "DELETE FROM " + t));
        });
        toExecute.push(comb.hitch(db, "run", "SET foreign_key_checks = 1"));
        comb.serial(toExecute).then(comb.hitch(ret, "callback"), comb.hitch(ret, "errback"));

        return ret;
    },

    getSyncedModels : function() {
        var self = this;
        var ret = new comb.Promise();

        var db = utils.getDbConnection();

        var models = require(__dirname + "/../app/models");
        patio.syncModels().then(
            function() {
                self.syncedModels = models;
                ret.callback(models);
            },
            comb.hitch(ret, "errback")
        );

        return ret;
    },
    
    dropTables: function() {
        log('- db:dropping tables'.yellow);
        
        var db = this.getDbConnection();
        return serial([
            comb.hitch(db, "run", "SET foreign_key_checks = 0"),
            function() {
		return db.forceDropTable(["schema_migrations"].concat(TABLES));
		},
            comb.hitch(db, "run", "SET foreign_key_checks = 1")
        ]);
    },
    
    createTables: function() {
        var db = this.getDbConnection();
        log('- db:creating tables'.yellow);
        return serial([
            // EXAMPLE: comb.hitch(db, "createTable", "tableName", tables.tableDefinition),
            comb.hitch(db, "run", "SET foreign_key_checks = 0"),
            comb.hitch(db, "createTable", "client",         tables.clientTable),
            comb.hitch(db, "createTable", "transaction",    tables.transactionTable),
            comb.hitch(db, "createTable", "bingo",          tables.bingoTable),
            comb.hitch(db, "createTable", "card",           tables.cardTable),
            comb.hitch(db, "createTable", "client_card",     tables.clientCardTable),
            comb.hitch(db, "run", "SET foreign_key_checks = 1")
        ]).then(
            function() {
                log('- db: tables have been successfully created'.green);
            },
            function(error) {
                log(('- db: ERROR creating tables: ' + error).red);
            }
        );
    },
    recreateSchema: function() {
        var self = this;
        var ret = new comb.Promise();
        var db = this.getDbConnection();
        
        return db.transaction(function() {
            
            serial([
                comb.hitch(self, "dropTables"),
                comb.hitch(self, "createTables")
            ]).then(
                comb.hitch(ret, "callback"),
                comb.hitch(ret, "errback")
            );
            
            return ret;
        }).then(
            function() {
                patio.disconnect();
            },
            function(error) {
                patio.disconnect();
            }
        );
    },

    /* Tests-related methods */
    stubPatioModel: function(model) {
        var modelStub = sinon.stub(model);
        
        model._static.__columns.forEach(function(column) { 
            model.__defineGetter__(column, function() { 
                return this["_" + column ]; 
            }); 
        });
        model._static.__columns.forEach(function(column) { 
            model.__defineSetter__(column, function(value) { 
                this["_" + column ] = value ; }
                                  ); 
        });
        
        return modelStub;
    },

    overridePatioAssociation: function(model, associationName) {
        model.prototype.__defineSetter__(associationName, function(value) { 
            eval("this.__" + associationName + " = value");
        });
        
        model.prototype.__defineGetter__(associationName, function() { 
            var value;
            eval("value = this.__" + associationName);
            return value;
        });
    },

    migrate: function() {
        var self = this;
        var ret = new comb.Promise();
        var db = this.getDbConnection();
        
        return db.transaction(function() {
            console.log("Beginning migration");
            return patio.migrate(db, __dirname + "/../migrations");
        }).then(
            function() {
                console.log("Successfully migration!");
                patio.disconnect();
            },
            function(error) {
                console.log("FUCKING migration: " + error);
                patio.disconnect();
            }
        );
    },

    generateRandomPassword : function(){
        var length = 9;
        var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        var randomPass = "";
        
        for( var x=0;x<length;x++) {
            var i = Math.floor(Math.random() * 62);
            randomPass += chars.charAt(i);
        }
        return randomPass;
    }
};

function NotFound(msg) {
  this.name = 'NotFound';
  this.msg  = msg;
  Error.call(this, msg);
  Error.captureStackTrace(this, arguments.callee);
}

NotFound.prototype.__proto__ = Error.prototype;

utils.NotFound = NotFound;

module.exports = utils;
