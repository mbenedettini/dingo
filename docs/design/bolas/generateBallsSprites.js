/* Once node-spritesheet is installed via npm, we must hack it in order to produce a transparent sprite.
 * 
 * Find the following line:
 * node_modules/node-spritesheet/lib/imagemagick.js:37
 * 
 * And modify it to make it look like this (the change is canvas:white -> canvas:transparent):
 * return this.exec("convert -size " + width + "x" + height + " canvas:transparent -alpha transparent " + filepath, function(error, stdout, stderr) {
 * 
 * Extract the contents of bolas.rar to the current directory and run rename.js to rename them to more suitable names for the sprite.
 * 
 * Now, run the sprite generator:
 * 
 * mariano@macondo:~/work/dingo/docs/design/bolas$ node generateBallsSprites.js
 * 
 *
 * Once generated, fix the image url in output/sprite.css, it should look like this:
 * 
 * background: url( '/img/sprite-balls.png' ) no-repeat; 
 * 
 * 
 * And use pngquant (http://pngquant.org/) to reduce the image size:
 * 
 * $ pngquant 256 sprite-balls.png
 * 
 * Its output file will be sprite-balls-fs8.png
 * 
 * 
 * Final step, copy :
 * 
 * output/sprite-balls-fs8.png to public/front/img/sprite-balls.png
 * output/sprite-balls.css to public/front/css/sprite-balls.css
 * 

 * 
 * There you are!
 * 
 */
var fs = require('fs');
var Builder = require('node-spritesheet').Builder,
    images = [],
    pngs = [];

fs.readdir(".", function(err, files) {
    console.log("Err : " + err);
    console.log("Files: " + files);
    console.log(files.length);

    pngs = files.filter(function(f) { return f.match(/\.png$/); });

    console.log("Pngs: " + pngs);
    console.log(pngs.length);

    new Builder( pngs, images, {
        outputDirectory: 'output',
        outputImage: 'sprite-balls.png',
        outputCss: 'sprite-balls.css',
        selector: '.sprite-balls'
    })
        .build( function() {
            console.log( "Built from " + images.length + " images" );
        });
});
