/* Renames balls filenames sent by Paola to a sprites-suitable form.
 * 
 * Examples: 
 *     33.png -> normal33.png
 *     7small.png -> small7.png
 */

var fs = require('fs');

fs.readdir(".", function(err, files) {
    var pngsNormal = files.filter(function(f) { return f.match(/\d\d?\.png$/); });
    var pngsSmall = files.filter(function(f) { return f.match(/\d\d?small.png$/); });
    
    pngsNormal.forEach(function(fileName) {
        var number = fileName.match(/(\d\d?).png$/)[1];
        var newName = "normal" + number + ".png";
        console.log("Renaming " + fileName + " to " + newName);
        fs.renameSync(fileName, newName);
    });

    pngsSmall.forEach(function(fileName) {
        var number = fileName.match(/(\d\d?)small.png$/)[1];
        var newName = "small" + number + ".png";
        console.log("Renaming " + fileName + " to " + newName);
        fs.renameSync(fileName, newName);
    });

});
