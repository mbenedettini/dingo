var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.alterTable("client_card", function() {
        this.addColumn("wonLine", Boolean, {allowNull: false, default: false});
        this.addColumn("wonBingo", Boolean, {allowNull: false, default: false});
    });
};

exports.down = function(db) {
    return db.alterTable("client_card", function() {
        this.dropColumn("wonLine");
        this.dropColumn("wonBingo");
    });
};
