var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.alterTable("bingo", function() {
        this.addColumn("finished", patio.sql.Timestamp, {allowNull: true});
    });
};

exports.down = function(db) {
    return db.alterTable("bingo", function() {
        this.dropColumn("finished");
    });
};
