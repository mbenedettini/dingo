var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.from("setting").insertMultiple([
            {"key": "lapseBetweenNumbers", "value": 5000, "description": "Tiempo entre numeros (milisegundos)."},
        ]);
};

exports.down = function(db) {
    return db.from("setting").filter({"key": "lapseBetweenNumbers"}).remove();
};
