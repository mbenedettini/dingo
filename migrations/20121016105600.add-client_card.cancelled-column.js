var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.alterTable("client_card", function() {
        this.addColumn("cancelled", Boolean, {allowNull: false, default: false});
    });
};

exports.down = function(db) {
    return db.alterTable("client_card", function() {
        this.dropColumn("cancelled");
    });
};
