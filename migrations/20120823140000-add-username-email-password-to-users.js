var comb = require("comb"),
    when = comb.when;
    
exports.up = function(DB){
    return comb.when(
      DB.alterTable("client", function() {
      console.log("ooooooooooooo!");
      this.addColumn("username", String);
      this.addColumn("email", String);
      this.addColumn("password", String);
      })
        
    );
};

exports.down = function(DB) {
    return DB.alterTable("client", function() {
      this.dropColumn("username");
      this.dropColumn("email");
      this.dropColumn("password");
    });
  }
};
 
