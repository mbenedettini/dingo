var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.from("setting").insertMultiple([
            {"key": "sellingCancellationTimeout", "value": 120, "description": "Tiempo de espera hasta lograr las condiciones de comienzo"},
        ]);
};

exports.down = function(db) {
    return comb.when([
        db.from("setting").filter({"key": "sellingCancellationTimeout"}).remove()
    ]);
};
