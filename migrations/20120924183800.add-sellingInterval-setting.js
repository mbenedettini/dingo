var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.from("setting").insertMultiple([
            {"key": "sellingInterval", "value": 300, "description": "Duracion de la venta de cartones (segundos)."},
        ]);
};

exports.down = function(db) {
    return db.from("setting").filter({"key": "sellingInterval"}).remove();
};
