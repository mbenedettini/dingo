var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.alterTable("client", function() {
        this.setColumnType("balance", "numeric(10,2)");
    });
};

exports.down = function(db) {
    return db.alterTable("client", function() {
        this.setColumnType("balance", patio.sql.Float);
    });
};
