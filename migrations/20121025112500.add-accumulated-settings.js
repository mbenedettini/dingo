var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.from("setting").insertMultiple([
        { "key": "accumulatedOrder", "value": 39, "description": "Bola max para el pozo acumulado" } ,
        { "key": "accumulatedPrize", "value": 0, "description": "Pozo acumulado" },
        { "key": "accumulatedRatio", "value": 0.05, "description": "Porcentaje de cada carton destinado al Pozo acumulado" },
        { "key": "accumulatedEnabled", "value": 0, "description": "Pozo acumulado activado"}            
    ]);
};

exports.down = function(db) {
    return comb.when([
        db.from("setting").filter({"key": "accumulatedOrder"}).remove(),
        db.from("setting").filter({"key": "accumulatedPrize"}).remove(),
        db.from("setting").filter({"key": "accumulatedRatio"}).remove(),
        db.from("setting").filter({"key": "accumulatedEnabled"}).remove()
    ]);
};
