var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.alterTable("client", function() {
        this.addColumn("status", Number, {allowNull: false, default:0});
    });
};

exports.down = function(db) {
    return db.alterTable("client", function() {
        this.dropColumn("status");
    });
};
