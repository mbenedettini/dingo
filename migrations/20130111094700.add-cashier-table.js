var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.createTable("cashier", function(table) {
        this.primaryKey("id");
        this.username(String, {allowNull: false});
        this.password("VARBINARY(50)", {allowNull: false, binary: true});
        this.email(String, {allowNull: false, default: ""});
        this.name(String, {allowNull: false, default: ""});
        this.status(Number, {allowNull: false, default: 0});
        this.badLoginsCount(Number, {allowNull: false, default: 0});
        //
        this.created(patio.sql.TimeStamp, {allowNull:false, default: patio.sql.literal('NOW()') });
        this.updated(patio.sql.TimeStamp, {allowNull:true});
        //
        this.unique("username");
    });
};

exports.down = function(db) {
    return db.dropTable("cashier");
};
