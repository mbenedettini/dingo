var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.from("setting").insertMultiple([
            {"key": "minClientsCount", "value": 2, "description": "Cantidad minima de clientes compradores para comenzar"},
            {"key": "minTotalClientsCount", "value": 2, "description": "Cantidad minima de clientes conectados para comenzar"}
        ]);
};

exports.down = function(db) {
    return comb.when([
        db.from("setting").filter({"key": "minClientsCount"}).remove(),
        db.from("setting").filter({"key": "minTotalClientsCount"}).remove()
    ]);
};
