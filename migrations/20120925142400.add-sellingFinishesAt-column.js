var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.alterTable("bingo", function() {
        this.addColumn("sellingFinishesAt", patio.sql.identifier("TIMESTAMP"), {allowNull: true});
    });
};

exports.down = function(db) {
    return db.alterTable("bingo", function() {
        this.dropColumn("sellingFinishesAt");
    });
};
