var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.alterTable("bingo", function() {
        this.addColumn("cardPurchaseQuantity", Number, {allowNull: false, default: 1});
    });
};

exports.down = function(db) {
    return db.alterTable("bingo", function() {
        this.dropColumn("cardPurchaseQuantity");
    });
};
