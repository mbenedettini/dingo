var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.alterTable("bingo", function() {
        this.addColumn("sellingInterval", Number, {allowNull: false, default: 300});
    });
};

exports.down = function(db) {
    return db.alterTable("bingo", function() {
        this.dropColumn("sellingInterval");
    });
};
