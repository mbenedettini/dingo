var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.alterTable("bingo", function() {
        this.addColumn("clientCardsCount", Number, {allowNull: false, default: 0});
    });
};

exports.down = function(db) {
    return db.alterTable("bingo", function() {
        this.dropColumn("clientCardsCount");
    });
};
