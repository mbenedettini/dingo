var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.from("client").insertMultiple([
        { "name": "Virtual", "lastName": "Virtual0", "username": "virtual0", "email": "mbenedettini@gmail.com", "confirmed": true, "confirmationToken": "", "telephone": "", "created": new Date() },
        { "name": "Virtual", "lastName": "Virtual1", "username": "virtual1", "email": "mbenedettini@gmail.com", "confirmed": true, "confirmationToken": "", "telephone": "", "created": new Date() },
        { "name": "Virtual", "lastName": "Virtual2", "username": "virtual2", "email": "mbenedettini@gmail.com", "confirmed": true, "confirmationToken": "", "telephone": "", "created": new Date() },
        { "name": "Virtual", "lastName": "Virtual3", "username": "virtual3", "email": "mbenedettini@gmail.com", "confirmed": true, "confirmationToken": "", "telephone": "", "created": new Date() },
        { "name": "Virtual", "lastName": "Virtual4", "username": "virtual4", "email": "mbenedettini@gmail.com", "confirmed": true, "confirmationToken": "", "telephone": "", "created": new Date() },
        { "name": "Virtual", "lastName": "Virtual5", "username": "virtual5", "email": "mbenedettini@gmail.com", "confirmed": true, "confirmationToken": "", "telephone": "", "created": new Date() },
        { "name": "Virtual", "lastName": "Virtual6", "username": "virtual6", "email": "mbenedettini@gmail.com", "confirmed": true, "confirmationToken": "", "telephone": "", "created": new Date() },
        { "name": "Virtual", "lastName": "Virtual7", "username": "virtual7", "email": "mbenedettini@gmail.com", "confirmed": true, "confirmationToken": "", "telephone": "", "created": new Date() },
        { "name": "Virtual", "lastName": "Virtual8", "username": "virtual8", "email": "mbenedettini@gmail.com", "confirmed": true, "confirmationToken": "", "telephone": "", "created": new Date() },
        { "name": "Virtual", "lastName": "Virtual9", "username": "virtual9", "email": "mbenedettini@gmail.com", "confirmed": true, "confirmationToken": "", "telephone": "", "created": new Date() },
        { "name": "Virtual", "lastName": "Virtual10", "username": "virtual10", "email": "mbenedettini@gmail.com", "confirmed": true, "confirmationToken": "", "telephone": "", "created": new Date() },
        { "name": "Virtual", "lastName": "Virtual11", "username": "virtual11", "email": "mbenedettini@gmail.com", "confirmed": true, "confirmationToken": "", "telephone": "", "created": new Date() },
        { "name": "Virtual", "lastName": "Virtual12", "username": "virtual12", "email": "mbenedettini@gmail.com", "confirmed": true, "confirmationToken": "", "telephone": "", "created": new Date() },
        { "name": "Virtual", "lastName": "Virtual13", "username": "virtual13", "email": "mbenedettini@gmail.com", "confirmed": true, "confirmationToken": "", "telephone": "", "created": new Date() },
        { "name": "Virtual", "lastName": "Virtual14", "username": "virtual14", "email": "mbenedettini@gmail.com", "confirmed": true, "confirmationToken": "", "telephone": "", "created": new Date() },
        { "name": "Virtual", "lastName": "Virtual15", "username": "virtual15", "email": "mbenedettini@gmail.com", "confirmed": true, "confirmationToken": "", "telephone": "", "created": new Date() }
    ]);
};

exports.down = function(db) {
    return comb.when([
        db.from("client").filter({"name": "Virtual"}).remove()
    ]);
};
