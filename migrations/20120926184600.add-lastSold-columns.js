var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.alterTable("bingo", function() {
        this.addColumn("firstSoldCardNumber", Number, {allowNull: true});
        this.addColumn("lastSoldCardNumber", Number, {allowNull: true});
    });
};

exports.down = function(db) {
    return db.alterTable("bingo", function() {
        this.dropColumn("firstSoldCardNumber");
        this.dropColumn("lastSoldCardNumber");
    });
};
