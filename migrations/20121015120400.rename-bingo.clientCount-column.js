var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.alterTable("bingo", function() {
        this.renameColumn("clientCount", "clientsCount");
    });
};

exports.down = function(db) {
    return db.alterTable("bingo", function() {
        this.renameColumn("clientsCount", "clientCount");
    });
};
