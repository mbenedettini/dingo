var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.from("setting").insertMultiple([
        { "key": "cardPrice", "value": 0, "description": "Precio de carton" },
        { "key": "cardColor", "value": 1, "description": "Color de carton" },
        { "key": "autoBingoEnabled", "value": 0, "description": "Bingo automatico habilitado" }

    ]);
};

exports.down = function(db) {
    return comb.when([
        db.from("setting").filter({"key": "cardPrice"}).remove(),
        db.from("setting").filter({"key": "cardColor"}).remove(),
        db.from("setting").filter({"key": "autoBingoEnabled"}).remove()
    ]);
};
