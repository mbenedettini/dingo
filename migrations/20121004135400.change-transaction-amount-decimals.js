var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.alterTable("transaction", function() {
        this.setColumnType("amount", "numeric(10,2)");
    });
};

exports.down = function(db) {
    return db.alterTable("transaction", function() {
        this.setColumnType("amount", patio.sql.Float);
    });
};
