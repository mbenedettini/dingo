var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.alterTable("transaction", function() {
        this.addForeignKey("cashierId", "cashier", {key:"id"});
    });
};

exports.down = function(db) {
    return comb.serial([
        comb.hitch(db, "run", "ALTER TABLE transaction DROP FOREIGN KEY transaction_ibfk_3"),
        comb.hitch(db, "run", "ALTER TABLE transaction DROP COLUMN cashierId")
    ]);
};
