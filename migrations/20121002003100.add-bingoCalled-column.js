var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.alterTable("bingo", function() {
        this.addColumn("bingoCalled", Boolean, {allowNull: false, default: false});
    });
};

exports.down = function(db) {
    return db.alterTable("bingo", function() {
        this.dropColumn("bingoCalled");
    });
};
