var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.alterTable("client", function() {
        this.addForeignKey("cashierId", "cashier", {key:"id"});
    });
};

exports.down = function(db) {
    return comb.serial([
        comb.hitch(db, "run", "ALTER TABLE client DROP FOREIGN KEY client_ibfk_1"),
        comb.hitch(db, "run", "ALTER TABLE client DROP COLUMN cashierId")
    ]);
};
