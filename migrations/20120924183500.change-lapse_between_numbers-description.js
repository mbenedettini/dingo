var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.from("setting").filter({"key": "lapseBetweenNumbers"}).update(
            {"value": 5, "description": "Tiempo entre numeros (segundos)."});
};

exports.down = function(db) {
    return db.from("setting").filter({"key": "lapseBetweenNumbers"}).update(
        {"value": 5000, "description": "Tiempo entre numeros (milisegundos)."}
    );
};
