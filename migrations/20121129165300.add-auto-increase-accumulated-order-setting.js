var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.from("setting").insertMultiple([
        { "key": "increaseAccumulatedOrderEvery", "value": 24, "description": "Cada cuantas horas incrementar bola max" }
    ]);
};

exports.down = function(db) {
    return comb.when([
        db.from("setting").filter({"key": "increaseAccumulatedOrderEvery"}).remove()
    ]);
};
