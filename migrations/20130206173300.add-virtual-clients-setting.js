var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.from("setting").insertMultiple([
        { "key": "virtualClientsId", "value": "", "description": "Ids de clientes virtuales" }
    ]);
};

exports.down = function(db) {
    return comb.when([
        db.from("setting").filter({"key": "virtualClientsId"}).remove()
    ]);
};
