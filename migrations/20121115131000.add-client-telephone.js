var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.alterTable("client", function() {
        this.addColumn("telephone", String, {allowNull: false, default: ""});
    });
};

exports.down = function(db) {
    return db.alterTable("client", function() {
        this.dropColumn("telephone");
    });
};
