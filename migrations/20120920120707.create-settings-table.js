var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    var ret = new comb.Promise();
    return comb.serial([
        function() {
            return db.createTable("setting", function(table) {
                this.primaryKey("id");
                this.key(String, {allowNull: false});
                this.value(String, {allowNull: false, default: ""});
                this.description(String, {allowNull: false, default: ""});
                //
                this.created(patio.sql.TimeStamp, {allowNull:false, default: patio.sql.literal('NOW()') });
                this.updated(patio.sql.TimeStamp, {allowNull:true});
            });
        },
        function() {
            return db.from("setting").insertMultiple([
                {"key": "linePrizeRate", "value": 0.15, "description": "Porcentaje de cada carton destinado al premio de Linea."},
                {"key": "bingoPrizeRate", "value": 0.50, "description": "Porcentaje de cada carton destinado al premio de Bingo."}
            ]);
        }
    ]).then(comb.hitch(ret, "callback"), comb.hitch(ret, "errback"));
    return ret;
};

exports.down = function(db) {
    return db.dropTable("setting");
};
