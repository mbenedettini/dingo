var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return db.alterTable("client", function() {
        this.addColumn("try", Number, {allowNull: false, default: 0});
        this.addColumn("confirmed", Boolean, {allowNull: false, default: false});
        this.addColumn("confirmationToken", String, {allowNull: false, default: ""});
    });
};

exports.down = function(db) {
    return db.alterTable("client_card", function() {
        this.dropColumn("try");
        this.dropColumn("confirmed");
        this.dropColumn("confirmationToken");
    });
};
