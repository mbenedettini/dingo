var comb = require('comb');
var patio = require('patio');

exports.up = function(db) {
    return comb.serial([
        comb.hitch(db, "alterTable", "setting", function() {
            this.addIndex("key");
        }),
        comb.hitch(db, "alterTable", "bingo", function() {
            this.addIndex(["currentState", "starts"]);
        }),
        comb.hitch(db, "alterTable", "client_card", function() {
            this.addIndex(["clientId", "bingoId", "cancelled", "isSeries"]);
        }),
        comb.hitch(db, "alterTable", "card", function() {
            this.addIndex("number");
        })
    ]);
};

exports.down = function(db) {
    return comb.serial([
        comb.hitch(db, "alterTable", "setting", function() {
            this.dropIndex("key");
        }),
        comb.hitch(db, "alterTable", "bingo", function() {
            this.dropIndex(["currentState", "starts"]);
        }),
        comb.hitch(db, "alterTable", "client_card", function() {
            this.dropIndex(["clientId", "bingoId", "cancelled", "isSeries"]);
        }),
        comb.hitch(db, "alterTable", "card", function() {
            this.dropIndex("number");
        })
    ]);
};